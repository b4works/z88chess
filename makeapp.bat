:: **************************************************************************************************
:: Chezz compilation script for Windows/DOS
:: (C) Gunther Strube (hello@bits4fun.net) 2019
::
::   ____   ___
::   6MMMMb/ `MM
::  8P    YM  MM
:: 6M      Y  MM  __     ____   _________ _________
:: MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
:: MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
:: MM         MM'   MM MM    MM     dMP       dMP
:: MM         MM    MM MMMMMMMM    dMP       dMP
:: YM      6  MM    MM MM         dMP       dMP
::  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
::   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
::
:: Based on Cyrus IS Chess for the Sinclair ZX Spectrum
:: (C) 1982 Richard Lang, Intelligent Software Ltd
::
:: Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
::
:: Chezz is free software; you can redistribute it and/or modify it under the terms of
:: the GNU General Public License as published by the Free Software Foundation; either
:: version 2, or (at your option) any later version. Chezz is distributed in the hope
:: that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
:: of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:: See the GNU General Public License for more details. You should have received a copy
:: of the GNU General Public License along with Chezz; see the file LICENSE.
:: If not, write to:
::                                  Free Software Foundation, Inc.
::                                  59 Temple Place-Suite 330,
::                                  Boston, MA 02111-1307, USA.
::
:: **************************************************************************************************

@ECHO OFF

mpm.exe 2>nul >nul
if ERRORLEVEL 9009 goto MPM_NOTAVAILABLE

z88card.exe 2>nul >nul
if ERRORLEVEL 9009 goto Z88CARD_NOTAVAILABLE

mthtoken.exe 2>nul >nul
if ERRORLEVEL 9009 goto MTHTOKEN_NOTAVAILABLE

:: return version of Mpm to command line environment.
:: Only V1.5 or later of Mpm supports source file dependency
mpm -version 2>nul >nul
if ERRORLEVEL 15 goto CHECK_Z88CARD
echo Mpm version is less than V1.5, compilation aborted.
echo Mpm displays the following:
mpm
goto END

:CHECK_Z88CARD
:: return version of Z88Card to command line environment.
:: validate that Z88Card is V2.0 or later - only this version or later supports advanced loadmap syntax
z88card.exe -v 2>nul >nul
if ERRORLEVEL 20 goto COMPILE_Z88CHESS
echo Z88Card version is less than V2.0, compilation aborted.
echo Z88Card displays the following:
z88card -v
goto END

:COMPILE_Z88CHESS

:: delete previous binary outut, error and warning files...
del /Q *.epr gitrevision.inc romupdate.cfg 2>nul >nul
del /S /Q *.err *.wrn 2>nul >nul


:: try to locate git.exe...
set gitfound=0
set git_folder=%cd%\.git\
set gitrevision_file="%cd%\gitrevision.inc"
for %%x in (git.exe) do if not [%%~$PATH:x]==[] set gitfound=1
if "%gitfound%"=="0" goto NO_GITBUILDID
if not exist %git_folder% goto NO_GITBUILDID

:: output current Git revision as a 32bit hex integer, to be included as part of build
git.exe log --pretty=format:"defm \"%%h\"" -n 1  > %gitrevision_file%
echo. >> %gitrevision_file%
goto BUILD_CHEZZ

:: Git nor .git folder was found, use "0" as build commit ID
:NO_GITBUILDID
echo defm "0" > %gitrevision_file%
echo. >> %gitrevision_file%


:: build Chezz game executable to be loaded at beginning of bank $3E
:BUILD_CHEZZ
mpm -gb -ochezz.bin z88cyrus.asm chezz.asm zcprint.asm

if ERRORLEVEL 0 goto COMPILE_GAMEDATA
goto COMPILE_ERROR

:COMPILE_GAMEDATA
:: build Chezz game data and MTH structures to be loaded into bank $3F
mpm -gb -nMap zcdata.asm mth.asm
if ERRORLEVEL 0 goto COMPILE_ROMHDR
goto COMPILE_ERROR

:COMPILE_ROMHDR
:: build Application Card header to be loaded at top of bank $3F
mpm -b -nMap romhdr.asm
if ERRORLEVEL 0 goto BUILD_CARD
goto COMPILE_ERROR

:BUILD_CARD
z88card -f chezz.ldm
if ERRORLEVEL 0 goto BUILD_RAMAPP
goto COMPILE_ERROR

:BUILD_RAMAPP
:: the chezz.app RAM Application template file already exists in project, copy the 16K images for .app
copy /Y /B chezz.63 chezz.ap0 2>nul >nul
copy /Y /B chezz.62 chezz.ap1 2>nul >nul
goto END

:MPM_NOTAVAILABLE
echo Mpm assembler tool not found in PATH
goto END

:Z88CARD_NOTAVAILABLE
echo Z88Card build tool not found in PATH
goto END

:MTHTOKEN_NOTAVAILABLE
echo MthToken build tool not found in PATH
goto END

:COMPILE_ERROR
echo Build failed!

:END
