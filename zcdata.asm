; ------------------------------------------------------------------------------------
;   ____   ___
;   6MMMMb/ `MM
;  8P    YM  MM
; 6M      Y  MM  __     ____   _________ _________
; MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
; MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
; MM         MM'   MM MM    MM     dMP       dMP
; MM         MM    MM MMMMMMMM    dMP       dMP
; YM      6  MM    MM MM         dMP       dMP
;  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
;   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
;
; Based on Cyrus IS Chess for the Sinclair ZX Spectrum
; (C) 1982 Richard Lang, Intelligent Software Ltd
;
; Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
;
; Chezz is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published by the Free Software Foundation; either
; version 2, or (at your option) any later version. Chezz is distributed in the hope
; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details. You should have received a copy
; of the GNU General Public License along with Chezz; see the file LICENSE.
; If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ------------------------------------------------------------------------------------

        MODULE  ZcData

        INCLUDE "chezz.inc"

        ORG GameMap

        DEFW    $9226,$C469,$B0CA,$2E4C,$0A0D,$4916,$90E0
        DEFW    $3400,$970E,$0313,$8001,$22A0,$B670,$054B
        DEFW    $8094,$0000,$6480,$2912,$8A4A,$0000,$8060
        DEFW    $1051,$054F,$4282,$0040,$6400,$2912,$C94A
        DEFW    $6281,$A020,$1040,$0D28,$4208,$3002,$7130
        DEFW    $1400,$8626,$6005,$20A0,$000C,$2792,$4386
        DEFW    $E228,$6458,$0A38,$2404,$C1CC,$C090,$1C08
        DEFW    $001E,$C225,$8021,$5490,$8B00,$1203,$0490
        DEFW    $C0C4,$1A14,$4005,$2A11,$F0C0,$24B9,$5328
        DEFW    $8726,$0501,$0070,$4828,$059B,$0000,$4002
        DEFW    $1960,$5106,$530E,$4081,$0240,$A404,$070A
        DEFW    $0000,$4081,$040A,$0A00,$8005,$4002,$2A81
        DEFW    $1E3C,$2D1D,$4008,$F000,$0042,$0E32,$2000
        DEFW    $A50A,$50D2,$167C,$0D18,$0308,$30E1,$0100
        DEFW    $5A42,$8607,$A1C2,$FAA0,$123E,$001C,$8BA3
        DEFW    $31E1,$3808,$9E30,$82A7,$0080,$D0B0,$1A48
        DEFW    $93A0,$C286,$E080,$2852,$5D2C,$942C,$4103
        DEFW    $D850,$1661,$0A18,$0208,$E4A1,$747C,$0938
        DEFW    $0105,$9594,$5090,$3C54,$4600,$81D6,$2041
        DEFW    $8D58,$5B0B,$5745,$6203,$6870,$1D79,$491F
        DEFW    $4B94,$A141,$5010,$172E,$020E,$A0C2,$0404
        DEFW    $2E50,$095F,$1285,$B0C4,$4DB8,$4C3A,$034E
        DEFW    $C1C2,$9090,$1A35,$3155,$0111,$F0A0,$48AA
        DEFW    $1416,$840A,$2082,$6084,$0850,$070B,$0095
        DEFW    $0080,$4800,$2946,$184A,$2282,$8180,$26AC
        DEFW    $082A,$D009,$30E1,$3878,$1302,$8708,$A610
        DEFW    $0080,$3019,$0C69,$8308,$7422,$70B1,$1A00
        DEFW    $044A,$0680,$B880,$3671,$0C45,$C209,$C0C1
        DEFW    $88B8,$1008,$830D,$6092,$C000,$2C8C,$0318
        DEFW    $9184,$22A1,$58D1,$0B36,$1105,$4182,$C080
        DEFW    $1E00,$041A,$E020,$38A9,$88C8,$9720,$04AE
        DEFW    $88C2,$C8AA,$126C,$0F0B,$ABA1,$4001,$2692
        DEFW    $94AC,$A28B,$C4C0,$6892,$3A20,$0B0A,$C281
        DEFW    $5288,$28E1,$901A,$8386,$2802,$1A08,$A254
        DEFW    $0840,$4185,$74C0,$30A9,$0020,$8406,$E050
        DEFW    $88D2,$1C00,$030A,$0925,$00C0,$4890,$0F1A
        DEFW    $0105,$A511,$0020,$141C,$0911,$5185,$1020
        DEFW    $2C68,$142C,$8603,$0180,$AD24,$3054,$434B
        DEFW    $1311,$1084,$1D80,$4D00,$8307,$8192,$2820
        DEFW    $2C64,$0416,$4003,$14E1,$01A5,$4131,$850A
        DEFW    $0100,$C820,$1749,$0947,$4202,$64E0,$2C7C
        DEFW    $4818,$8348,$0413,$B0E0,$2E4C,$0A50,$4352
        DEFW    $40A1,$5420,$124C,$830D,$2083,$2855,$9E58
        DEFW    $3393,$E524,$C100,$3838,$8F10,$0627,$4083
        DEFW    $D2E2,$1C34,$0989,$0015,$F228,$3A7A,$1044
        DEFW    $020A,$6982,$3818,$0A4C,$2598,$0B01,$6081
        DEFW    $1058,$972C,$840D,$6182,$40A0,$3C54,$4217
        DEFW    $C814,$80A1,$5C30,$9F22,$8606,$C100,$1071
        DEFW    $3065,$0759,$D203,$60B1,$7458,$1A16,$0802
        DEFW    $2452,$71DA,$8C2C,$A353,$C814,$5004,$5C18
        DEFW    $580C,$8009,$240B,$40C0,$2C60,$2A81,$1086
        DEFW    $80C4,$2CB8,$842E,$0688,$6901,$B050,$BC28
        DEFW    $1090,$4002,$70A1,$5C20,$0938,$020B,$8104
        DEFW    $D534,$2618,$0A1C,$0286,$6021,$5C58,$1F40
        DEFW    $4042,$C10A,$F880,$224C,$0901,$0204,$7288
        DEFW    $1490,$0DAE,$4347,$E151,$B830,$AE54,$0A11
        DEFW    $C382,$6081,$70D0,$1220,$A903,$E103,$E050
        DEFW    $1E10,$2987,$0B05,$B0A0,$68F8,$411A,$8209
        DEFW    $0280,$2835,$2668,$4012,$0018,$F0C1,$44B8
        DEFW    $183A,$8100,$C2C1,$7C24,$0221,$2318,$0205
        DEFW    $0041,$7428,$1936,$C44A,$E450,$9880,$224C
        DEFW    $450D,$8B95,$0040,$7428,$5524,$8208,$6102
        DEFW    $00C0,$244D,$0C06,$4204,$4481,$48CC,$2208
        DEFW    $8412,$00C4,$0031,$2F51,$0249,$4292,$0081
        DEFW    $31C8,$000C,$810B,$1053,$B830,$102C,$0218
        DEFW    $4306,$80E1,$7800,$253C,$1653,$640A,$B890
        DEFW    $005C,$080D,$23A0,$80A1,$0062,$1AAC,$0407
        DEFW    $4183,$E280,$9024,$0A13,$C105,$E040,$3082
        DEFW    $0CA8,$030D,$C900,$0048,$2C30,$0912,$C285
        DEFW    $72E9,$0049,$1132,$0507,$E042,$18D4,$3A4C
        DEFW    $0108,$C205,$0400,$6CDD,$1B2C,$8210,$0180
        DEFW    $6060,$3860,$4305,$C045,$D0C0,$64B8,$0E0E
        DEFW    $8352,$640A,$3802,$3C48,$0A12,$D083,$F005
        DEFW    $3490,$0C32,$0601,$A1C0,$8414,$2844,$0929
        DEFW    $C080,$54E1,$4D19,$5428,$810E,$21C1,$2820
        DEFW    $0020,$460C,$8301,$60E1,$1CD0,$160E,$4443
        DEFW    $8102,$3944,$3658,$2B08,$C306,$60C1,$3202
        DEFW    $9B46,$8426,$A082,$8800,$1E20,$4B41,$9201
        DEFW    $82C4,$58C0,$0000,$820C,$0104,$4820,$2660
        DEFW    $8984,$C184,$4A48,$0088,$1820,$8003,$0143
        DEFW    $7840,$1E80,$8C92,$8386,$0022,$0090,$152E
        DEFW    $2606,$A408,$D182,$3A08,$000D,$5105,$6005
        DEFW    $78E8,$074E,$8205,$8942,$E078,$3E74,$0727
        DEFW    $4200,$5440,$6CD9,$142C,$030F,$A104,$2810
        DEFW    $1254,$A084,$8305,$9020,$24D0,$9A40,$818C
        DEFW    $40C3,$E890,$0A50,$1407,$4006,$2009,$5089
        DEFW    $1EB0,$0309,$A243,$0901,$4C30,$130F,$D203
        DEFW    $40C4,$7458,$0842,$4252,$C044,$30F1,$113D
        DEFW    $0806,$8180,$A040,$7D00,$5C40,$900C,$000A
        DEFW    $F060,$1054,$0610,$8284,$A5C0,$1818,$1C44
        DEFW    $870F,$C0C0,$B8C0,$2628,$0A0A,$4CA0,$80C1
        DEFW    $5C98,$0A38,$870B,$01C0,$5514,$307C,$0124
        DEFW    $D200,$E0D1,$1D89,$1902,$0100,$C0C4,$18D0
        DEFW    $3494,$4407,$D043,$60C5,$5CC9,$1A84,$944A
        DEFW    $8082,$F870,$2854,$2499,$8002,$2562,$1000
        DEFW    $0A2E,$9347,$0181,$98F0,$3080,$001A,$C401
        DEFW    $E480,$78E4,$18B2,$050C,$2511,$1081,$4020
        DEFW    $0A1D,$4106,$90E1,$14AA,$0F34,$124C,$2502
        DEFW    $3884,$161D,$2C13,$C300,$9421,$4468,$192E
        DEFW    $0311,$E983,$69D2,$AE58,$0620,$4301,$9082
        DEFW    $78D1,$0E20,$1847,$00C2,$F8B0,$1E58,$4000
        DEFW    $C146,$30E0,$44E0,$1906,$4347,$8101,$D0C0
        DEFW    $003D,$2A4E,$0284,$6005,$38F0,$001C,$010B
        DEFW    $6011,$1010,$3058,$040D,$D102,$C030,$0408
        DEFW    $1414,$0615,$8183,$0871,$500C,$4B20,$4141
        DEFW    $F080,$6998,$552C,$130B,$A141,$0220,$A224
        DEFW    $0511,$8083,$0480,$7004,$191A,$960F,$E080
        DEFW    $6800,$1E01,$031B,$4488,$9081,$3058,$1B38
        DEFW    $D652,$C001,$E030,$2C7C,$471D,$8245,$F0E1
        DEFW    $0809,$0C1A,$8808,$2253,$B020,$3654,$481E
        DEFW    $8992,$A0A1,$6CB0,$5428,$0607,$0040,$18E1
        DEFW    $2391,$0A43,$5385,$9051,$51B0,$0B38,$0424
        DEFW    $8043,$6070,$2278,$8D8A,$0302,$E041,$20D0
        DEFW    $9826,$8323,$4143,$E970,$4858,$100F,$0001
        DEFW    $A022,$02BA,$0718,$8609,$62C0,$A090,$3004
        DEFW    $2B96,$C905,$B000,$4821,$98A6,$012D,$20C2
        DEFW    $0200,$4A12,$0912,$4587,$B808,$6848,$11A6
        DEFW    $0000,$8802,$A090,$0044,$8E80,$63A0,$7000
        DEFW    $4478,$9EA8,$070C,$E882,$6182,$2C58,$2C52
        DEFW    $4386,$9064,$58A0,$0024,$860F,$E041,$A8C4
        DEFW    $3A48,$0B04,$1280,$2045,$48C8,$160E,$830E
        DEFW    $6903,$81A2,$2C44,$0B17,$8002,$F4E0,$5888
        DEFW    $1B3A,$0504,$0045,$4090,$3001,$071A,$0305
        DEFW    $9401,$4C51,$5F98,$070E,$C109,$8282,$065C
        DEFW    $0817,$8923,$02E2,$10C0,$2B24,$864A,$6143
        DEFW    $E860,$1E3D,$0F1A,$D385,$F0A4,$7CE0,$0918
        DEFW    $820A,$C5D3,$58B0,$2C60,$0804,$1104,$9034
        DEFW    $0CE8,$0014,$D754,$C083,$1D34,$2A10,$030F
        DEFW    $0181,$5520,$6014,$123A,$8308,$4552,$6122
        DEFW    $4A1C,$2913,$C300,$00E0,$5CD0,$0B3C,$2707
        DEFW    $6925,$5022,$4090,$060E,$4106,$30E8,$68D0
        DEFW    $1112,$2607,$01A4,$0821,$3C58,$8D8D,$4205
        DEFW    $8081,$28CA,$19BE,$042E,$2003,$7232,$1E5A
        DEFW    $121B,$8AA3,$80E1,$4C28,$A112,$0422,$0104
        DEFW    $2051,$283C,$0FA1,$4304,$F020,$9A72,$1B3C
        DEFW    $0507,$280B,$DAF0,$2A32,$0616,$8384,$A021
        DEFW    $7CBA,$5690,$1406,$81C2,$5052,$2250,$0D64
        DEFW    $C286,$3002,$4068,$0614,$2600,$0182,$E070
        DEFW    $326D,$2E4D,$4202,$3441,$4068,$1B12,$8009
        DEFW    $89C1,$7930,$AA60,$0810,$0326,$3001,$0862
        DEFW    $0E28,$974C,$8582,$70D0,$1C4D,$460E,$C197
        DEFW    $E004,$2042,$192A,$C446,$8492,$60A0,$1248
        DEFW    $AA9C,$0203,$40E1,$469A,$8C1C,$1521,$65C0
        DEFW    $7960,$0C5C,$060B,$0380,$A0C1,$6DE0,$0F41
        DEFW    $0308,$8592,$4140,$1454,$0961,$4287,$E0A1
        DEFW    $54A0,$232E,$A70D,$C183,$6054,$064C,$070D
        DEFW    $8284,$7029,$1450,$8BA6,$2208,$40E1,$D2F0
        DEFW    $963C,$2617,$8115,$D2A1,$888A,$1720,$A304
        DEFW    $0081,$3850,$301C,$4508,$C100,$6045,$64C8
        DEFW    $0A20,$4450,$2603,$D804,$3660,$5114,$4210
        DEFW    $D0C4,$5198,$0F40,$030F,$C101,$CDA4,$2C40
        DEFW    $0900,$0C84,$7441,$68E9,$8C0C,$0408,$7010
        DEFW    $B0A0,$2628,$2D1A,$1102,$A084,$9040,$8600
        DEFW    $0231,$0403,$F004,$3C6D,$044C,$0222,$8000
        DEFW    $00F8,$0708,$9253,$75C9,$8922,$2850,$070F
        DEFW    $4103,$A0C0,$8CDA,$0D98,$0609,$A10B,$0891
        DEFW    $3A48,$0E1C,$0386,$A0C9,$2CC0,$2138,$0507
        DEFW    $0880,$7800,$1A34,$0E59,$C3A3,$8001,$7058
        DEFW    $0E42,$000B,$0410,$61A2,$1438,$0E10,$02A4
        DEFW    $7081,$0C50,$1922,$8108,$61C0,$6830,$1C2D
        DEFW    $034F,$C090,$A0E1,$5998,$581E,$0404,$A0C1
        DEFW    $B0D0,$2C61,$4D1A,$0045,$4165,$4838,$0C34
        DEFW    $860C,$A104,$1875,$4C3C,$0858,$4000,$64E0
        DEFW    $34D0,$5B32,$820A,$E1D5,$E0A0,$1A59,$A753
        DEFW    $4393,$B021,$7060,$1700,$2402,$E109,$48A0
        DEFW    $2E40,$4A00,$82C4,$50A1,$08D0,$172C,$030D
        DEFW    $24D4,$B0C0,$3260,$4914,$8255,$81E1,$460A
        DEFW    $172A,$0807,$2904,$A520,$A430,$230A,$8087
        DEFW    $D001,$2C50,$1128,$834D,$C103,$1051,$286C
        DEFW    $0C87,$C385,$2061,$78A0,$8D34,$060A,$81C1
        DEFW    $4964,$1630,$0B17,$0382,$E021,$5872,$0B12
        DEFW    $9203,$814A,$6030,$3248,$051C,$0824,$F180
        DEFW    $4040,$931C,$830D,$4183,$1821,$4A50,$2383
        DEFW    $1203,$50E5,$4C62,$8CB4,$030B,$0413,$9122
        DEFW    $9842,$2C46,$8915,$60C8,$1DA9,$061E,$9048
        DEFW    $C101,$E880,$0068,$065A,$C1A3,$C0E1,$6060
        DEFW    $0F36,$9640,$A14A,$8890,$1838,$0F19,$4206
        DEFW    $10A1,$0092,$0D1C,$820E,$61C3,$02A1,$9C7C
        DEFW    $0E15,$5284,$B401,$4CC1,$100E,$0520,$4552
        DEFW    $A820,$2258,$079D,$4185,$4421,$2841,$0E3C
        DEFW    $A60D,$2108,$EA90,$A400,$2199,$4206,$E0E8
        DEFW    $6CD0,$8F26,$068A,$8502,$5210,$321C,$0C14
        DEFW    $0B07,$E3A8,$3822,$8E26,$D407,$2049,$FA30
        DEFW    $3828,$0603,$8002,$0008,$1C30,$1906,$0300
        DEFW    $2511,$6122,$B404,$2D13,$0100,$CAE1,$3801
        DEFW    $103C,$A629,$818B,$F200,$2840,$0B13,$4105
        DEFW    $D008,$7860,$0110,$984D,$C042,$E0F0,$1F4D
        DEFW    $0D20,$0083,$A040,$5C10,$1F22,$144B,$0040
        DEFW    $C8B2,$3A88,$0E8C,$0383,$14A2,$8861,$0B00
        DEFW    $162E,$2293,$C924,$3434,$0317,$C3A0,$4109
        DEFW    $0018,$1930,$8626,$09C3,$F000,$1C6C,$5018
        DEFW    $8244,$F061,$3400,$0016,$880B,$6282,$1034
        DEFW    $AE44,$0C0F,$0304,$B060,$00A8,$593A,$1440
        DEFW    $00D0,$0124,$1248,$0E42,$4001,$6402,$7551
        DEFW    $0A00,$1240,$2241,$0004,$1E28,$070E,$9104
        DEFW    $F434,$690C,$4D02,$940A,$A101,$C840,$B050
        DEFW    $2E1C,$9283,$4006,$4C72,$0E26,$064E,$80C3
        DEFW    $72D0,$1C5C,$0712,$8183,$80E1,$8A3A,$1522
        DEFW    $A426,$81C1,$6230,$9242,$030F,$4A82,$3081
        DEFW    $4860,$1C32,$2405,$600B,$D0E0,$4648,$1214
        DEFW    $C0A0,$F180,$4040,$931C,$14A6,$C410,$30F4
        DEFW    $3335,$0206,$0382,$3001,$44C0,$1214,$D240
        DEFW    $8442,$50C0,$2A61,$0B0E,$1385,$F2D0,$0C90
        DEFW    $1134,$8105,$E002,$0000,$2059,$0109,$C9A1
        DEFW    $D024,$3419,$4994,$012C,$41C1,$80D0,$0A2C
        DEFW    $0B9E,$C001,$0020,$12A2,$0D2E,$8508,$0983
        DEFW    $B1A0,$2258,$0214,$C826,$40C0,$4800,$0006
        DEFW    $844D,$81D1,$5252,$2C70,$4211,$4812,$50C0
        DEFW    $1420,$1328,$250B,$6040,$A840,$0854,$0904
        DEFW    $8004,$4480,$08A0,$1006,$9509,$2003,$C8E0
        DEFW    $164C,$030E,$D486,$6041,$6CE0,$1734,$0008
        DEFW    $81C2,$B8E0,$2065,$2543,$4A83,$1081,$5458
        DEFW    $0C1C,$0202,$61C4,$E974,$1A44,$010D,$6222
        DEFW    $5268,$4810,$0124,$8000,$0100,$9010,$3921
        DEFW    $2909,$4123,$DA48,$3431,$8B00,$8005,$7011
        DEFW    $59F0,$1610,$4311,$4183,$F460,$4038,$4800
        DEFW    $0006,$844D,$81D1,$5252,$2C70,$4211,$4812
        DEFW    $50C0,$1420,$1328,$250B,$6040,$A840,$0854
        DEFW    $0904,$8004,$4480,$08A0,$1006,$9509,$2003
        DEFW    $C8E0,$164C,$030E,$FFEF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFEB,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFB3,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFD3
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FF97,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FF77,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FF6F,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0105,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFDB,$0101,$FFFF,$0101,$FFFF,$0101,$FFFB
        DEFW    $0101,$FF87,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FF9F,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FF8B,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FF0F,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFB3
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FF57,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0105,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0105,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFB,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101
