   ____   ___
  6MMMMb/ `MM
 8P    YM  MM
6M      Y  MM  __     ____   _________ _________
MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
MM         MM'   MM MM    MM     dMP       dMP
MM         MM    MM MMMMMMMM    dMP       dMP
YM      6  MM    MM MM         dMP       dMP
 8b    d9  MM    MM YM    d9  dMP    /  dMP    /
  YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM

Chezz Version 0.1 Beta

Conversion and enhancements (c) 1999 Keith Rickard

Based on Cyrus IS Chess for the Sinclair ZXSpectrum 48K
(c) 1982 Intelligent Software Ltd
Author: Richard Lang

For the Cambridge Computer Z88.

=======================================================

Dear Reader

Chezz is a conversion of one of the strongest chess programs available for the ZX Spectrum, Cyrus IS Chess. This is a beta version of the software, so may still contain some bugs, and has several unimplemented features. However, none of this should detract from what I hope is an excellent and extremely playable game for all you travelling chessmasters.

Current available features include:

- 8 levels of play (including 3 problem-solving levels)
- take back move facility
- game replay
- force Chezz to play best move
- board setup
- sound effects
- 0-2 player modes
- invert board (black at bottom, white at top)
- keeps a record of pieces taken
- swap chessmen to text notation
- call up board square references

Press ESC to stop Chezz thinking; this may turn the Z88 off if its timeout expires.

Now the geeky stuff...

The Z80 assembler source files included with this README.TXT file can be assembled using DOSBox emulator (or even a PC running MS-DOS!) by executing the makeapp.bat batch file from the command prompt while in the Chezz directory. For Unix users, use the makeapp.sh script.

The 32K file ZCHESS.EPR is the file that needs to be written to a 32K Z88 EPROM card or equivalent.  It can be loaded into a Z88 emulator such as OZvm.  This is Version 0.01 Beta.  However, also included is the file ZC_003.EPR which is Version 0.03 Beta.  I do not have the source code for this but I know this version corrects a DOR bug in the application file structure as identified by Gary Lancaster - damn if I can remember what the bug is now.

I've not seen these files since 2000, so my memory of how I got all of this together and working originally is now very vague!

I hope you enjoy looking at these files!


Keith Rickard
1 October 2019