; ------------------------------------------------------------------------------------
;   ____   ___
;   6MMMMb/ `MM
;  8P    YM  MM
; 6M      Y  MM  __     ____   _________ _________
; MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
; MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
; MM         MM'   MM MM    MM     dMP       dMP
; MM         MM    MM MMMMMMMM    dMP       dMP
; YM      6  MM    MM MM         dMP       dMP
;  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
;   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
;
; Based on Cyrus IS Chess for the Sinclair ZX Spectrum
; (C) 1982 Richard Lang, Intelligent Software Ltd
;
; Ported to Cambridge Z88 by Keith Rickard (C) 1999-2019
; Project setup for Mpm and tool, source code analysis, (C) Gunther Strube 2019-2021
;
; Chezz is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published by the Free Software Foundation; either
; version 2, or (at your option) any later version. Chezz is distributed in the hope
; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details. You should have received a copy
; of the GNU General Public License along with Chezz; see the file LICENSE.
; If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ------------------------------------------------------------------------------------


        MODULE  Z88Cyrus

        ORG     $8000

INCLUDE "ozdefc.asm"
INCLUDE "chezz.inc"

        XDEF Z88CYRUS

; chezz.asm
        XREF SETUP_BOARD
        XREF Z88CTL1, Z88CTL2
; zcprint.asm
        XREF PRINT_MOVE
        XREF PRINT_BOARD
        XREF PRINT_LEVEL
        XREF REDRAW_MOVE
        XREF P_CURSOR_REF
        XREF MP01, MP02, MP03, MP04, MP05, MP06, MP07
        XREF MP08, MP09, MP10, MP11, MP12, MP13


;***************************************************************************
.Z88CYRUS
        CALL    BEEP
        DEFW    $0C50
.auto605B
        LD      A,$01
        LD      (autoBCBD),A
        LD      (level),A                       ; Level +1 (1 = level 2)
        XOR     A                               ; A=0
        LD      (orientation),A                 ; Board orientation; B at top, W at Bottom
        LD      (autoBCDD),A
        LD      (autoC006),A
        LD      (autoBCDB),A
        LD      (replayGame),A
        LD      (beeper),A                      ; Sound on
        CALL    CY_DEFAULT_COLOURS              ; Set default colours

.CY_NEW_GAME
        LD      A,(autoC006)                    ; Move number?
        DEC     A
        JR      Z,auto608A
        LD      A,$FF                           ; End marker of move history
        LD      (GDmoveHistory),A                    ; Reset (clear) move history
.auto608A
        LD      A,$01                           ; Computer plays black, Human plays white
        LD      (contenders),A                  ; Bit 1 = White, Bit 0 = Black. =1 means computer plays
.CY_NEW_GAME_DEMO
        LD      HL,0
        LD      (autoC003),HL
        CALL    BEEP
        DEFW    $1032
.auto60A4
        CALL    CY_INIT_BOARD                   ; Intialise board
.auto60A7
        LD      HL,0
        LD      (GDmoveOffset),HL
        XOR     A
        LD      (autoC047),A
        LD      (autoBCDF),A
.auto60B7                                       ; Start thinking?
        LD      SP,(zcstack)                    ; Reset SP to same value as when Chezz was started
        LD      A,(replayGame)                  ; Is game being replayed?
        OR      A
        JR      Z,auto60E3                      ; Skip if not
; Game is being replayed here
        LD      BC,GDmoveHistory                ; Move history starts here
        LD      HL,(GDmoveOffset)               ; Offset to point to move history
        ADD     HL,BC                           ; HL now points to last move
        LD      A,(HL)                          ; Is this a $FF (i.e. end of move history)
        INC     A
        JP      Z,auto671F                      ; Jump if it is - game is still being replayed

        CALL    CY_PRINT_BOARD                  ; Print board
        LD      HL,autoC000
.auto60D2
        CALL    CY_ESCKEY_CHK                   ; Check if ESC key has been pressed
        JP      NZ,auto671F                     ; Jump if it has - game replay has stopped

        LD      A,(autoBCDB)
        OR      A
        JR      NZ,auto60E3
        DEC     HL
        LD      A,L
        OR      H
        JR      NZ,auto60D2

.auto60E3
        LD      HL,(autoC003)                   ; Play a move?
        LD      A,L                             ; See if HL<>0 (if so, an action is required)
        OR      H
        JP      NZ,auto6690                     ; If so, jump to play a move

        LD      A,R                             ; Generate a random number (R= 0 to 127)
        LD      B,A                             ; Save this in B
        RRA                                     ; Rotate right through Fc twice
        RRA
        ADD     A,B                             ; Add it to B
        LD      HL,randomNo                     ; Add it to (randomNo)
        ADD     A,(HL)
        LD      (HL),A                          ; Update (randomNo)

        LD      A,(repeatedMoveCount)           ; DRAWN game?
        CP      $06
        JP      Z,CY_GAME_DRAWN                 ; Jump if so
        LD      A,(autoBD0A)                    ; DRAWN game?
        CP      'd'
        JP      NC,CY_GAME_DRAWN                ; Jump if so
        XOR     A                               ; Game is in progress
        LD      (gameOver),A                    ; 0 = game is in progress

        CALL    PRINT_LEVEL                     ; display selected level
        CALL    auto756E
        LD      A,(autoBCDD)
        OR      A
        JR      Z,auto6134
        LD      A,(nextToPlay)
        AND     $01
        XOR     $01
        LD      (playerTurn),A
        CALL    auto7562
        JR      auto6147
.auto6134
        LD      A,(nextToPlay)
        RRA
        LD      A,(playerTurn)
        JR      NC,auto6143
        OR      A
        JR      Z,auto6147
        JP      auto61C8
.auto6143
        OR      A
        JP      Z,auto61C8
.auto6147
        LD      A,(contenders)                  ; Look at who's playing who
        OR      A                               ; Is computer playing both sides?
        JP      Z,auto696A                      ; Jump if so
        LD      A,@01000000
        LD      (autoBD4C),A
        LD      IX,autoBD58
        CALL    auto78CE
        CALL    auto7B96
        LD      A,(autoBD19)
        OR      A
        JP      Z,CY_GAME_OVER
        XOR     A
        LD      (autoBCDB),A
        LD      A,(autoBCDD)
        OR      A
        CALL    Z,auto6FAA
        CALL    CY_PRINT_BOARD
        CALL    BEEP
        DEFW    $1014
        CALL    MP01                            ; "Thinking.."
        CALL    auto7BE1
        CALL    BEEP
        DEFW    $2014
        XOR     A
        LD      (autoBCDD),A
        LD      A,(level)
        CP      $08
        JR      C,auto61C1
        LD      A,(autoBCA0)
        CP      $FE
        JR      NC,auto61C1
        CALL    MP02                            ; "No checkmate found"
        CALL    CY_WARNING_BEEP
        JP      CY_CMD_CHANGE_POSN
.auto61C1
        LD      IX,(autoBD29)
        JP      auto6323

;***************************************************************************
.auto61C8
        XOR     A
        LD      (autoBD4C),A
        LD      IX,autoBD58
        CALL    auto78CE
        CALL    auto7B96
        LD      A,(autoBD19)
        OR      A
        JP      Z,CY_GAME_OVER
        XOR     A
        LD      (autoBCDB),A
        LD      A,(contenders)                  ; Look at who's playing who
        CP      $03                             ; Is computer playing both sides?
        JP      Z,auto696A                      ; Jump if so
        OR      A
        JR      Z,auto61EF
        CALL    auto7562
.auto61EF
        CALL    auto70D3
.auto62B7
        CALL    MP03                            ; "Your move please"
        LD      A,$FF
        LD      (autoBCCB),A
.auto62CD
        CALL    CY_MAIN_KEY_WAIT
        CP      $0D                             ; ENTER
        JR      NZ,auto62CD
        CALL    BEEP
        DEFW    $2032
        LD      A,'8'
        LD      (ZX_scn_Sqpc_attr),A
        LD      A,(autoBCCB)
        INC     A
        LD      A,(cur_pos)
        JR      NZ,auto62F7
        LD      (autoBCCB),A
        EXX
        PUSH    HL
        CALL    P_CURSOR_REF
        POP     HL
        EXX
        JR      auto62CD
.auto62F7
        LD      (autoBCCC),A
        EXX
        PUSH    HL
        CALL    P_CURSOR_REF
        PUSH    HL
        EXX
        LD      A,(autoBCCB)
        EX      AF,AF'
        LD      A,(autoBCCC)
        LD      HL,autoB401
        LD      B,(HL)
        INC     L
        CALL    auto7BCD
        JR      C,auto6318
        DEC     L
        XOR     A
        OR      (HL)
        JR      NZ,auto631D
.auto6318
        CALL    CY_PRINT_ILLEGAL
        JR      auto62B7
.auto631D
        CALL    auto6FAA
        PUSH    HL
        POP     IX
.auto6323
        CALL    auto70BF
        CALL    auto810F
        XOR     A
        LD      (promoted),A
        POP     AF
        PUSH    AF
        CALL    PE,auto63B1
        RES     7,(IX+$03)
        CALL    auto8400
        CALL    PRINT_MOVE
        CALL    auto70C7
        PUSH    IX
        POP     HL
        LD      BC,autoB400
        XOR     A
        LD      E,A
        SBC     HL,BC
        LD      BC,$0004
.auto6353
        INC     E
        SBC     HL,BC
        JR      NZ,auto6353
        DEC     E
        LD      HL,(GDmoveOffset)
        INC     HL
        LD      (GDmoveOffset),HL
        LD      A,(IX+$03)
        AND     $80
        OR      E
        LD      BC,autoC057
        ADD     HL,BC
        LD      (HL),A
        INC     HL
        LD      (HL),$FF
.auto636E
        LD      HL,autoBD0A
        INC     (HL)
        POP     AF
        PUSH    AF
        JP      P,auto637C
        AND     $0F
        DEC     A
        JR      NZ,auto6380
.auto637C
        XOR     A
        LD      (autoBD0A),A
.auto6380
        CALL    auto6560
        LD      A,(nextToPlay)
        XOR     $01
        LD      (nextToPlay),A
        RRA
        JR      NC,auto6392
        LD      HL,moveNo
        INC     (HL)
.auto6392
        LD      A,(autoBCDD)
        OR      A
        JR      NZ,auto63AE
        LD      HL,autoBF00
.auto639B
        LD      A,(HL)
        INC     A
        JR      Z,auto63A2
        INC     HL
        JR      auto639B
.auto63A2
        LD      A,(IX+$01)
        LD      (HL),A
        INC     HL
        LD      A,(IX+$02)
        LD      (HL),A
        INC     HL
        LD      (HL),$FF
.auto63AE
        JP      auto60B7
.auto63B1
        LD      A,(autoBD4C)
        BIT     6,A
        JR      NZ,auto63FE
        CALL    MP04                            ; "Promote to?"
        CALL    CY_KEYBD_READ
        LD      B,$05
        CP      'R'
        JR      Z,auto6402
        LD      B,$03
        CP      'N'
        JR      Z,auto6402
        LD      B,$83
        CP      'B'
        JR      Z,auto6402
.auto63FE
        LD      B,$09
        JR      auto640D
.auto6402
        LD      L,(IX+$02)
        LD      H,auto00BD
        LD      L,(HL)
        RES     2,L
        INC     L
        INC     H
        LD      (HL),B
.auto640D
        LD      HL,autoC047
        INC     (HL)
        LD      A,(HL)
        ADD     A,L
        LD      L,A
        LD      (HL),B
        LD      A,B
        LD      (promoted),A
        RET

;***************************************************************************
.CY_GAME_OVER
        LD      A,(autoBD5B)
        RLA
        JR      C,auto6434
        CALL    MP05                            ; "Stalemate"
        JR      CY_GAME_OVER1
.auto6434
        CALL    MP06                            ; "Checkmate"
        JR      CY_GAME_OVER1
.CY_GAME_DRAWN
        CALL    MP07                            ; "DRAWN"
.CY_GAME_OVER1
        LD      A,$01                           ; Game has ended
        LD      (gameOver),A                    ; 1= game over
        XOR     A
        LD      (autoBCDB),A
        CALL    CY_PRINT_BOARD
        CALL    BEEP
        DEFW    $18C8
        LD      A,(contenders)                  ; Look at who's playing who
        CP      $03                             ; Is computer playing both sides
        JR      NZ,auto648B                     ; Jump if not

        LD      B,$09                           ; Computer played both sides
        LD      HL,0                            ; So, wait a while (a few seconds)
.auto6479
        CALL    CY_ESCKEY_CHK                   ; Check if ESC key has been pressed
        JR      NZ,auto6488                     ; Skip if it hasn't
        DEC     HL
        LD      A,L
        OR      H
        JR      NZ,auto6479
        DJNZ    auto6479                        ; Decrement HL from 65536 to 0 nine times
        JP      CY_NEW_GAME_DEMO                ; Start new game, computer to play both sides
.auto6488
        CALL    auto7562
.auto648B
        CALL    CY_MAIN_KEY_WAIT
        JP      auto648B

;***************************************************************************
;Initialise board
;
.CY_INIT_BOARD                                  ; Prepare board and info workspace
        POP     DE                              ; Get return address from stack (why?)
        XOR     A                               ; A=0
        LD      B,A                             ; B=256
        LD      HL,cyrusBoard+$0200             ; Top of the board workspace area ($C000)
.auto650B                                       ; Clear the board workspace area ($BD00 - $BEFF)
        DEC     HL                              ; 512 bytes in total
        LD      (HL),A
        DEC     HL
        LD      (HL),A
        DJNZ    auto650B
        PUSH    DE                              ; Put return address back onto the stack
                                                ; HL points to cyrusBoard ($BD00 area)
        LD      C,A                             ; C=0 - Mask for White pieces to be put on the board
        CALL    CY_PUT_PIECES                   ; Initialise White pieces in their starting postions
        LD      DE,autoBE00                     ; Copy the pieces corresponding info (all bytes at DE are $00!)
        LD      C,@01000001                     ; Initialise rank of White Pawns
        CALL    CY_PUT_PAWNS
        LD      L,$60                           ; Move to the rank on board for Black Pawns
        LD      C,@11000001                     ; Initialise row of Black Pawns
        CALL    CY_PUT_PAWNS
        LD      C,@10000001                     ; Mask for Black pieces to be put on the board
        CALL    CY_PUT_PIECES                   ; Initialise Black pieces in their starting positions
        LD      A,$01
        LD      (playerTurn),A                  ; Black's turn
        LD      (nextToPlay),A                  ; Black's turn
        LD      (moveNo),A                      ; First move to be played
        LD      (autoBCA9),A
        LD      (autoC006),A                    ; Current move number=1
        LD      HL,autoB400                     ; Thinking workspace
        LD      (autoBCAB),HL
        CALL    auto659C
        JP      auto65E5

;***************************************************************************
.CY_PUT_PIECES
        LD      DE,CY_PIECE_INFO
.CY_PUT_PAWNS
        LD      B,$08                           ; 8 pieces to put on the board
.auto654C
        LD      A,(DE)
        OR      C                               ; Apply mask (for piece colour, etc)
        LD      (HL),A
        INC     L
        INC     DE
        DJNZ    auto654C
        LD      A,$08                           ; Move board pointer on by 8 bytes
        ADD     A,L
        LD      L,A
        RET

;***************************************************************************
; This is the row of pieces on the board at the start of the game, i.e. RNBQKBNR
;
.CY_PIECE_INFO
        DEFB    $11,$31, $21,$09, $01,$21, $31,$11

;***************************************************************************
.auto6560
        LD      HL,(autoBD5C)
        LD      B,(IX+$01)
        LD      C,(IX+$02)
        XOR     A
        SBC     HL,BC
        LD      HL,repeatedMoveCount
        JR      NZ,auto6574
        INC     (HL)
        JR      auto6575
.auto6574
        LD      (HL),A
.auto6575
        LD      HL,(autoBD59)
        LD      (autoBD5C),HL
        LD      A,B
        LD      (autoBD59),A
        LD      H,(IX+$03)
        LD      L,C
        LD      (autoBD5A),HL
        RET

;***************************************************************************
.auto6587
        POP     HL
.auto6588
        CALL    CY_PRINT_ILLEGAL
        JP      auto6D6A

;***************************************************************************
.CY_CLEAR_BOARD
        XOR     A
        LD      HL,cyrusBoard
.auto6592
        LD      (HL),A
.auto6593
        INC     L
        BIT     3,L
        JR      Z,auto6592
        BIT     7,L
        JR      Z,auto6593
.auto659C
        XOR     A
        LD      HL,autoBE00
.auto65A0
        SET     7,(HL)
        ADD     A,$08
        LD      L,A
        JR      NC,auto65A0
        RET

;***************************************************************************
.auto65A8
        LD      D,(HL)
        BIT     7,D
        JR      NZ,auto65D1
        INC     L
        LD      B,(HL)
        DEC     L
        LD      A,B
        AND     $0F
        DEC     A
        JR      Z,auto65D1
        SET     7,(HL)
        LD      A,L
        AND     $80
        OR      $03
        LD      E,A
        LD      A,B
        AND     $8F
        PUSH    HL
        LD      HL,CY_PIECE_TABLE               ; Find piece info
        CALL    CY_PIECE_LOOKUP                 ; A holds code for the piece to lookup
        INC     HL                              ; Skip the 1st byte (piece value)
        INC     HL                              ; Skip the 2nd byte (piece name)
        LD      A,(HL)                          ; Get the 3rd byte of data
        OR      E
        LD      H,auto00BD
        LD      L,D
        LD      (HL),A
        POP     HL
.auto65D1
        LD      A,L
        ADD     A,$08
        LD      L,A
        BIT     6,A
        JR      NZ,auto65A8
        RET

;***************************************************************************
.auto65DA
        LD      HL,autoBE40
        CALL    auto65A8
        LD      L,$C0
        CALL    auto65A8
.auto65E5
        LD      DE,cyrusBoard
        LD      H,auto00BE
.auto65EA
        LD      A,(DE)
        BIT     0,A
        CALL    NZ,auto6610
        LD      A,$10
        ADD     A,E
        LD      E,A
        JP      P,auto65EA
        RES     7,E
        INC     E
        BIT     3,A
        JR      Z,auto65EA
        LD      L,$00
        CALL    auto660A
        LD      L,$80
        LD      A,B
        OR      (HL)
        JP      M,auto6587
.auto660A
        LD      B,(HL)
        INC     L
        LD      A,(HL)
        JP      auto8203
.auto6610
        LD      L,A
        DEC     L
        BIT     6,L
        JR      Z,auto662B
        LD      A,'p'
        AND     E
        JR      Z,auto6674
        CP      'p'
        JR      Z,auto6674
        LD      B,$01
        LD      A,$80
        AND     L
        XOR     $80
        SCF
        RRA
        LD      C,A
        JR      auto666D
.auto662B
        LD      A,'x'
        AND     L
        JR      NZ,auto663F
        LD      C,$01
        LD      B,$00
        BIT     1,L
        JR      Z,auto663A
        LD      B,'0'
.auto663A
        CALL    auto6678
        JR      auto6674
.auto663F
        CP      $10
        LD      C,$0E
        LD      B,$09
        JR      C,auto6664
        JR      NZ,auto6655
        LD      C,$08
        LD      B,$05
        BIT     1,L
        JR      Z,auto6661
        LD      B,$15
        JR      auto6661
.auto6655
        LD      B,$03
        LD      C,$00
        CP      ' '
        JR      NZ,auto6661
        LD      B,$83
        LD      C,$04
.auto6661
        CALL    auto6678
.auto6664
        CALL    auto6678
        LD      A,$82
        AND     L
        SET     6,A
        LD      L,A
.auto666D
        CALL    auto6678
        BIT     6,L
        JR      NZ,auto666D
.auto6674
        POP     HL
        JP      auto6587

;***************************************************************************
.auto6678
        RES     1,L
        LD      A,(HL)
        RLA
        JR      C,auto6683
        LD      A,$08
        ADD     A,L
        LD      L,A
        RET

;***************************************************************************
.auto6683
        LD      (HL),E
        LD      A,L
        SET     2,A
        LD      (DE),A
        INC     L
        LD      (HL),B
        SET     2,L
        INC     L
        LD      (HL),C
        POP     BC
        RET

;***************************************************************************
.auto6690                                       ; PLAY A MOVE @@
        LD      BC,(GDmoveOffset)               ; If take-Back, take-Forward or Replay was actioned
        XOR     A                               ; then HL will be different to GDmoveOffset
        SBC     HL,BC
        JP      Z,auto671F                      ; Jump if not - look to play a new move

        LD      (autoBCCF),HL
        LD      HL,(GDmoveOffset)
        INC     HL
        LD      (GDmoveOffset),HL
        LD      BC,autoC057
        ADD     HL,BC
        LD      C,(HL)
        LD      A,C
        RES     7,C
        LD      B,$00
        LD      HL,autoB404
        ADD     HL,BC
        ADD     HL,BC
        ADD     HL,BC
        ADD     HL,BC
        PUSH    AF
        PUSH    HL
        LD      IX,autoBD58
        CALL    auto78CE
        POP     IX
        POP     AF
        INC     (IX+$00)
        RLA
        JR      NC,auto66CB
        SET     7,(IX+$03)
.auto66CB
        LD      A,(replayGame)
        OR      A
        CALL    NZ,auto70BF
        CALL    auto810F
        XOR     A
        LD      (promoted),A
        POP     AF
        PUSH    AF
        JP      PO,auto66F4
        LD      HL,autoC047
        INC     (HL)
        LD      A,(HL)
        ADD     A,L
        LD      L,A
        LD      A,(HL)
        LD      L,(IX+$02)
        LD      H,auto00BD
        LD      L,(HL)
        RES     2,L
        INC     L
        INC     H
        LD      (HL),A
        LD      (promoted),A
.auto66F4
        LD      A,(replayGame)
        OR      A
        JR      Z,auto6709
        CALL    PRINT_MOVE
        CALL    auto70C7
        JR      auto6715
.auto6709
        LD      HL,(autoBCCF)
        XOR     A
        LD      BC,$0003
        SBC     HL,BC
        CALL    C,PRINT_MOVE
.auto6715
        LD      A,(autoBCDD)
        OR      A
        CALL    Z,auto6FAA
        JP      auto636E

;***************************************************************************
.auto671F
        LD      HL,0
        LD      (autoC003),HL
        XOR     A
        LD      (replayGame),A
        LD      A,(nextToPlay)
        AND     $01
        LD      (playerTurn),A
        JP      auto60B7

;***************************************************************************
.CY_ESCKEY_CHK
; Return Fz=Z if ESC key has not been pressed, else Fz=NZ if it has
        XOR     A
        CALL    Z88CTL2                         ; Check for a key being pressed
        RET     Z                               ; Return if ESC key not pressed
        CALL    MP08                            ; "Interrupted"
        CALL    BEEP
        DEFW    $50C8
        LD      A,$01
        OR      A
        RET

;***************************************************************************
.CY_KEYBD_READ
.auto675F
        CALL    Z88CTL1                         ; Waiting for a menu key press
        AND     @11011111                       ; Clear bit 5
        LD      (lastKey),A                     ; Store code of pressed key
        CALL    BEEP
        DEFW    $0402
        LD      A,(lastKey)
        RET

;***************************************************************************
.CY_KEY_WAIT
        CALL    CY_KEYBD_READ                   ; Get a key code
        CP      'S'                             ; 'Sound' command key code?
        JR      NZ,CY_NOT_S                     ; Skip if not

        LD      A,(beeper)                      ; Toggle the beeper switch
        XOR     $01
        LD      (beeper),A
        CALL    BEEP                            ; Sound beeper (if switched on)
        DEFW    $1408
        JR      CY_KEY_WAIT                     ; Loop back

.CY_NOT_S
        CP      'O'                             ; Orient'n command key code?
        JR      NZ,CY_NOT_O                     ; Skip if not

        LD      A,(orientation)                 ; Flip board - White at top/bottom
        XOR     $01
        LD      (orientation),A
        CALL    PRINT_BOARD                     ; Redraw board
        JR      CY_KEY_WAIT                     ; Loop back

.CY_NOT_O                                       ; Test for cursor keys ('5','6','7' or '8')
        CP      $19                             ; As bit 5 has been set to 0, '5'=$15, '8'=$18, etc
        RET     NC                              ; Return if key code is >=19
        CP      $15
        RET     C                               ; Return if key code is < 15
        LD      B,A                             ; A cursor key has been pressed. Save key code in B.
        LD      A,(cur_pos)                     ; Is cursor present?
        INC     A                               ; If A=$FF then cursor is not present
        LD      A,B
        RET     Z                               ; Return is cursor is not present

        LD      A,(orientation)                 ; Which way is the board oriented?
        OR      A
        LD      A,B                             ; Get key code
        JR      Z,CY_W_BOTTOM                   ; Skip if White is at the bottom

        XOR     $01                             ; Flip direction Up<>Down, Left<>Right

.CY_W_BOTTOM
        CP      $16
        JR      Z,CY_CUR_UP
        CP      $17
        JR      Z,CY_CUR_DOWN
        AND     $03
        JR      Z,CY_CUR_RIGHT

.CY_CUR_LEFT
        LD      A,(cur_pos)                     ; Move cursor left a column
        DEC     A
        BIT     3,A                             ; Skip if not beyond left edge of the board
        JR      Z,auto67FA
        ADD     A,$08                           ; Move cursor to right edge of the board
        JR      auto67FA

.CY_CUR_RIGHT
        LD      A,(cur_pos)                     ; Move cursor right a column
        INC     A
        BIT     3,A                             ; Skip if not beyond right edge of the board
        JR      Z,auto67FA
        SUB     $08                             ; Move cursor to left edge of the board
        JR      auto67FA

.CY_CUR_UP
        LD      A,(cur_pos)                     ; Move cursor up a row
        ADD     A,$10
        BIT     7,A                             ; Skip if not beyond top edge of the board
        JR      Z,auto67FA
        SUB     $80                             ; Move cursor to bottom edge of the board
        JR      auto67FA

.CY_CUR_DOWN
        LD      A,(cur_pos)                     ; Move cursor down a row
        SUB     $10
        BIT     7,A                             ; Skip if not beyond bottom edge of the board
        JR      Z,auto67FA
        ADD     A,$80                           ; Move cursor to the top edge of the board
.auto67FA
        CALL    CY_PRINT_BOARD_CSR              ; Redraw board with cursor in new position
        JP      CY_KEY_WAIT                     ; Loop back

;***************************************************************************
.CY_MAIN_KEY_WAIT
        CALL    CY_KEY_WAIT
        LD      A,(gameOver)                    ; Has game ended?
        OR      A                               ; 0 = in progress, 1 = game has ended
        LD      A,(lastKey)                     ; (Get last key code)
        JP      NZ,auto681A                     ; Skip if so (Codes D,E and M are not valid)
        CP      'D'                             ; Demo          (Computer plays both sides)
        JR      Z,CY_CMD_DEMO
        CP      'E'                             ; Enter         (Humans to play each other)
        JR      Z,CY_CMD_ENTER
        CP      'M'                             ; Move          (Computer to play next move)
        JP      Z,CY_CMD_MOVE
.auto681A
        CP      'A'                             ; Alter
        JP      Z,CY_CMD_ALTER
        CP      'B'                             ; take-Back
        JP      Z,CY_CMD_TAKE_BACK
        CP      'C'                             ; Change-pos'n
        JP      Z,CY_CMD_CHANGE_POSN
        CP      'F'                             ; take-Forward
        JP      Z,CY_CMD_TAKE_FORWARD
        CP      'G'                             ; new-Game
        JP      Z,CY_CMD_NEW_GAME
        CP      'L'                             ; Level
        JP      Z,CY_CMD_LEVEL
        CP      'N'                             ; Next-best
        JP      Z,CY_CMD_NEXT_BEST
        CP      'R'                             ; Replay
        JP      Z,CY_CMD_REPLAY
        RET

;***************************************************************************
.CY_CMD_DEMO
; Computer is set to play both White and Black
        LD      A,$03                           ; Computer to play both sides
        LD      (contenders),A
        JP      auto60B7

;***************************************************************************
.CY_CMD_ENTER
; Humans to play each other
        LD      A,$00
        LD      (contenders),A                  ; Humans play each side
        CALL    auto756E
        JP      CY_MAIN_KEY_WAIT

;***************************************************************************
.CY_CMD_REPLAY                                  ; Replay
        LD      A,(GDmoveHistory)
        INC     A
        JP      Z,CY_MAIN_KEY_WAIT
.auto6876
        CALL    MP09                            ; "Replaying game"
        LD      A,$01                           ; Replay game
        LD      (replayGame),A
        LD      HL,$FFFF
        JR      auto68B2

;***************************************************************************
.CY_CMD_TAKE_BACK
        LD      HL,(GDmoveOffset)               ; First move?
        LD      A,L                             ; HL will 0 if so
        OR      H                               ; If HL is 0 then can't take-Back
        JP      Z,CY_MAIN_KEY_WAIT              ; Jump if so
.auto68AE
        LD      HL,(GDmoveOffset)               ; Get offset to current move
        DEC     HL                              ; Update value to previous move
.auto68B2
        LD      (autoC003),HL                   ; Point to previous move
        LD      A,(autoC006)                    ; Move number
        LD      (moveNo),A                      ; Move number
        DEC     A                               ; First move?
        JP      Z,auto60A4                      ; Skip if so
        CALL    CY_CLEAR_BOARD
        LD      A,(autoC005)                    ; Get previous player: Black or White
        LD      (nextToPlay),A                  ; Make this the next player to play
        AND     $01
        LD      (playerTurn),A
        LD      DE,autoC007
        LD      HL,autoBE00
        LD      B,$20
.auto68D5
        LD      A,(DE)
        LD      (HL),A
        INC     HL
        INC     DE
        LD      A,(DE)
        LD      (HL),A
        INC     DE
        LD      A,L
        ADD     A,$07
        LD      L,A
        DJNZ    auto68D5
        LD      HL,autoBE00
        LD      DE,cyrusBoard
.auto68E8
        LD      A,(HL)
        OR      A
        JP      M,auto68F2
        LD      E,A
        LD      A,L
        OR      $04
        LD      (DE),A
.auto68F2
        LD      A,L
        ADD     A,$08
        LD      L,A
        JR      NC,auto68E8
        CALL    auto6F99
        LD      A,(autoBCDB)
        OR      A
        JR      Z,auto6907
        CALL    CY_PRINT_BOARD                  ; Print board
.auto6907
        JP      auto60A7

;***************************************************************************
.CY_CMD_TAKE_FORWARD
        LD      HL,(GDmoveOffset)               ; Get move history offset
        LD      BC,GDmoveHistory                ; Point to move history
        ADD     HL,BC                           ; Get current move
        LD      A,(HL)                          ; Is there a $FF marker at HL?
        INC     A                               ; $FF signifies end of move history
        JP      Z,CY_MAIN_KEY_WAIT              ; Jump if so - can't take-Forward

        LD      HL,(GDmoveOffset)               ; Get current move offset
        INC     HL                              ; Advance to next move
        LD      (autoC003),HL                   ; Next move offset
        JP      auto60B7

;***************************************************************************
.CY_CMD_LEVEL
        LD      A,(level)
        INC     A
        CP      $0B
        JR      NZ,auto693B
        XOR     A
.auto693B
        LD      (level),A
        CP      $08
        JR      NZ,auto6946
        LD      A,$00
        JR      auto6954
.auto6946
        CP      $09
        JR      NZ,auto694E
        LD      A,$03
        JR      auto6954
.auto694E
        CP      $0A
        JR      NZ,auto6954
        LD      A,$07
.auto6954
        LD      (autoBCBD),A
        CALL    PRINT_LEVEL                     ; display selected level
        JP      CY_MAIN_KEY_WAIT

;***************************************************************************
.CY_CMD_MOVE
        LD      A,(playerTurn)
        OR      A
        LD      A,$01                           ; White = Human, Black = Computer
        JR      Z,auto6967
        LD      A,$02                           ; Black = Human, White = Computer
.auto6967
        LD      (contenders),A                  ; Update who's playing who
.auto696A
        LD      A,(playerTurn)
        OR      A
        LD      A,$01
        JR      Z,auto6973
        XOR     A
.auto6973
        LD      (playerTurn),A
        LD      HL,autoBCA0
        XOR     A
        SUB     (HL)
        LD      (HL),A
        JP      auto60B7

;***************************************************************************
.CY_CMD_NEW_GAME
        CALL    YES_NO
        JP      auto60B7
.YES_NO
        CALL    MP10                            ; "New game (Y/N)?"
        CALL    CY_KEYBD_READ
        CP      'Y'
        RET     NZ
        CALL    SETUP_BOARD
        CALL    REDRAW_MOVE
        JP      CY_NEW_GAME

;***************************************************************************
.CY_CMD_NEXT_BEST                               ; Next best move
        LD      HL,(GDmoveOffset)
        LD      A,L
        OR      H
        JP      Z,CY_MAIN_KEY_WAIT
        LD      A,$01
        LD      (autoBCDD),A
        JP      auto68AE

;***************************************************************************
.CY_CMD_ALTER
        LD      HL,blackPcCol
        CALL    auto6B97

        LD      HL,whitePcCol
        CALL    auto6B97

        LD      HL,blackSqCol
        CALL    auto6B97
        LD      HL,whiteSqCol
        CALL    auto6B97
        JP      auto60B7
.auto6B97
        PUSH    HL
.auto6B98
        CALL    CY_KEYBD_READ
        CP      'D'
        JR      NZ,auto6BA7
        CALL    CY_DEFAULT_COLOURS
        CALL    CY_PRINT_BOARD
        JR      auto6B98
.auto6BA7
        SUB     $10
        JP      M,auto60B7
        CP      $09
        JP      P,auto60B7
        POP     HL
        CP      $08
        RET     Z
        LD      (HL),A
        CALL    CY_PRINT_BOARD
        RET

;***************************************************************************
.CY_DEFAULT_COLOURS                             ; Default colours
        LD      A,$00
        LD      (blackPcCol),A                  ;BPc colour
        LD      A,$07
        LD      (whitePcCol),A                  ;WPc colour
        LD      A,$04
        LD      (blackSqCol),A                  ;BSq colour
        LD      A,$06
        LD      (whiteSqCol),A                  ;WSq colour
        RET

;***************************************************************************
.CY_CMD_TAPE
        CALL    CY_KEYBD_READ                   ; Get key code
        CP      'S'                             ; Save-record?
        JR      NZ,CY_LOAD_GAME                 ; Skip to see if loading a game is required
.CY_SAVE_GAME
        LD      A,(GDmoveHistory)               ; Is there any move history?
        INC     A                               ; This is the first byte of move history
        JP      Z,auto60B7                      ; Jump if it is the $FF end marker (i.e. no moves made)
        CALL    CY_KEYBD_READ

        LD      DE,$0058                        ; Bytes counter
        LD      HL,GDmoveHistory                ; Start at the move history
.auto6C78
        INC     DE                              ; Increase counter
        LD      A,(HL)                          ; Has the end marker $FF been found?
        INC     HL
        INC     A
        JR      NZ,auto6C78                     ; Loop back if not
        LD      IX,autoC000                     ; IX = address of the first byte to be save in the GAME_DATA
                                                ; DE = number of bytes to be saved
        LD      A,$FF                           ; Mark the first byte to be saved
        LD      (autoC000),A
        CALL    CY_ZX_SAVE_CODE                 ; Now save the game data to tape
        JP      auto60B7

;***************************************************************************
.CY_LOAD_GAME
        CP      'L'                             ; Load-record?
        JP      NZ,auto60B7                     ; Jump if not
        LD      DE,$1F00                        ; Assume a maximum of $1F00 bytes will be loaded
        SCF
        LD      IX,autoE000                     ; This is where saved game will be loaded
        LD      A,$FF                           ; data block to be loaded
        CALL    CY_ZX_LOAD_CODE                 ; Load the game data
        JR      NC,auto6D0C                     ; Skip if game didn't load

                                                ; Check validity of loaded data

        LD      HL,autoE058                     ; Pointer to loaded move history
        OR      A                               ; Clear Fc
        PUSH    IX                              ; IX points to address one after loaded game data
        POP     BC                              ; Put this in BC
        SBC     HL,BC                           ; Subtract last address of game data (dunno what this check does)
        LD      A,H                             ; Check sign of H register (Fs=1-Bit7)
        OR      A
        JP      P,auto6CF4                      ; Jump if positive - the loaded data is invalid

        LD      A,(IX-2)                        ; Is the marker at the end of the move history $FF?
        INC     A                               ; A should now be $00
        JR      NZ,auto6CF4                     ; Skip if not zero - the loaded data is invalid

        LD      A,(autoE000)                    ; The 1st byte should be $FF
        INC     A                               ; A should now be $00
        JR      NZ,auto6CF4                     ; Skip if not zero - the loaded data is invalid

        LD      HL,autoC000                     ; Data checks are OK, so move data into GAME_DATA area
        LD      DE,autoE000                     ; Address of loaded data
        LD      BC,$1F00                        ; Wow - move $1F00 bytes!  Why not the number loaded?
.auto6CD4
        LD      A,(DE)                          ; A LDIR instruction would have been better here!
        LD      (HL),A
        INC     DE
        INC     HL
        DEC     BC
        LD      A,B
        OR      C
        JR      NZ,auto6CD4
        LD      HL,(GDmoveOffset)
        JP      auto68B2                        ; Game has been loaded successfully, jump!
.auto6CF4
        CALL    CY_WARNING_BEEP
.auto6D0C
        JP      auto60B7

;***************************************************************************
.CY_ZX_SAVE_CODE
        LD      HL,CY_ZX_TAPE_DONE
        PUSH    HL
        JP      auto04C6                        ;ZX Spectrum tape save routine SA-BYTES

;***************************************************************************
; Load code from the tape
.CY_ZX_LOAD_CODE
        INC     D                               ; Reset Fz (D can't be $FF)
        EX      AF,AF'                          ; Save A=$FF (Data block to be loaded)
                                                ; Save Fz=0 (first byte received will first loaded)
                                                ; Save Fc=1 (bytes received to be loade, not verified)
        DEC     D                               ; Restore DE
                                                ; DE=$1F00 bytes to load, IX=LOADSPACE - where to load bytes
        LD      A,$0F                           ; White border for ZX Spectrum screen
 ;      OUT     (IOPORTFEH),A
        LD      HL,CY_ZX_TAPE_DONE              ; Return address after loading code
        PUSH    HL                              ; Put this on the stack
        JP      auto0562                        ; ZX Spectrum tape load routine LD-BYTES

;***************************************************************************
.CY_ZX_TAPE_DONE
        LD      A,$07                           ; Make ZX Spectrum border White
;        OUT     (IOPORTFEH),A
        LD      A,$7F                           ; Is a key being pressed?
        IN      A,(IOPORTFEH)                   ; i.e. was load aborted?
        RRA
        JR      C,CY_ZX_TAPE_DONE1              ; Skip and return if not (code loaded OK)
        CALL    CY_WARNING_BEEP
        LD      HL,FLAGS
        RES     5,(HL)
        OR      A
.CY_ZX_TAPE_DONE1
        RET                                     ; Fc=1 code loaded OK, Fc=0 code failed to load

;***************************************************************************
;Setup chess pieces on board
.CY_CMD_CHANGE_POSN
        CALL    auto70D3
        XOR     A
        LD      (autoBD4D),A
        LD      A,(nextToPlay)
        RRA
        LD      A,$83
        JR      C,auto6D67
        LD      A,$03
.auto6D67
        LD      (pieceColour),A
.auto6D6A
        CALL    PRINT_BOARD                     ; Print board with cursor
        CALL    MP11                            ; "BOARD SETUP MODE"
        LD      A,(pieceColour)
        CP      $03
        JR      Z,auto6E50
        JR      auto6E58
.auto6E50
.auto6E58
        CALL    CY_KEY_WAIT
        LD      A,(lastKey)
        CP      'E'                             ; Exit ?
        LD      HL,zcflags
        SET     sb,(HL)
        JP      Z,auto6ECC                      ; If so, exit Board Setup mode
        CP      'G'                             ; new-Game ?
        JR      NZ,auto6E73                     ; Skip if not
        CALL    YES_NO                          ; Check that new-Game is wanted
        JP      auto6D6A                        ; Loop back if not
.auto6E73
        CP      'T'                             ; Toggle-colour ?
        JR      NZ,auto6E82                     ; Skip if not
        LD      A,(pieceColour)                 ; Switch colour of piece to be place
        XOR     $80
        LD      (pieceColour),A
        JP      auto6D6A                        ; Loop back
.auto6E82
        CP      'A'                             ; All-clear?
        JR      NZ,auto6E8B                     ; Skip if board not to be cleared
        CALL    CY_CLEAR_BOARD                  ; Clear the board
        JR      auto6EC4
.auto6E8B
        CP      'U'                             ; Unoccupied?
        JR      NZ,auto6E98
        LD      A,(cur_pos)                     ; Clear the square that cursor is on
        LD      L,A
        LD      H,auto00BD
        XOR     A
        JR      auto6EB4
.auto6E98
        LD      HL,CY_PIECE_TABLE+1             ; Piece lookup table
        CALL    CY_PIECE_LOOKUP                 ; Is key code a piece name to be placed?
        JP      C,auto6D6A                      ; Loop back if not

        INC     HL                              ; Place the piece on the board
        LD      A,(HL)
        LD      (autoBCA3),A
        LD      A,(cur_pos)
        LD      L,A
        LD      H,auto00BD
        LD      A,(autoBCA3)
        LD      B,A
        LD      A,(pieceColour)
        ADD     A,B
.auto6EB4
        LD      C,(HL)
        LD      (HL),A
        BIT     0,C
        JR      NZ,auto6EC4
        BIT     2,C
        JR      Z,auto6EC4
        RES     2,C
        LD      L,C
        INC     H
        SET     7,(HL)
.auto6EC4
        LD      A,$02
        LD      (autoBD4D),A
        JP      auto6D6A

;***************************************************************************
.auto6ECC
        LD      A,(pieceColour)
        LD      HL,nextToPlay
        LD      B,(HL)
        RES     0,B
        SUB     $03
        LD      (playerTurn),A
        LD      A,$02                           ; Black = Human, White = Computer
        JR      Z,auto6EE2
        SET     0,B
        LD      A,$01                           ; White = Human, Black = Computer
.auto6EE2
        LD      (contenders),A                  ; Update who's playing who
        LD      A,B
        CP      (HL)
        JR      Z,auto6EEF
        LD      (HL),B
        LD      A,$02
        LD      (autoBD4D),A
.auto6EEF
        LD      A,(autoBD4D)
        OR      A
        JP      Z,auto60B7
        CALL    auto65DA
        CALL    auto6F99
        LD      A,$0A
        LD      (moveNo),A
        LD      A,$80
        LD      (autoBCA0),A
        CALL    auto7810
        JP      Z,auto6588
        CALL    auto6F77
        LD      A,$FF
        LD      (GDmoveHistory),A
        JP      auto60A7

;***************************************************************************
; Look for the piece data (value held in A) in PIECE_TABLE. HL points to PIECE_TABLE
;
.CY_PIECE_LOOKUP
        LD      B,$06                           ; Six pieces to look at
.auto6F25
        CP      (HL)                            ; Piece found
        RET     Z                               ; Return if so. HL points to the piece's data. Fc=0, piece found
        INC     HL                              ; Move to the next piece's data (3 bytes of data to skip)
        INC     HL
        INC     HL
        DJNZ    auto6F25                        ; Loop back
        SCF                                     ; Fc=1 to signify piece has not been found.
        RET

;***************************************************************************
; The 1st byte seems to give the piece a value:
;   King = 0; Queen = 9, Rook = 5, Bishop = 3, Knight = 3, Pawn =1
;
; Bit 7 signifies a Bishop.  These values are used to identify which piece is
; is occupying a square when drawing the board.
;
; The 2nd byte is the ASCII initial of the piece name (N for kNight as K is used for King)
; The 3rd byte must contain piece specific properties - not sure what
;
.CY_PIECE_TABLE
        DEFB    $00,'K',@00000000       ; King parameters
        DEFB    $09,'Q',@00001000       ; Queen parameters
        DEFB    $05,'R',@00010000       ; Rook parameters
        DEFB    $83,'B',@00100000       ; Bishop parameters
        DEFB    $03,'N',@00110000       ; Knight parameters
        DEFB    $01,'P',@01000000       ; Pawn parameters

;       DEFB    0
;       DEFM    "K",$00,$09,"Q",$08,$05,"R",$10,$03+$80,"B "
;       DEFM    $03,"N0",$01,"P@"

;***************************************************************************
.auto6F77
        LD      A,(nextToPlay)                  ; Whose turn to play: 0=White, 1=Black
        LD      (autoC005),A
        LD      A,(moveNo)                      ; Current move
        LD      (autoC006),A
        LD      HL,autoBE00
        LD      DE,autoC007
        LD      B,$20
.auto6F8B
        LD      A,(HL)
        LD      (DE),A
        INC     HL
        INC     DE
        LD      A,(HL)
        LD      (DE),A
        INC     DE
        LD      A,L
        ADD     A,$07
        LD      L,A
        DJNZ    auto6F8B
        RET

;***************************************************************************
.auto6F99
        XOR     A
        LD      (autoBD0A),A
        LD      (autoBD0D),A
        LD      HL,autoBD59
        LD      B,$06
.auto6FA5
        LD      (HL),A
        INC     L
        DJNZ    auto6FA5
        RET

;***************************************************************************
.auto6FAA
        LD      A,$FF
        LD      (autoBF00),A
        RET

;***************************************************************************
; Sounds beeper only if it is switched on
.BEEP
        POP     HL
        LD      A,(HL)
        INC     HL
        LD      (autoBCD1),A
        LD      A,(HL)
        INC     HL
        LD      (autoBCD2),A
        PUSH    HL
        LD      A,($04B0)
        LD      HL,autoBCD1
        LD      D,(HL)
        LD      E,$00
        LD      HL,autoBCD2
.auto6FC7
        LD      B,(HL)
        XOR     @01000000
        LD      C,A
        LD      A,(beeper)
        OR      A
        LD      A,C
        JR      NZ,auto6FD4
        OUT     (IOPORTFEH),A
.auto6FD4
        DEC     E
        JR      NZ,auto6FD9
        DEC     D
        RET     Z
.auto6FD9
        DJNZ    auto6FD4
        JR      auto6FC7

;***************************************************************************
.auto70BF
        LD      A,(IX+$01)
        CALL    CY_PRINT_BOARD_CSR
        JR      auto70CD
.auto70C7
        LD      A,(IX+$02)
        CALL    CY_PRINT_BOARD_CSR
.auto70CD
        CALL    BEEP
        DEFW    $2896
        RET

;***************************************************************************
.auto70D3
        LD      A,(nextToPlay)
        RRA
        LD      A,$10
        JR      NC,CY_PRINT_BOARD_CSR
        LD      A,@01100000
        JR      CY_PRINT_BOARD_CSR

; Draw the chess board and pieces
.CY_PRINT_BOARD
        LD      A,$FF                           ; Don't print cursor
.CY_PRINT_BOARD_CSR
        LD      (cur_pos),A                     ; Cursor postion
        JP      PRINT_BOARD

;***************************************************************************
.CY_PRINT_ILLEGAL
        CALL    MP12                            ; "ILLEGAL"
.CY_WARNING_BEEP
        CALL    BEEP
        DEFW    $80FF
        RET

.auto7562
        LD      A,(playerTurn)
        OR      A                               ; Is Human to play Black and Computer White?
        LD      A,$02                           ; (Black = Human, White = Computer)
        JR      Z,auto756B                      ; Skip if so
        DEC     A                               ; White = Human, Black = Computer
.auto756B
        LD      (contenders),A                  ; Update who's playing who
.auto756E
        LD      A,(contenders)                  ; Print who's playing White
        AND     $02
        LD      A,(contenders)                  ; Print who's playing Black
        AND     $01
        RET

;***************************************************************************
.auto7800
        DEFB    $0E,$12,$1F,$21,$F2,$EE,$E1,$DF
;       DEFM    $0E,$12,$1F,"!",'r'+$80,'n'+$80,'a'+$80,'_'+$80

;***************************************************************************
.auto7808
        DEFB    $0F,$11,$EF,$F1
;       DEFM    $0F,$11,'o'+$80,'q'+$80

;***************************************************************************
.auto780C
        DEFB    $01,$FF,$10,$F0
;       DEFM    $01,$FF,$10,'p'+$80

;***************************************************************************
.auto7810
        LD      IX,autoBD58
        LD      A,(nextToPlay)
        XOR     $01
        LD      (nextToPlay),A
        CALL    auto8400
        PUSH    AF
        LD      A,(autoBCA0)
        ADD     A,$80
        LD      E,A
        LD      A,(playerTurn)
        OR      A
        JR      Z,auto7832
        XOR     A
        SUB     B
        LD      B,A
        XOR     A
        SUB     E
        LD      E,A
.auto7832
        LD      A,(autoBD2E)
        SRA     A
        SRA     E
        ADD     A,E
        SRA     A
        SRA     A
        JR      Z,auto7841
        INC     A
.auto7841
        LD      (autoBD1E),A
        LD      A,(autoBD4A)
        ADD     A,B
        LD      (autoBD4A),A
        LD      HL,autoBE08
        XOR     A
        LD      (autoBD6D),A
        CALL    auto789D
        LD      E,C
        LD      L,$88
        CALL    auto789D
        LD      A,C
        SUB     E
        CP      $0E
        JR      C,auto786C
        CP      $F2
        JR      NC,auto786C
        RLA
        LD      A,$0E
        JR      NC,auto786C
        LD      A,$F2
.auto786C
        ADD     A,A
        ADD     A,A
        ADD     A,A
        LD      (autoBD2E),A
        LD      A,(moveNo)
        LD      B,$00
        CP      $08
        JR      C,auto7891
        LD      A,C
        ADD     A,E
        SET     1,B
        CP      '4'
        JR      NC,auto7891
        SET     2,B
        CP      '('
        JR      NC,auto7891
        SET     3,B
        CP      $19
        JR      NC,auto7891
        SET     4,B
.auto7891
        LD      HL,nextToPlay
        LD      A,(HL)
        INC     A
        AND     $01
        ADD     A,B
        LD      (HL),A
        POP     AF
        OR      A
        RET

;***************************************************************************
.auto789D
        LD      C,$00
        LD      B,$0F
.auto78A1
        BIT     7,(HL)
        JR      NZ,auto78B3
        INC     L
        LD      A,(HL)
        AND     $0F
        CP      $01
        JR      NZ,auto78B0
        LD      (autoBD6D),A
.auto78B0
        ADD     A,C
        LD      C,A
        DEC     L
.auto78B3
        LD      A,$08
        ADD     A,L
        LD      L,A
        DJNZ    auto78A1
        RET

;***************************************************************************
.auto78BA
        LD      HL,autoBD4C
        BIT     7,(HL)
        JR      NZ,auto78CE
        CALL    CY_ESCKEY_CHK                   ; Check if ESC key has been pressed
        JR      Z,auto78CE                      ; Skip if so
        LD      HL,autoBD4C
        SET     7,(HL)
        CALL    auto7562
.auto78CE
        LD      HL,autoBCA9
        LD      A,(HL)
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        INC     L
        PUSH    DE
        INC     E
        INC     E
        INC     E
        INC     DE
        PUSH    HL
        EX      AF,AF'
        LD      A,auto00BC
        EX      AF,AF'
        LD      H,auto00BD
        LD      IY,autoBE00
        LD      A,(nextToPlay)
        EXX
        RRCA
        LD      C,A
        EXX
        JR      NC,auto78F6
        LD      IY,autoBE80
.auto78F6
        LD      B,(IY+$00)
        BIT     4,(IY+$01)
        CALL    Z,auto7A61
        LD      C,$88
        EXX
        LD      DE,auto7808
        CALL    auto7A18
        LD      A,(IY+$08)
        OR      A
        CALL    P,auto7A57
        LD      A,(IY+$10)
        OR      A
        CALL    P,auto7A2E
        LD      A,(IY+$18)
        OR      A
        CALL    P,auto7A2E
        LD      A,(IY+$20)
        OR      A
        CALL    P,auto7A4F
        LD      A,(IY+$28)
        OR      A
        CALL    P,auto7A4F
        LD      A,(IY+$30)
        LD      C,$88
        OR      A
        CALL    P,auto7A13
        LD      A,(IY+$38)
        OR      A
        CALL    P,auto7A13
        LD      C,$11
        EXX
        LD      HL,autoBE40
        BIT     7,C
        JR      Z,auto794C
        LD      L,$C0
        EXX
        LD      C,$F1
        EXX
.auto794C
        LD      A,(HL)
        RLA
        JR      C,auto799C
        INC     L
        LD      A,(HL)
        DEC     L
        AND     $0E
        LD      A,(HL)
        EXX
        LD      B,A
        JR      NZ,auto79C8
        ADD     A,C
        LD      L,A
        XOR     A
        OR      (HL)
        CALL    NZ,auto7A97
        DEC     L
        DEC     L
        XOR     A
        OR      (HL)
        CALL    NZ,auto7A97
        INC     L
        XOR     A
        OR      (HL)
        JR      NZ,auto7994
        CALL    auto7A9D
        LD      A,B
        ADD     A,' '
        BIT     6,A
        JR      NZ,auto7994
        LD      A,C
        DEC     A
        ADD     A,L
        JP      M,auto799B
        LD      L,A
        XOR     A
        OR      (HL)
        JR      NZ,auto799B
        CALL    auto7A9D
        DEC     L
        OR      (HL)
        CALL    NZ,auto79FC
        INC     L
        INC     L
        LD      A,(HL)
        OR      A
        CALL    NZ,auto79FC
        JP      auto799B
.auto7994
        BIT     5,(IX+$03)
        CALL    NZ,auto79E4
.auto799B
        EXX
.auto799C
        LD      A,$08
        ADD     A,L
        LD      L,A
        BIT     6,A
        JP      NZ,auto794C
        EXX
        LD      A,$FF
        LD      (DE),A
        POP     HL
        LD      (HL),E
        INC     L
        LD      (HL),D
        POP     HL
        EX      DE,HL
        OR      A
        SBC     HL,DE
        RR      H
        RR      L
        RR      L
        DEC     L
        EX      DE,HL
        LD      (HL),A
        INC     L
        LD      (HL),E
        INC     L
        LD      (HL),$00
        INC     L
        LD      A,(IX+$03)
        AND     $81
        LD      (HL),A
        RET

;***************************************************************************
.auto79C8
        PUSH    BC
        LD      C,$88
        EXX
        INC     L
        LD      A,(HL)
        DEC     L
        LD      DE,auto79E0
        PUSH    DE
        OR      A
        JP      M,auto7A51
        RRA
        RRA
        JR      C,auto7A15
        RRA
        JR      C,auto7A30
        JR      auto7A59
.auto79E0
        POP     BC
        EXX
        JR      auto799C
.auto79E4
        BIT     3,(IX+$03)
        RET     NZ
        LD      A,(IX+$02)
        INC     A
        CP      B
        JR      Z,auto79F6
        DEC     A
        DEC     A
        CP      B
        RET     NZ
        INC     L
        INC     L
.auto79F6
        DEC     L
        LD      A,'('
        JP      auto7A9D
.auto79FC
        EXX
        XOR     C
        EXX
        RET     P
        LD      A,(HL)
        EXX
        LD      B,L
        SUB     $03
        LD      L,A
        LD      A,(HL)
        LD      L,B
        EXX
        AND     $0F
        DEC     A
        RET     NZ
        LD      A,' '
        DEC     DE
        LD      (DE),A
        INC     DE
        RET

;***************************************************************************
.auto7A13
        LD      B,A
        EXX
.auto7A15
        LD      DE,auto7800
.auto7A18
        LD      B,$08
.auto7A1A
        LD      A,(DE)
        INC     E
        EXX
        ADD     A,B
        LD      L,A
        AND     C
        JR      NZ,auto7A29
        OR      (HL)
        CALL    Z,auto7A9D
        CALL    NZ,auto7A97
.auto7A29
        EXX
        DJNZ    auto7A1A
        EXX
        RET

;***************************************************************************
.auto7A2E
        LD      B,A
        EXX
.auto7A30
        LD      DE,auto780C
.auto7A33
        LD      B,$04
.auto7A35
        LD      A,(DE)
        INC     E
        EXX
        LD      C,A
        LD      L,B
.auto7A3A
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     $88
        JR      NZ,auto7A4A
        OR      (HL)
        CALL    Z,auto7A9D
        JR      Z,auto7A3A
        CALL    auto7A97
.auto7A4A
        EXX
        DJNZ    auto7A35
        EXX
        RET

;***************************************************************************
.auto7A4F
        LD      B,A
        EXX
.auto7A51
        LD      DE,auto7808
        JP      auto7A33

;***************************************************************************
.auto7A57
        LD      B,A
        EXX
.auto7A59
        LD      DE,auto7808
        LD      B,$08
        JP      auto7A35

;***************************************************************************
.auto7A61
        BIT     7,(IX+$03)
        RET     NZ
        BIT     4,(IY+$19)
        JR      NZ,auto7A7D
        BIT     7,(IY+$18)
        JR      NZ,auto7A7D
        XOR     A
        LD      L,B
        INC     L
        OR      (HL)
        JR      NZ,auto7A7D
        INC     L
        OR      (HL)
        CALL    Z,auto7A93
.auto7A7D
        BIT     4,(IY+$11)
        RET     NZ
        BIT     7,(IY+$10)
        RET     NZ
        LD      L,B
        XOR     A
        DEC     L
        OR      (HL)
        RET     NZ
        DEC     L
        OR      (HL)
        RET     NZ
        DEC     L
        OR      (HL)
        RET     NZ
        INC     L
.auto7A93
        LD      A,$10
        JR      auto7A9D
.auto7A97
        EXX
        XOR     C
        EXX
        RET     P
        LD      A,$08
.auto7A9D
        EX      AF,AF'
        CP      D
        JR      Z,auto7AAF
.auto7AA1
        EX      AF,AF'
        EX      DE,HL
        LD      (HL),$FF
        INC     L
        LD      (HL),B
        INC     L
        LD      (HL),E
        INC     L
        LD      (HL),A
        INC     HL
        EX      DE,HL
        OR      A
        RET

.auto7AAF
        LD      A,$80
        CP      E
        LD      A,auto00BC
        JR      NC,auto7AA1
        PUSH    HL
        LD      HL,autoBD4C
        SET     7,(HL)
        POP     HL
        EX      AF,AF'
        OR      A
        RET

;***************************************************************************
.auto7AC0
        LD      A,$FF
        LD      (autoBCDF),A
        OR      A
        RET

;***************************************************************************
.auto7AC7
        LD      A,(autoBCDF)
        OR      A
        RET     NZ
        LD      A,(autoC006)
        DEC     A
        RET     NZ
        CALL    CY_RANDOM_GEN
        LD      HL,$FFFF
.auto7AD7
        INC     HL
        LD      A,(GDmoveOffset)
        LD      C,A
        LD      A,(autoBCDF)
        INC     A
        LD      (autoBCDF),A
        DEC     A
        CP      C
        JR      Z,auto7B05
        LD      BC,GDmoveHistory
        ADD     A,C
        LD      C,A
        JR      NC,auto7AEF
        INC     B
.auto7AEF
        LD      A,(BC)
        AND     $7F
        LD      C,A
        CALL    auto7B57
.auto7AF6
        LD      B,A
        RES     6,A
        CP      C
        JR      Z,auto7AD7
        BIT     6,B
        JR      Z,auto7AC0
        CALL    auto7B4C
        JR      auto7AF6
.auto7B05
        CALL    CY_RANDOM_GEN
        CALL    auto7B57
        LD      C,A
        BIT     6,A
        JR      Z,auto7B30
        LD      A,$01
        RLA
        AND     B
        JR      NZ,auto7B30
.auto7B16
        PUSH    HL
        CALL    CY_RANDOM_GEN
        CALL    auto7B4C
        JR      C,auto7B2B
        POP     DE
        LD      C,A
        BIT     6,A
        JR      Z,auto7B30
        RR      B
        JR      C,auto7B16
        JR      auto7B30
.auto7B2B
        POP     HL
        CALL    auto7B57
        LD      C,A
.auto7B30
        LD      A,C
        AND     '?'
        RLA
        RLA
        LD      E,A
        LD      D,$00
        LD      IX,autoB404
        ADD     IX,DE
        LD      (autoBD29),IX
        XOR     A
        BIT     7,C
        JR      NZ,auto7B4A
        LD      (autoBCDF),A
.auto7B4A
        XOR     A
        RET

;***************************************************************************
.auto7B4C
        LD      DE,$FFFF
.auto7B4F
        CALL    auto7B57
        INC     HL
        BIT     7,D
        JR      Z,auto7B4F
.auto7B57
        CALL    auto7B64
        BIT     7,A
        JR      Z,auto7B5F
        DEC     DE
.auto7B5F
        BIT     6,A
        RET     Z
        INC     DE
        RET

.auto7B64
        PUSH    BC
        PUSH    HL
        LD      A,L
        SRL     H
        RRA
        SRL     H
        RRA
        SRL     H
        RRA
        LD      B,H
        LD      C,A
        POP     HL
        PUSH    HL
        LD      A,$07
        AND     L
        ADD     HL,BC
        LD      BC,GameMap                      ; zcdata.asm
        ADD     HL,BC
        LD      B,A
        INC     B
        LD      A,(HL)
        INC     HL
        LD      C,(HL)
.auto7B81
        RL      C
        RLA
        DJNZ    auto7B81
        POP     HL
        POP     BC
        RET

;***************************************************************************
.CY_RANDOM_GEN                                  ;This generates a random number
        LD      A,R                             ;Helps to make all games different, I guess
        LD      B,A
        LD      A,(randomNo)
        XOR     B
        RRA
        LD      (randomNo),A
        LD      B,A
        RET

;***************************************************************************
.auto7B96
        INC     HL
        PUSH    HL
        POP     IX
        XOR     A
        LD      (autoBD19),A
        INC     E
        LD      A,E
        LD      (autoBD0E),A
.auto7BA3
        LD      HL,autoBD0E
        DEC     (HL)
        RET     Z
        LD      B,(IX+$00)
        INC     B
        JR      NZ,auto7BC5
        CALL    auto810F
        CALL    auto8400
        LD      (IX+$00),A
        LD      HL,autoBD19
        CP      (HL)
        JR      C,auto7BC2
        LD      (HL),A
        LD      (autoBD1A),IX
.auto7BC2
        CALL    auto821C
.auto7BC5
        LD      BC,$0004
        ADD     IX,BC
        JP      auto7BA3

;***************************************************************************
.auto7BCD
        LD      DE,$0004
.auto7BD0
        ADD     HL,DE
        CP      (HL)
        JR      Z,auto7BD8
.auto7BD4
        DJNZ    auto7BD0
        SCF
        RET

;***************************************************************************
.auto7BD8
        EX      AF,AF'
        DEC     L
        CP      (HL)
        RET     Z
        INC     L
        EX      AF,AF'
        JP      auto7BD4

;***************************************************************************
.auto7BE1
        CALL    auto7810
.auto7BE4
        CALL    auto7BF1
        LD      A,(autoBD6E)
        OR      A
        JR      NZ,auto7BE4
        RET

;***************************************************************************
.auto7BEE
        POP     HL
        SCF
        RET

;***************************************************************************
.auto7BF1
        LD      IX,autoBD58
        LD      HL,autoBC91
        LD      A,$FF
        LD      (autoBD0C),A
        LD      B,$0D
.auto7BFF
        LD      (HL),A
        INC     L
        DJNZ    auto7BFF
        INC     A
        LD      (autoBD0D),A
        LD      HL,autoBC9F
        LD      B,$09
.auto7C0C
        LD      (HL),A
        INC     L
        DJNZ    auto7C0C
        CALL    auto78CE
        SET     5,(HL)
        CALL    auto7B96
        LD      A,(autoBCDD)
        OR      A
        JR      NZ,auto7C2A
        CALL    auto7AC7
        JR      NZ,auto7C2A
        CALL    CY_ESCKEY_CHK                   ; Check if ESC key has been pressed
        RET     Z                               ; Return if so
        JP      auto7562
.auto7C2A
        LD      A,(autoBD19)
        OR      A
        JP      Z,auto7BEE
        LD      D,A
        LD      A,(autoBD6E)
        OR      A
        JR      Z,auto7C49
        LD      HL,(autoBD29)
        LD      (HL),$FD
        LD      (autoBD1A),HL
        XOR     A
        LD      (autoBD6E),A
        LD      A,(autoBCBD)
        JR      auto7C5B
.auto7C49
        LD      HL,(autoBD1A)
        LD      (autoBD29),HL
        LD      A,(autoBCBD)
        CP      $05
        JR      C,auto7C5B
        LD      A,$01
        LD      (autoBD6E),A
.auto7C5B
        LD      HL,autoBDF6
        LD      (HL),$00
        LD      (autoBD09),A
        BIT     1,A
        JR      Z,auto7C71
        LD      (HL),A
        BIT     0,A
        JR      Z,auto7C71
        LD      HL,autoBD09
        SET     7,(HL)
.auto7C71
        LD      HL,autoBCAA
        LD      (HL),$01
        OR      A
        JR      Z,auto7C83
        INC     (HL)
        CP      $04
        JR      C,auto7C83
        LD      (HL),$03
        JR      Z,auto7C83
        INC     (HL)
.auto7C83
        LD      B,$07
        LD      A,(autoBD09)
        RLA
        JR      C,auto7CBA
        LD      A,D
        LD      HL,autoB403
        BIT     7,(HL)
        JR      NZ,auto7CB4
        CALL    auto805F
        SRL     D
        SRL     D
        DEC     D
        SRL     D
        LD      B,$0B
        LD      A,(nextToPlay)
        BIT     4,A
        JR      NZ,auto7CAE
        DEC     B
        SRA     A
        DEC     A
        JR      NZ,auto7CAE
        DEC     B
        DEC     B
.auto7CAE
        DEC     D
        JP      M,auto7CB4
        DJNZ    auto7CAE
.auto7CB4
        LD      A,$02
        ADD     A,B
        LD      (autoBD09),A
.auto7CBA
        LD      HL,autoBF00
.auto7CBD
        LD      A,(HL)
        CP      $FF
        JR      Z,auto7CD5
        INC     HL
        EX      AF,AF'
        LD      A,(HL)
        INC     HL
        PUSH    HL
        LD      HL,autoB401
        LD      B,(HL)
        INC     L
        CALL    auto7BCD
        DEC     L
        LD      (HL),$00
        POP     HL
        JR      auto7CBD
.auto7CD5
        LD      HL,autoB403
        CALL    auto8015
        BIT     0,E
        JR      Z,auto7D00
        CALL    MP13                            ; "No more moves!"
        CALL    BEEP
        DEFW    $40FF
        CALL    auto6FAA
        POP     HL
        JP      auto7BE1
.auto7D00
        LD      (autoBD1A),DE
        LD      A,(repeatedMoveCount)
        LD      C,A
        CP      $02
        JP      C,auto7DCF
        LD      HL,(autoBD5C)
        LD      A,H
        EX      AF,AF'
        LD      A,L
        LD      HL,autoB401
        LD      B,(HL)
        INC     L
        CALL    auto7BCD
        DEC     L
        LD      A,(HL)
        OR      A
        JP      Z,auto7DCF
        LD      (HL),$00
        LD      A,(nextToPlay)
        RRA
        LD      A,(autoBD2E)
        JR      NC,auto7D2E
        NEG
.auto7D2E
        ADD     A,'~'
        BIT     2,C
        JR      Z,auto7D36
        SUB     $08
.auto7D36
        LD      (autoBCA0),A
        LD      (autoBD2B),HL
        JP      auto7E9B
        LD      (autoBCA0),A
        RET

;***************************************************************************
.auto7D43
        CALL    auto78BA
        LD      A,E
        OR      A
        JR      Z,auto7DA8
        LD      A,(autoBCA9)
        LD      B,E
        BIT     2,(IX+$03)
        JP      NZ,auto802B
        BIT     7,(HL)
        JR      NZ,auto7DA8
        DEC     A
        LD      DE,autoBC91
        ADD     A,A
        ADD     A,E
        LD      E,A
        LD      A,(DE)
        OR      A
        JP      M,auto7DA8
        INC     E
        EX      AF,AF'
        LD      A,(DE)
        DEC     L
        CALL    auto7BCD
        JR      C,auto7DA8
        DEC     L
        LD      (HL),$00
        PUSH    HL
        POP     IX
        CALL    auto810F
.auto7D77
        CALL    auto8400
        OR      A
        JR      Z,auto7DA5
        LD      C,A
        EX      AF,AF'
        LD      HL,autoBCAA
        LD      A,(HL)
        DEC     L
        LD      B,(HL)
        CP      B
        JP      C,auto7F20
        CALL    Z,auto80C4
        CALL    auto8003
        SET     6,(HL)
        EX      AF,AF'
        DEC     L
        LD      (HL),A
        BIT     1,(IX+$03)
        JP      Z,auto7DEE
        BIT     3,(IX+$03)
        JP      NZ,auto7DEE
        LD      (IX+$00),A
.auto7DA5
        CALL    auto821C
.auto7DA8
        LD      HL,autoBCAA
        LD      A,(HL)
        DEC     L
        CP      (HL)
        JP      C,auto7EE4
        CALL    auto8006
        BIT     5,(HL)
        JP      NZ,auto7DDA
        SET     5,(HL)
        PUSH    HL
        CALL    auto7B96
        POP     HL
        LD      A,(autoBD19)
        OR      A
        JP      Z,auto7FEA
        BIT     7,(HL)
        JP      NZ,auto80E2
        CALL    auto805F
.auto7DCF
        LD      IX,(autoBD1A)
        LD      (IX+$00),$00
        JP      auto7DEB
.auto7DDA
        BIT     7,(HL)
        JP      NZ,auto80ED
        CALL    auto8015
        BIT     0,E
        JR      NZ,auto7E25
        PUSH    DE
        POP     IX
        XOR     A
        LD      (DE),A
.auto7DEB
        CALL    auto810F
.auto7DEE
        LD      A,(autoBCA9)
        DEC     A
        JR      Z,auto7E06
        ADD     A,A
        LD      HL,autoBC83
        ADD     A,L
        LD      L,A
        LD      A,(IX+$01)
        LD      (HL),A
        INC     L
        LD      A,(IX+$02)
        LD      (HL),A
        JP      auto7E0A
.auto7E06
        LD      (autoBD2B),IX
.auto7E0A
        LD      A,(nextToPlay)
        XOR     $01
        LD      (nextToPlay),A
        LD      HL,autoBCA9
        LD      A,(HL)
        INC     (HL)
        DEC     A
        DEC     A
        LD      HL,autoBCA0
        ADD     A,L
        LD      L,A
        LD      A,(HL)
        INC     L
        INC     L
        LD      (HL),A
        JP      auto7D43
.auto7E25
        LD      A,(autoBCA9)
        DEC     A
        RET     Z
        LD      (autoBCA9),A
        CALL    auto821C
        LD      A,(nextToPlay)
        XOR     $01
        LD      (nextToPlay),A
        LD      A,(autoBD4C)
        RLA
        JR      C,auto7E25
        LD      A,(autoBCA9)
        LD      HL,autoBCA0
        ADD     A,L
        LD      L,A
        LD      A,(HL)
        NEG
.auto7E49
        DEC     L
        DEC     L
        CPL
        CP      (HL)
        JR      C,auto7E79
        CPL
        INC     L
        CP      (HL)
        JP      C,auto7DA8
        JP      Z,auto7DA8
        LD      (HL),A
        LD      A,(autoBCA9)
        DEC     A
        JR      Z,auto7E95
.auto7E5F
        ADD     A,A
        LD      B,A
        LD      HL,autoBC91
        ADD     A,L
        LD      L,A
        LD      DE,autoBC83
        LD      A,B
        ADD     A,E
        LD      E,A
        LD      A,(DE)
        OR      A
        JP      M,auto7DA8
        LD      (HL),A
        INC     E
        INC     L
        LD      A,(DE)
        LD      (HL),A
        JP      auto7DA8
.auto7E79
        CALL    auto821C
        LD      HL,autoBCA9
        DEC     (HL)
        LD      A,(nextToPlay)
        XOR     $01
        LD      (nextToPlay),A
        LD      A,(HL)
        JP      auto7E5F
.auto7E8C
        LD      IX,(autoBD2B)
        LD      (autoBD29),IX
        RET

;***************************************************************************
.auto7E95
        LD      A,(autoBCA0)
        INC     A
        JR      Z,auto7EA1
.auto7E9B
        CALL    auto7E8C
        JP      auto7DA8
.auto7EA1
        LD      HL,autoBD0B
        LD      A,$FE
        LD      (autoBCA0),A
        LD      A,(HL)
        CP      $02
        JR      Z,auto7E8C
        INC     L
        CP      (HL)
        JP      NC,auto7DA8
        LD      (HL),A
        JR      auto7E9B
.auto7EB6
        LD      A,(autoBCB6)
        CP      $B4
        LD      A,(autoBCAA)
        JR      Z,auto7ED3
        CP      $03
        JR      C,auto7F27
        LD      L,A
        LD      A,(autoBCB3)
        CP      'X'
        JR      NC,auto7F27
        LD      A,L
        LD      HL,autoBCA9
        JP      auto7F3E
.auto7ED3
        CP      $03
        JR      C,auto7F27
        INC     A
        LD      HL,autoBCA9
        JP      auto7F3E
.auto7EDE
        CALL    auto821C
.auto7EE1
        LD      HL,autoBCA9
.auto7EE4
        LD      A,(HL)
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      (autoBD4E),A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        EX      DE,HL
        INC     L
        LD      A,(HL)
        INC     L
        LD      C,(HL)
        SUB     C
        JP      Z,auto7E25
        INC     (HL)
        INC     L
        LD      A,(HL)
        LD      (autoBD4B),A
        INC     HL
        LD      B,$00
        SLA     C
        RL      B
        ADD     HL,BC
        ADD     HL,BC
        INC     (HL)
        JR      NZ,auto7EE1
        PUSH    HL
        POP     IX
        CALL    auto810F
        CALL    auto8400
        LD      C,A
        OR      A
        JR      NZ,auto7F20
        LD      A,(autoBD4B)
        INC     C
        RLA
        JR      C,auto7F43
        JP      auto7EDE
.auto7F20
        LD      A,(autoBCB2)
        CP      $B4
        JR      Z,auto7EB6
.auto7F27
        LD      A,(autoBD4E)
        OR      A
        JR      Z,auto7F39
        LD      A,(IX+$03)
        OR      A
        JP      P,auto7F43
        BIT     2,A
        JP      NZ,auto7F43
.auto7F39
        LD      HL,autoBCAA
        LD      A,(HL)
        DEC     L
.auto7F3E
        INC     A
        CP      (HL)
        JP      NC,auto7DEE
.auto7F43
        LD      A,(autoBCA9)
        LD      HL,autoBCA0
        ADD     A,L
        LD      L,A
        INC     C
        LD      A,C
        DEC     L
        DEC     L
        CPL
        CP      (HL)
        JR      C,auto7F81
        INC     L
        LD      A,(IX+$03)
        AND     'B'
        JR      NZ,auto7FAD
.auto7F5B
        LD      A,C
        CP      (HL)
        JP      C,auto7EDE
        JP      Z,auto7EDE
        LD      (HL),A
        LD      A,(autoBD4B)
        RLA
        JP      C,auto7EDE
        LD      A,(autoBCA9)
        LD      HL,autoBC91
        DEC     A
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      A,(IX+$01)
        LD      (HL),A
        INC     L
        LD      A,(IX+$02)
        LD      (HL),A
        JP      auto7EDE
.auto7F81
        CALL    auto821C
        CALL    auto821C
        LD      A,(nextToPlay)
        XOR     $01
        LD      (nextToPlay),A
        LD      HL,autoBCA9
        DEC     (HL)
        LD      A,(autoBD4B)
        RLA
        JP      C,auto7DA8
        LD      A,(HL)
        LD      HL,autoBC91
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      A,(IX+$01)
        LD      (HL),A
        INC     L
        LD      A,(IX+$02)
        LD      (HL),A
        JP      auto7DA8
.auto7FAD
        LD      A,(autoBCA9)
        LD      B,A
        LD      A,(autoBCAA)
        INC     A
        INC     A
        CP      B
        JR      C,auto7F5B
        LD      A,C
        JR      Z,auto7FC0
        ADD     A,$01
        JR      C,auto7F5B
.auto7FC0
        CP      (HL)
        JP      C,auto7EDE
        LD      A,(autoBDF6)
        OR      A
        JR      Z,auto7F5B
        LD      A,C
        CP      $02
        JR      Z,auto7F5B
        JP      auto7DEE
.auto7FD2
        LD      A,(autoBCA9)
        LD      HL,autoBCA0
        ADD     A,L
        LD      L,A
        LD      A,(nextToPlay)
        RRA
        LD      A,(autoBD2E)
        JR      NC,auto7FE5
        NEG
.auto7FE5
        ADD     A,'x'
        JP      auto7E49
.auto7FEA
        BIT     6,(HL)
        JP      NZ,auto7E25
        BIT     7,(HL)
        JR      Z,auto7FD2
        LD      A,(autoBCA9)
        LD      (autoBD0B),A
        LD      HL,autoBCA0
        ADD     A,L
        LD      L,A
        LD      A,$01
        JP      auto7E49
.auto8003
        LD      HL,autoBCA9
.auto8006
        LD      A,(HL)
        LD      B,A
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        EX      DE,HL
        INC     L
        LD      E,(HL)
        INC     L
        LD      C,(HL)
        INC     L
        RET

;***************************************************************************
.auto8015
        XOR     A
        LD      BC,$0004
        DEC     L
        DEC     L
        LD      E,L
        DEC     L
.auto801D
        ADD     HL,BC
        CP      (HL)
        JP      NC,auto801D
        LD      A,(HL)
        CP      $FF
        RET     Z
        LD      D,H
        LD      E,L
        JP      auto801D
.auto802B
        LD      HL,autoBCA9
        INC     A
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        EX      DE,HL
        INC     L
        INC     L
        LD      DE,$FFFC
        LD      A,(IX+$02)
.auto803E
        ADD     HL,DE
        CP      (HL)
        JR      Z,auto8047
        DJNZ    auto803E
        JP      auto7DA8
.auto8047
        LD      D,$01
        DEC     L
        DEC     L
        PUSH    HL
        POP     IX
        LD      (HL),$00
        CALL    auto810F
        SET     7,(IX+$01)
        LD      A,$80
        LD      (autoBD4B),A
        JP      auto7D77
.auto805F
        LD      HL,autoBD09
        BIT     7,(HL)
        RET     NZ
        LD      IY,auto80A2
        EX      AF,AF'
        CALL    auto8003
        LD      A,B
        CP      $03
        LD      A,(nextToPlay)
        JR      NC,auto807A
        LD      A,E
        ADD     A,A
        LD      D,A
        LD      A,$10
.auto807A
        LD      B,E
        BIT     4,A
        JR      Z,auto8083
        LD      IY,auto80BB
.auto8083
        BIT     3,A
        JR      Z,auto808B
        LD      IY,auto80A7
.auto808B
        EX      AF,AF'
        CP      C
        JR      NC,auto8090
        LD      A,C
.auto8090
        SUB     $09
        RET     C
        LD      C,A
        INC     HL
.auto8095
        LD      A,(HL)
        INC     L
        INC     L
        INC     L
        SUB     C
        JR      C,auto80A0
.auto809C
        INC     HL
        DJNZ    auto8095
        RET
.auto80A0
        JP      (IY)

.auto80A2
        ADD     A,$10
        JP      M,auto80AB
.auto80A7
        BIT     2,(HL)
        JR      Z,auto809C
.auto80AB
        LD      A,$C9
        AND     (HL)
        JR      NZ,auto809C
        DEC     L
        DEC     L
        DEC     L
        LD      (HL),$00
        INC     L
        INC     L
        INC     L
        JP      auto809C
.auto80BB
        DEC     D
        ADD     A,$10
        JP      P,auto809C
        JP      auto80A7
.auto80C4
        BIT     0,A
        RET     NZ
        LD      HL,autoBC9E
        ADD     A,L
        LD      L,A
        LD      A,(autoBD09)
        OR      A
        RET     M
        SUB     C
        RET     NC
        CP      (HL)
        RET     NC
        CALL    auto8003
        BIT     0,(HL)
        RET     NZ
        POP     HL
        CALL    auto821C
        JP      auto7E79
.auto80E2
        LD      IX,(autoBD1A)
        LD      (IX+$00),$00
        JP      auto80FA
.auto80ED
        CALL    auto8015
        BIT     0,E
        JP      NZ,auto7E25
        PUSH    DE
        POP     IX
        XOR     A
        LD      (DE),A
.auto80FA
        CALL    auto810F
        LD      A,(autoBCA9)
        DEC     A
        JP      Z,auto7E06
        ADD     A,A
        LD      HL,autoBC83
        ADD     A,L
        LD      L,A
        SET     7,(HL)
        JP      auto7E0A

;***************************************************************************
.auto810F
        POP     HL
        EXX
        LD      H,auto00BD
        LD      L,(IX+$01)
        LD      E,(HL)
        LD      D,L
        PUSH    DE
        LD      (HL),$00
        LD      L,(IX+$02)
        LD      C,(HL)
        LD      (HL),E
        LD      B,L
        INC     H
        RES     2,E
        LD      L,E
        LD      (HL),B
        INC     L
        LD      E,D
        LD      D,(HL)
        LD      A,$0F
        AND     D
        JP      Z,auto81BF
        DEC     A
        JR      Z,auto813D
        SET     4,(HL)
        DEC     C
        INC     C
        JR      NZ,auto81A5
.auto8138
        LD      E,$80
.auto813A
        PUSH    DE
        EXX
        JP      (HL)
.auto813D
        LD      A,B
        AND     'p'
        CP      E
        JR      NC,auto8145
        XOR     'p'
.auto8145
        SUB     '0'
        JR      C,auto8184
        JR      Z,auto8181
        SUB     $10
        JR      Z,auto817F
        SET     0,(IX+$03)
        SUB     ' '
        LD      A,'a'
        JR      C,auto8183
        LD      A,$91
        JR      Z,auto8183
        RES     0,(IX+$03)
        LD      (HL),$19
        LD      A,(autoBD0D)
        BIT     7,L
        JR      Z,auto816C
        ADD     A,$10
.auto816C
        SUB     $08
        LD      (autoBD0D),A
        LD      E,$84
        BIT     3,(IX+$03)
        JP      Z,auto813A
        LD      E,$04
        JP      auto81A7
.auto817F
        ADD     A,' '
.auto8181
        ADD     A,$11
.auto8183
        LD      (HL),A
.auto8184
        LD      A,(IX+$03)
        BIT     3,A
        JP      Z,auto8138
        BIT     5,A
        JP      Z,auto81A5
        LD      A,B
        CP      E
        JR      NC,auto8197
        ADD     A,' '
.auto8197
        SUB     $10
        DEC     H
        LD      L,A
        LD      C,(HL)
        LD      (HL),$00
        INC     H
        LD      B,L
        LD      E,$01
        JP      auto81A7
.auto81A5
        LD      E,$00
.auto81A7
        PUSH    BC
        RES     2,C
        LD      L,C
        SET     7,(HL)
        PUSH    DE
        INC     L
        LD      A,$0F
        AND     (HL)
        BIT     7,L
        JR      Z,auto81B8
        NEG
.auto81B8
        LD      HL,autoBD0D
        ADD     A,(HL)
        LD      (HL),A
        EXX
        JP      (HL)
.auto81BF
        CALL    auto8200
        LD      A,(IX+$03)
        AND     $18
        JP      Z,auto8138
        AND     $08
        JP      NZ,auto81A5
        SET     5,(HL)
        DEC     H
        BIT     2,B
        JR      Z,auto81E0
        INC     B
        LD      L,B
        LD      C,(HL)
        LD      (HL),$00
        DEC     L
        DEC     L
        JP      auto81E9
.auto81E0
        DEC     B
        DEC     B
        LD      L,B
        LD      C,(HL)
        LD      (HL),$00
        INC     L
        INC     L
        INC     L
.auto81E9
        LD      (HL),C
        LD      A,(IX+$00)
        INC     A
        JR      NZ,auto81F4
        LD      A,$84
        AND     C
        LD      (HL),A
.auto81F4
        PUSH    BC
        RES     2,C
        LD      B,auto00BE
        LD      A,L
        LD      (BC),A
        LD      E,'@'
        PUSH    DE
        EXX
        JP      (HL)
.auto8200
        LD      A,D
        OR      $10
.auto8203
        AND     '?'
        LD      E,$11
.auto8207
        LD      (HL),A
        LD      A,B
        ADD     A,E
        AND     $88
        RET     NZ
        LD      A,B
        SUB     E
        AND     $88
        RET     NZ
        LD      A,$11
        ADD     A,E
        LD      E,A
        LD      A,'@'
        ADD     A,(HL)
        JP      auto8207
.auto821C
        POP     HL
        EXX
        POP     AF
        POP     HL
        LD      C,A
        LD      A,$00
        LD      D,auto00BD
        CALL    PE,auto826E
        JP      M,auto8247
        JR      Z,auto825F
        JR      C,auto8258
        LD      B,L
.auto8230
        RES     2,L
        LD      H,auto00BE
        RES     7,(HL)
        INC     L
        LD      A,$0F
        AND     (HL)
        BIT     7,L
        JR      NZ,auto8240
        NEG
.auto8240
        LD      HL,autoBD0D
        ADD     A,(HL)
        LD      (HL),A
        LD      A,B
.auto8246
        POP     HL
.auto8247
        LD      E,H
        EX      DE,HL
        LD      (HL),E
        EX      DE,HL
        RES     2,L
        LD      H,auto00BE
        LD      B,(HL)
        LD      (HL),E
        INC     L
        LD      (HL),C
        LD      L,B
        DEC     H
        LD      (HL),A
        EXX
        JP      (HL)
.auto8258
        LD      E,H
        LD      B,A
        LD      A,L
        LD      (DE),A
        JP      auto8230
.auto825F
        LD      E,H
        LD      A,L
        LD      (DE),A
        RES     2,L
        LD      H,auto00BE
        LD      B,(HL)
        LD      (HL),E
        XOR     A
        LD      E,B
        LD      (DE),A
        JP      auto8246
.auto826E
        JP      P,auto8282
        EX      AF,AF'
        LD      A,(autoBD0D)
        BIT     7,L
        JR      Z,auto827B
.auto8279
        SUB     $10
.auto827B
        ADD     A,$08
        LD      (autoBD0D),A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8282
        EX      AF,AF'
        LD      A,(autoBD0D)
        BIT     7,L
        JR      NZ,auto827B
        JR      auto8279

.auto8400
        LD      (autoBD3D),SP
        XOR     A
        LD      E,A
        EX      AF,AF'
        LD      H,auto00BD
        EXX
        LD      H,auto00BE
        LD      A,(nextToPlay)
        RRCA
        LD      IY,auto8A7A
        BIT     1,A
        JR      Z,auto8424
        LD      IY,auto8A6F
        BIT     2,A
        JR      Z,auto8424
        LD      IY,auto8A2B
.auto8424
        ADD     A,$80
        LD      B,A
        EXX
        JP      C,auto883A
.auto842B
        LD      D,$88
        LD      A,(autoBE88)
        OR      A
        CALL    P,auto8C43
        LD      A,(autoBE90)
        OR      A
        CALL    P,auto8C4E
        LD      A,(autoBE98)
        OR      A
        CALL    P,auto8C4E
        LD      A,(autoBEA0)
        OR      A
        CALL    P,auto8D80
        LD      A,(autoBEA8)
        OR      A
        CALL    P,auto8D80
        LD      A,(autoBEB0)
        OR      A
        CALL    P,auto8CBB
        LD      A,(autoBEB8)
        OR      A
        CALL    P,auto8CBB
        LD      D,$F1
        LD      BC,$FFFF
        LD      HL,(autoBEC0)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBEC8)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBED0)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBED8)
        BIT     7,L
        CALL    Z,auto8A54
        LD      HL,(autoBEE0)
        BIT     7,L
        CALL    Z,auto8A54
        LD      HL,(autoBEE8)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBEF0)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBEF8)
        BIT     7,L
        CALL    Z,auto8A5B
        EXX
        BIT     4,B
        JP      Z,auto882C
.auto84A9
        BIT     6,B
        JR      NZ,auto84BA
        LD      L,(IX+$02)
        DEC     H
        LD      L,(HL)
        INC     H
        BIT     2,L
        JR      Z,auto84BA
        INC     L
        SET     1,(HL)
.auto84BA
        BIT     3,B
        JP      Z,auto84CB
        EXX
        LD      A,(autoBD6D)
        OR      A
        JR      NZ,auto84C8
        SRA     E
.auto84C8
        SRA     E
        EXX
.auto84CB
        BIT     5,B
        EXX
        CALL    NZ,auto8E74
        LD      HL,autoBD4A
        EX      AF,AF'
        SRA     A
        SRA     A
        ADD     A,E
        ADD     A,(HL)
        LD      E,A
        LD      HL,0
        LD      (autoBD39),HL
        LD      (autoBD3B),IX
        LD      HL,(autoBE8A)
        LD      A,H
        OR      L
        JR      Z,auto84F9
        LD      BC,(autoBE8C)
        LD      A,$09
        CALL    auto8AD6
        LD      (autoBE8A),HL
.auto84F9
        LD      (autoBE8C),HL
        LD      HL,(autoBE92)
        LD      A,H
        OR      L
        JR      Z,auto850F
        LD      BC,(autoBE94)
        LD      A,$05
        CALL    auto8AD6
        LD      (autoBE92),HL
.auto850F
        LD      (autoBE94),HL
        LD      HL,(autoBE9A)
        LD      A,H
        OR      L
        JR      Z,auto8525
        LD      BC,(autoBE9C)
        LD      A,$05
        CALL    auto8AD6
        LD      (autoBE9A),HL
.auto8525
        LD      (autoBE9C),HL
        LD      HL,(autoBEA2)
        LD      A,H
        OR      L
        JR      Z,auto853B
        LD      BC,(autoBEA4)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBEA2),HL
.auto853B
        LD      (autoBEA4),HL
        LD      HL,(autoBEAA)
        LD      A,H
        OR      L
        JR      Z,auto8551
        LD      BC,(autoBEAC)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBEAA),HL
.auto8551
        LD      (autoBEAC),HL
        LD      HL,(autoBEB2)
        LD      A,H
        OR      L
        JR      Z,auto8567
        LD      BC,(autoBEB4)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBEB2),HL
.auto8567
        LD      (autoBEB4),HL
        LD      HL,(autoBEBA)
        LD      A,H
        OR      L
        JR      Z,auto857D
        LD      BC,(autoBEBC)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBEBA),HL
.auto857D
        LD      (autoBEBC),HL
        LD      HL,(autoBEC2)
        LD      A,H
        OR      L
        JR      Z,auto8594
        LD      BC,(autoBEC4)
        LD      A,(autoBEC1)
        CALL    auto8AD4
        LD      (autoBEC2),HL
.auto8594
        LD      (autoBEC4),HL
        LD      HL,(autoBECA)
        LD      A,H
        OR      L
        JR      Z,auto85AB
        LD      BC,(autoBECC)
        LD      A,(autoBEC9)
        CALL    auto8AD4
        LD      (autoBECA),HL
.auto85AB
        LD      (autoBECC),HL
        LD      HL,(autoBED2)
        LD      A,H
        OR      L
        JR      Z,auto85C2
        LD      BC,(autoBED4)
        LD      A,(autoBED1)
        CALL    auto8AD4
        LD      (autoBED2),HL
.auto85C2
        LD      (autoBED4),HL
        LD      HL,(autoBEDA)
        LD      A,H
        OR      L
        JR      Z,auto85D9
        LD      BC,(autoBEDC)
        LD      A,(autoBED9)
        CALL    auto8AD4
        LD      (autoBEDA),HL
.auto85D9
        LD      (autoBEDC),HL
        LD      HL,(autoBEE2)
        LD      A,H
        OR      L
        JR      Z,auto85F0
        LD      BC,(autoBEE4)
        LD      A,(autoBEE1)
        CALL    auto8AD4
        LD      (autoBEE2),HL
.auto85F0
        LD      (autoBEE4),HL
        LD      HL,(autoBEEA)
        LD      A,H
        OR      L
        JR      Z,auto8607
        LD      BC,(autoBEEC)
        LD      A,(autoBEE9)
        CALL    auto8AD4
        LD      (autoBEEA),HL
.auto8607
        LD      (autoBEEC),HL
        LD      HL,(autoBEF2)
        LD      A,H
        OR      L
        JR      Z,auto861E
        LD      BC,(autoBEF4)
        LD      A,(autoBEF1)
        CALL    auto8AD4
        LD      (autoBEF2),HL
.auto861E
        LD      (autoBEF4),HL
        LD      HL,(autoBEFA)
        LD      A,H
        OR      L
        JR      Z,auto8635
        LD      BC,(autoBEFC)
        LD      A,(autoBEF9)
        CALL    auto8AD4
        LD      (autoBEFA),HL
.auto8635
        LD      (autoBEFC),HL
        XOR     A
        SUB     E
        LD      E,A
        LD      HL,(autoBD39)
        PUSH    HL
        LD      HL,0
        LD      (autoBD39),HL
        LD      HL,(autoBE0A)
        LD      A,H
        OR      L
        JR      Z,auto8658
        LD      BC,(autoBE0C)
        LD      A,$09
        CALL    auto8AD6
        LD      (autoBE0A),HL
.auto8658
        LD      (autoBE0C),HL
        LD      HL,(autoBE12)
        LD      A,H
        OR      L
        JR      Z,auto866E
        LD      BC,(autoBE14)
        LD      A,$05
        CALL    auto8AD6
        LD      (autoBE12),HL
.auto866E
        LD      (autoBE14),HL
        LD      HL,(autoBE1A)
        LD      A,H
        OR      L
        JR      Z,auto8684
        LD      BC,(autoBE1C)
        LD      A,$05
        CALL    auto8AD6
        LD      (autoBE1A),HL
.auto8684
        LD      (autoBE1C),HL
        LD      HL,(autoBE22)
        LD      A,H
        OR      L
        JR      Z,auto869A
        LD      BC,(autoBE24)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBE22),HL
.auto869A
        LD      (autoBE24),HL
        LD      HL,(autoBE2A)
        LD      A,H
        OR      L
        JR      Z,auto86B0
        LD      BC,(autoBE2C)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBE2A),HL
.auto86B0
        LD      (autoBE2C),HL
        LD      HL,(autoBE32)
        LD      A,H
        OR      L
        JR      Z,auto86C6
        LD      BC,(autoBE34)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBE32),HL
.auto86C6
        LD      (autoBE34),HL
        LD      HL,(autoBE3A)
        LD      A,H
        OR      L
        JR      Z,auto86DC
        LD      BC,(autoBE3C)
        LD      A,$03
        CALL    auto8AD6
        LD      (autoBE3A),HL
.auto86DC
        LD      (autoBE3C),HL
        LD      HL,(autoBE42)
        LD      A,H
        OR      L
        JR      Z,auto86F3
        LD      BC,(autoBE44)
        LD      A,(autoBE41)
        CALL    auto8AD4
        LD      (autoBE42),HL
.auto86F3
        LD      (autoBE44),HL
        LD      HL,(autoBE4A)
        LD      A,H
        OR      L
        JR      Z,auto870A
        LD      BC,(autoBE4C)
        LD      A,(autoBE49)
        CALL    auto8AD4
        LD      (autoBE4A),HL
.auto870A
        LD      (autoBE4C),HL
        LD      HL,(autoBE52)
        LD      A,H
        OR      L
        JR      Z,auto8721
        LD      BC,(autoBE54)
        LD      A,(autoBE51)
        CALL    auto8AD4
        LD      (autoBE52),HL
.auto8721
        LD      (autoBE54),HL
        LD      HL,(autoBE5A)
        LD      A,H
        OR      L
        JR      Z,auto8738
        LD      BC,(autoBE5C)
        LD      A,(autoBE59)
        CALL    auto8AD4
        LD      (autoBE5A),HL
.auto8738
        LD      (autoBE5C),HL
        LD      HL,(autoBE62)
        LD      A,H
        OR      L
        JR      Z,auto874F
        LD      BC,(autoBE64)
        LD      A,(autoBE61)
        CALL    auto8AD4
        LD      (autoBE62),HL
.auto874F
        LD      (autoBE64),HL
        LD      HL,(autoBE6A)
        LD      A,H
        OR      L
        JR      Z,auto8766
        LD      BC,(autoBE6C)
        LD      A,(autoBE69)
        CALL    auto8AD4
        LD      (autoBE6A),HL
.auto8766
        LD      (autoBE6C),HL
        LD      HL,(autoBE72)
        LD      A,H
        OR      L
        JR      Z,auto877D
        LD      BC,(autoBE74)
        LD      A,(autoBE71)
        CALL    auto8AD4
        LD      (autoBE72),HL
.auto877D
        LD      (autoBE74),HL
        LD      HL,(autoBE7A)
        LD      A,H
        OR      L
        JR      Z,auto8794
        LD      BC,(autoBE7C)
        LD      A,(autoBE79)
        CALL    auto8AD4
        LD      (autoBE7A),HL
.auto8794
        LD      (autoBE7C),HL
        LD      B,E
        LD      IX,(autoBD3B)
        POP     DE
        LD      HL,(autoBD39)
        LD      A,(nextToPlay)
        RRA
        JR      NC,auto87B0
        XOR     A
        SUB     B
        LD      B,A
        EX      DE,HL
        LD      A,(autoBD0D)
        JP      auto87B5
.auto87B0
        LD      A,(autoBD0D)
        NEG
.auto87B5
        SUB     L
        ADD     A,A
        DEC     L
        JP      M,auto87C3
        SET     1,(IX+$03)
        JR      Z,auto87C2
        DEC     A
.auto87C2
        SUB     H
.auto87C3
        DEC     E
        JP      M,auto87ED
        BIT     2,(IX+$03)
        JR      NZ,auto87E2
        INC     B
        INC     B
        DEC     D
        JP      M,auto87E2
        SET     6,(IX+$03)
        INC     B
        INC     B
        DEC     D
        JP      M,auto87ED
        INC     B
        INC     B
        JP      auto87ED
.auto87E2
        EX      AF,AF'
        LD      A,E
        INC     L
        CP      L
        JR      C,auto87EC
        SET     6,(IX+$03)
.auto87EC
        EX      AF,AF'
.auto87ED
        CP      $1C
        JR      C,auto87FD
        CP      $E4
        JP      NC,auto87FD
        RLA
        LD      A,$02
        RET     C
        LD      A,$FC
        RET

;***************************************************************************
.auto87FD
        ADD     A,A
        ADD     A,A
        LD      C,A
        LD      A,B
        SRA     A
        ADC     A,B
        SRA     A
        LD      D,A
        LD      HL,randomNo
        INC     (HL)
        LD      A,(moveNo)
        DEC     A
        LD      A,(HL)
        JR      NZ,auto8816
        AND     $03
        JR      auto8818
.auto8816
        AND     $01
.auto8818
        ADD     A,D
        CP      $0C
        JR      C,auto8828
        CP      $F4
        JR      NC,auto8828
        RLA
        LD      A,$0C
        JR      NC,auto8828
        LD      A,$F4
.auto8828
        ADD     A,C
        ADD     A,$80
        RET

;***************************************************************************
.auto882C
        LD      A,$90
        ADD     A,B
        LD      B,A
        EXX
        XOR     A
        SUB     E
        LD      E,A
        LD      H,auto00BD
        EX      AF,AF'
        NEG
        EX      AF,AF'
.auto883A
        LD      A,(autoBE08)
        LD      D,$88
        OR      A
        CALL    P,auto8C43
        LD      A,(autoBE10)
        OR      A
        CALL    P,auto8C4E
        LD      A,(autoBE18)
        OR      A
        CALL    P,auto8C4E
        LD      A,(autoBE20)
        OR      A
        CALL    P,auto8D74
        LD      A,(autoBE28)
        OR      A
        CALL    P,auto8D74
        LD      A,(autoBE30)
        OR      A
        CALL    P,auto8CC8
        LD      A,(autoBE38)
        OR      A
        CALL    P,auto8CC8
        LD      D,$11
        LD      BC,$FFFF
        LD      HL,(autoBE40)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBE48)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBE50)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBE58)
        BIT     7,L
        CALL    Z,auto8A54
        LD      HL,(autoBE60)
        BIT     7,L
        CALL    Z,auto8A54
        LD      HL,(autoBE68)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBE70)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBE78)
        BIT     7,L
        CALL    Z,auto8A5B
        LD      HL,(autoBE00)
        CALL    auto8978
        EX      AF,AF'
        NEG
        EX      AF,AF'
        XOR     A
        SUB     E
        LD      E,A
        LD      HL,(autoBE80)
        EXX
        SET     7,B
        CALL    auto897D
        EXX
        BIT     4,B
        JP      NZ,auto84A9
        SET     4,B
        EXX
        JP      auto842B
.auto88D4
        OR      A
        JP      NZ,auto896F
        LD      A,D
        OR      A
        JR      NZ,auto88DF
        SUB     $0A
        LD      D,A
.auto88DF
        LD      A,$07
        AND     L
        INC     A
        RRA
        CP      $02
        JR      Z,auto88F3
        LD      A,'p'
        AND     L
        CP      '0'
        JR      Z,auto88F3
        CP      '@'
        JR      NZ,auto88F9
.auto88F3
        EX      AF,AF'
        ADD     A,$10
        JP      auto8970
.auto88F9
        LD      A,$07
        AND     L
        CP      $02
        JR      Z,auto890F
        CP      $05
        JR      Z,auto890F
        LD      A,'p'
        AND     L
        CP      ' '
        JR      Z,auto890F
        CP      'P'
        JR      NZ,auto896F
.auto890F
        EX      AF,AF'
        ADD     A,$08
        JP      auto8970
.auto8915
        LD      A,(moveNo)
        CPL
        ADD     A,$0A
        EXX
        LD      C,A
        LD      A,(autoBE89)
        BIT     4,A
        JR      Z,auto892E
        LD      A,(autoBE88)
        OR      A
        JP      M,auto892E
        LD      A,E
        ADD     A,C
        LD      E,A
.auto892E
        LD      A,(autoBE09)
        BIT     4,A
        JR      Z,auto893F
        LD      A,(autoBE08)
        OR      A
        JP      M,auto893F
        LD      A,E
        SUB     C
        LD      E,A
.auto893F
        EXX
        JP      auto897D
.auto8943
        BIT     3,A
        JR      NZ,auto894F
        LD      H,auto00BD
        LD      B,L
        LD      C,$08
        JP      auto89AC
.auto894F
        LD      B,A
        LD      C,$04
        LD      A,$C0
        AND     H
        RRA
        RRA
        RRA
        LD      D,A
        LD      A,(autoBD1E)
        OR      A
        JR      Z,auto8966
        XOR     B
        LD      A,(autoBD6D)
        JP      P,auto88D4
.auto8966
        RR      D
        OR      A
        JR      NZ,auto896F
        BIT     7,H
        JR      NZ,auto890F
.auto896F
        EX      AF,AF'
.auto8970
        ADD     A,D
        EX      AF,AF'
        LD      H,auto00BD
        LD      B,L
        JP      auto89AC
.auto8978
        EXX
        BIT     0,B
        JR      Z,auto8915
.auto897D
        LD      A,B
        EXX
        BIT     2,A
        JR      NZ,auto8943
        BIT     5,H
        JP      Z,auto8A0D
        LD      A,$C0
        AND     H
        JR      NZ,auto89FB
        LD      C,$0A
        LD      B,L
        LD      H,auto00BD
        LD      A,$1F
        ADD     A,B
        JP      P,auto899A
        SUB     '@'
.auto899A
        LD      L,A
        BIT     6,(HL)
        JR      NZ,auto89AB
        INC     L
        BIT     6,(HL)
        JR      NZ,auto89AB
        INC     L
        BIT     6,(HL)
        JR      NZ,auto89AB
        DEC     E
        DEC     E
.auto89AB
        LD      L,B
.auto89AC
        DEC     L
        XOR     A
        OR      (HL)
        CALL    NZ,auto89E2
        INC     L
        INC     L
        OR      (HL)
        CALL    NZ,auto89E2
        LD      A,$10
        ADD     A,B
        JP      M,auto89CF
        LD      L,A
        XOR     A
        OR      (HL)
        CALL    NZ,auto89E2
        INC     L
        OR      (HL)
        CALL    NZ,auto89E2
        DEC     L
        DEC     L
        OR      (HL)
        CALL    NZ,auto89E2
.auto89CF
        LD      A,$F0
        ADD     A,B
        RET     M
        LD      L,A
        XOR     A
        OR      (HL)
        CALL    NZ,auto89E2
        INC     L
        OR      (HL)
        CALL    NZ,auto89E2
        DEC     L
        DEC     L
        OR      (HL)
        RET     Z
.auto89E2
        EXX
        LD      L,A
        INC     L
        XOR     B
        JP      P,auto89F1
        LD      A,'x'
        AND     L
        JP      Z,auto8C28
        DEC     L
        DEC     L
.auto89F1
        INC     (HL)
        XOR     A
        BIT     6,L
        EXX
        RET     Z
        EX      AF,AF'
        ADD     A,C
        EX      AF,AF'
        RET

;***************************************************************************
.auto89FB
        LD      C,$04
        BIT     7,H
        JP      Z,auto8A04
        DEC     E
        DEC     E
.auto8A04
        DEC     E
        DEC     E
        DEC     E
        LD      B,L
        LD      H,auto00BD
        JP      auto89AC
.auto8A0D
        BIT     4,H
        JR      NZ,auto8A1A
        LD      H,auto00BD
        LD      B,L
        LD      C,$00
        INC     E
        JP      auto89AC
.auto8A1A
        LD      A,E
        SUB     $06
        LD      E,A
        LD      C,$08
        LD      A,$C0
        AND     H
        JR      NZ,auto89FB
        LD      H,auto00BD
        LD      B,L
        JP      auto89AC
.auto8A2B
        SRL     H
        SRL     H
        EX      AF,AF'
        ADD     A,H
        EX      AF,AF'
        BIT     5,H
        JP      Z,auto8A7A
        EXX
        BIT     4,B
        EXX
        JR      NZ,auto8A7A
        LD      H,auto00BD
        DEC     L
        LD      A,(HL)
        INC     L
        LD      (autoBD4E),A
        JP      auto8A7C
.auto8A48
        DEC     E
        DEC     E
        DEC     E
        JP      auto8A6B
.auto8A4E
        DEC     E
        DEC     E
        DEC     E
        JP      auto8A5B
.auto8A54
        LD      A,L
        ADD     A,' '
        BIT     6,A
        JR      Z,auto8A4E
.auto8A5B
        LD      A,$0E
        AND     H
        JR      NZ,auto8A96
        LD      A,D
        ADD     A,L
        LD      L,A
        AND     $0F
        CP      B
        JR      Z,auto8A48
        CP      C
        JR      Z,auto8A48
.auto8A6B
        LD      C,B
        LD      B,A
        JP      (IY)
.auto8A6F
        SRL     H
        JR      Z,auto8A7A
        SRL     H
        SRL     H
        EX      AF,AF'
        ADD     A,H
        EX      AF,AF'
.auto8A7A
        LD      H,auto00BD
.auto8A7C
        LD      A,(HL)
        OR      A
        CALL    NZ,auto8A85
        DEC     L
        DEC     L
        OR      (HL)
        RET     Z
.auto8A85
        EXX
        LD      L,A
        XOR     B
        JP      P,auto8A92
        LD      A,'x'
        AND     L
        JR      Z,auto8ABA
        DEC     L
        DEC     L
.auto8A92
        INC     (HL)
        EXX
        XOR     A
        RET

;***************************************************************************
.auto8A96
        PUSH    BC
        PUSH    DE
        LD      BC,auto8AB5
        PUSH    BC
        LD      C,H
        LD      H,auto00BD
        LD      D,$88
        LD      A,L
        BIT     3,C
        JP      NZ,auto8C43
        BIT     2,C
        JP      NZ,auto8C4E
        BIT     7,C
        LD      B,A
        JP      NZ,auto8D86
        JP      auto8CCD
.auto8AB5
        LD      L,E
        POP     DE
        LD      E,L
        POP     BC
        RET

;***************************************************************************
.auto8ABA
        BIT     4,B
        JP      Z,auto8C28
        SET     7,(IX+$03)
        EXX
        INC     E
        RET

;***************************************************************************
.auto8AC6
        BIT     0,H
        JR      NZ,auto8ACD
        LD      H,$00
        RET

;***************************************************************************
.auto8ACD
        LD      A,H
        CALL    auto8B6A
        JP      auto8AE0
.auto8AD4
        AND     $0F
.auto8AD6
        EXX
        LD      D,A
        EXX
        LD      A,L
        OR      A
        JR      Z,auto8AC6
        CALL    auto8B50
.auto8AE0
        CALL    auto8BB9
        LD      E,$FF
        LD      A,D
.auto8AE6
        LD      B,$00
        BIT     0,H
        JR      Z,auto8B1A
        SUB     L
        EXX
        DEC     D
        EXX
        CALL    Z,auto8B8C
        CP      E
        JP      M,auto8B12
        CP      D
        JP      P,auto8B11
        LD      E,A
.auto8AFC
        BIT     0,L
        JR      Z,auto8B1A
        ADD     A,H
        CP      E
        JP      M,auto8B17
        CP      D
        JP      P,auto8B16
        LD      D,A
.auto8B0A
        DEC     C
        CALL    Z,auto8BFA
        JP      auto8AE6
.auto8B11
        INC     B
.auto8B12
        INC     B
        JP      auto8AFC
.auto8B16
        DEC     B
.auto8B17
        DEC     B
        JR      NZ,auto8B0A
.auto8B1A
        CP      E
        JP      P,auto8B1F
        LD      A,E
.auto8B1F
        CP      D
        JP      M,auto8B24
        LD      A,D
.auto8B24
        OR      A
        EXX
        JP      M,auto8B3E
        JR      Z,auto8B39
        LD      HL,autoBD39
        CP      (HL)
        JR      C,auto8B34
        LD      C,A
        LD      A,(HL)
        LD      (HL),C
.auto8B34
        INC     L
        CP      (HL)
        JR      C,auto8B39
        LD      (HL),A
.auto8B39
        DEC     E
        BIT     1,B
        JR      NZ,auto8B42
.auto8B3E
        LD      HL,0
        RET

;***************************************************************************
.auto8B42
        LD      IX,(autoBD3B)
        SET     2,(IX+$03)
        LD      HL,0
        DEC     E
        DEC     E
        RET

;***************************************************************************
.auto8B50
        AND     $03
        CALL    NZ,auto8B84
        LD      A,$0C
        AND     L
        CALL    NZ,auto8B91
        LD      A,'0'
        AND     L
        CALL    NZ,auto8B9B
        LD      A,$C0
        AND     L
        CALL    NZ,auto8BA7
        OR      H
        JR      Z,auto8B80
.auto8B6A
        AND     $0C
        CALL    NZ,auto8B91
        LD      A,'0'
        AND     H
        CALL    NZ,auto8B9B
        LD      A,$C0
        AND     H
        CALL    NZ,auto8BA7
        INC     A
        AND     H
        CALL    NZ,auto8BB1
.auto8B80
        EXX
        LD      L,A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8B84
        LD      D,A
        POP     IY
        EXX
        LD      L,$01
        EX      AF,AF'
        RET

;***************************************************************************
.auto8B8C
        EX      AF,AF'
        EXX
        XOR     A
        JP      (IY)

;***************************************************************************
.auto8B91
        RRA
        RRA
        LD      D,A
        POP     IY
        EXX
        LD      L,$03
        EX      AF,AF'
        RET

;***************************************************************************
.auto8B9B
        RRA
        RRA
        RRA
        RRA
        LD      D,A
        POP     IY
        EXX
        LD      L,$05
        EX      AF,AF'
        RET

;***************************************************************************
.auto8BA7
        RLCA
        RLCA
        LD      D,A
        POP     IY
        EXX
        LD      L,$09
        EX      AF,AF'
        RET

;***************************************************************************
.auto8BB1
        LD      D,A
        POP     IY
        EXX
        LD      L,$0D
        EX      AF,AF'
        RET

;***************************************************************************
.auto8BB9
        EXX
        LD      A,C
        OR      A
        JR      Z,auto8BD5
        AND     $03
        CALL    NZ,auto8BF2
        LD      A,$0C
        AND     C
        CALL    NZ,auto8BFF
        LD      A,'0'
        AND     C
        CALL    NZ,auto8C09
        LD      A,$C0
        AND     C
        CALL    NZ,auto8C15
.auto8BD5
        OR      B
        JR      Z,auto8BEE
        AND     $0C
        CALL    NZ,auto8BFF
        LD      A,'0'
        AND     B
        CALL    NZ,auto8C09
        LD      A,$C0
        AND     B
        CALL    NZ,auto8C15
        INC     A
        AND     B
        CALL    NZ,auto8C1F
.auto8BEE
        EXX
        LD      H,A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8BF2
        EXX
        LD      C,A
        LD      H,$01
        POP     IX
        EX      AF,AF'
        RET

;***************************************************************************
.auto8BFA
        EX      AF,AF'
        EXX
        XOR     A
        JP      (IX)
.auto8BFF
        RRA
        RRA
        POP     IX
        EXX
        LD      H,$03
        LD      C,A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8C09
        RRA
        RRA
        RRA
        RRA
        POP     IX
        EXX
        LD      H,$05
        LD      C,A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8C15
        RLCA
        RLCA
        POP     IX
        EXX
        LD      H,$09
        LD      C,A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8C1F
        DEC     E
        POP     IX
        EXX
        LD      H,$0D
        LD      C,A
        EX      AF,AF'
        RET

;***************************************************************************
.auto8C28
        XOR     A
        LD      (autoBD79),A
        LD      B,$1F
        LD      DE,$0005
        LD      HL,autoBE0A
.auto8C34
        LD      (HL),A
        INC     L
        LD      (HL),A
        INC     L
        LD      (HL),A
        INC     L
        LD      (HL),A
        ADD     HL,DE
        DJNZ    auto8C34
        LD      SP,(autoBD3D)
        RET

;***************************************************************************
.auto8C43
        LD      B,A
        EXX
        LD      C,'@'
        CALL    auto8C52
        EXX
        JP      auto8D89
.auto8C4E
        LD      B,A
        EXX
        LD      C,$10
.auto8C52
        LD      E,$08
        EXX
        LD      L,B
        XOR     A
.auto8C57
        INC     L
        BIT     3,L
        JR      NZ,auto8C65
        OR      (HL)
        JR      Z,auto8C57
        LD      C,$01
        CALL    auto8C98
        XOR     A
.auto8C65
        LD      L,B
.auto8C66
        DEC     L
        BIT     3,L
        JR      NZ,auto8C73
        OR      (HL)
        JR      Z,auto8C66
        LD      C,$FF
        CALL    auto8C98
.auto8C73
        LD      C,$10
        LD      A,B
        ADD     A,C
        CALL    P,auto8C80
        LD      C,$F0
        LD      L,B
.auto8C7D
        LD      A,L
        ADD     A,C
        RET     M
.auto8C80
        LD      L,A
        INC     E
        XOR     A
        OR      (HL)
        JR      Z,auto8C7D
        EXX
        LD      L,A
        XOR     B
        JP      M,auto8DB9
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        SET     1,L
        LD      A,(HL)
        AND     E
        EXX
        RET     Z
        JP      auto8DF8
.auto8C98
        EXX
        LD      L,A
        XOR     B
        JP      M,auto8DB9
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        SET     1,L
        LD      A,(HL)
        AND     E
        EXX
        RET     Z
.auto8CA7
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,auto8CA7
        EXX
        LD      L,A
        XOR     B
        JP      M,auto8E07
        INC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

;***************************************************************************
.auto8CBB
        LD      B,A
        OR      'p'
        CP      B
        JP      NZ,auto8CCD
.auto8CC2
        DEC     E
        DEC     E
        DEC     E
        JP      auto8CCD
.auto8CC8
        LD      B,A
        AND     'p'
        JR      Z,auto8CC2
.auto8CCD
        EXX
        LD      C,$04
        EXX
        DEC     E
.auto8CD2
        LD      A,$0E
        ADD     A,B
        JP      M,auto8CF5
        LD      L,A
        AND     D
        CALL    Z,auto8D15
        LD      A,$12
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,auto8D15
        LD      A,$1F
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,auto8D15
        LD      A,'!'
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,auto8D15
.auto8CF5
        LD      A,$F2
        ADD     A,B
        RET     M
        LD      L,A
        AND     D
        CALL    Z,auto8D15
        LD      A,$EE
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,auto8D15
        LD      A,$E1
        ADD     A,B
        RET     M
        LD      L,A
        AND     D
        CALL    Z,auto8D15
        LD      A,$DF
        ADD     A,B
        LD      L,A
        AND     D
        RET     NZ
.auto8D15
        INC     E
        OR      (HL)
        RET     Z
        EXX
        LD      L,A
        XOR     B
        JP      P,auto8D25
        LD      A,'x'
        AND     L
        JR      Z,auto8D2A
        DEC     L
        DEC     L
.auto8D25
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

;***************************************************************************
.auto8D2A
        BIT     4,B
        JP      Z,auto8C28
        SET     7,(IX+$03)
        BIT     6,B
        SET     6,B
        EXX
        JR      NZ,auto8D44
        LD      L,B                             ; !! interesting lookup stuff here...
        LD      L,(HL)
        INC     L
        INC     H
        SET     1,(HL)
        DEC     H
        INC     E
        INC     E
        RET

;***************************************************************************
.auto8D44
        LD      (autoBD4E),A
        EX      AF,AF'
        ADD     A,$14
        EX      AF,AF'
        RET

;***************************************************************************
.auto8D4C
        BIT     4,B
        JP      Z,auto8C28
        SET     7,(IX+$03)
        BIT     6,B
        SET     6,B
        EXX
        JR      NZ,auto8D6A
        LD      A,L
        LD      L,B
        LD      L,(HL)
        INC     L
        INC     H
        SET     1,(HL)
        DEC     H
        LD      L,A
        INC     E
        INC     E
        JP      auto8DAA
.auto8D6A
        LD      (autoBD4E),A
        EX      AF,AF'
        ADD     A,$14
        EX      AF,AF'
        JP      auto8DAA
.auto8D74
        LD      B,A
        AND     'p'
        JP      NZ,auto8D86
.auto8D7A
        DEC     E
        DEC     E
        DEC     E
        JP      auto8D86
.auto8D80
        LD      B,A
        OR      'p'
        CP      B
        JR      Z,auto8D7A
.auto8D86
        EXX
        LD      C,$04
.auto8D89
        LD      E,$C4
        EXX
        LD      C,$0F
        LD      A,B
        ADD     A,C
        LD      L,A
        AND     D
        CALL    Z,auto8DAF
        LD      C,$11
        LD      A,B
        ADD     A,C
        LD      L,A
        AND     D
        CALL    Z,auto8DAF
        LD      C,$EF
        LD      A,B
        ADD     A,C
        LD      L,A
        AND     D
        CALL    Z,auto8DAF
        LD      C,$F1
        LD      L,B
.auto8DAA
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
.auto8DAF
        INC     E
        OR      (HL)
        JR      Z,auto8DAA
        EXX
        LD      L,A
        XOR     B
        JP      P,auto8DEC
.auto8DB9
        LD      A,'x'
        AND     L
        JR      Z,auto8D4C
        DEC     L
        DEC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        SET     2,L
        LD      A,(HL)
        AND     E
        JR      Z,auto8E19
        JP      M,auto8E19
        EXX
.auto8DCD
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,auto8DCD
        EXX
        LD      D,L
        LD      L,A
        XOR     B
        JP      P,auto8E15
        DEC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        INC     L
.auto8DE1
        INC     L
        INC     L
        LD      A,(HL)
        RRA
        JR      C,auto8E2F
        RRA
        JR      C,auto8E2B
        EXX
        RET

;***************************************************************************
.auto8DEC
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
.auto8DEF
        SET     1,L
        LD      A,(HL)
        AND     E
        EXX
        RET     Z
        JP      M,auto8E5C
.auto8DF8
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        INC     E
        OR      (HL)
        JR      Z,auto8DF8
        EXX
        LD      L,A
        XOR     B
        JP      P,auto8E0D
.auto8E07
        DEC     L
.auto8E08
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

;***************************************************************************
.auto8E0D
        INC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        DEC     L
        JP      auto8DEF
.auto8E15
        INC     L
        JP      auto8E08
.auto8E19
        EXX
.auto8E1A
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,auto8E1A
        EXX
        LD      D,L
        LD      L,A
        XOR     B
        JP      M,auto8DE1
        EXX
        RET

;***************************************************************************
.auto8E2B
        BIT     6,C
        JR      NZ,auto8E58
.auto8E2F
        RES     1,D
        RES     2,D
        LD      HL,autoBD79
.auto8E36
        LD      A,(HL)
        OR      A
        JR      Z,auto8E47
        INC     L
        INC     L
        CP      D
        JP      NZ,auto8E36
        DEC     L
        LD      (HL),$00
        INC     H
        EXX
        INC     E
        RET

;***************************************************************************
.auto8E47
        LD      A,L
        CP      $84
        JR      NC,auto8E57
        LD      (HL),D
        INC     L
        EXX
        LD      A,C
        EXX
        LD      (HL),A
        INC     L
        LD      (HL),$00
        SET     5,B
.auto8E57
        INC     H
.auto8E58
        EXX
        INC     E
        INC     E
        RET

;***************************************************************************
.auto8E5C
        RLA
        XOR     C
        RET     P
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        RET     Z
        EXX
        LD      L,A
        INC     L
        XOR     B
        JP      P,auto8E6F
        DEC     L
        DEC     L
.auto8E6F
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

;***************************************************************************
.auto8E74
        LD      IY,autoBD79
        PUSH    DE
        LD      H,auto00BD
        EXX
        LD      L,(IY+$00)
.auto8E7F
        LD      BC,auto8F1B
        PUSH    BC
        LD      A,L
        AND     $80
        LD      B,A
        LD      E,(HL)
        INC     L
        LD      A,(HL)
        AND     $8E
        JR      Z,auto8E9F
        JP      M,auto8EE4
        CP      $04
        JR      C,auto8EC9
        JR      Z,auto8ED5
        LD      C,$C0
        CALL    auto8ED7
        JP      auto8EED
.auto8E9F
        BIT     7,B
        LD      A,E
        EXX
        LD      B,A
        LD      C,$0F
        JR      Z,auto8EAA
        LD      C,$EF
.auto8EAA
        CALL    auto8EAF
        INC     C
        INC     C
.auto8EAF
        LD      A,(IY+$01)
        CP      C
        RET     Z
        NEG
        CP      C
        RET     Z
        LD      A,C
        ADD     A,B
        LD      L,A
        XOR     A
        OR      (HL)
        RET     Z
        EXX
        LD      L,A
        XOR     B
        JP      P,auto8EC6
        DEC     L
        DEC     L
.auto8EC6
        DEC     (HL)
        EXX
        RET

;***************************************************************************
.auto8EC9
        LD      C,$FC
        SET     4,B
        LD      A,E
        EXX
        LD      D,$88
        LD      B,A
        JP      auto8CD2
.auto8ED5
        LD      C,$F0
.auto8ED7
        LD      A,E
        EXX
        LD      C,$01
        LD      B,A
        CALL    auto8EF4
        LD      C,$10
        JP      auto8EF4
.auto8EE4
        AND     $02
        JR      Z,auto8E9F
        LD      C,$FC
        LD      A,E
        EXX
        LD      B,A
.auto8EED
        LD      C,$0F
        CALL    auto8EF4
        LD      C,$11
.auto8EF4
        LD      A,(IY+$01)
        CP      C
        RET     Z
        NEG
        CP      C
        RET     Z
        LD      D,$88
        CALL    auto8F05
        XOR     A
        SUB     C
        LD      C,A
.auto8F05
        LD      L,B
.auto8F06
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,auto8F06
        EXX
        LD      L,A
        XOR     B
        JP      P,auto8F16
        DEC     L
        DEC     L
.auto8F16
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

;***************************************************************************
.auto8F1B
        LD      A,(IY+$02)
        OR      A
        JR      Z,auto8F2A
        INC     IY
        INC     IY
        EXX
        LD      L,A
        JP      auto8E7F
.auto8F2A
        POP     DE
        LD      (autoBD79),A
        RET
