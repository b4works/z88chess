;Disassembly of Cyrus IS Chess for the ZXSpectrum 48K

        MODULE  CYRUS

        ORG     $6000

        DEFC    GAME_DATA = $C000               ;Default $C000
        DEFC    WORKSPACE = $B000               ;Default $B000
        DEFC    THINKAREA = WORKSPACE+$0400     ;Default $B400
        DEFC    LOADSPACE = $E000               ;Default $E000

;
;       External ref equates
;

        DEFC    AUTO00BC = WORKSPACE/256+$0C
        DEFC    AUTO00BD = WORKSPACE/256+$0D
        DEFC    AUTO00BE = WORKSPACE/256+$0E
        DEFC    AUTO00BF = WORKSPACE/256+$0F
        DEFC    AUTOB400 = THINKAREA+$0000      ;Thinking workspace
        DEFC    AUTOB401 = THINKAREA+$0001
        DEFC    AUTOB403 = THINKAREA+$0003
        DEFC    AUTOB404 = THINKAREA+$0004
        DEFC    AUTOBC83 = WORKSPACE+$0C83
        DEFC    AUTOBC91 = WORKSPACE+$0C91
        DEFC    AUTOBC9E = WORKSPACE+$0C9E
        DEFC    AUTOBC9F = WORKSPACE+$0C9F
        DEFC    AUTOBCA0 = WORKSPACE+$0CA0
        DEFC    AUTOBCA2 = WORKSPACE+$0CA2
        DEFC    AUTOBCA3 = WORKSPACE+$0CA3
        DEFC    AUTOBCA9 = WORKSPACE+$0CA9
        DEFC    AUTOBCAA = WORKSPACE+$0CAA
        DEFC    AUTOBCAB = WORKSPACE+$0CAB      ;Thinking workspace pointer
        DEFC    AUTOBCB2 = WORKSPACE+$0CB2
        DEFC    AUTOBCB3 = WORKSPACE+$0CB3
        DEFC    AUTOBCB6 = WORKSPACE+$0CB6
        DEFC    AUTOBCBD = WORKSPACE+$0CBD
        DEFC    AUTOBCBE = WORKSPACE+$0CBE      ;0=White's turn !0 =Black's turn
        DEFC    AUTOBCBF = WORKSPACE+$0CBF      ;<>0 Game finished
        DEFC    AUTOBCC0 = WORKSPACE+$0CC0
        DEFC    AUTOBCC1 = WORKSPACE+$0CC1      ;Character to be printed
        DEFC    AUTOBCC2 = WORKSPACE+$0CC2      ;Bit 0=1, Comp plays Black, Bit 1=1 Comp plays W
        DEFC    AUTOBCC3 = WORKSPACE+$0CC3      ;Play this move?
        DEFC    AUTOBCC4 = WORKSPACE+$0CC4      ;Scn [Sq/Pc] attribute
        DEFC    AUTOBCC5 = WORKSPACE+$0CC5      ;Scn y-coord
        DEFC    AUTOBCC6 = WORKSPACE+$0CC6      ;Scn x-coord
        DEFC    AUTOBCC7 = WORKSPACE+$0CC7      ;Board orientation
        DEFC    AUTOBCC8 = WORKSPACE+$0CC8      ;ZXSpectrum IY value
        DEFC    AUTOBCCA = WORKSPACE+$0CCA      ;Cursor position
        DEFC    AUTOBCCB = WORKSPACE+$0CCB
        DEFC    AUTOBCCC = WORKSPACE+$0CCC
        DEFC    AUTOBCCD = WORKSPACE+$0CCD      ;Computer's playing level
        DEFC    AUTOBCCE = WORKSPACE+$0CCE
        DEFC    AUTOBCCF = WORKSPACE+$0CCF
        DEFC    AUTOBCD1 = WORKSPACE+$0CD1      ;Beep parameter 1
        DEFC    AUTOBCD2 = WORKSPACE+$0CD2      ;Beep parameter 2
        DEFC    AUTOBCD3 = WORKSPACE+$0CD3      ;Sound, 0=on, 1=off
        DEFC    AUTOBCD4 = WORKSPACE+$0CD4      ;Print listing on (0) or off(1)?
        DEFC    AUTOBCD5 = WORKSPACE+$0CD5      ;ZXSpectrum CHARS pointer
        DEFC    AUTOBCD7 = WORKSPACE+$0CD7      ;BPc colour
        DEFC    AUTOBCD8 = WORKSPACE+$0CD8      ;WPc colour
        DEFC    AUTOBCD9 = WORKSPACE+$0CD9      ;BSq colour
        DEFC    AUTOBCDA = WORKSPACE+$0CDA      ;WSq colour
        DEFC    AUTOBCDB = WORKSPACE+$0CDB
        DEFC    AUTOBCDC = WORKSPACE+$0CDC      ;ZXPrinter buffer x-coord
        DEFC    AUTOBCDD = WORKSPACE+$0CDD
        DEFC    AUTOBCDE = WORKSPACE+$0CDE      ;Last promoted piece
        DEFC    AUTOBCDF = WORKSPACE+$0CDF
        DEFC    AUTOBD00 = WORKSPACE+$0D00
        DEFC    AUTOBD09 = WORKSPACE+$0D09
        DEFC    AUTOBD0A = WORKSPACE+$0D0A
        DEFC    AUTOBD0B = WORKSPACE+$0D0B
        DEFC    AUTOBD0C = WORKSPACE+$0D0C
        DEFC    AUTOBD0D = WORKSPACE+$0D0D
        DEFC    AUTOBD0E = WORKSPACE+$0D0E
        DEFC    AUTOBD19 = WORKSPACE+$0D19
        DEFC    AUTOBD1A = WORKSPACE+$0D1A
        DEFC    AUTOBD1E = WORKSPACE+$0D1E
        DEFC    AUTOBD29 = WORKSPACE+$0D29
        DEFC    AUTOBD2B = WORKSPACE+$0D2B
        DEFC    AUTOBD2D = WORKSPACE+$0D2D      ;0=White's turn, 1=Black's turn
        DEFC    AUTOBD2E = WORKSPACE+$0D2E
        DEFC    AUTOBD39 = WORKSPACE+$0D39
        DEFC    AUTOBD3B = WORKSPACE+$0D3B
        DEFC    AUTOBD3D = WORKSPACE+$0D3D
        DEFC    AUTOBD49 = WORKSPACE+$0D49      ;Move number
        DEFC    AUTOBD4A = WORKSPACE+$0D4A
        DEFC    AUTOBD4B = WORKSPACE+$0D4B
        DEFC    AUTOBD4C = WORKSPACE+$0D4C
        DEFC    AUTOBD4D = WORKSPACE+$0D4D
        DEFC    AUTOBD4E = WORKSPACE+$0D4E
        DEFC    AUTOBD58 = WORKSPACE+$0D58
        DEFC    AUTOBD59 = WORKSPACE+$0D59
        DEFC    AUTOBD5A = WORKSPACE+$0D5A
        DEFC    AUTOBD5B = WORKSPACE+$0D5B
        DEFC    AUTOBD5C = WORKSPACE+$0D5C
        DEFC    AUTOBD5E = WORKSPACE+$0D5E
        DEFC    AUTOBD6D = WORKSPACE+$0D6D
        DEFC    AUTOBD6E = WORKSPACE+$0D6E
        DEFC    AUTOBD79 = WORKSPACE+$0D79
        DEFC    AUTOBDF4 = WORKSPACE+$0DF4      ;Initial stack pointer
        DEFC    AUTOBDF6 = WORKSPACE+$0DF6
        DEFC    AUTOBE00 = WORKSPACE+$0E00
        DEFC    AUTOBE08 = WORKSPACE+$0E08
        DEFC    AUTOBE09 = WORKSPACE+$0E09
        DEFC    AUTOBE0A = WORKSPACE+$0E0A
        DEFC    AUTOBE0C = WORKSPACE+$0E0C
        DEFC    AUTOBE10 = WORKSPACE+$0E10
        DEFC    AUTOBE12 = WORKSPACE+$0E12
        DEFC    AUTOBE14 = WORKSPACE+$0E14
        DEFC    AUTOBE18 = WORKSPACE+$0E18
        DEFC    AUTOBE1A = WORKSPACE+$0E1A
        DEFC    AUTOBE1C = WORKSPACE+$0E1C
        DEFC    AUTOBE20 = WORKSPACE+$0E20
        DEFC    AUTOBE22 = WORKSPACE+$0E22
        DEFC    AUTOBE24 = WORKSPACE+$0E24
        DEFC    AUTOBE28 = WORKSPACE+$0E28
        DEFC    AUTOBE2A = WORKSPACE+$0E2A
        DEFC    AUTOBE2C = WORKSPACE+$0E2C
        DEFC    AUTOBE30 = WORKSPACE+$0E30
        DEFC    AUTOBE32 = WORKSPACE+$0E32
        DEFC    AUTOBE34 = WORKSPACE+$0E34
        DEFC    AUTOBE38 = WORKSPACE+$0E38
        DEFC    AUTOBE3A = WORKSPACE+$0E3A
        DEFC    AUTOBE3C = WORKSPACE+$0E3C
        DEFC    AUTOBE40 = WORKSPACE+$0E40
        DEFC    AUTOBE41 = WORKSPACE+$0E41
        DEFC    AUTOBE42 = WORKSPACE+$0E42
        DEFC    AUTOBE44 = WORKSPACE+$0E44
        DEFC    AUTOBE48 = WORKSPACE+$0E48
        DEFC    AUTOBE49 = WORKSPACE+$0E49
        DEFC    AUTOBE4A = WORKSPACE+$0E4A
        DEFC    AUTOBE4C = WORKSPACE+$0E4C
        DEFC    AUTOBE50 = WORKSPACE+$0E50
        DEFC    AUTOBE51 = WORKSPACE+$0E51
        DEFC    AUTOBE52 = WORKSPACE+$0E52
        DEFC    AUTOBE54 = WORKSPACE+$0E54
        DEFC    AUTOBE58 = WORKSPACE+$0E58
        DEFC    AUTOBE59 = WORKSPACE+$0E59
        DEFC    AUTOBE5A = WORKSPACE+$0E5A
        DEFC    AUTOBE5C = WORKSPACE+$0E5C
        DEFC    AUTOBE60 = WORKSPACE+$0E60
        DEFC    AUTOBE61 = WORKSPACE+$0E61
        DEFC    AUTOBE62 = WORKSPACE+$0E62
        DEFC    AUTOBE64 = WORKSPACE+$0E64
        DEFC    AUTOBE68 = WORKSPACE+$0E68
        DEFC    AUTOBE69 = WORKSPACE+$0E69
        DEFC    AUTOBE6A = WORKSPACE+$0E6A
        DEFC    AUTOBE6C = WORKSPACE+$0E6C
        DEFC    AUTOBE70 = WORKSPACE+$0E70
        DEFC    AUTOBE71 = WORKSPACE+$0E71
        DEFC    AUTOBE72 = WORKSPACE+$0E72
        DEFC    AUTOBE74 = WORKSPACE+$0E74
        DEFC    AUTOBE78 = WORKSPACE+$0E78
        DEFC    AUTOBE79 = WORKSPACE+$0E79
        DEFC    AUTOBE7A = WORKSPACE+$0E7A
        DEFC    AUTOBE7C = WORKSPACE+$0E7C
        DEFC    AUTOBE80 = WORKSPACE+$0E80
        DEFC    AUTOBE88 = WORKSPACE+$0E88
        DEFC    AUTOBE89 = WORKSPACE+$0E89
        DEFC    AUTOBE8A = WORKSPACE+$0E8A
        DEFC    AUTOBE8C = WORKSPACE+$0E8C
        DEFC    AUTOBE90 = WORKSPACE+$0E90
        DEFC    AUTOBE92 = WORKSPACE+$0E92
        DEFC    AUTOBE94 = WORKSPACE+$0E94
        DEFC    AUTOBE98 = WORKSPACE+$0E98
        DEFC    AUTOBE9A = WORKSPACE+$0E9A
        DEFC    AUTOBE9C = WORKSPACE+$0E9C
        DEFC    AUTOBEA0 = WORKSPACE+$0EA0
        DEFC    AUTOBEA2 = WORKSPACE+$0EA2
        DEFC    AUTOBEA4 = WORKSPACE+$0EA4
        DEFC    AUTOBEA8 = WORKSPACE+$0EA8
        DEFC    AUTOBEAA = WORKSPACE+$0EAA
        DEFC    AUTOBEAC = WORKSPACE+$0EAC
        DEFC    AUTOBEB0 = WORKSPACE+$0EB0
        DEFC    AUTOBEB2 = WORKSPACE+$0EB2
        DEFC    AUTOBEB4 = WORKSPACE+$0EB4
        DEFC    AUTOBEB8 = WORKSPACE+$0EB8
        DEFC    AUTOBEBA = WORKSPACE+$0EBA
        DEFC    AUTOBEBC = WORKSPACE+$0EBC
        DEFC    AUTOBEC0 = WORKSPACE+$0EC0
        DEFC    AUTOBEC1 = WORKSPACE+$0EC1
        DEFC    AUTOBEC2 = WORKSPACE+$0EC2
        DEFC    AUTOBEC4 = WORKSPACE+$0EC4
        DEFC    AUTOBEC8 = WORKSPACE+$0EC8
        DEFC    AUTOBEC9 = WORKSPACE+$0EC9
        DEFC    AUTOBECA = WORKSPACE+$0ECA
        DEFC    AUTOBECC = WORKSPACE+$0ECC
        DEFC    AUTOBED0 = WORKSPACE+$0ED0
        DEFC    AUTOBED1 = WORKSPACE+$0ED1
        DEFC    AUTOBED2 = WORKSPACE+$0ED2
        DEFC    AUTOBED4 = WORKSPACE+$0ED4
        DEFC    AUTOBED8 = WORKSPACE+$0ED8
        DEFC    AUTOBED9 = WORKSPACE+$0ED9
        DEFC    AUTOBEDA = WORKSPACE+$0EDA
        DEFC    AUTOBEDC = WORKSPACE+$0EDC
        DEFC    AUTOBEE0 = WORKSPACE+$0EE0
        DEFC    AUTOBEE1 = WORKSPACE+$0EE1
        DEFC    AUTOBEE2 = WORKSPACE+$0EE2
        DEFC    AUTOBEE4 = WORKSPACE+$0EE4
        DEFC    AUTOBEE8 = WORKSPACE+$0EE8
        DEFC    AUTOBEE9 = WORKSPACE+$0EE9
        DEFC    AUTOBEEA = WORKSPACE+$0EEA
        DEFC    AUTOBEEC = WORKSPACE+$0EEC
        DEFC    AUTOBEF0 = WORKSPACE+$0EF0
        DEFC    AUTOBEF1 = WORKSPACE+$0EF1
        DEFC    AUTOBEF2 = WORKSPACE+$0EF2
        DEFC    AUTOBEF4 = WORKSPACE+$0EF4
        DEFC    AUTOBEF8 = WORKSPACE+$0EF8
        DEFC    AUTOBEF9 = WORKSPACE+$0EF9
        DEFC    AUTOBEFA = WORKSPACE+$0EFA
        DEFC    AUTOBEFC = WORKSPACE+$0EFC
        DEFC    AUTOBF00 = WORKSPACE+$0F00
        DEFC    AUTOC000 = GAME_DATA+$0000
        DEFC    AUTOC001 = GAME_DATA+$0001      ;Offset to last move/start move
        DEFC    AUTOC003 = GAME_DATA+$0003      ;To move (for automatic playback?)
        DEFC    AUTOC005 = GAME_DATA+$0005
        DEFC    AUTOC006 = GAME_DATA+$0006      ;Move number
        DEFC    AUTOC007 = GAME_DATA+$0007
        DEFC    AUTOC047 = GAME_DATA+$0047
        DEFC    AUTOC057 = GAME_DATA+$0057
        DEFC    AUTOC058 = GAME_DATA+$0058      ;Move history
        DEFC    AUTOE000 = LOADSPACE+$0000
        DEFC    AUTOE058 = LOADSPACE+$0058

        DEFC    IOPORTFEH = $FE
        DEFC    CHARS = $5C36
        DEFC    FLAGS = $5C3B
        DEFC    LAST_K = $5C08
        DEFC    RAMTOP = $5CB2
        DEFC    P_RAMT = $5CB4

;
;       End of external equates
;


.CYRUS
        LD      HL,$1111
        LD      (RAMTOP),HL
        LD      (P_RAMT),HL
        DI
        LD      (AUTOBCC8),IY
        LD      HL,(CHARS)
        LD      (AUTOBCD5),HL
        CALL    AUTO7004
        CALL    PRINT_AT
        DEFW    $0012

        CALL    AUTO6FEF
        CALL    BEEP
        DEFW    $0C50

        LD      (CHARS),HL
        CALL    AUTO6734        ;Check for a pressed key
        JR      Z,AUTO605B      ;Skip if no key

.AUTO602C
        CALL    PRINT_TXT_AT
        DEFW    $0000

        DEFM    $7F," 1982 Intelligent Software Ltd",'.'+$80

        CALL    BEEP
        DEFW    $10FF

        CALL    AUTO6734
        JR      Z,AUTO602C

.AUTO605B
        LD      A,$01
        LD      (AUTOBCBD),A
        LD      (AUTOBCCD),A    ;Level +1 (1 = level 2)
        LD      (AUTOBCD4),A    ;Print listing off
        XOR     A
        LD      (AUTOBCC7),A    ;Board orientation; B at top, W at Bottom
        LD      (AUTOBCDD),A
        LD      (AUTOC006),A
        LD      (AUTOBCDB),A
        LD      (AUTOBCC3),A
        LD      (AUTOBCD3),A    ;Sound on
        CALL    AUTO6BBA        ;Default colours
        CALL    AUTO709C        ;Clear print buffer

.AUTO607F
        LD      A,(AUTOC006)    ;Move number?
        DEC     A
        JR      Z,AUTO608A
        LD      A,$FF           ;End marker of move history
        LD      (AUTOC058),A

.AUTO608A
        LD      A,$01           ;Computer plays black, Human plays white
        LD      (AUTOBCC2),A

.AUTO608F
        LD      HL,$0000
        LD      (AUTOC003),HL
        CALL    AUTO7004
        CALL    BEEP
        DEFW    $1032

        LD      A,(AUTOBCD4)
        OR      A
        CALL    Z,AUTO7065              ;Printer listing on? Call if so

.AUTO60A4
        CALL    AUTO6505                ;Setup board

.AUTO60A7
        LD      HL,$0000
        LD      (AUTOC001),HL
        XOR     A
        LD      (AUTOC047),A
        LD      (AUTOBCDF),A
        CALL    AUTO701D

.AUTO60B7
        LD      SP,AUTOBDF4
        LD      A,(AUTOBCC3)
        OR      A
        JR      Z,AUTO60E3
        LD      BC,AUTOC058
        LD      HL,(AUTOC001)
        ADD     HL,BC
        LD      A,(HL)
        INC     A
        JP      Z,AUTO671F
        CALL    AUTO70DF        ;Print board
        LD      HL,AUTOC000

.AUTO60D2
        CALL    AUTO6734
        JP      NZ,AUTO671F
        LD      A,(AUTOBCDB)
        OR      A
        JR      NZ,AUTO60E3
        DEC     HL
        LD      A,L
        OR      H
        JR      NZ,AUTO60D2

.AUTO60E3
        LD      HL,(AUTOC003)
        LD      A,L
        OR      H
        JP      NZ,AUTO6690
        LD      A,R
        LD      B,A
        RRA
        RRA
        ADD     A,B
        LD      HL,AUTOBCCE
        ADD     A,(HL)
        LD      (HL),A
        LD      A,(AUTOBD5E)
        CP      $06
        JP      Z,AUTO6448
        LD      A,(AUTOBD0A)
        CP      'd'
        JP      NC,AUTO6448
        XOR     A
        LD      (AUTOBCBF),A
        CALL    AUTO7046
        CALL    AUTO703F
        CALL    AUTO704D
        CALL    AUTO737C
        CALL    AUTO756E
        CALL    AUTO759A
        CALL    AUTO75B4
        LD      A,(AUTOBCDD)
        OR      A
        JR      Z,AUTO6134
        LD      A,(AUTOBD2D)
        AND     $01
        XOR     $01
        LD      (AUTOBCBE),A
        CALL    AUTO7562
        JR      AUTO6147

.AUTO6134
        LD      A,(AUTOBD2D)
        RRA
        LD      A,(AUTOBCBE)
        JR      NC,AUTO6143
        OR      A
        JR      Z,AUTO6147
        JP      AUTO61C8

.AUTO6143
        OR      A
        JP      Z,AUTO61C8

.AUTO6147
        LD      A,(AUTOBCC2)
        OR      A
        JP      Z,AUTO696A
        LD      A,'@'
        LD      (AUTOBD4C),A
        LD      IX,AUTOBD58
        CALL    AUTO78CE
        CALL    AUTO7B96
        LD      A,(AUTOBD19)
        OR      A
        JP      Z,AUTO641A
        XOR     A
        LD      (AUTOBCDB),A
        LD      A,(AUTOBCDD)
        OR      A
        CALL    Z,AUTO6FAA
        CALL    AUTO70DF
        CALL    BEEP
        DEFW    $1014

        CALL    AUTO73AE
        CALL    PRINT_TXT
        DEFM    "LET ME THINK..",'.'+$80

        CALL    AUTO7BE1
        CALL    BEEP
        DEFW    $2014

        XOR     A
        LD      (AUTOBCDD),A
        LD      A,(AUTOBCCD)
        CP      $08
        JR      C,AUTO61C1
        LD      A,(AUTOBCA0)
        CP      $FE
        JR      NC,AUTO61C1
        CALL    AUTO703F
        CALL    PRINT_TXT
        DEFM    "*NO MATE FOUND",'*'+$80

        CALL    AUTO73A8
        JP      AUTO6D4D

.AUTO61C1
        LD      IX,(AUTOBD29)
        JP      AUTO6323

.AUTO61C8
        XOR     A
        LD      (AUTOBD4C),A
        LD      IX,AUTOBD58
        CALL    AUTO78CE
        CALL    AUTO7B96
        LD      A,(AUTOBD19)
        OR      A
        JP      Z,AUTO641A
        XOR     A
        LD      (AUTOBCDB),A
        LD      A,(AUTOBCC2)    ;Players
        CP      $03             ;Computer playing both W & B?
        JP      Z,AUTO696A
        OR      A
        JR      Z,AUTO61EF
        CALL    AUTO7562

.AUTO61EF
        CALL    AUTO70D3
        CALL    PRINT_AT_12_0
        DEFM    "Alter     take-Back Change-pos'nDemo      Ent"
        DEFM    "er     take-Forwardnew-Game  Level     Move  "
        DEFM    "      Next-best Orient'n  Printer     Replay "
        DEFM    "   Sound     Tape        5-left 6-down 7-up 8"
        DEFM    "-right <cr",'>'+$80

        CALL    AUTO6FEF

.AUTO62B7
        CALL    PRINT_TXT_0_0
        DEFM    "YOUR MOVE?   ",' '+$80

        LD      A,$FF
        LD      (AUTOBCCB),A

.AUTO62CD
        CALL    AUTO6800
        CP      $0D
        JR      NZ,AUTO62CD
        CALL    BEEP
        DEFW    $2032

        CALL    AUTO703F
        LD      A,'8'
        LD      (AUTOBCC4),A
        LD      A,(AUTOBCCB)
        INC     A
        LD      A,(AUTOBCCA)
        JR      NZ,AUTO62F7
        LD      (AUTOBCCB),A
        CALL    PRINT_AT
        DEFW    $0A0A

        CALL    AUTO74F7
        JR      AUTO62CD

.AUTO62F7
        LD      (AUTOBCCC),A
        CALL    PRINT_AT
        DEFW    $0C0A

        CALL    AUTO74F7
        LD      A,(AUTOBCCB)
        EX      AF,AF'
        LD      A,(AUTOBCCC)
        LD      HL,AUTOB401
        LD      B,(HL)
        INC     L
        CALL    AUTO7BCD
        JR      C,AUTO6318
        DEC     L
        XOR     A
        OR      (HL)
        JR      NZ,AUTO631D

.AUTO6318
        CALL    AUTO739E
        JR      AUTO62B7

.AUTO631D
        CALL    AUTO6FAA
        PUSH    HL
        POP     IX

.AUTO6323
        CALL    AUTO70BF
        CALL    AUTO810F
        XOR     A
        LD      (AUTOBCDE),A
        POP     AF
        PUSH    AF
        CALL    PE,AUTO63B1
        RES     7,(IX+$03)
        CALL    AUTO8400
        CALL    AUTO746E
        CALL    AUTO70C7
        LD      A,(AUTOBCD4)
        OR      A
        CALL    Z,AUTO7452
        PUSH    IX
        POP     HL
        LD      BC,AUTOB400
        XOR     A
        LD      E,A
        SBC     HL,BC
        LD      BC,$0004

.AUTO6353
        INC     E
        SBC     HL,BC
        JR      NZ,AUTO6353
        DEC     E
        LD      HL,(AUTOC001)
        INC     HL
        LD      (AUTOC001),HL
        LD      A,(IX+$03)
        AND     $80
        OR      E
        LD      BC,AUTOC057
        ADD     HL,BC
        LD      (HL),A
        INC     HL
        LD      (HL),$FF

.AUTO636E
        LD      HL,AUTOBD0A
        INC     (HL)
        POP     AF
        PUSH    AF
        JP      P,AUTO637C
        AND     $0F
        DEC     A
        JR      NZ,AUTO6380

.AUTO637C
        XOR     A
        LD      (AUTOBD0A),A

.AUTO6380
        CALL    AUTO6560
        LD      A,(AUTOBD2D)
        XOR     $01
        LD      (AUTOBD2D),A
        RRA
        JR      NC,AUTO6392
        LD      HL,AUTOBD49
        INC     (HL)

.AUTO6392
        LD      A,(AUTOBCDD)
        OR      A
        JR      NZ,AUTO63AE
        LD      HL,AUTOBF00

.AUTO639B
        LD      A,(HL)
        INC     A
        JR      Z,AUTO63A2
        INC     HL
        JR      AUTO639B

.AUTO63A2
        LD      A,(IX+$01)
        LD      (HL),A
        INC     HL
        LD      A,(IX+$02)
        LD      (HL),A
        INC     HL
        LD      (HL),$FF

.AUTO63AE
        JP      AUTO60B7

.AUTO63B1
        LD      A,(AUTOBD4C)
        BIT     6,A
        JR      NZ,AUTO63FE
        CALL    PRINT_TXT
        DEFM    "PROMOTE TO",'?'+$80

        CALL    PRINT_AT_12_0
        DEFM    "kNight  Bishop  Rook    Quee",'n'+$80

        CALL    AUTO6FEF
        CALL    AUTO675A
        LD      B,$05
        CP      'R'
        JR      Z,AUTO6402
        LD      B,$03
        CP      'N'
        JR      Z,AUTO6402
        LD      B,$83
        CP      'B'
        JR      Z,AUTO6402

.AUTO63FE
        LD      B,$09
        JR      AUTO640D

.AUTO6402
        LD      L,(IX+$02)
        LD      H,AUTO00BD
        LD      L,(HL)
        RES     2,L
        INC     L
        INC     H
        LD      (HL),B

.AUTO640D
        LD      HL,AUTOC047
        INC     (HL)
        LD      A,(HL)
        ADD     A,L
        LD      L,A
        LD      (HL),B
        LD      A,B
        LD      (AUTOBCDE),A

        DEFC    AUTO04C6 = $04C6
        DEFC    AUTO0562 = $0562

        RET

.AUTO641A
        LD      A,(AUTOBD5B)
        RLA
        JR      C,AUTO6434
        CALL    PRINT_AT_B_0

.AUTO6423
        DEFM    "STALEMAT",'E'+$80

        LD      HL,AUTO6423
        CALL    AUTO742C
        JR      AUTO6456

.AUTO6434
        CALL    PRINT_AT_B_0

.AUTO6437
        DEFM    "CHECKMAT",'E'+$80

        LD      HL,AUTO6437
        CALL    AUTO742C
        JR      AUTO6456

.AUTO6448
        CALL    PRINT_AT_B_0

.AUTO644B
        DEFM    "DRAW",'N'+$80

        LD      HL,AUTO644B
        CALL    AUTO742C

.AUTO6456
        LD      A,$01
        LD      (AUTOBCBF),A
        XOR     A
        LD      (AUTOBCDB),A
        CALL    AUTO703F
        CALL    AUTO704D
        CALL    AUTO70DF
        CALL    BEEP
        DEFW    $18C8

        LD      A,(AUTOBCC2)
        CP      $03
        JR      NZ,AUTO648B
        LD      B,$09
        LD      HL,$0000

.AUTO6479
        CALL    AUTO6734
        JR      NZ,AUTO6488
        DEC     HL
        LD      A,L
        OR      H
        JR      NZ,AUTO6479
        DJNZ    AUTO6479
        JP      AUTO608F

.AUTO6488
        CALL    AUTO7562

.AUTO648B
        CALL    PRINT_AT_12_0
        DEFM    "Alter     take-Back Change"
        DEFM    "-pos'nnew-Game  Level     Next-best   Orient'"
        DEFM    "n  Printer   Replay      Sound     Tap",'e'+$80

        CALL    AUTO6FEF
        CALL    AUTO6800
        JP      AUTO648B

;Setup board
.AUTO6505                       ;Prepare board and info workspace
        POP     DE              ;Get return address (why?)
        XOR     A
        LD      B,A
        LD      HL,AUTOBF00     ;Top of the board workspace area

.AUTO650B                       ;Clear the board workspace area
        DEC     HL              ;512 bytes in total
        LD      (HL),A
        DEC     HL
        LD      (HL),A
        DJNZ    AUTO650B
        PUSH    DE              ;Put return address back onto the stack
        LD      C,A
        CALL    AUTO6547        ;Setup BD00 area
        LD      DE,AUTOBE00
        LD      C,'A'
        CALL    AUTO654A
        LD      L,'`'
        LD      C,$C1
        CALL    AUTO654A
        LD      C,$81
        CALL    AUTO6547
        LD      A,$01
        LD      (AUTOBCBE),A    ;Black's turn
        LD      (AUTOBD2D),A    ;Black's turn
        LD      (AUTOBD49),A
        LD      (AUTOBCA9),A
        LD      (AUTOC006),A
        LD      HL,AUTOB400     ;Thinking workspace
        LD      (AUTOBCAB),HL
        CALL    AUTO659C
        JP      AUTO65E5

.AUTO6547
        LD      DE,AUTO6558

.AUTO654A
        LD      B,$08

.AUTO654C
        LD      A,(DE)
        OR      C
        LD      (HL),A
        INC     L
        INC     DE
        DJNZ    AUTO654C
        LD      A,$08
        ADD     A,L
        LD      L,A
        RET

.AUTO6558
        DEFW    $3111,$0921,$2101,$1131

.AUTO6560
        LD      HL,(AUTOBD5C)
        LD      B,(IX+$01)
        LD      C,(IX+$02)
        XOR     A
        SBC     HL,BC
        LD      HL,AUTOBD5E
        JR      NZ,AUTO6574
        INC     (HL)
        JR      AUTO6575

.AUTO6574
        LD      (HL),A

.AUTO6575
        LD      HL,(AUTOBD59)
        LD      (AUTOBD5C),HL
        LD      A,B
        LD      (AUTOBD59),A
        LD      H,(IX+$03)
        LD      L,C
        LD      (AUTOBD5A),HL
        RET

.AUTO6587
        POP     HL

.AUTO6588
        CALL    AUTO739E
        JP      AUTO6D6A

.AUTO658E
        XOR     A
        LD      HL,AUTOBD00

.AUTO6592
        LD      (HL),A

.AUTO6593
        INC     L
        BIT     3,L
        JR      Z,AUTO6592
        BIT     7,L
        JR      Z,AUTO6593

.AUTO659C
        XOR     A
        LD      HL,AUTOBE00

.AUTO65A0
        SET     7,(HL)
        ADD     A,$08
        LD      L,A
        JR      NC,AUTO65A0
        RET

.AUTO65A8
        LD      D,(HL)
        BIT     7,D
        JR      NZ,AUTO65D1
        INC     L
        LD      B,(HL)
        DEC     L
        LD      A,B
        AND     $0F
        DEC     A
        JR      Z,AUTO65D1
        SET     7,(HL)
        LD      A,L
        AND     $80
        OR      $03
        LD      E,A
        LD      A,B
        AND     $8F
        PUSH    HL
        LD      HL,AUTO6F2E
        CALL    AUTO6F23
        INC     HL
        INC     HL
        LD      A,(HL)
        OR      E
        LD      H,AUTO00BD
        LD      L,D
        LD      (HL),A
        POP     HL

.AUTO65D1
        LD      A,L
        ADD     A,$08
        LD      L,A
        BIT     6,A
        JR      NZ,AUTO65A8
        RET

.AUTO65DA
        LD      HL,AUTOBE40
        CALL    AUTO65A8
        LD      L,$C0
        CALL    AUTO65A8

.AUTO65E5
        LD      DE,AUTOBD00
        LD      H,AUTO00BE

.AUTO65EA
        LD      A,(DE)
        BIT     0,A
        CALL    NZ,AUTO6610
        LD      A,$10
        ADD     A,E
        LD      E,A
        JP      P,AUTO65EA
        RES     7,E
        INC     E
        BIT     3,A
        JR      Z,AUTO65EA
        LD      L,$00
        CALL    AUTO660A
        LD      L,$80
        LD      A,B
        OR      (HL)
        JP      M,AUTO6587

.AUTO660A
        LD      B,(HL)
        INC     L
        LD      A,(HL)
        JP      AUTO8203

.AUTO6610
        LD      L,A
        DEC     L
        BIT     6,L
        JR      Z,AUTO662B
        LD      A,'p'
        AND     E
        JR      Z,AUTO6674
        CP      'p'
        JR      Z,AUTO6674
        LD      B,$01
        LD      A,$80
        AND     L
        XOR     $80
        SCF
        RRA
        LD      C,A
        JR      AUTO666D

.AUTO662B
        LD      A,'x'
        AND     L
        JR      NZ,AUTO663F
        LD      C,$01
        LD      B,$00
        BIT     1,L
        JR      Z,AUTO663A
        LD      B,'0'

.AUTO663A
        CALL    AUTO6678
        JR      AUTO6674

.AUTO663F
        CP      $10
        LD      C,$0E
        LD      B,$09
        JR      C,AUTO6664
        JR      NZ,AUTO6655
        LD      C,$08
        LD      B,$05
        BIT     1,L
        JR      Z,AUTO6661
        LD      B,$15
        JR      AUTO6661

.AUTO6655
        LD      B,$03
        LD      C,$00
        CP      ' '
        JR      NZ,AUTO6661
        LD      B,$83
        LD      C,$04

.AUTO6661
        CALL    AUTO6678

.AUTO6664
        CALL    AUTO6678
        LD      A,$82
        AND     L
        SET     6,A
        LD      L,A

.AUTO666D
        CALL    AUTO6678
        BIT     6,L
        JR      NZ,AUTO666D

.AUTO6674
        POP     HL
        JP      AUTO6587

.AUTO6678
        RES     1,L
        LD      A,(HL)
        RLA
        JR      C,AUTO6683
        LD      A,$08
        ADD     A,L
        LD      L,A
        RET

.AUTO6683
        LD      (HL),E
        LD      A,L
        SET     2,A
        LD      (DE),A
        INC     L
        LD      (HL),B
        SET     2,L
        INC     L
        LD      (HL),C
        POP     BC
        RET

.AUTO6690
        LD      BC,(AUTOC001)
        XOR     A
        SBC     HL,BC
        JP      Z,AUTO671F
        LD      (AUTOBCCF),HL
        LD      HL,(AUTOC001)
        INC     HL
        LD      (AUTOC001),HL
        LD      BC,AUTOC057
        ADD     HL,BC
        LD      C,(HL)
        LD      A,C
        RES     7,C
        LD      B,$00
        LD      HL,AUTOB404
        ADD     HL,BC
        ADD     HL,BC
        ADD     HL,BC
        ADD     HL,BC
        PUSH    AF
        PUSH    HL
        LD      IX,AUTOBD58
        CALL    AUTO78CE
        POP     IX
        POP     AF
        INC     (IX+$00)
        RLA
        JR      NC,AUTO66CB
        SET     7,(IX+$03)

.AUTO66CB
        LD      A,(AUTOBCC3)
        OR      A
        CALL    NZ,AUTO70BF
        CALL    AUTO810F
        XOR     A
        LD      (AUTOBCDE),A
        POP     AF
        PUSH    AF
        JP      PO,AUTO66F4
        LD      HL,AUTOC047
        INC     (HL)
        LD      A,(HL)
        ADD     A,L
        LD      L,A
        LD      A,(HL)
        LD      L,(IX+$02)
        LD      H,AUTO00BD
        LD      L,(HL)
        RES     2,L
        INC     L
        INC     H
        LD      (HL),A
        LD      (AUTOBCDE),A

.AUTO66F4
        LD      A,(AUTOBCC3)
        OR      A
        JR      Z,AUTO6709
        CALL    AUTO746E
        CALL    AUTO70C7
        LD      A,(AUTOBCDB)
        OR      A
        CALL    NZ,AUTO7452
        JR      AUTO6715

.AUTO6709
        LD      HL,(AUTOBCCF)
        XOR     A
        LD      BC,$0003
        SBC     HL,BC
        CALL    C,AUTO746E

.AUTO6715
        LD      A,(AUTOBCDD)
        OR      A
        CALL    Z,AUTO6FAA
        JP      AUTO636E

.AUTO671F
        LD      HL,$0000
        LD      (AUTOC003),HL
        XOR     A
        LD      (AUTOBCC3),A
        LD      A,(AUTOBD2D)
        AND     $01
        LD      (AUTOBCBE),A
        JP      AUTO60B7

.AUTO6734                       ;Check for a key being pressed
        XOR     A
        IN      A,(IOPORTFEH)
        AND     $1F
        CP      $1F
        RET     Z               ;Return if no key pressed
        CALL    PRINT_TXT
        DEFM    "**INTERRUPTED*",'*'+$80

        CALL    BEEP
        DEFW    $50C8

        CALL    AUTO703F
        LD      A,$01
        OR      A
        RET

.AUTO675A
        LD      IY,(AUTOBCC8)
        EI

.AUTO675F
        LD      HL,AUTOBCCE
        INC     (HL)
        LD      HL,FLAGS
        BIT     5,(HL)
        JR      Z,AUTO675F
        DI
        LD      A,(LAST_K)
        RES     5,(HL)
        AND     $DF
        LD      (AUTOBCC0),A
        CALL    BEEP
        DEFW    $0402

        LD      A,(AUTOBCC0)
        RET

.AUTO677E
        CALL    AUTO675A
        CP      'S'
        JR      NZ,AUTO6797
        LD      A,(AUTOBCD3)
        XOR     $01
        LD      (AUTOBCD3),A
        CALL    AUTO75B4
        CALL    BEEP
        DEFW    $1408

        JR      AUTO677E

.AUTO6797
        CP      'O'
        JR      NZ,AUTO67A8
        LD      A,(AUTOBCC7)
        XOR     $01
        LD      (AUTOBCC7),A
        CALL    AUTO70E4
        JR      AUTO677E

.AUTO67A8
        CP      $19
        RET     NC
        CP      $15
        RET     C
        LD      B,A
        LD      A,(AUTOBCCA)
        INC     A
        LD      A,B
        RET     Z
        LD      A,(AUTOBCC7)
        OR      A
        LD      A,B
        JR      Z,AUTO67BE
        XOR     $01

.AUTO67BE
        CP      $16
        JR      Z,AUTO67E2
        CP      $17
        JR      Z,AUTO67EF
        AND     $03
        JR      Z,AUTO67D6
        LD      A,(AUTOBCCA)
        DEC     A
        BIT     3,A
        JR      Z,AUTO67FA
        ADD     A,$08
        JR      AUTO67FA

.AUTO67D6
        LD      A,(AUTOBCCA)
        INC     A
        BIT     3,A
        JR      Z,AUTO67FA
        SUB     $08
        JR      AUTO67FA

.AUTO67E2
        LD      A,(AUTOBCCA)
        ADD     A,$10
        BIT     7,A
        JR      Z,AUTO67FA
        SUB     $80
        JR      AUTO67FA

.AUTO67EF
        LD      A,(AUTOBCCA)
        SUB     $10
        BIT     7,A
        JR      Z,AUTO67FA
        ADD     A,$80

.AUTO67FA
        CALL    AUTO70E1
        JP      AUTO677E

.AUTO6800
        CALL    AUTO677E
        LD      A,(AUTOBCBF)
        OR      A
        LD      A,(AUTOBCC0)
        JP      NZ,AUTO681A
        CP      'D'
        JR      Z,AUTO684D
        CP      'E'
        JR      Z,AUTO6858
        CP      'M'
        JP      Z,AUTO695D

.AUTO681A
        CP      'A'
        JP      Z,AUTO6A8D
        CP      'B'
        JP      Z,AUTO68A0
        CP      'C'
        JP      Z,AUTO6D4D
        CP      'F'
        JP      Z,AUTO6916
        CP      'G'
        JP      Z,AUTO697F
        CP      'L'
        JP      Z,AUTO6932
        CP      'N'
        JP      Z,AUTO69B3
        CP      'P'
        JP      Z,AUTO69C9
        CP      'R'
        JP      Z,AUTO6869
        CP      'T'
        JP      Z,AUTO6BCF
        RET

.AUTO684D
        LD      A,$03
        LD      (AUTOBCC2),A
        CALL    AUTO73AE
        JP      AUTO60B7

.AUTO6858
        LD      A,$00
        LD      (AUTOBCC2),A
        CALL    AUTO756E
        JP      AUTO6800

.AUTO6863
        DEFM    "Repla",'y'+$80


.AUTO6869                       ;Replay
        LD      A,(AUTOC058)
        INC     A
        JP      Z,AUTO6800
        LD      HL,AUTO6863
        CALL    AUTO743B

.AUTO6876
        CALL    AUTO7046
        CALL    AUTO704D
        CALL    PRINT_TXT
        DEFM    "*REPLAYING",'*'+$80

        CALL    AUTO73AE        ;@@@
        LD      A,$01
        LD      (AUTOBCC3),A
        LD      HL,$FFFF
        JR      AUTO68B2

.AUTO6897
        DEFM    "take-Bac",'k'+$80

.AUTO68A0
        LD      HL,(AUTOC001)
        LD      A,L
        OR      H
        JP      Z,AUTO6800
        LD      HL,AUTO6897
        CALL    AUTO743B

.AUTO68AE
        LD      HL,(AUTOC001)
        DEC     HL

.AUTO68B2
        LD      (AUTOC003),HL
        LD      A,(AUTOC006)    ;Move number
        LD      (AUTOBD49),A    ;Move number
        DEC     A
        JP      Z,AUTO60A4
        CALL    AUTO658E
        LD      A,(AUTOC005)
        LD      (AUTOBD2D),A
        AND     $01
        LD      (AUTOBCBE),A
        LD      DE,AUTOC007
        LD      HL,AUTOBE00
        LD      B,' '

.AUTO68D5
        LD      A,(DE)
        LD      (HL),A
        INC     HL
        INC     DE
        LD      A,(DE)
        LD      (HL),A
        INC     DE
        LD      A,L
        ADD     A,$07
        LD      L,A
        DJNZ    AUTO68D5
        LD      HL,AUTOBE00
        LD      DE,AUTOBD00

.AUTO68E8
        LD      A,(HL)
        OR      A
        JP      M,AUTO68F2
        LD      E,A
        LD      A,L
        OR      $04
        LD      (DE),A

.AUTO68F2
        LD      A,L
        ADD     A,$08
        LD      L,A
        JR      NC,AUTO68E8
        CALL    AUTO6F99
        LD      A,(AUTOBCDB)
        OR      A
        JR      Z,AUTO6907
        CALL    AUTO70DF        ;Print board
        CALL    AUTO729D

.AUTO6907
        JP      AUTO60A7

.AUTO690A
        DEFM    "take-Forwar",'d'+$80


.AUTO6916
        LD      HL,(AUTOC001)
        LD      BC,AUTOC058
        ADD     HL,BC
        LD      A,(HL)
        INC     A
        JP      Z,AUTO6800
        LD      HL,AUTO690A
        CALL    AUTO743B
        LD      HL,(AUTOC001)
        INC     HL
        LD      (AUTOC003),HL
        JP      AUTO60B7

.AUTO6932
        LD      A,(AUTOBCCD)
        INC     A
        CP      $0B
        JR      NZ,AUTO693B
        XOR     A

.AUTO693B
        LD      (AUTOBCCD),A
        CP      $08
        JR      NZ,AUTO6946
        LD      A,$00
        JR      AUTO6954

.AUTO6946
        CP      $09
        JR      NZ,AUTO694E
        LD      A,$03
        JR      AUTO6954

.AUTO694E
        CP      $0A
        JR      NZ,AUTO6954
        LD      A,$07

.AUTO6954
        LD      (AUTOBCBD),A
        CALL    AUTO737C
        JP      AUTO6800

.AUTO695D
        LD      A,(AUTOBCBE)
        OR      A
        LD      A,$01
        JR      Z,AUTO6967
        LD      A,$02

.AUTO6967
        LD      (AUTOBCC2),A

.AUTO696A
        LD      A,(AUTOBCBE)
        OR      A
        LD      A,$01
        JR      Z,AUTO6973
        XOR     A

.AUTO6973
        LD      (AUTOBCBE),A
        LD      HL,AUTOBCA0
        XOR     A
        SUB     (HL)
        LD      (HL),A
        JP      AUTO60B7

.AUTO697F
        CALL    AUTO6985
        JP      AUTO60B7

.AUTO6985
        CALL    AUTO704D
        CALL    PRINT_TXT_0_0
        DEFM    "NEW GAME",'?'+$80

        CALL    PRINT_AT_12_0
        DEFM    "Yes  N",'o'+$80

        CALL    AUTO6FEF
        CALL    AUTO675A
        CP      'Y'
        JP      Z,AUTO607F
        RET

.AUTO69AA
        DEFM    "Next-bes",'t'+$80

.AUTO69B3                       ;Next best move
        LD      HL,(AUTOC001)
        LD      A,L
        OR      H
        JP      Z,AUTO6800
        LD      HL,AUTO69AA
        CALL    AUTO743B
        LD      A,$01
        LD      (AUTOBCDD),A
        JP      AUTO68AE

.AUTO69C9
        IN      A,($FB)
        AND     '@'
        JR      Z,AUTO69EA
        CALL    AUTO703F
        CALL    PRINT_TXT
        DEFM    "*NO PRINTER",'*'+$80

        CALL    AUTO73A8
        CALL    AUTO703F
        JP      AUTO6800

.AUTO69EA
        CALL    AUTO704D
        CALL    PRINT_TXT_0_0
        DEFM    "COMMAND",'?'+$80

        CALL    PRINT_AT_12_0
        DEFM    "print-Board        print-Record          Togg"
        DEFM    "le-listing             -any other key to exit"
        DEFM    '-'+$80

        CALL    AUTO6FEF
        CALL    AUTO675A
        CP      'B'
        JR      NZ,AUTO6A66
        CALL    AUTO729D
        JP      AUTO60B7

.AUTO6A66
        CP      'R'
        JR      NZ,AUTO6A7A
        LD      A,(AUTOC058)
        INC     A
        JP      Z,AUTO60B7
        LD      (AUTOBCDB),A
        CALL    AUTO7065
        JP      AUTO6876

.AUTO6A7A
        CP      'T'
        JP      NZ,AUTO60B7
        LD      A,(AUTOBCD4)
        XOR     $01
        LD      (AUTOBCD4),A
        CALL    AUTO759A
        JP      AUTO60B7

.AUTO6A8D
        CALL    AUTO704D
        CALL    PRINT_AT_12_0
        DEFM    "0-black 1-blue 2-red 3-magenta  4-green 5-cya"
        DEFM    "n 6-yellow 7-white           8-no change     "
        DEFM    "             D-default colours            -an"
        DEFM    "y other key to exit",'-'+$80

        CALL    AUTO6FEF
        CALL    PRINT_TXT_0_0
        DEFM    "COLOUR FO",'R'+$80
        CALL    PRINT_AT_B_0
        DEFM    "BLACK PIECE",'?'+$80

        LD      HL,AUTOBCD7
        CALL    AUTO6B97
        CALL    PRINT_AT_B_0
        DEFM    "WHITE PIECE",'?'+$80

        LD      HL,AUTOBCD8
        CALL    AUTO6B97
        CALL    PRINT_AT_B_0
        DEFM    "BLACK SQUARE",'?'+$80

        LD      HL,AUTOBCD9
        CALL    AUTO6B97
        CALL    PRINT_AT_B_0
        DEFM    "WHITE SQUARE",'?'+$80

        LD      HL,AUTOBCDA
        CALL    AUTO6B97
        JP      AUTO60B7

.AUTO6B97
        PUSH    HL

.AUTO6B98
        CALL    AUTO675A
        CP      'D'
        JR      NZ,AUTO6BA7
        CALL    AUTO6BBA
        CALL    AUTO70DF
        JR      AUTO6B98

.AUTO6BA7
        SUB     $10
        JP      M,AUTO60B7
        CP      $09
        JP      P,AUTO60B7
        POP     HL
        CP      $08
        RET     Z
        LD      (HL),A
        CALL    AUTO70DF
        RET

.AUTO6BBA                       ;Default colours
        LD      A,$00
        LD      (AUTOBCD7),A    ;BPc colour
        LD      A,$07
        LD      (AUTOBCD8),A    ;WPc colour
        LD      A,$04
        LD      (AUTOBCD9),A    ;BSq colour
        LD      A,$06
        LD      (AUTOBCDA),A    ;WSq colour
        RET

.AUTO6BCF
        CALL    AUTO704D
        CALL    PRINT_TXT_0_0
        DEFM    "COMMAND",'?'+$80

        CALL    PRINT_AT_12_0
        DEFM    "Save-record         Load-record     -any othe"
        DEFM    "r key to exit",'-'+$80

        CALL    AUTO6FEF
        CALL    AUTO675A
        CP      'S'
        JR      NZ,AUTO6C8D
        LD      A,(AUTOC058)
        INC     A
        JP      Z,AUTO60B7
        CALL    PRINT_TXT_0_0
        DEFM    "START TAP",'E'+$80

        CALL    PRINT_AT_12_0
        DEFM    "--Hit any key to initiate save-",'-'+$80

        CALL    AUTO6FEF
        CALL    AUTO675A
        CALL    PRINT_TXT_0_0
        DEFM    "SAVING...",' '+$80

        CALL    AUTO73D4
        LD      DE,$0058
        LD      HL,AUTOC058

.AUTO6C78
        INC     DE
        LD      A,(HL)
        INC     HL
        INC     A
        JR      NZ,AUTO6C78
        LD      IX,AUTOC000
        LD      A,$FF
        LD      (AUTOC000),A
        CALL    AUTO6D0F
        JP      AUTO60B7

.AUTO6C8D
        CP      'L'
        JP      NZ,AUTO60B7
        CALL    PRINT_TXT_0_0
        DEFM    "LOADING..",'.'+$80

        CALL    AUTO73D4
        LD      DE,$1F00
        SCF
        LD      IX,AUTOE000
        LD      A,$FF
        CALL    AUTO6D16
        JR      NC,AUTO6D0C
        LD      HL,AUTOE058
        OR      A
        PUSH    IX
        POP     BC
        SBC     HL,BC
        LD      A,H
        OR      A
        JP      P,AUTO6CF4
        LD      A,(IX+$FE)
        INC     A
        JR      NZ,AUTO6CF4
        LD      A,(AUTOE000)
        INC     A
        JR      NZ,AUTO6CF4
        LD      HL,AUTOC000
        LD      DE,AUTOE000
        LD      BC,$1F00

.AUTO6CD4
        LD      A,(DE)
        LD      (HL),A
        INC     DE
        INC     HL
        DEC     BC
        LD      A,B
        OR      C
        JR      NZ,AUTO6CD4
        LD      HL,AUTO6CE9
        CALL    AUTO743B
        LD      HL,(AUTOC001)
        JP      AUTO68B2

.AUTO6CE9
        DEFM    "Tape loade",'d'+$80


.AUTO6CF4
        CALL    AUTO703F
        CALL    PRINT_TXT
        DEFM    "*TAPE ERROR",'*'+$80

        CALL    AUTO73A8
        CALL    AUTO703F

.AUTO6D0C
        JP      AUTO60B7

.AUTO6D0F
        LD      HL,AUTO6D25
        PUSH    HL
        JP      AUTO04C6

.AUTO6D16
        INC     D
        EX      AF,AF'
        DEC     D
        DI
        LD      A,$0F
        OUT     (IOPORTFEH),A
        LD      HL,AUTO6D25
        PUSH    HL
        JP      AUTO0562

.AUTO6D25
        LD      A,$07
        OUT     (IOPORTFEH),A
        LD      A,$7F
        IN      A,(IOPORTFEH)
        RRA
        EI
        JR      C,AUTO6D4C
        CALL    AUTO703F
        CALL    PRINT_TXT
        DEFM    "*ABORTED",'*'+$80

        CALL    AUTO73A8
        CALL    AUTO703F
        LD      HL,FLAGS
        RES     5,(HL)
        OR      A

.AUTO6D4C
        RET

.AUTO6D4D
        CALL    AUTO7046
        CALL    AUTO703F
        CALL    AUTO701D
        CALL    AUTO70D3
        XOR     A
        LD      (AUTOBD4D),A
        LD      A,(AUTOBD2D)
        RRA
        LD      A,$83
        JR      C,AUTO6D67
        LD      A,$03

.AUTO6D67
        LD      (AUTOBCA2),A

.AUTO6D6A
        CALL    AUTO70E4
        CALL    PRINT_AT_12_0
        DEFM    "All-clear     Exit      new-GameOrient'n     "
        DEFM    " Sound             Toggle-colour Unoccupied  "
        DEFM    "      Pawn       kNight     Bishop    Rook   "
        DEFM    "    Queen      King      5-left 6-down 7-up 8"
        DEFM    "-righ",'t'+$80

        CALL    AUTO6FEF
        CALL    PRINT_TXT_0_0
        DEFM    "CHANGE POSITIO",'N'+$80

        LD      A,(AUTOBCA2)
        CP      $03
        JR      Z,AUTO6E50
        CALL    PRINT_AT_B_0
        DEFM    "WHIT",'E'+$80

        JR      AUTO6E58

.AUTO6E50
        CALL    PRINT_AT_B_0
        DEFM    "BLAC",'K'+$80


.AUTO6E58
        CALL    AUTO677E
        CALL    AUTO703F
        LD      A,(AUTOBCC0)
        CP      'E'
        JP      Z,AUTO6ECC
        CP      'G'
        JR      NZ,AUTO6E73
        CALL    AUTO7046
        CALL    AUTO6985
        JP      AUTO6D6A

.AUTO6E73
        CP      'T'
        JR      NZ,AUTO6E82
        LD      A,(AUTOBCA2)
        XOR     $80
        LD      (AUTOBCA2),A
        JP      AUTO6D6A

.AUTO6E82
        CP      'A'
        JR      NZ,AUTO6E8B
        CALL    AUTO658E
        JR      AUTO6EC4

.AUTO6E8B
        CP      'U'
        JR      NZ,AUTO6E98
        LD      A,(AUTOBCCA)
        LD      L,A
        LD      H,AUTO00BD
        XOR     A
        JR      AUTO6EB4

.AUTO6E98
        LD      HL,AUTO6F2F
        CALL    AUTO6F23
        JP      C,AUTO6D6A
        INC     HL
        LD      A,(HL)
        LD      (AUTOBCA3),A
        LD      A,(AUTOBCCA)
        LD      L,A
        LD      H,AUTO00BD
        LD      A,(AUTOBCA3)
        LD      B,A
        LD      A,(AUTOBCA2)
        ADD     A,B

.AUTO6EB4
        LD      C,(HL)
        LD      (HL),A
        BIT     0,C
        JR      NZ,AUTO6EC4
        BIT     2,C
        JR      Z,AUTO6EC4
        RES     2,C
        LD      L,C
        INC     H
        SET     7,(HL)

.AUTO6EC4
        LD      A,$02
        LD      (AUTOBD4D),A
        JP      AUTO6D6A

.AUTO6ECC
        LD      A,(AUTOBCA2)
        LD      HL,AUTOBD2D
        LD      B,(HL)
        RES     0,B
        SUB     $03
        LD      (AUTOBCBE),A
        LD      A,$02
        JR      Z,AUTO6EE2
        SET     0,B
        LD      A,$01

.AUTO6EE2
        LD      (AUTOBCC2),A
        LD      A,B
        CP      (HL)
        JR      Z,AUTO6EEF
        LD      (HL),B
        LD      A,$02
        LD      (AUTOBD4D),A

.AUTO6EEF
        LD      A,(AUTOBD4D)
        OR      A
        JP      Z,AUTO60B7
        CALL    AUTO65DA
        CALL    AUTO6F99
        LD      A,$0A
        LD      (AUTOBD49),A
        LD      A,$80
        LD      (AUTOBCA0),A
        CALL    AUTO7810
        JP      Z,AUTO6588
        CALL    AUTO6F77
        LD      A,$FF
        LD      (AUTOC058),A
        LD      A,(AUTOBCD4)
        OR      A
        JR      NZ,AUTO6F20
        CALL    AUTO7065
        CALL    AUTO729D

.AUTO6F20
        JP      AUTO60A7

.AUTO6F23
        LD      B,$06

.AUTO6F25
        CP      (HL)
        RET     Z
        INC     HL
        INC     HL
        INC     HL
        DJNZ    AUTO6F25
        SCF
        RET

.AUTO6F2E
        DEFM    $00

.AUTO6F2F
        DEFM    "K",$00,$09,"Q",$08,$05,"R",$10,$03+$80,"B "
        DEFM    $03,"N0",$01,"P@"

        DEFM    "(C) 1982 Intelligent Software Ltd."
        DEFM    "Author : Richard Lang"

.AUTO6F77
        LD      A,(AUTOBD2D)
        LD      (AUTOC005),A
        LD      A,(AUTOBD49)
        LD      (AUTOC006),A
        LD      HL,AUTOBE00
        LD      DE,AUTOC007
        LD      B,' '

.AUTO6F8B
        LD      A,(HL)
        LD      (DE),A
        INC     HL
        INC     DE
        LD      A,(HL)
        LD      (DE),A
        INC     DE
        LD      A,L
        ADD     A,$07
        LD      L,A
        DJNZ    AUTO6F8B
        RET

.AUTO6F99
        XOR     A
        LD      (AUTOBD0A),A
        LD      (AUTOBD0D),A
        LD      HL,AUTOBD59
        LD      B,$06

.AUTO6FA5
        LD      (HL),A
        INC     L
        DJNZ    AUTO6FA5
        RET

.AUTO6FAA
        LD      A,$FF
        LD      (AUTOBF00),A
        RET

.BEEP
        POP     HL
        LD      A,(HL)
        INC     HL
        LD      (AUTOBCD1),A
        LD      A,(HL)
        INC     HL
        LD      (AUTOBCD2),A
        PUSH    HL
        LD      A,$07
        LD      HL,AUTOBCD1
        LD      D,(HL)
        LD      E,$00
        LD      HL,AUTOBCD2

.AUTO6FC7
        LD      B,(HL)
        XOR     $10
        LD      C,A
        LD      A,(AUTOBCD3)
        OR      A
        LD      A,C
        JR      NZ,AUTO6FD4
        OUT     (IOPORTFEH),A

.AUTO6FD4
        DEC     E
        JR      NZ,AUTO6FD9
        DEC     D
        RET     Z

.AUTO6FD9
        DJNZ    AUTO6FD4
        JR      AUTO6FC7

.PRINT_AT
        POP     HL
        PUSH    AF
        CALL    AUTO6FE4
        POP     AF
        JP      (HL)

.AUTO6FE4
        LD      A,(HL)
        LD      (AUTOBCC5),A
        INC     HL
        LD      A,(HL)
        LD      (AUTOBCC6),A
        INC     HL
        RET

.AUTO6FEF
        LD      A,$18
        LD      HL,AUTOBCC5
        SUB     (HL)
        LD      C,A
        LD      A,' '
        LD      HL,AUTOBCC6
        SUB     (HL)
        LD      B,A
        LD      A,'('
        LD      (AUTOBCC4),A
        JR      AUTO7012

.AUTO7004
        CALL    PRINT_AT
        DEFW    $0000

        LD      A,$38
        LD      (AUTOBCC4),A    ;Scn Attribute
        LD      C,$12

.AUTO7010
        LD      B,' '

.AUTO7012
        LD      A,' '
        CALL    AUTO7226
        DJNZ    AUTO7012
        DEC     C
        JR      NZ,AUTO7010
        RET

.AUTO701D
        CALL    PRINT_TXT_AT
        DEFW    $0005

        DEFM    "           ",' '+$80

        CALL    PRINT_TXT_AT
        DEFW    $0406

        DEFM    "          ",' '+$80


.AUTO703E
        RET

.AUTO703F
        CALL    PRINT_AT
        DEFW    $000C

        JR      AUTO7052

.AUTO7046
        CALL    PRINT_AT
        DEFW    $000B

        JR      AUTO7052

.AUTO704D
        CALL    PRINT_AT
        DEFW    $000A


.AUTO7052
        CALL    PRINT_TEXT
        DEFM    "              ",' '+$80

        RET

.AUTO7065
        CALL    AUTO7071
        LD      A,'='
        LD      B,' '

.AUTO706C
        CALL    AUTO734D
        DJNZ    AUTO706C

.AUTO7071
        PUSH    AF
        PUSH    BC
        PUSH    HL
        LD      HL,$5B00
        XOR     A
        OUT     ($FB),A
        LD      B,$08

.AUTO707C
        IN      A,($FB)
        AND     $80
        JR      Z,AUTO707C
        LD      C,' '

.AUTO7084
        CALL    AUTO70A9
        LD      A,L
        ADD     A,$08
        LD      L,A
        DEC     C
        JR      NZ,AUTO7084
        INC     HL
        DJNZ    AUTO707C
        LD      A,$04
        OUT     ($FB),A
        CALL    AUTO709C
        POP     HL
        POP     BC
        POP     AF
        RET

.AUTO709C                       ;Clear ZXPrinter buffer
        XOR     A
        LD      (AUTOBCDC),A
        LD      B,A
        LD      HL,$5B00

.AUTO70A4
        LD      (HL),A
        INC     HL
        DJNZ    AUTO70A4
        RET

.AUTO70A9
        PUSH    BC
        LD      B,$08

.AUTO70AC
        IN      A,($FB)
        AND     $01
        JR      Z,AUTO70AC
        XOR     A
        RL      (HL)
        JR      NC,AUTO70B9
        SET     7,A

.AUTO70B9
        OUT     ($FB),A
        DJNZ    AUTO70AC
        POP     BC
        RET

.AUTO70BF
        LD      A,(IX+$01)
        CALL    AUTO70E1
        JR      AUTO70CD

.AUTO70C7
        LD      A,(IX+$02)
        CALL    AUTO70E1

.AUTO70CD
        CALL    BEEP
        DEFW    $2896

        RET

.AUTO70D3
        LD      A,(AUTOBD2D)
        RRA
        LD      A,$10
        JR      NC,AUTO70E1
        LD      A,'`'
        JR      AUTO70E1

;Draw the chess board and pieces
.AUTO70DF
        LD      A,$FF           ;Don't print cursor

.AUTO70E1
        LD      (AUTOBCCA),A    ;Cursor postion

.AUTO70E4
        EXX
        LD      H,AUTO00BE      ;Piece info pointer
        EXX
        LD      HL,AUTOBD00     ;Address of the chess board data
        LD      B,$00           ;Scn y-coord

.AUTO70ED
        LD      C,$10           ;Scn x-coord

.AUTO70EF
        LD      A,L             ;Reach end for printing rank?
        AND     $11
        OR      A
        JR      Z,AUTO70F7
        CP      $11

.AUTO70F7
        LD      A,(AUTOBCDA)    ;WSq colour
        JR      Z,AUTO70FF      ;Skip if on a WSq
        LD      A,(AUTOBCD9)    ;Else pick up BSq colour

.AUTO70FF
        SLA     A               ;Move colour into the PAPER position
        SLA     A
        SLA     A
        LD      D,A             ;Put Sq colour in D
        LD      A,(AUTOBCD7)    ;Get BPc colour
        ADD     A,D
        OR      $40             ;Make bright
        LD      (AUTOBCC4),A    ;Store Pc attribute
        PUSH    HL              ;Save the board pointer
        LD      A,(AUTOBCC7)    ;Orientation of the board?
        OR      A               ;If 0, W at bottom, else W at top
        JR      Z,AUTO711A      ;Skip if W at bottom
        LD      A,$77
        SUB     L
        LD      L,A

.AUTO711A
        LD      A,(AUTOBCCA)    ;Is the cursor on this Sq?
        CP      L
        JR      NZ,AUTO7128     ;Skip if not
        LD      A,(AUTOBCC4)    ;Set FLASH bit of the Sq/Pc attribute
        OR      $80             ;to signify the cursor
        LD      (AUTOBCC4),A

.AUTO7128
        LD      A,(HL)          ;Now find what Pc, if any, is on this Sq
        POP     HL              ;Retrieve the board pointer
        EXX
        OR      A               ;If Sq info is 0, this is an empty square
        JR      NZ,AUTO7137     ;Skip if so
        CALL    PRINT_PIECE     ;Print an empty square
        DEFW    $2020,$2020

        JR      AUTO719B

.AUTO7137
        LD      D,A             ;Store Pc info in D
        AND     $F8             ;Identify the colour of the piece
        INC     A
        LD      L,A
        BIT     7,L
        JR      Z,AUTO714D      ;Skip if a black piece
        LD      A,(AUTOBCC4)    ;Sq/Pc attribute
        AND     $F8             ;Clear the INK for BPc
        LD      E,A
        LD      A,(AUTOBCD8)    ;Get WPc colour
        OR      E
        LD      (AUTOBCC4),A    ;Merge WPc colour

.AUTO714D
        LD      A,D             ;Get Pc info
        AND     $7F
        CP      $43
        JR      Z,AUTO7166
        LD      A,$0F
        AND     (HL)
        JR      NZ,AUTO7162
        CALL    PRINT_PIECE     ;Print a King
        DEFW    $A5A4,$A3A2

        JR      AUTO719B

.AUTO7162
        CP      $01
        JR      NZ,AUTO716F

.AUTO7166
        CALL    PRINT_PIECE     ;Print a Pawn
        DEFW    $9190,$9392

        JR      AUTO719B

.AUTO716F
        CP      $05
        JR      NZ,AUTO717C
        CALL    PRINT_PIECE     ;Print a Rook
        DEFW    $9D9C,$9F9E

        JR      AUTO719B

.AUTO717C
        JR      C,AUTO7187
        CALL    PRINT_PIECE     ;Print a Queen
        DEFW    $A1A0,$A3A2

        JR      AUTO719B

.AUTO7187
        BIT     7,(HL)
        JR      NZ,AUTO7194
        CALL    PRINT_PIECE     ;Print a Knight
        DEFW    $9594,$9796

        JR      AUTO719B

.AUTO7194
        CALL    PRINT_PIECE     ;Print a Bishop
        DEFW    $9998,$9B9A


.AUTO719B
        EXX
        INC     C               ;Move x-coord along for next Sq
        INC     C
        INC     HL              ;Move board pointer to next Sq info
        BIT     3,L             ;End of the rank?
        JP      Z,AUTO70EF      ;Loop back if not
        INC     B               ;Move y-coord down for next rank
        INC     B
        LD      DE,$0008        ;Advance board pointer to next rank
        ADD     HL,DE
        BIT     7,L             ;At the bottom of the board?
        JP      Z,AUTO70ED      ;Loop back if not
        LD      A,$01           ;Print rank numbers
        LD      (AUTOBCC5),A    ;Scn y-coord
        LD      A,$0F
        LD      (AUTOBCC6),A    ;Scn x-coord
        LD      A,$38
        LD      (AUTOBCC4),A    ;Scn attribute
        LD      B,$08
        LD      C,$00

.AUTO71C2
        LD      A,(AUTOBCC7)    ;Board orientation
        OR      A
        LD      A,C
        JR      NZ,AUTO71CC
        LD      A,$07
        SUB     C

.AUTO71CC
        ADD     A,'1'
        CALL    AUTO7226
        LD      HL,AUTOBCC6
        DEC     (HL)
        LD      HL,AUTOBCC5     ;Scn y-coord
        INC     (HL)
        INC     (HL)
        INC     C
        DJNZ    AUTO71C2
        LD      A,$10           ;Print file letters
        LD      (AUTOBCC5),A    ;Scn y-coord
        LD      (AUTOBCC6),A    ;Scn x-coord
        LD      B,$08
        LD      C,$00

.AUTO71E9
        LD      A,(AUTOBCC7)    ;Board orientation
        OR      A
        LD      A,C
        JR      Z,AUTO71F3
        LD      A,$07
        SUB     C

.AUTO71F3
        ADD     A,'A'
        CALL    AUTO7226
        LD      HL,AUTOBCC6     ;Scn x-coord
        INC     (HL)
        INC     C
        DJNZ    AUTO71E9
        RET                     ;Done

.PRINT_PIECE
        EXX
        LD      A,B
        LD      (AUTOBCC5),A    ;Scn y-coord
        LD      A,C
        LD      (AUTOBCC6),A    ;Scn x-coord
        EXX
        EX      (SP),HL
        CALL    AUTO7224
        CALL    AUTO7224
        EXX
        LD      A,B
        INC     A
        LD      (AUTOBCC5),A    ;Scn y-coord
        LD      A,C
        LD      (AUTOBCC6),A    ;Scn x-coord
        EXX
        CALL    AUTO7224
        CALL    AUTO7224
        EX      (SP),HL
        RET

.AUTO7224
        LD      A,(HL)
        INC     HL

.AUTO7226
        LD      (AUTOBCC1),A
        PUSH    AF
        PUSH    BC
        PUSH    DE
        PUSH    HL
        LD      H,$00
        LD      A,(AUTOBCC5)
        LD      L,A
        ADD     HL,HL
        ADD     HL,HL
        ADD     HL,HL
        ADD     HL,HL
        ADD     HL,HL
        LD      A,(AUTOBCC6)
        ADD     A,L
        LD      L,A
        LD      BC,$5800
        ADD     HL,BC
        LD      A,(AUTOBCC4)
        LD      (HL),A
        LD      A,(AUTOBCC1)
        LD      H,$00
        LD      L,A
        ADD     HL,HL
        ADD     HL,HL
        ADD     HL,HL
        CP      $90
        JR      NC,AUTO7259
        LD      BC,(AUTOBCD5)
        JP      AUTO725C

.AUTO7259
        LD      BC,CHESS_UDGS-$0480

.AUTO725C
        ADD     HL,BC
        LD      D,H
        LD      E,L
        LD      A,(AUTOBCC5)
        AND     $18
        LD      H,A
        LD      A,(AUTOBCC5)
        AND     $07
        SLA     A
        SLA     A
        SLA     A
        SLA     A
        SLA     A
        LD      L,A
        LD      A,(AUTOBCC6)
        ADD     A,L
        LD      L,A
        LD      BC,$4000
        ADD     HL,BC
        LD      B,$08

.AUTO7280
        LD      A,(DE)
        LD      (HL),A
        INC     DE
        INC     H
        DJNZ    AUTO7280
        LD      A,(AUTOBCC6)
        INC     A
        CP      ' '
        JR      C,AUTO7293
        XOR     A
        LD      HL,AUTOBCC5
        INC     (HL)

.AUTO7293
        LD      (AUTOBCC6),A
        POP     HL
        POP     DE
        POP     BC
        POP     AF
        RET

.AUTO729B
        JP      (IY)

.AUTO729D
        EXX
        LD      H,AUTO00BE
        EXX
        LD      HL,AUTOBD00
        CALL    AUTO7071
        LD      B,$08

.AUTO72A9
        DEC     B
        LD      A,' '
        CALL    AUTO734D
        CALL    AUTO734D
        LD      A,(AUTOBCC7)
        OR      A
        LD      A,B
        JR      Z,AUTO72BC
        LD      A,$07
        SUB     B

.AUTO72BC
        ADD     A,'1'
        CALL    AUTO734D

.AUTO72C1
        LD      A,' '
        CALL    AUTO734D
        PUSH    HL
        LD      A,(AUTOBCC7)
        OR      A
        JR      Z,AUTO72D1
        LD      A,'w'
        SUB     L
        LD      L,A

.AUTO72D1
        LD      A,(HL)
        POP     HL
        EXX
        OR      A
        JR      NZ,AUTO72DB
        LD      A,'.'
        JR      AUTO7305

.AUTO72DB
        AND     $F8
        INC     A
        LD      L,A
        LD      A,$0F
        AND     (HL)
        LD      C,'K'
        JR      Z,AUTO72FE
        CP      $01
        LD      C,'P'
        JR      Z,AUTO72FE
        CP      $05
        LD      C,'R'
        JR      Z,AUTO72FE
        LD      C,'Q'
        JR      NC,AUTO72FE
        BIT     7,(HL)
        LD      C,'N'
        JR      Z,AUTO72FE
        LD      C,'B'

.AUTO72FE
        LD      A,C
        BIT     7,L
        JR      NZ,AUTO7305
        OR      ' '

.AUTO7305
        CALL    AUTO734D
        EXX
        INC     HL
        BIT     3,L
        JR      Z,AUTO72C1
        CALL    AUTO7071
        CALL    AUTO7071
        LD      DE,$0008
        ADD     HL,DE
        BIT     7,L
        JP      Z,AUTO72A9
        LD      A,' '
        CALL    AUTO734D
        CALL    AUTO734D
        CALL    AUTO734D
        LD      B,$08
        LD      C,$00

.AUTO732C
        LD      A,' '
        CALL    AUTO734D
        LD      A,(AUTOBCC7)
        OR      A
        LD      A,C
        JR      Z,AUTO733B
        LD      A,$07
        SUB     C

.AUTO733B
        ADD     A,'A'
        CALL    AUTO734D
        LD      HL,AUTOBCC6
        INC     (HL)
        INC     C
        DJNZ    AUTO732C
        CALL    AUTO7071
        JP      AUTO7071

.AUTO734D
        PUSH    AF
        PUSH    BC
        PUSH    DE
        PUSH    HL
        LD      H,$00
        LD      L,A
        ADD     HL,HL
        ADD     HL,HL
        ADD     HL,HL
        LD      BC,(AUTOBCD5)
        ADD     HL,BC
        LD      D,H
        LD      E,L
        LD      A,(AUTOBCDC)
        LD      H,$00
        LD      L,A
        ADD     HL,HL
        ADD     HL,HL
        ADD     HL,HL
        LD      BC,$5B00
        ADD     HL,BC
        LD      B,$08

.AUTO736D
        LD      A,(DE)
        LD      (HL),A
        INC     DE
        INC     HL
        DJNZ    AUTO736D
        LD      HL,AUTOBCDC
        INC     (HL)
        POP     HL
        POP     DE
        POP     BC
        POP     AF
        RET

.AUTO737C
        CALL    PRINT_TXT_AT
        DEFW    $000E

        DEFM    "LEVEL",' '+$80

        LD      A,(AUTOBCCD)
        PUSH    AF
        AND     $08
        LD      A,' '
        JR      Z,AUTO7393
        LD      A,'P'

.AUTO7393
        CALL    AUTO7226
        POP     AF
        AND     $07
        ADD     A,'1'
        JP      AUTO7226

.AUTO739E
        CALL    PRINT_TXT
        DEFM    "ILLEGA",'L'+$80


.AUTO73A8
        CALL    BEEP
        DEFW    $80FF

        RET

.AUTO73AE
        CALL    PRINT_AT_12_0
        DEFM    "-Hit any key to restore control",'-'+$80

        JP      AUTO6FEF

.AUTO73D4
        CALL    PRINT_AT_12_0
        DEFM    "   --Hit BREAK key to abort-",'-'+$80

        JP      AUTO6FEF

.PRINT_TXT
        CALL    PRINT_AT
        DEFW    $000C

        JR      PRINT_TEXT

.PRINT_AT_B_0
        CALL    PRINT_AT
        DEFW    $000B

        JR      PRINT_TEXT

.PRINT_AT_12_0
        CALL    PRINT_AT
        DEFW    $0012

        LD      A,'('
        JR      PRINT_TEXT2

.PRINT_TXT_0_0
        CALL    PRINT_AT
        DEFW    $000A

        JR      PRINT_TEXT

.PRINT_TXT_AT
        POP     HL
        CALL    AUTO6FE4
        PUSH    HL

.PRINT_TEXT
        LD      A,'8'

.PRINT_TEXT2
        LD      (AUTOBCC4),A
        POP     HL

.AUTO7420
        LD      A,(HL)
        AND     $7F
        CALL    AUTO7226
        BIT     7,(HL)
        INC     HL
        JR      Z,AUTO7420
        JP      (HL)

.AUTO742C
        LD      A,(AUTOBCD4)
        OR      A
        JR      Z,AUTO7437
        LD      A,(AUTOBCDB)
        OR      A
        RET     Z

.AUTO7437
        LD      A,$07
        JR      AUTO7441

.AUTO743B
        LD      A,(AUTOBCD4)    ;Print listing on?
        OR      A
        RET     NZ
        XOR     A

.AUTO7441
        LD      (AUTOBCDC),A

.AUTO7444
        LD      A,(HL)
        AND     $7F
        CALL    AUTO734D
        BIT     7,(HL)
        INC     HL
        JR      Z,AUTO7444
        JP      AUTO7071

.AUTO7452
        LD      IY,AUTO734D
        LD      A,(AUTOBD2D)
        RRA
        JR      NC,AUTO7463
        CALL    AUTO7511
        LD      A,$04
        JR      AUTO7465

.AUTO7463
        LD      A,$0E

.AUTO7465
        LD      (AUTOBCDC),A
        CALL    AUTO749F
        JP      AUTO7071

;Print moves
.AUTO746E                       ;Print move
        LD      IY,AUTO7226
        LD      A,$38
        LD      (AUTOBCC4),A    ;Scn attribute
        CALL    AUTO751B        ;Print move number
        LD      A,(AUTOBD2D)    ;Whose move was played
        RRA
        JR      NC,AUTO7497     ;Skip if black's
        CALL    PRINT_TXT_AT    ;Prepare to print W's move
        DEFW    $0406

        DEFM    "          ",' '+$80

        CALL    PRINT_AT
        DEFW    $0405

        JR      AUTO749F

.AUTO7497
        CALL    PRINT_TXT_AT    ;Prepare to print B's move
        DEFW    $0406

        DEFM    "..",'.'+$80


.AUTO749F
        LD      A,(IX+$01)      ;Get From Sq
        CALL    AUTO74FB        ;Print From Sq
        LD      A,'-'           ;To an empty Sq?
        BIT     3,(IX+$03)      ;Or was a piece taken?
        JR      Z,AUTO74AF      ;Jump if to an empty Sq
        LD      A,'x'

.AUTO74AF
        CALL    AUTO729B        ;Print the move type
        LD      A,(IX+$02)      ;Get the To Sq
        CALL    AUTO74FB        ;Print To Sq
        LD      HL,AUTOBCDE     ;See if a pawn was promoted
        LD      A,(HL)
        OR      A
        JR      Z,AUTO74DD      ;Skip if a pawn wasn't promoted
        LD      A,'/'           ;Print this character
        CALL    AUTO729B
        LD      A,(HL)          ;And then the piece promoted to.
        AND     $0F
        CP      $05
        LD      C,'R'
        JR      Z,AUTO74D9
        LD      C,'Q'
        JR      NC,AUTO74D9
        BIT     7,(HL)
        LD      C,'N'
        JR      Z,AUTO74D9
        LD      C,'B'

.AUTO74D9
        LD      A,C
        CALL    AUTO729B        ;Print the piece promoted to

.AUTO74DD
        BIT     7,(IX+$03)      ;Is the enemy king in check?
        LD      A,' '
        JR      Z,AUTO74E7      ;Skip if not in check
        LD      A,'+'

.AUTO74E7
        CALL    AUTO729B        ;Print the check symbol (or space)
        LD      A,(AUTOBCDE)    ;Last promoted piece
        OR      A               ;If no promotion print 2 spaces
        LD      A,' '
        CALL    Z,AUTO729B
        CALL    Z,AUTO729B
        RET                     ;Done

.AUTO74F7                       ;Prints Sq reference
        LD      IY,AUTO7226

.AUTO74FB
        LD      B,A
        AND     $07
        ADD     A,'A'
        CALL    AUTO729B
        LD      A,'p'
        AND     B
        RRA
        RRA
        RRA
        RRA
        NEG
        ADD     A,'8'
        JP      AUTO729B

.AUTO7511
        LD      IY,AUTO734D
        XOR     A
        LD      (AUTOBCDC),A
        JR      AUTO7524

.AUTO751B
        LD      IY,AUTO7226
        CALL    PRINT_AT
        DEFW    $0005


.AUTO7524
        LD      A,(AUTOBD49)
        LD      B,$FF

.AUTO7529
        INC     B
        SUB     $0A
        JR      NC,AUTO7529
        PUSH    AF
        LD      A,B
        CP      $0A
        JR      C,AUTO7548
        LD      B,$FF

.AUTO7536
        INC     B
        SUB     $0A
        JR      NC,AUTO7536
        PUSH    AF
        LD      A,B
        ADD     A,'0'
        CALL    AUTO729B
        POP     AF
        ADD     A,$0A
        JP      AUTO7552

.AUTO7548
        PUSH    AF
        LD      A,' '
        CALL    AUTO729B
        POP     AF
        OR      A
        JR      Z,AUTO7557

.AUTO7552
        ADD     A,'0'
        CALL    AUTO729B

.AUTO7557
        POP     AF
        ADD     A,':'
        CALL    AUTO729B
        LD      A,' '
        JP      AUTO729B

.AUTO7562
        LD      A,(AUTOBCBE)
        OR      A
        LD      A,$02
        JR      Z,AUTO756B
        DEC     A

.AUTO756B
        LD      (AUTOBCC2),A

.AUTO756E
        CALL    PRINT_AT
        DEFW    $0100

        LD      A,(AUTOBCC2)
        AND     $02
        CALL    AUTO7586
        CALL    PRINT_TEXT
        DEFM    " v",' '+$80

        LD      A,(AUTOBCC2)
        AND     $01

.AUTO7586
        JR      Z,AUTO7591
        CALL    PRINT_TEXT
        DEFM    "CYRU",'S'+$80
        RET

.AUTO7591
        CALL    PRINT_TEXT
        DEFM    "HUMA",'N'+$80

        RET

.AUTO759A
        CALL    PRINT_TXT_AT
        DEFW    $0010

        DEFM    "LISTING",' '+$80

        LD      A,(AUTOBCD4)
        OR      A
        JR      Z,AUTO75C5

.AUTO75AD
        CALL    PRINT_TEXT
        DEFM    "OF",'F'+$80

        RET

.AUTO75B4
        CALL    PRINT_TXT_AT
        DEFW    $000F

        DEFM    "SOUND",' '+$80

        LD      A,(AUTOBCD3)
        OR      A
        JR      NZ,AUTO75AD

.AUTO75C5
        CALL    PRINT_TEXT
        DEFM    "ON",' '+$80

        RET

.CHESS_UDGS
        DEFM    $00,$00,$00,$00,$01,$03,$03,$01,$00,$00,$00,$00
        DEFM    $00+$80,'@'+$80,'@'+$80,$00+$80,$03,$01,$01,$01
        DEFM    $03,$07,$00,$00,'@'+$80,$00+$80,$00+$80,$00+$80
        DEFM    '@'+$80,'`'+$80,$00,$00,$00,$00,$01,$03,$06,$0F
        DEFM    $1F,$1E,$00,$00,"@",'`'+$80,'p'+$80,'p'+$80,'p'+$80
        DEFM    'p'+$80,$0D,$01,$03,$03,$07,$07,$00,$00,'p'+$80
        DEFM    'x'+$80,'x'+$80,'x'+$80,'x'+$80,'x'+$80,$00,$00
        DEFM    $00,$00,$01,$03,$06,$0E,$18,$18,$00,$00,$00+$80
        DEFM    '@'+$80,"`p",$18,$18,$0E,$06,$03,$01,$1D,"7",$00
        DEFM    $00,"p`",'@'+$80,$00+$80,'8'+$80,'l'+$80,$00,$00
        DEFM    $00,$00,$00,$00,$0A,$0F,$07,$03,$00,$00,$00,$00,"P"
        DEFM    'p'+$80,'`'+$80,'@'+$80,$03,$03,$03,$07,$0F,$1F,$00
        DEFM    $00,'@'+$80,'@'+$80,'@'+$80,'`'+$80,'p'+$80,'x'+$80
        DEFM    $00,$00,$00,$00,$04,$0E,"$t$?",$00,$00," p$.$",'|'+$80
        DEFM    "??",$10,$1F,$08,$0F,$00,$00,'|'+$80,'|'+$80,$08,'x'+$80
        DEFM    $10,'p'+$80,$00,$00,$00,$00,$01,$01,$07,"71=",$00
        DEFM    $00,$00+$80,$00+$80,'`'+$80,'l'+$80,$0C+$80,'<'+$80
        DEFM    $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
        DEFM    $00,$00,$00,$00

        DEFS    $0174

.AUTO7800
        DEFM    $0E,$12,$1F,"!",'r'+$80,'n'+$80,'a'+$80,'_'+$80

.AUTO7808
        DEFM    $0F,$11,'o'+$80,'q'+$80

.AUTO780C
        DEFM    $01,$FF,$10,'p'+$80

.AUTO7810
        LD      IX,AUTOBD58
        LD      A,(AUTOBD2D)
        XOR     $01
        LD      (AUTOBD2D),A
        CALL    AUTO8400
        PUSH    AF
        LD      A,(AUTOBCA0)
        ADD     A,$80
        LD      E,A
        LD      A,(AUTOBCBE)
        OR      A
        JR      Z,AUTO7832
        XOR     A
        SUB     B
        LD      B,A
        XOR     A
        SUB     E
        LD      E,A

.AUTO7832
        LD      A,(AUTOBD2E)
        SRA     A
        SRA     E
        ADD     A,E
        SRA     A
        SRA     A
        JR      Z,AUTO7841
        INC     A

.AUTO7841
        LD      (AUTOBD1E),A
        LD      A,(AUTOBD4A)
        ADD     A,B
        LD      (AUTOBD4A),A
        LD      HL,AUTOBE08
        XOR     A
        LD      (AUTOBD6D),A
        CALL    AUTO789D
        LD      E,C
        LD      L,$88
        CALL    AUTO789D
        LD      A,C
        SUB     E
        CP      $0E
        JR      C,AUTO786C
        CP      $F2
        JR      NC,AUTO786C
        RLA
        LD      A,$0E
        JR      NC,AUTO786C
        LD      A,$F2

.AUTO786C
        ADD     A,A
        ADD     A,A
        ADD     A,A
        LD      (AUTOBD2E),A
        LD      A,(AUTOBD49)
        LD      B,$00
        CP      $08
        JR      C,AUTO7891
        LD      A,C
        ADD     A,E
        SET     1,B
        CP      '4'
        JR      NC,AUTO7891
        SET     2,B
        CP      '('
        JR      NC,AUTO7891
        SET     3,B
        CP      $19
        JR      NC,AUTO7891
        SET     4,B

.AUTO7891
        LD      HL,AUTOBD2D
        LD      A,(HL)
        INC     A
        AND     $01
        ADD     A,B
        LD      (HL),A
        POP     AF
        OR      A
        RET

.AUTO789D
        LD      C,$00
        LD      B,$0F

.AUTO78A1
        BIT     7,(HL)
        JR      NZ,AUTO78B3
        INC     L
        LD      A,(HL)
        AND     $0F
        CP      $01
        JR      NZ,AUTO78B0
        LD      (AUTOBD6D),A

.AUTO78B0
        ADD     A,C
        LD      C,A
        DEC     L

.AUTO78B3
        LD      A,$08
        ADD     A,L
        LD      L,A
        DJNZ    AUTO78A1
        RET

.AUTO78BA
        LD      HL,AUTOBD4C
        BIT     7,(HL)
        JR      NZ,AUTO78CE
        CALL    AUTO6734
        JR      Z,AUTO78CE
        LD      HL,AUTOBD4C
        SET     7,(HL)
        CALL    AUTO7562

.AUTO78CE
        LD      HL,AUTOBCA9
        LD      A,(HL)
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        INC     L
        PUSH    DE
        INC     E
        INC     E
        INC     E
        INC     DE
        PUSH    HL
        EX      AF,AF'
        LD      A,AUTO00BC
        EX      AF,AF'
        LD      H,AUTO00BD
        LD      IY,AUTOBE00
        LD      A,(AUTOBD2D)
        EXX
        RRCA
        LD      C,A
        EXX
        JR      NC,AUTO78F6
        LD      IY,AUTOBE80

.AUTO78F6
        LD      B,(IY+$00)
        BIT     4,(IY+$01)
        CALL    Z,AUTO7A61
        LD      C,$88
        EXX
        LD      DE,AUTO7808
        CALL    AUTO7A18
        LD      A,(IY+$08)
        OR      A
        CALL    P,AUTO7A57
        LD      A,(IY+$10)
        OR      A
        CALL    P,AUTO7A2E
        LD      A,(IY+$18)
        OR      A
        CALL    P,AUTO7A2E
        LD      A,(IY+$20)
        OR      A
        CALL    P,AUTO7A4F
        LD      A,(IY+$28)
        OR      A
        CALL    P,AUTO7A4F
        LD      A,(IY+$30)
        LD      C,$88
        OR      A
        CALL    P,AUTO7A13
        LD      A,(IY+$38)
        OR      A
        CALL    P,AUTO7A13
        LD      C,$11
        EXX
        LD      HL,AUTOBE40
        BIT     7,C
        JR      Z,AUTO794C
        LD      L,$C0
        EXX
        LD      C,$F1
        EXX

.AUTO794C
        LD      A,(HL)
        RLA
        JR      C,AUTO799C
        INC     L
        LD      A,(HL)
        DEC     L
        AND     $0E
        LD      A,(HL)
        EXX
        LD      B,A
        JR      NZ,AUTO79C8
        ADD     A,C
        LD      L,A
        XOR     A
        OR      (HL)
        CALL    NZ,AUTO7A97
        DEC     L
        DEC     L
        XOR     A
        OR      (HL)
        CALL    NZ,AUTO7A97
        INC     L
        XOR     A
        OR      (HL)
        JR      NZ,AUTO7994
        CALL    AUTO7A9D
        LD      A,B
        ADD     A,' '
        BIT     6,A
        JR      NZ,AUTO7994
        LD      A,C
        DEC     A
        ADD     A,L
        JP      M,AUTO799B
        LD      L,A
        XOR     A
        OR      (HL)
        JR      NZ,AUTO799B
        CALL    AUTO7A9D
        DEC     L
        OR      (HL)
        CALL    NZ,AUTO79FC
        INC     L
        INC     L
        LD      A,(HL)
        OR      A
        CALL    NZ,AUTO79FC
        JP      AUTO799B

.AUTO7994
        BIT     5,(IX+$03)
        CALL    NZ,AUTO79E4

.AUTO799B
        EXX

.AUTO799C
        LD      A,$08
        ADD     A,L
        LD      L,A
        BIT     6,A
        JP      NZ,AUTO794C
        EXX
        LD      A,$FF
        LD      (DE),A
        POP     HL
        LD      (HL),E
        INC     L
        LD      (HL),D
        POP     HL
        EX      DE,HL
        OR      A
        SBC     HL,DE
        RR      H
        RR      L
        RR      L
        DEC     L
        EX      DE,HL
        LD      (HL),A
        INC     L
        LD      (HL),E
        INC     L
        LD      (HL),$00
        INC     L
        LD      A,(IX+$03)
        AND     $81
        LD      (HL),A
        RET

.AUTO79C8
        PUSH    BC
        LD      C,$88
        EXX
        INC     L
        LD      A,(HL)
        DEC     L
        LD      DE,AUTO79E0
        PUSH    DE
        OR      A
        JP      M,AUTO7A51
        RRA
        RRA
        JR      C,AUTO7A15
        RRA
        JR      C,AUTO7A30
        JR      AUTO7A59
.AUTO79E0
        POP     BC
        EXX
        JR      AUTO799C

.AUTO79E4
        BIT     3,(IX+$03)
        RET     NZ
        LD      A,(IX+$02)
        INC     A
        CP      B
        JR      Z,AUTO79F6
        DEC     A
        DEC     A
        CP      B
        RET     NZ
        INC     L
        INC     L

.AUTO79F6
        DEC     L
        LD      A,'('
        JP      AUTO7A9D

.AUTO79FC
        EXX
        XOR     C
        EXX
        RET     P
        LD      A,(HL)
        EXX
        LD      B,L
        SUB     $03
        LD      L,A
        LD      A,(HL)
        LD      L,B
        EXX
        AND     $0F
        DEC     A
        RET     NZ
        LD      A,' '
        DEC     DE
        LD      (DE),A
        INC     DE
        RET

.AUTO7A13
        LD      B,A
        EXX

.AUTO7A15
        LD      DE,AUTO7800

.AUTO7A18
        LD      B,$08

.AUTO7A1A
        LD      A,(DE)
        INC     E
        EXX
        ADD     A,B
        LD      L,A
        AND     C
        JR      NZ,AUTO7A29
        OR      (HL)
        CALL    Z,AUTO7A9D
        CALL    NZ,AUTO7A97

.AUTO7A29
        EXX
        DJNZ    AUTO7A1A
        EXX
        RET

.AUTO7A2E
        LD      B,A
        EXX

.AUTO7A30
        LD      DE,AUTO780C

.AUTO7A33
        LD      B,$04

.AUTO7A35
        LD      A,(DE)
        INC     E
        EXX
        LD      C,A
        LD      L,B

.AUTO7A3A
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     $88
        JR      NZ,AUTO7A4A
        OR      (HL)
        CALL    Z,AUTO7A9D
        JR      Z,AUTO7A3A
        CALL    AUTO7A97

.AUTO7A4A
        EXX
        DJNZ    AUTO7A35
        EXX
        RET

.AUTO7A4F
        LD      B,A
        EXX

.AUTO7A51
        LD      DE,AUTO7808
        JP      AUTO7A33

.AUTO7A57
        LD      B,A
        EXX

.AUTO7A59
        LD      DE,AUTO7808
        LD      B,$08
        JP      AUTO7A35

.AUTO7A61
        BIT     7,(IX+$03)
        RET     NZ
        BIT     4,(IY+$19)
        JR      NZ,AUTO7A7D
        BIT     7,(IY+$18)
        JR      NZ,AUTO7A7D
        XOR     A
        LD      L,B
        INC     L
        OR      (HL)
        JR      NZ,AUTO7A7D
        INC     L
        OR      (HL)
        CALL    Z,AUTO7A93

.AUTO7A7D
        BIT     4,(IY+$11)
        RET     NZ
        BIT     7,(IY+$10)
        RET     NZ
        LD      L,B
        XOR     A
        DEC     L
        OR      (HL)
        RET     NZ
        DEC     L
        OR      (HL)
        RET     NZ
        DEC     L
        OR      (HL)
        RET     NZ
        INC     L

.AUTO7A93
        LD      A,$10
        JR      AUTO7A9D

.AUTO7A97
        EXX
        XOR     C
        EXX
        RET     P
        LD      A,$08

.AUTO7A9D
        EX      AF,AF'
        CP      D
        JR      Z,AUTO7AAF

.AUTO7AA1
        EX      AF,AF'
        EX      DE,HL
        LD      (HL),$FF
        INC     L
        LD      (HL),B
        INC     L
        LD      (HL),E
        INC     L
        LD      (HL),A
        INC     HL
        EX      DE,HL
        OR      A
        RET

.AUTO7AAF
        LD      A,$80
        CP      E
        LD      A,AUTO00BC
        JR      NC,AUTO7AA1
        PUSH    HL
        LD      HL,AUTOBD4C
        SET     7,(HL)
        POP     HL
        EX      AF,AF'
        OR      A
        RET

.AUTO7AC0
        LD      A,$FF
        LD      (AUTOBCDF),A
        OR      A
        RET

.AUTO7AC7
        LD      A,(AUTOBCDF)
        OR      A
        RET     NZ
        LD      A,(AUTOC006)
        DEC     A
        RET     NZ
        CALL    AUTO7B89
        LD      HL,$FFFF

.AUTO7AD7
        INC     HL
        LD      A,(AUTOC001)
        LD      C,A
        LD      A,(AUTOBCDF)
        INC     A
        LD      (AUTOBCDF),A
        DEC     A
        CP      C
        JR      Z,AUTO7B05
        LD      BC,AUTOC058
        ADD     A,C
        LD      C,A
        JR      NC,AUTO7AEF
        INC     B

.AUTO7AEF
        LD      A,(BC)
        AND     $7F
        LD      C,A
        CALL    AUTO7B57

.AUTO7AF6
        LD      B,A
        RES     6,A
        CP      C
        JR      Z,AUTO7AD7
        BIT     6,B
        JR      Z,AUTO7AC0
        CALL    AUTO7B4C
        JR      AUTO7AF6

.AUTO7B05
        CALL    AUTO7B89
        CALL    AUTO7B57
        LD      C,A
        BIT     6,A
        JR      Z,AUTO7B30
        LD      A,$01
        RLA
        AND     B
        JR      NZ,AUTO7B30

.AUTO7B16
        PUSH    HL
        CALL    AUTO7B89
        CALL    AUTO7B4C
        JR      C,AUTO7B2B
        POP     DE
        LD      C,A
        BIT     6,A
        JR      Z,AUTO7B30
        RR      B
        JR      C,AUTO7B16
        JR      AUTO7B30

.AUTO7B2B
        POP     HL
        CALL    AUTO7B57
        LD      C,A

.AUTO7B30
        LD      A,C
        AND     '?'
        RLA
        RLA
        LD      E,A
        LD      D,$00
        LD      IX,AUTOB404
        ADD     IX,DE
        LD      (AUTOBD29),IX
        XOR     A
        BIT     7,C
        JR      NZ,AUTO7B4A
        LD      (AUTOBCDF),A

.AUTO7B4A
        XOR     A
        RET

.AUTO7B4C
        LD      DE,$FFFF

.AUTO7B4F
        CALL    AUTO7B57
        INC     HL
        BIT     7,D
        JR      Z,AUTO7B4F

.AUTO7B57
        CALL    AUTO7B64
        BIT     7,A
        JR      Z,AUTO7B5F
        DEC     DE

.AUTO7B5F
        BIT     6,A
        RET     Z
        INC     DE
        RET

.AUTO7B64
        PUSH    BC
        PUSH    HL
        LD      A,L
        SRL     H
        RRA
        SRL     H
        RRA
        SRL     H
        RRA
        LD      B,H
        LD      C,A
        POP     HL
        PUSH    HL
        LD      A,$07
        AND     L
        ADD     HL,BC
        LD      BC,BASE
        ADD     HL,BC
        LD      B,A
        INC     B
        LD      A,(HL)
        INC     HL
        LD      C,(HL)

.AUTO7B81
        RL      C
        RLA
        DJNZ    AUTO7B81
        POP     HL
        POP     BC
        RET

.AUTO7B89
        LD      A,R
        LD      B,A
        LD      A,(AUTOBCCE)
        XOR     B
        RRA
        LD      (AUTOBCCE),A
        LD      B,A
        RET

.AUTO7B96
        INC     HL
        PUSH    HL
        POP     IX
        XOR     A
        LD      (AUTOBD19),A
        INC     E
        LD      A,E
        LD      (AUTOBD0E),A

.AUTO7BA3
        LD      HL,AUTOBD0E
        DEC     (HL)
        RET     Z
        LD      B,(IX+$00)
        INC     B
        JR      NZ,AUTO7BC5
        CALL    AUTO810F
        CALL    AUTO8400
        LD      (IX+$00),A
        LD      HL,AUTOBD19
        CP      (HL)
        JR      C,AUTO7BC2
        LD      (HL),A
        LD      (AUTOBD1A),IX

.AUTO7BC2
        CALL    AUTO821C

.AUTO7BC5
        LD      BC,$0004
        ADD     IX,BC
        JP      AUTO7BA3

.AUTO7BCD
        LD      DE,$0004

.AUTO7BD0
        ADD     HL,DE
        CP      (HL)
        JR      Z,AUTO7BD8

.AUTO7BD4
        DJNZ    AUTO7BD0
        SCF
        RET

.AUTO7BD8
        EX      AF,AF'
        DEC     L
        CP      (HL)
        RET     Z
        INC     L
        EX      AF,AF'
        JP      AUTO7BD4

.AUTO7BE1
        CALL    AUTO7810

.AUTO7BE4
        CALL    AUTO7BF1
        LD      A,(AUTOBD6E)
        OR      A
        JR      NZ,AUTO7BE4
        RET

.AUTO7BEE
        POP     HL
        SCF
        RET

.AUTO7BF1
        LD      IX,AUTOBD58
        LD      HL,AUTOBC91
        LD      A,$FF
        LD      (AUTOBD0C),A
        LD      B,$0D

.AUTO7BFF
        LD      (HL),A
        INC     L
        DJNZ    AUTO7BFF
        INC     A
        LD      (AUTOBD0D),A
        LD      HL,AUTOBC9F
        LD      B,$09

.AUTO7C0C
        LD      (HL),A
        INC     L
        DJNZ    AUTO7C0C
        CALL    AUTO78CE
        SET     5,(HL)
        CALL    AUTO7B96
        LD      A,(AUTOBCDD)
        OR      A
        JR      NZ,AUTO7C2A
        CALL    AUTO7AC7
        JR      NZ,AUTO7C2A
        CALL    AUTO6734
        RET     Z
        JP      AUTO7562

.AUTO7C2A
        LD      A,(AUTOBD19)
        OR      A
        JP      Z,AUTO7BEE
        LD      D,A
        LD      A,(AUTOBD6E)
        OR      A
        JR      Z,AUTO7C49
        LD      HL,(AUTOBD29)
        LD      (HL),$FD
        LD      (AUTOBD1A),HL
        XOR     A
        LD      (AUTOBD6E),A
        LD      A,(AUTOBCBD)
        JR      AUTO7C5B

.AUTO7C49
        LD      HL,(AUTOBD1A)
        LD      (AUTOBD29),HL
        LD      A,(AUTOBCBD)
        CP      $05
        JR      C,AUTO7C5B
        LD      A,$01
        LD      (AUTOBD6E),A

.AUTO7C5B
        LD      HL,AUTOBDF6
        LD      (HL),$00
        LD      (AUTOBD09),A
        BIT     1,A
        JR      Z,AUTO7C71
        LD      (HL),A
        BIT     0,A
        JR      Z,AUTO7C71
        LD      HL,AUTOBD09
        SET     7,(HL)

.AUTO7C71
        LD      HL,AUTOBCAA
        LD      (HL),$01
        OR      A
        JR      Z,AUTO7C83
        INC     (HL)
        CP      $04
        JR      C,AUTO7C83
        LD      (HL),$03
        JR      Z,AUTO7C83
        INC     (HL)

.AUTO7C83
        LD      B,$07
        LD      A,(AUTOBD09)
        RLA
        JR      C,AUTO7CBA
        LD      A,D
        LD      HL,AUTOB403
        BIT     7,(HL)
        JR      NZ,AUTO7CB4
        CALL    AUTO805F
        SRL     D
        SRL     D
        DEC     D
        SRL     D
        LD      B,$0B
        LD      A,(AUTOBD2D)
        BIT     4,A
        JR      NZ,AUTO7CAE
        DEC     B
        SRA     A
        DEC     A
        JR      NZ,AUTO7CAE
        DEC     B
        DEC     B

.AUTO7CAE
        DEC     D
        JP      M,AUTO7CB4
        DJNZ    AUTO7CAE

.AUTO7CB4
        LD      A,$02
        ADD     A,B
        LD      (AUTOBD09),A

.AUTO7CBA
        LD      HL,AUTOBF00

.AUTO7CBD
        LD      A,(HL)
        CP      $FF
        JR      Z,AUTO7CD5
        INC     HL
        EX      AF,AF'
        LD      A,(HL)
        INC     HL
        PUSH    HL
        LD      HL,AUTOB401
        LD      B,(HL)
        INC     L
        CALL    AUTO7BCD
        DEC     L
        LD      (HL),$00
        POP     HL
        JR      AUTO7CBD

.AUTO7CD5
        LD      HL,AUTOB403
        CALL    AUTO8015
        BIT     0,E
        JR      Z,AUTO7D00
        CALL    PRINT_TXT_0_0
        DEFM    "*NO MORE MOVES",'*'+$80

        CALL    BEEP
        DEFW    $40FF

        CALL    AUTO6FAA
        CALL    AUTO704D
        POP     HL
        JP      AUTO7BE1

.AUTO7D00
        LD      (AUTOBD1A),DE
        LD      A,(AUTOBD5E)
        LD      C,A
        CP      $02
        JP      C,AUTO7DCF
        LD      HL,(AUTOBD5C)
        LD      A,H
        EX      AF,AF'
        LD      A,L
        LD      HL,AUTOB401
        LD      B,(HL)
        INC     L
        CALL    AUTO7BCD
        DEC     L
        LD      A,(HL)
        OR      A
        JP      Z,AUTO7DCF
        LD      (HL),$00
        LD      A,(AUTOBD2D)
        RRA
        LD      A,(AUTOBD2E)
        JR      NC,AUTO7D2E
        NEG

.AUTO7D2E
        ADD     A,'~'
        BIT     2,C
        JR      Z,AUTO7D36
        SUB     $08

.AUTO7D36
        LD      (AUTOBCA0),A
        LD      (AUTOBD2B),HL
        JP      AUTO7E9B
        LD      (AUTOBCA0),A
        RET

.AUTO7D43
        CALL    AUTO78BA
        LD      A,E
        OR      A
        JR      Z,AUTO7DA8
        LD      A,(AUTOBCA9)
        LD      B,E
        BIT     2,(IX+$03)
        JP      NZ,AUTO802B
        BIT     7,(HL)
        JR      NZ,AUTO7DA8
        DEC     A
        LD      DE,AUTOBC91
        ADD     A,A
        ADD     A,E
        LD      E,A
        LD      A,(DE)
        OR      A
        JP      M,AUTO7DA8
        INC     E
        EX      AF,AF'
        LD      A,(DE)
        DEC     L
        CALL    AUTO7BCD
        JR      C,AUTO7DA8
        DEC     L
        LD      (HL),$00
        PUSH    HL
        POP     IX
        CALL    AUTO810F

.AUTO7D77
        CALL    AUTO8400
        OR      A
        JR      Z,AUTO7DA5
        LD      C,A
        EX      AF,AF'
        LD      HL,AUTOBCAA
        LD      A,(HL)
        DEC     L
        LD      B,(HL)
        CP      B
        JP      C,AUTO7F20
        CALL    Z,AUTO80C4
        CALL    AUTO8003
        SET     6,(HL)
        EX      AF,AF'
        DEC     L
        LD      (HL),A
        BIT     1,(IX+$03)
        JP      Z,AUTO7DEE
        BIT     3,(IX+$03)
        JP      NZ,AUTO7DEE
        LD      (IX+$00),A

.AUTO7DA5
        CALL    AUTO821C

.AUTO7DA8
        LD      HL,AUTOBCAA
        LD      A,(HL)
        DEC     L
        CP      (HL)
        JP      C,AUTO7EE4
        CALL    AUTO8006
        BIT     5,(HL)
        JP      NZ,AUTO7DDA
        SET     5,(HL)
        PUSH    HL
        CALL    AUTO7B96
        POP     HL
        LD      A,(AUTOBD19)
        OR      A
        JP      Z,AUTO7FEA
        BIT     7,(HL)
        JP      NZ,AUTO80E2
        CALL    AUTO805F

.AUTO7DCF
        LD      IX,(AUTOBD1A)
        LD      (IX+$00),$00
        JP      AUTO7DEB

.AUTO7DDA
        BIT     7,(HL)
        JP      NZ,AUTO80ED
        CALL    AUTO8015
        BIT     0,E
        JR      NZ,AUTO7E25
        PUSH    DE
        POP     IX
        XOR     A
        LD      (DE),A

.AUTO7DEB
        CALL    AUTO810F

.AUTO7DEE
        LD      A,(AUTOBCA9)
        DEC     A
        JR      Z,AUTO7E06
        ADD     A,A
        LD      HL,AUTOBC83
        ADD     A,L
        LD      L,A
        LD      A,(IX+$01)
        LD      (HL),A
        INC     L
        LD      A,(IX+$02)
        LD      (HL),A
        JP      AUTO7E0A

.AUTO7E06
        LD      (AUTOBD2B),IX

.AUTO7E0A
        LD      A,(AUTOBD2D)
        XOR     $01
        LD      (AUTOBD2D),A
        LD      HL,AUTOBCA9
        LD      A,(HL)
        INC     (HL)
        DEC     A
        DEC     A
        LD      HL,AUTOBCA0
        ADD     A,L
        LD      L,A
        LD      A,(HL)
        INC     L
        INC     L
        LD      (HL),A
        JP      AUTO7D43

.AUTO7E25
        LD      A,(AUTOBCA9)
        DEC     A
        RET     Z
        LD      (AUTOBCA9),A
        CALL    AUTO821C
        LD      A,(AUTOBD2D)
        XOR     $01
        LD      (AUTOBD2D),A
        LD      A,(AUTOBD4C)
        RLA
        JR      C,AUTO7E25
        LD      A,(AUTOBCA9)
        LD      HL,AUTOBCA0
        ADD     A,L
        LD      L,A
        LD      A,(HL)
        NEG

.AUTO7E49
        DEC     L
        DEC     L
        CPL
        CP      (HL)
        JR      C,AUTO7E79
        CPL
        INC     L
        CP      (HL)
        JP      C,AUTO7DA8
        JP      Z,AUTO7DA8
        LD      (HL),A
        LD      A,(AUTOBCA9)
        DEC     A
        JR      Z,AUTO7E95

.AUTO7E5F
        ADD     A,A
        LD      B,A
        LD      HL,AUTOBC91
        ADD     A,L
        LD      L,A
        LD      DE,AUTOBC83
        LD      A,B
        ADD     A,E
        LD      E,A
        LD      A,(DE)
        OR      A
        JP      M,AUTO7DA8
        LD      (HL),A
        INC     E
        INC     L
        LD      A,(DE)
        LD      (HL),A
        JP      AUTO7DA8

.AUTO7E79
        CALL    AUTO821C
        LD      HL,AUTOBCA9
        DEC     (HL)
        LD      A,(AUTOBD2D)
        XOR     $01
        LD      (AUTOBD2D),A
        LD      A,(HL)
        JP      AUTO7E5F

.AUTO7E8C
        LD      IX,(AUTOBD2B)
        LD      (AUTOBD29),IX
        RET

.AUTO7E95
        LD      A,(AUTOBCA0)
        INC     A
        JR      Z,AUTO7EA1

.AUTO7E9B
        CALL    AUTO7E8C
        JP      AUTO7DA8

.AUTO7EA1
        LD      HL,AUTOBD0B
        LD      A,$FE
        LD      (AUTOBCA0),A
        LD      A,(HL)
        CP      $02
        JR      Z,AUTO7E8C
        INC     L
        CP      (HL)
        JP      NC,AUTO7DA8
        LD      (HL),A
        JR      AUTO7E9B

.AUTO7EB6
        LD      A,(AUTOBCB6)
        CP      $B4
        LD      A,(AUTOBCAA)
        JR      Z,AUTO7ED3
        CP      $03
        JR      C,AUTO7F27
        LD      L,A
        LD      A,(AUTOBCB3)
        CP      'X'
        JR      NC,AUTO7F27
        LD      A,L
        LD      HL,AUTOBCA9
        JP      AUTO7F3E

.AUTO7ED3
        CP      $03
        JR      C,AUTO7F27
        INC     A
        LD      HL,AUTOBCA9
        JP      AUTO7F3E

.AUTO7EDE
        CALL    AUTO821C

.AUTO7EE1
        LD      HL,AUTOBCA9

.AUTO7EE4
        LD      A,(HL)
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      (AUTOBD4E),A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        EX      DE,HL
        INC     L
        LD      A,(HL)
        INC     L
        LD      C,(HL)
        SUB     C
        JP      Z,AUTO7E25
        INC     (HL)
        INC     L
        LD      A,(HL)
        LD      (AUTOBD4B),A
        INC     HL
        LD      B,$00
        SLA     C
        RL      B
        ADD     HL,BC
        ADD     HL,BC
        INC     (HL)
        JR      NZ,AUTO7EE1
        PUSH    HL
        POP     IX
        CALL    AUTO810F
        CALL    AUTO8400
        LD      C,A
        OR      A
        JR      NZ,AUTO7F20
        LD      A,(AUTOBD4B)
        INC     C
        RLA
        JR      C,AUTO7F43
        JP      AUTO7EDE

.AUTO7F20
        LD      A,(AUTOBCB2)
        CP      $B4
        JR      Z,AUTO7EB6

.AUTO7F27
        LD      A,(AUTOBD4E)
        OR      A
        JR      Z,AUTO7F39
        LD      A,(IX+$03)
        OR      A
        JP      P,AUTO7F43
        BIT     2,A
        JP      NZ,AUTO7F43

.AUTO7F39
        LD      HL,AUTOBCAA
        LD      A,(HL)
        DEC     L

.AUTO7F3E
        INC     A
        CP      (HL)
        JP      NC,AUTO7DEE

.AUTO7F43
        LD      A,(AUTOBCA9)
        LD      HL,AUTOBCA0
        ADD     A,L
        LD      L,A
        INC     C
        LD      A,C
        DEC     L
        DEC     L
        CPL
        CP      (HL)
        JR      C,AUTO7F81
        INC     L
        LD      A,(IX+$03)
        AND     'B'
        JR      NZ,AUTO7FAD

.AUTO7F5B
        LD      A,C
        CP      (HL)
        JP      C,AUTO7EDE
        JP      Z,AUTO7EDE
        LD      (HL),A
        LD      A,(AUTOBD4B)
        RLA
        JP      C,AUTO7EDE
        LD      A,(AUTOBCA9)
        LD      HL,AUTOBC91
        DEC     A
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      A,(IX+$01)
        LD      (HL),A
        INC     L
        LD      A,(IX+$02)
        LD      (HL),A
        JP      AUTO7EDE

.AUTO7F81
        CALL    AUTO821C
        CALL    AUTO821C
        LD      A,(AUTOBD2D)
        XOR     $01
        LD      (AUTOBD2D),A
        LD      HL,AUTOBCA9
        DEC     (HL)
        LD      A,(AUTOBD4B)
        RLA
        JP      C,AUTO7DA8
        LD      A,(HL)
        LD      HL,AUTOBC91
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      A,(IX+$01)
        LD      (HL),A
        INC     L
        LD      A,(IX+$02)
        LD      (HL),A
        JP      AUTO7DA8

.AUTO7FAD
        LD      A,(AUTOBCA9)
        LD      B,A
        LD      A,(AUTOBCAA)
        INC     A
        INC     A
        CP      B
        JR      C,AUTO7F5B
        LD      A,C
        JR      Z,AUTO7FC0
        ADD     A,$01
        JR      C,AUTO7F5B

.AUTO7FC0
        CP      (HL)
        JP      C,AUTO7EDE
        LD      A,(AUTOBDF6)
        OR      A
        JR      Z,AUTO7F5B
        LD      A,C
        CP      $02
        JR      Z,AUTO7F5B
        JP      AUTO7DEE

.AUTO7FD2
        LD      A,(AUTOBCA9)
        LD      HL,AUTOBCA0
        ADD     A,L
        LD      L,A
        LD      A,(AUTOBD2D)
        RRA
        LD      A,(AUTOBD2E)
        JR      NC,AUTO7FE5
        NEG

.AUTO7FE5
        ADD     A,'x'
        JP      AUTO7E49

.AUTO7FEA
        BIT     6,(HL)
        JP      NZ,AUTO7E25
        BIT     7,(HL)
        JR      Z,AUTO7FD2
        LD      A,(AUTOBCA9)
        LD      (AUTOBD0B),A
        LD      HL,AUTOBCA0
        ADD     A,L
        LD      L,A
        LD      A,$01
        JP      AUTO7E49

.AUTO8003
        LD      HL,AUTOBCA9

.AUTO8006
        LD      A,(HL)
        LD      B,A
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        EX      DE,HL
        INC     L
        LD      E,(HL)
        INC     L
        LD      C,(HL)
        INC     L
        RET

.AUTO8015
        XOR     A
        LD      BC,$0004
        DEC     L
        DEC     L
        LD      E,L
        DEC     L

.AUTO801D
        ADD     HL,BC
        CP      (HL)
        JP      NC,AUTO801D
        LD      A,(HL)
        CP      $FF
        RET     Z
        LD      D,H
        LD      E,L
        JP      AUTO801D

.AUTO802B
        LD      HL,AUTOBCA9
        INC     A
        ADD     A,A
        ADD     A,L
        LD      L,A
        LD      E,(HL)
        INC     L
        LD      D,(HL)
        EX      DE,HL
        INC     L
        INC     L
        LD      DE,$FFFC
        LD      A,(IX+$02)

.AUTO803E
        ADD     HL,DE
        CP      (HL)
        JR      Z,AUTO8047
        DJNZ    AUTO803E
        JP      AUTO7DA8

.AUTO8047
        LD      D,$01
        DEC     L
        DEC     L
        PUSH    HL
        POP     IX
        LD      (HL),$00
        CALL    AUTO810F
        SET     7,(IX+$01)
        LD      A,$80
        LD      (AUTOBD4B),A
        JP      AUTO7D77

.AUTO805F
        LD      HL,AUTOBD09
        BIT     7,(HL)
        RET     NZ
        LD      IY,AUTO80A2
        EX      AF,AF'
        CALL    AUTO8003
        LD      A,B
        CP      $03
        LD      A,(AUTOBD2D)
        JR      NC,AUTO807A
        LD      A,E
        ADD     A,A
        LD      D,A
        LD      A,$10

.AUTO807A
        LD      B,E
        BIT     4,A
        JR      Z,AUTO8083
        LD      IY,AUTO80BB

.AUTO8083
        BIT     3,A
        JR      Z,AUTO808B
        LD      IY,AUTO80A7

.AUTO808B
        EX      AF,AF'
        CP      C
        JR      NC,AUTO8090
        LD      A,C

.AUTO8090
        SUB     $09
        RET     C
        LD      C,A
        INC     HL

.AUTO8095
        LD      A,(HL)
        INC     L
        INC     L
        INC     L
        SUB     C
        JR      C,AUTO80A0

.AUTO809C
        INC     HL
        DJNZ    AUTO8095
        RET

.AUTO80A0
        JP      (IY)
.AUTO80A2
        ADD     A,$10
        JP      M,AUTO80AB

.AUTO80A7
        BIT     2,(HL)
        JR      Z,AUTO809C

.AUTO80AB
        LD      A,$C9
        AND     (HL)
        JR      NZ,AUTO809C
        DEC     L
        DEC     L
        DEC     L
        LD      (HL),$00
        INC     L
        INC     L
        INC     L
        JP      AUTO809C

.AUTO80BB
        DEC     D
        ADD     A,$10
        JP      P,AUTO809C
        JP      AUTO80A7

.AUTO80C4
        BIT     0,A
        RET     NZ
        LD      HL,AUTOBC9E
        ADD     A,L
        LD      L,A
        LD      A,(AUTOBD09)
        OR      A
        RET     M
        SUB     C
        RET     NC
        CP      (HL)
        RET     NC
        CALL    AUTO8003
        BIT     0,(HL)
        RET     NZ
        POP     HL
        CALL    AUTO821C
        JP      AUTO7E79

.AUTO80E2
        LD      IX,(AUTOBD1A)
        LD      (IX+$00),$00
        JP      AUTO80FA

.AUTO80ED
        CALL    AUTO8015
        BIT     0,E
        JP      NZ,AUTO7E25
        PUSH    DE
        POP     IX
        XOR     A
        LD      (DE),A

.AUTO80FA
        CALL    AUTO810F
        LD      A,(AUTOBCA9)
        DEC     A
        JP      Z,AUTO7E06
        ADD     A,A
        LD      HL,AUTOBC83
        ADD     A,L
        LD      L,A
        SET     7,(HL)
        JP      AUTO7E0A

.AUTO810F
        POP     HL
        EXX
        LD      H,AUTO00BD
        LD      L,(IX+$01)
        LD      E,(HL)
        LD      D,L
        PUSH    DE
        LD      (HL),$00
        LD      L,(IX+$02)
        LD      C,(HL)
        LD      (HL),E
        LD      B,L
        INC     H
        RES     2,E
        LD      L,E
        LD      (HL),B
        INC     L
        LD      E,D
        LD      D,(HL)
        LD      A,$0F
        AND     D
        JP      Z,AUTO81BF
        DEC     A
        JR      Z,AUTO813D
        SET     4,(HL)
        DEC     C
        INC     C
        JR      NZ,AUTO81A5

.AUTO8138
        LD      E,$80

.AUTO813A
        PUSH    DE
        EXX
        JP      (HL)

.AUTO813D
        LD      A,B
        AND     'p'
        CP      E
        JR      NC,AUTO8145
        XOR     'p'

.AUTO8145
        SUB     '0'
        JR      C,AUTO8184
        JR      Z,AUTO8181
        SUB     $10
        JR      Z,AUTO817F
        SET     0,(IX+$03)
        SUB     ' '
        LD      A,'a'
        JR      C,AUTO8183
        LD      A,$91
        JR      Z,AUTO8183
        RES     0,(IX+$03)
        LD      (HL),$19
        LD      A,(AUTOBD0D)
        BIT     7,L
        JR      Z,AUTO816C
        ADD     A,$10

.AUTO816C
        SUB     $08
        LD      (AUTOBD0D),A
        LD      E,$84
        BIT     3,(IX+$03)
        JP      Z,AUTO813A
        LD      E,$04
        JP      AUTO81A7

.AUTO817F
        ADD     A,' '

.AUTO8181
        ADD     A,$11

.AUTO8183
        LD      (HL),A

.AUTO8184
        LD      A,(IX+$03)
        BIT     3,A
        JP      Z,AUTO8138
        BIT     5,A
        JP      Z,AUTO81A5
        LD      A,B
        CP      E
        JR      NC,AUTO8197
        ADD     A,' '

.AUTO8197
        SUB     $10
        DEC     H
        LD      L,A
        LD      C,(HL)
        LD      (HL),$00
        INC     H
        LD      B,L
        LD      E,$01
        JP      AUTO81A7

.AUTO81A5
        LD      E,$00

.AUTO81A7
        PUSH    BC
        RES     2,C
        LD      L,C
        SET     7,(HL)
        PUSH    DE
        INC     L
        LD      A,$0F
        AND     (HL)
        BIT     7,L
        JR      Z,AUTO81B8
        NEG

.AUTO81B8
        LD      HL,AUTOBD0D
        ADD     A,(HL)
        LD      (HL),A
        EXX
        JP      (HL)

.AUTO81BF
        CALL    AUTO8200
        LD      A,(IX+$03)
        AND     $18
        JP      Z,AUTO8138
        AND     $08
        JP      NZ,AUTO81A5
        SET     5,(HL)
        DEC     H
        BIT     2,B
        JR      Z,AUTO81E0
        INC     B
        LD      L,B
        LD      C,(HL)
        LD      (HL),$00
        DEC     L
        DEC     L
        JP      AUTO81E9

.AUTO81E0
        DEC     B
        DEC     B
        LD      L,B
        LD      C,(HL)
        LD      (HL),$00
        INC     L
        INC     L
        INC     L

.AUTO81E9
        LD      (HL),C
        LD      A,(IX+$00)
        INC     A
        JR      NZ,AUTO81F4
        LD      A,$84
        AND     C
        LD      (HL),A

.AUTO81F4
        PUSH    BC
        RES     2,C
        LD      B,AUTO00BE
        LD      A,L
        LD      (BC),A
        LD      E,'@'
        PUSH    DE
        EXX
        JP      (HL)

.AUTO8200
        LD      A,D
        OR      $10

.AUTO8203
        AND     '?'
        LD      E,$11

.AUTO8207
        LD      (HL),A
        LD      A,B
        ADD     A,E
        AND     $88
        RET     NZ
        LD      A,B
        SUB     E
        AND     $88
        RET     NZ
        LD      A,$11
        ADD     A,E
        LD      E,A
        LD      A,'@'
        ADD     A,(HL)
        JP      AUTO8207

.AUTO821C
        POP     HL
        EXX
        POP     AF
        POP     HL
        LD      C,A
        LD      A,$00
        LD      D,AUTO00BD
        CALL    PE,AUTO826E
        JP      M,AUTO8247
        JR      Z,AUTO825F
        JR      C,AUTO8258
        LD      B,L

.AUTO8230
        RES     2,L
        LD      H,AUTO00BE
        RES     7,(HL)
        INC     L
        LD      A,$0F
        AND     (HL)
        BIT     7,L
        JR      NZ,AUTO8240
        NEG

.AUTO8240
        LD      HL,AUTOBD0D
        ADD     A,(HL)
        LD      (HL),A
        LD      A,B

.AUTO8246
        POP     HL

.AUTO8247
        LD      E,H
        EX      DE,HL
        LD      (HL),E
        EX      DE,HL
        RES     2,L
        LD      H,AUTO00BE
        LD      B,(HL)
        LD      (HL),E
        INC     L
        LD      (HL),C
        LD      L,B
        DEC     H
        LD      (HL),A
        EXX
        JP      (HL)

.AUTO8258
        LD      E,H
        LD      B,A
        LD      A,L
        LD      (DE),A
        JP      AUTO8230

.AUTO825F
        LD      E,H
        LD      A,L
        LD      (DE),A
        RES     2,L
        LD      H,AUTO00BE
        LD      B,(HL)
        LD      (HL),E
        XOR     A
        LD      E,B
        LD      (DE),A
        JP      AUTO8246

.AUTO826E
        JP      P,AUTO8282
        EX      AF,AF'
        LD      A,(AUTOBD0D)
        BIT     7,L
        JR      Z,AUTO827B

.AUTO8279
        SUB     $10

.AUTO827B
        ADD     A,$08
        LD      (AUTOBD0D),A
        EX      AF,AF'
        RET

.AUTO8282
        EX      AF,AF'
        LD      A,(AUTOBD0D)
        BIT     7,L
        JR      NZ,AUTO827B
        JR      AUTO8279

        DEFS    $0174

.AUTO8400
        LD      (AUTOBD3D),SP
        XOR     A
        LD      E,A
        EX      AF,AF'
        LD      H,AUTO00BD
        EXX
        LD      H,AUTO00BE
        LD      A,(AUTOBD2D)
        RRCA
        LD      IY,AUTO8A7A
        BIT     1,A
        JR      Z,AUTO8424
        LD      IY,AUTO8A6F
        BIT     2,A
        JR      Z,AUTO8424
        LD      IY,AUTO8A2B

.AUTO8424
        ADD     A,$80
        LD      B,A
        EXX
        JP      C,AUTO883A

.AUTO842B
        LD      D,$88
        LD      A,(AUTOBE88)
        OR      A
        CALL    P,AUTO8C43
        LD      A,(AUTOBE90)
        OR      A
        CALL    P,AUTO8C4E
        LD      A,(AUTOBE98)
        OR      A
        CALL    P,AUTO8C4E
        LD      A,(AUTOBEA0)
        OR      A
        CALL    P,AUTO8D80
        LD      A,(AUTOBEA8)
        OR      A
        CALL    P,AUTO8D80
        LD      A,(AUTOBEB0)
        OR      A
        CALL    P,AUTO8CBB
        LD      A,(AUTOBEB8)
        OR      A
        CALL    P,AUTO8CBB
        LD      D,$F1
        LD      BC,$FFFF
        LD      HL,(AUTOBEC0)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBEC8)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBED0)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBED8)
        BIT     7,L
        CALL    Z,AUTO8A54
        LD      HL,(AUTOBEE0)
        BIT     7,L
        CALL    Z,AUTO8A54
        LD      HL,(AUTOBEE8)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBEF0)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBEF8)
        BIT     7,L
        CALL    Z,AUTO8A5B
        EXX
        BIT     4,B
        JP      Z,AUTO882C

.AUTO84A9
        BIT     6,B
        JR      NZ,AUTO84BA
        LD      L,(IX+$02)
        DEC     H
        LD      L,(HL)
        INC     H
        BIT     2,L
        JR      Z,AUTO84BA
        INC     L
        SET     1,(HL)

.AUTO84BA
        BIT     3,B
        JP      Z,AUTO84CB
        EXX
        LD      A,(AUTOBD6D)
        OR      A
        JR      NZ,AUTO84C8
        SRA     E

.AUTO84C8
        SRA     E
        EXX

.AUTO84CB
        BIT     5,B
        EXX
        CALL    NZ,AUTO8E74
        LD      HL,AUTOBD4A
        EX      AF,AF'
        SRA     A
        SRA     A
        ADD     A,E
        ADD     A,(HL)
        LD      E,A
        LD      HL,$0000
        LD      (AUTOBD39),HL
        LD      (AUTOBD3B),IX
        LD      HL,(AUTOBE8A)
        LD      A,H
        OR      L
        JR      Z,AUTO84F9
        LD      BC,(AUTOBE8C)
        LD      A,$09
        CALL    AUTO8AD6
        LD      (AUTOBE8A),HL

.AUTO84F9
        LD      (AUTOBE8C),HL
        LD      HL,(AUTOBE92)
        LD      A,H
        OR      L
        JR      Z,AUTO850F
        LD      BC,(AUTOBE94)
        LD      A,$05
        CALL    AUTO8AD6
        LD      (AUTOBE92),HL

.AUTO850F
        LD      (AUTOBE94),HL
        LD      HL,(AUTOBE9A)
        LD      A,H
        OR      L
        JR      Z,AUTO8525
        LD      BC,(AUTOBE9C)
        LD      A,$05
        CALL    AUTO8AD6
        LD      (AUTOBE9A),HL

.AUTO8525
        LD      (AUTOBE9C),HL
        LD      HL,(AUTOBEA2)
        LD      A,H
        OR      L
        JR      Z,AUTO853B
        LD      BC,(AUTOBEA4)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBEA2),HL

.AUTO853B
        LD      (AUTOBEA4),HL
        LD      HL,(AUTOBEAA)
        LD      A,H
        OR      L
        JR      Z,AUTO8551
        LD      BC,(AUTOBEAC)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBEAA),HL

.AUTO8551
        LD      (AUTOBEAC),HL
        LD      HL,(AUTOBEB2)
        LD      A,H
        OR      L
        JR      Z,AUTO8567
        LD      BC,(AUTOBEB4)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBEB2),HL

.AUTO8567
        LD      (AUTOBEB4),HL
        LD      HL,(AUTOBEBA)
        LD      A,H
        OR      L
        JR      Z,AUTO857D
        LD      BC,(AUTOBEBC)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBEBA),HL

.AUTO857D
        LD      (AUTOBEBC),HL
        LD      HL,(AUTOBEC2)
        LD      A,H
        OR      L
        JR      Z,AUTO8594
        LD      BC,(AUTOBEC4)
        LD      A,(AUTOBEC1)
        CALL    AUTO8AD4
        LD      (AUTOBEC2),HL

.AUTO8594
        LD      (AUTOBEC4),HL
        LD      HL,(AUTOBECA)
        LD      A,H
        OR      L
        JR      Z,AUTO85AB
        LD      BC,(AUTOBECC)
        LD      A,(AUTOBEC9)
        CALL    AUTO8AD4
        LD      (AUTOBECA),HL

.AUTO85AB
        LD      (AUTOBECC),HL
        LD      HL,(AUTOBED2)
        LD      A,H
        OR      L
        JR      Z,AUTO85C2
        LD      BC,(AUTOBED4)
        LD      A,(AUTOBED1)
        CALL    AUTO8AD4
        LD      (AUTOBED2),HL

.AUTO85C2
        LD      (AUTOBED4),HL
        LD      HL,(AUTOBEDA)
        LD      A,H
        OR      L
        JR      Z,AUTO85D9
        LD      BC,(AUTOBEDC)
        LD      A,(AUTOBED9)
        CALL    AUTO8AD4
        LD      (AUTOBEDA),HL

.AUTO85D9
        LD      (AUTOBEDC),HL
        LD      HL,(AUTOBEE2)
        LD      A,H
        OR      L
        JR      Z,AUTO85F0
        LD      BC,(AUTOBEE4)
        LD      A,(AUTOBEE1)
        CALL    AUTO8AD4
        LD      (AUTOBEE2),HL

.AUTO85F0
        LD      (AUTOBEE4),HL
        LD      HL,(AUTOBEEA)
        LD      A,H
        OR      L
        JR      Z,AUTO8607
        LD      BC,(AUTOBEEC)
        LD      A,(AUTOBEE9)
        CALL    AUTO8AD4
        LD      (AUTOBEEA),HL

.AUTO8607
        LD      (AUTOBEEC),HL
        LD      HL,(AUTOBEF2)
        LD      A,H
        OR      L
        JR      Z,AUTO861E
        LD      BC,(AUTOBEF4)
        LD      A,(AUTOBEF1)
        CALL    AUTO8AD4
        LD      (AUTOBEF2),HL

.AUTO861E
        LD      (AUTOBEF4),HL
        LD      HL,(AUTOBEFA)
        LD      A,H
        OR      L
        JR      Z,AUTO8635
        LD      BC,(AUTOBEFC)
        LD      A,(AUTOBEF9)
        CALL    AUTO8AD4
        LD      (AUTOBEFA),HL

.AUTO8635
        LD      (AUTOBEFC),HL
        XOR     A
        SUB     E
        LD      E,A
        LD      HL,(AUTOBD39)
        PUSH    HL
        LD      HL,$0000
        LD      (AUTOBD39),HL
        LD      HL,(AUTOBE0A)
        LD      A,H
        OR      L
        JR      Z,AUTO8658
        LD      BC,(AUTOBE0C)
        LD      A,$09
        CALL    AUTO8AD6
        LD      (AUTOBE0A),HL

.AUTO8658
        LD      (AUTOBE0C),HL
        LD      HL,(AUTOBE12)
        LD      A,H
        OR      L
        JR      Z,AUTO866E
        LD      BC,(AUTOBE14)
        LD      A,$05
        CALL    AUTO8AD6
        LD      (AUTOBE12),HL

.AUTO866E
        LD      (AUTOBE14),HL
        LD      HL,(AUTOBE1A)
        LD      A,H
        OR      L
        JR      Z,AUTO8684
        LD      BC,(AUTOBE1C)
        LD      A,$05
        CALL    AUTO8AD6
        LD      (AUTOBE1A),HL

.AUTO8684
        LD      (AUTOBE1C),HL
        LD      HL,(AUTOBE22)
        LD      A,H
        OR      L
        JR      Z,AUTO869A
        LD      BC,(AUTOBE24)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBE22),HL

.AUTO869A
        LD      (AUTOBE24),HL
        LD      HL,(AUTOBE2A)
        LD      A,H
        OR      L
        JR      Z,AUTO86B0
        LD      BC,(AUTOBE2C)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBE2A),HL

.AUTO86B0
        LD      (AUTOBE2C),HL
        LD      HL,(AUTOBE32)
        LD      A,H
        OR      L
        JR      Z,AUTO86C6
        LD      BC,(AUTOBE34)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBE32),HL

.AUTO86C6
        LD      (AUTOBE34),HL
        LD      HL,(AUTOBE3A)
        LD      A,H
        OR      L
        JR      Z,AUTO86DC
        LD      BC,(AUTOBE3C)
        LD      A,$03
        CALL    AUTO8AD6
        LD      (AUTOBE3A),HL

.AUTO86DC
        LD      (AUTOBE3C),HL
        LD      HL,(AUTOBE42)
        LD      A,H
        OR      L
        JR      Z,AUTO86F3
        LD      BC,(AUTOBE44)
        LD      A,(AUTOBE41)
        CALL    AUTO8AD4
        LD      (AUTOBE42),HL

.AUTO86F3
        LD      (AUTOBE44),HL
        LD      HL,(AUTOBE4A)
        LD      A,H
        OR      L
        JR      Z,AUTO870A
        LD      BC,(AUTOBE4C)
        LD      A,(AUTOBE49)
        CALL    AUTO8AD4
        LD      (AUTOBE4A),HL

.AUTO870A
        LD      (AUTOBE4C),HL
        LD      HL,(AUTOBE52)
        LD      A,H
        OR      L
        JR      Z,AUTO8721
        LD      BC,(AUTOBE54)
        LD      A,(AUTOBE51)
        CALL    AUTO8AD4
        LD      (AUTOBE52),HL

.AUTO8721
        LD      (AUTOBE54),HL
        LD      HL,(AUTOBE5A)
        LD      A,H
        OR      L
        JR      Z,AUTO8738
        LD      BC,(AUTOBE5C)
        LD      A,(AUTOBE59)
        CALL    AUTO8AD4
        LD      (AUTOBE5A),HL

.AUTO8738
        LD      (AUTOBE5C),HL
        LD      HL,(AUTOBE62)
        LD      A,H
        OR      L
        JR      Z,AUTO874F
        LD      BC,(AUTOBE64)
        LD      A,(AUTOBE61)
        CALL    AUTO8AD4
        LD      (AUTOBE62),HL

.AUTO874F
        LD      (AUTOBE64),HL
        LD      HL,(AUTOBE6A)
        LD      A,H
        OR      L
        JR      Z,AUTO8766
        LD      BC,(AUTOBE6C)
        LD      A,(AUTOBE69)
        CALL    AUTO8AD4
        LD      (AUTOBE6A),HL

.AUTO8766
        LD      (AUTOBE6C),HL
        LD      HL,(AUTOBE72)
        LD      A,H
        OR      L
        JR      Z,AUTO877D
        LD      BC,(AUTOBE74)
        LD      A,(AUTOBE71)
        CALL    AUTO8AD4
        LD      (AUTOBE72),HL

.AUTO877D
        LD      (AUTOBE74),HL
        LD      HL,(AUTOBE7A)
        LD      A,H
        OR      L
        JR      Z,AUTO8794
        LD      BC,(AUTOBE7C)
        LD      A,(AUTOBE79)
        CALL    AUTO8AD4
        LD      (AUTOBE7A),HL

.AUTO8794
        LD      (AUTOBE7C),HL
        LD      B,E
        LD      IX,(AUTOBD3B)
        POP     DE
        LD      HL,(AUTOBD39)
        LD      A,(AUTOBD2D)
        RRA
        JR      NC,AUTO87B0
        XOR     A
        SUB     B
        LD      B,A
        EX      DE,HL
        LD      A,(AUTOBD0D)
        JP      AUTO87B5

.AUTO87B0
        LD      A,(AUTOBD0D)
        NEG

.AUTO87B5
        SUB     L
        ADD     A,A
        DEC     L
        JP      M,AUTO87C3
        SET     1,(IX+$03)
        JR      Z,AUTO87C2
        DEC     A

.AUTO87C2
        SUB     H

.AUTO87C3
        DEC     E
        JP      M,AUTO87ED
        BIT     2,(IX+$03)
        JR      NZ,AUTO87E2
        INC     B
        INC     B
        DEC     D
        JP      M,AUTO87E2
        SET     6,(IX+$03)
        INC     B
        INC     B
        DEC     D
        JP      M,AUTO87ED
        INC     B
        INC     B
        JP      AUTO87ED

.AUTO87E2
        EX      AF,AF'
        LD      A,E
        INC     L
        CP      L
        JR      C,AUTO87EC
        SET     6,(IX+$03)

.AUTO87EC
        EX      AF,AF'

.AUTO87ED
        CP      $1C
        JR      C,AUTO87FD
        CP      $E4
        JP      NC,AUTO87FD
        RLA
        LD      A,$02
        RET     C
        LD      A,$FC
        RET

.AUTO87FD
        ADD     A,A
        ADD     A,A
        LD      C,A
        LD      A,B
        SRA     A
        ADC     A,B
        SRA     A
        LD      D,A
        LD      HL,AUTOBCCE
        INC     (HL)
        LD      A,(AUTOBD49)
        DEC     A
        LD      A,(HL)
        JR      NZ,AUTO8816
        AND     $03
        JR      AUTO8818

.AUTO8816
        AND     $01

.AUTO8818
        ADD     A,D
        CP      $0C
        JR      C,AUTO8828
        CP      $F4
        JR      NC,AUTO8828
        RLA
        LD      A,$0C
        JR      NC,AUTO8828
        LD      A,$F4

.AUTO8828
        ADD     A,C
        ADD     A,$80
        RET

.AUTO882C
        LD      A,$90
        ADD     A,B
        LD      B,A
        EXX
        XOR     A
        SUB     E
        LD      E,A
        LD      H,AUTO00BD
        EX      AF,AF'
        NEG
        EX      AF,AF'

.AUTO883A
        LD      A,(AUTOBE08)
        LD      D,$88
        OR      A
        CALL    P,AUTO8C43
        LD      A,(AUTOBE10)
        OR      A
        CALL    P,AUTO8C4E
        LD      A,(AUTOBE18)
        OR      A
        CALL    P,AUTO8C4E
        LD      A,(AUTOBE20)
        OR      A
        CALL    P,AUTO8D74
        LD      A,(AUTOBE28)
        OR      A
        CALL    P,AUTO8D74
        LD      A,(AUTOBE30)
        OR      A
        CALL    P,AUTO8CC8
        LD      A,(AUTOBE38)
        OR      A
        CALL    P,AUTO8CC8
        LD      D,$11
        LD      BC,$FFFF
        LD      HL,(AUTOBE40)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBE48)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBE50)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBE58)
        BIT     7,L
        CALL    Z,AUTO8A54
        LD      HL,(AUTOBE60)
        BIT     7,L
        CALL    Z,AUTO8A54
        LD      HL,(AUTOBE68)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBE70)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBE78)
        BIT     7,L
        CALL    Z,AUTO8A5B
        LD      HL,(AUTOBE00)
        CALL    AUTO8978
        EX      AF,AF'
        NEG
        EX      AF,AF'
        XOR     A
        SUB     E
        LD      E,A
        LD      HL,(AUTOBE80)
        EXX
        SET     7,B
        CALL    AUTO897D
        EXX
        BIT     4,B
        JP      NZ,AUTO84A9
        SET     4,B
        EXX
        JP      AUTO842B

.AUTO88D4
        OR      A
        JP      NZ,AUTO896F
        LD      A,D
        OR      A
        JR      NZ,AUTO88DF
        SUB     $0A
        LD      D,A

.AUTO88DF
        LD      A,$07
        AND     L
        INC     A
        RRA
        CP      $02
        JR      Z,AUTO88F3
        LD      A,'p'
        AND     L
        CP      '0'
        JR      Z,AUTO88F3
        CP      '@'
        JR      NZ,AUTO88F9

.AUTO88F3
        EX      AF,AF'
        ADD     A,$10
        JP      AUTO8970

.AUTO88F9
        LD      A,$07
        AND     L
        CP      $02
        JR      Z,AUTO890F
        CP      $05
        JR      Z,AUTO890F
        LD      A,'p'
        AND     L
        CP      ' '
        JR      Z,AUTO890F
        CP      'P'
        JR      NZ,AUTO896F

.AUTO890F
        EX      AF,AF'
        ADD     A,$08
        JP      AUTO8970

.AUTO8915
        LD      A,(AUTOBD49)
        CPL
        ADD     A,$0A
        EXX
        LD      C,A
        LD      A,(AUTOBE89)
        BIT     4,A
        JR      Z,AUTO892E
        LD      A,(AUTOBE88)
        OR      A
        JP      M,AUTO892E
        LD      A,E
        ADD     A,C
        LD      E,A

.AUTO892E
        LD      A,(AUTOBE09)
        BIT     4,A
        JR      Z,AUTO893F
        LD      A,(AUTOBE08)
        OR      A
        JP      M,AUTO893F
        LD      A,E
        SUB     C
        LD      E,A

.AUTO893F
        EXX
        JP      AUTO897D

.AUTO8943
        BIT     3,A
        JR      NZ,AUTO894F
        LD      H,AUTO00BD
        LD      B,L
        LD      C,$08
        JP      AUTO89AC

.AUTO894F
        LD      B,A
        LD      C,$04
        LD      A,$C0
        AND     H
        RRA
        RRA
        RRA
        LD      D,A
        LD      A,(AUTOBD1E)
        OR      A
        JR      Z,AUTO8966
        XOR     B
        LD      A,(AUTOBD6D)
        JP      P,AUTO88D4

.AUTO8966
        RR      D
        OR      A
        JR      NZ,AUTO896F
        BIT     7,H
        JR      NZ,AUTO890F

.AUTO896F
        EX      AF,AF'

.AUTO8970
        ADD     A,D
        EX      AF,AF'
        LD      H,AUTO00BD
        LD      B,L
        JP      AUTO89AC

.AUTO8978
        EXX
        BIT     0,B
        JR      Z,AUTO8915

.AUTO897D
        LD      A,B
        EXX
        BIT     2,A
        JR      NZ,AUTO8943
        BIT     5,H
        JP      Z,AUTO8A0D
        LD      A,$C0
        AND     H
        JR      NZ,AUTO89FB
        LD      C,$0A
        LD      B,L
        LD      H,AUTO00BD
        LD      A,$1F
        ADD     A,B
        JP      P,AUTO899A
        SUB     '@'

.AUTO899A
        LD      L,A
        BIT     6,(HL)
        JR      NZ,AUTO89AB
        INC     L
        BIT     6,(HL)
        JR      NZ,AUTO89AB
        INC     L
        BIT     6,(HL)
        JR      NZ,AUTO89AB
        DEC     E
        DEC     E

.AUTO89AB
        LD      L,B

.AUTO89AC
        DEC     L
        XOR     A
        OR      (HL)
        CALL    NZ,AUTO89E2
        INC     L
        INC     L
        OR      (HL)
        CALL    NZ,AUTO89E2
        LD      A,$10
        ADD     A,B
        JP      M,AUTO89CF
        LD      L,A
        XOR     A
        OR      (HL)
        CALL    NZ,AUTO89E2
        INC     L
        OR      (HL)
        CALL    NZ,AUTO89E2
        DEC     L
        DEC     L
        OR      (HL)
        CALL    NZ,AUTO89E2

.AUTO89CF
        LD      A,$F0
        ADD     A,B
        RET     M
        LD      L,A
        XOR     A
        OR      (HL)
        CALL    NZ,AUTO89E2
        INC     L
        OR      (HL)
        CALL    NZ,AUTO89E2
        DEC     L
        DEC     L
        OR      (HL)
        RET     Z

.AUTO89E2
        EXX
        LD      L,A
        INC     L
        XOR     B
        JP      P,AUTO89F1
        LD      A,'x'
        AND     L
        JP      Z,AUTO8C28
        DEC     L
        DEC     L

.AUTO89F1
        INC     (HL)
        XOR     A
        BIT     6,L
        EXX
        RET     Z
        EX      AF,AF'
        ADD     A,C
        EX      AF,AF'
        RET

.AUTO89FB
        LD      C,$04
        BIT     7,H
        JP      Z,AUTO8A04
        DEC     E
        DEC     E

.AUTO8A04
        DEC     E
        DEC     E
        DEC     E
        LD      B,L
        LD      H,AUTO00BD
        JP      AUTO89AC

.AUTO8A0D
        BIT     4,H
        JR      NZ,AUTO8A1A
        LD      H,AUTO00BD
        LD      B,L
        LD      C,$00
        INC     E
        JP      AUTO89AC

.AUTO8A1A
        LD      A,E
        SUB     $06
        LD      E,A
        LD      C,$08
        LD      A,$C0
        AND     H
        JR      NZ,AUTO89FB
        LD      H,AUTO00BD
        LD      B,L
        JP      AUTO89AC
.AUTO8A2B
        SRL     H
        SRL     H
        EX      AF,AF'
        ADD     A,H
        EX      AF,AF'
        BIT     5,H
        JP      Z,AUTO8A7A
        EXX
        BIT     4,B
        EXX
        JR      NZ,AUTO8A7A
        LD      H,AUTO00BD
        DEC     L
        LD      A,(HL)
        INC     L
        LD      (AUTOBD4E),A
        JP      AUTO8A7C

.AUTO8A48
        DEC     E
        DEC     E
        DEC     E
        JP      AUTO8A6B

.AUTO8A4E
        DEC     E
        DEC     E
        DEC     E
        JP      AUTO8A5B

.AUTO8A54
        LD      A,L
        ADD     A,' '
        BIT     6,A
        JR      Z,AUTO8A4E

.AUTO8A5B
        LD      A,$0E
        AND     H
        JR      NZ,AUTO8A96
        LD      A,D
        ADD     A,L
        LD      L,A
        AND     $0F
        CP      B
        JR      Z,AUTO8A48
        CP      C
        JR      Z,AUTO8A48

.AUTO8A6B
        LD      C,B
        LD      B,A
        JP      (IY)

.AUTO8A6F
        SRL     H
        JR      Z,AUTO8A7A
        SRL     H
        SRL     H
        EX      AF,AF'
        ADD     A,H
        EX      AF,AF'

.AUTO8A7A
        LD      H,AUTO00BD

.AUTO8A7C
        LD      A,(HL)
        OR      A
        CALL    NZ,AUTO8A85
        DEC     L
        DEC     L
        OR      (HL)
        RET     Z

.AUTO8A85
        EXX
        LD      L,A
        XOR     B
        JP      P,AUTO8A92
        LD      A,'x'
        AND     L
        JR      Z,AUTO8ABA
        DEC     L
        DEC     L

.AUTO8A92
        INC     (HL)
        EXX
        XOR     A
        RET

.AUTO8A96
        PUSH    BC
        PUSH    DE
        LD      BC,AUTO8AB5
        PUSH    BC
        LD      C,H
        LD      H,AUTO00BD
        LD      D,$88
        LD      A,L
        BIT     3,C
        JP      NZ,AUTO8C43
        BIT     2,C
        JP      NZ,AUTO8C4E
        BIT     7,C
        LD      B,A
        JP      NZ,AUTO8D86
        JP      AUTO8CCD

.AUTO8AB5
        LD      L,E
        POP     DE
        LD      E,L
        POP     BC
        RET

.AUTO8ABA
        BIT     4,B
        JP      Z,AUTO8C28
        SET     7,(IX+$03)
        EXX
        INC     E
        RET

.AUTO8AC6
        BIT     0,H
        JR      NZ,AUTO8ACD
        LD      H,$00
        RET

.AUTO8ACD
        LD      A,H
        CALL    AUTO8B6A
        JP      AUTO8AE0

.AUTO8AD4
        AND     $0F

.AUTO8AD6
        EXX
        LD      D,A
        EXX
        LD      A,L
        OR      A
        JR      Z,AUTO8AC6
        CALL    AUTO8B50

.AUTO8AE0
        CALL    AUTO8BB9
        LD      E,$FF
        LD      A,D

.AUTO8AE6
        LD      B,$00
        BIT     0,H
        JR      Z,AUTO8B1A
        SUB     L
        EXX
        DEC     D
        EXX
        CALL    Z,AUTO8B8C
        CP      E
        JP      M,AUTO8B12
        CP      D
        JP      P,AUTO8B11
        LD      E,A

.AUTO8AFC
        BIT     0,L
        JR      Z,AUTO8B1A
        ADD     A,H
        CP      E
        JP      M,AUTO8B17
        CP      D
        JP      P,AUTO8B16
        LD      D,A

.AUTO8B0A
        DEC     C
        CALL    Z,AUTO8BFA
        JP      AUTO8AE6

.AUTO8B11
        INC     B

.AUTO8B12
        INC     B
        JP      AUTO8AFC

.AUTO8B16
        DEC     B

.AUTO8B17
        DEC     B
        JR      NZ,AUTO8B0A

.AUTO8B1A
        CP      E
        JP      P,AUTO8B1F
        LD      A,E

.AUTO8B1F
        CP      D
        JP      M,AUTO8B24
        LD      A,D

.AUTO8B24
        OR      A
        EXX
        JP      M,AUTO8B3E
        JR      Z,AUTO8B39
        LD      HL,AUTOBD39
        CP      (HL)
        JR      C,AUTO8B34
        LD      C,A
        LD      A,(HL)
        LD      (HL),C

.AUTO8B34
        INC     L
        CP      (HL)
        JR      C,AUTO8B39
        LD      (HL),A

.AUTO8B39
        DEC     E
        BIT     1,B
        JR      NZ,AUTO8B42

.AUTO8B3E
        LD      HL,$0000
        RET

.AUTO8B42
        LD      IX,(AUTOBD3B)
        SET     2,(IX+$03)
        LD      HL,$0000
        DEC     E
        DEC     E
        RET

.AUTO8B50
        AND     $03
        CALL    NZ,AUTO8B84
        LD      A,$0C
        AND     L
        CALL    NZ,AUTO8B91
        LD      A,'0'
        AND     L
        CALL    NZ,AUTO8B9B
        LD      A,$C0
        AND     L
        CALL    NZ,AUTO8BA7
        OR      H
        JR      Z,AUTO8B80

.AUTO8B6A
        AND     $0C
        CALL    NZ,AUTO8B91
        LD      A,'0'
        AND     H
        CALL    NZ,AUTO8B9B
        LD      A,$C0
        AND     H
        CALL    NZ,AUTO8BA7
        INC     A
        AND     H
        CALL    NZ,AUTO8BB1

.AUTO8B80
        EXX
        LD      L,A
        EX      AF,AF'
        RET

.AUTO8B84
        LD      D,A
        POP     IY
        EXX
        LD      L,$01
        EX      AF,AF'
        RET

.AUTO8B8C
        EX      AF,AF'
        EXX
        XOR     A
        JP      (IY)

.AUTO8B91
        RRA
        RRA
        LD      D,A
        POP     IY
        EXX
        LD      L,$03
        EX      AF,AF'
        RET

.AUTO8B9B
        RRA
        RRA
        RRA
        RRA
        LD      D,A
        POP     IY
        EXX
        LD      L,$05
        EX      AF,AF'
        RET

.AUTO8BA7
        RLCA
        RLCA
        LD      D,A
        POP     IY
        EXX
        LD      L,$09
        EX      AF,AF'
        RET

.AUTO8BB1
        LD      D,A
        POP     IY
        EXX
        LD      L,$0D
        EX      AF,AF'
        RET

.AUTO8BB9
        EXX
        LD      A,C
        OR      A
        JR      Z,AUTO8BD5
        AND     $03
        CALL    NZ,AUTO8BF2
        LD      A,$0C
        AND     C
        CALL    NZ,AUTO8BFF
        LD      A,'0'
        AND     C
        CALL    NZ,AUTO8C09
        LD      A,$C0
        AND     C
        CALL    NZ,AUTO8C15

.AUTO8BD5
        OR      B
        JR      Z,AUTO8BEE
        AND     $0C
        CALL    NZ,AUTO8BFF
        LD      A,'0'
        AND     B
        CALL    NZ,AUTO8C09
        LD      A,$C0
        AND     B
        CALL    NZ,AUTO8C15
        INC     A
        AND     B
        CALL    NZ,AUTO8C1F

.AUTO8BEE
        EXX
        LD      H,A
        EX      AF,AF'
        RET

.AUTO8BF2
        EXX
        LD      C,A
        LD      H,$01
        POP     IX
        EX      AF,AF'
        RET

.AUTO8BFA
        EX      AF,AF'
        EXX
        XOR     A
        JP      (IX)

.AUTO8BFF
        RRA
        RRA
        POP     IX
        EXX
        LD      H,$03
        LD      C,A
        EX      AF,AF'
        RET

.AUTO8C09
        RRA
        RRA
        RRA
        RRA
        POP     IX
        EXX
        LD      H,$05
        LD      C,A
        EX      AF,AF'
        RET

.AUTO8C15
        RLCA
        RLCA
        POP     IX
        EXX
        LD      H,$09
        LD      C,A
        EX      AF,AF'
        RET

.AUTO8C1F
        DEC     E
        POP     IX
        EXX
        LD      H,$0D
        LD      C,A
        EX      AF,AF'
        RET

.AUTO8C28
        XOR     A
        LD      (AUTOBD79),A
        LD      B,$1F
        LD      DE,$0005
        LD      HL,AUTOBE0A

.AUTO8C34
        LD      (HL),A
        INC     L
        LD      (HL),A
        INC     L
        LD      (HL),A
        INC     L
        LD      (HL),A
        ADD     HL,DE
        DJNZ    AUTO8C34
        LD      SP,(AUTOBD3D)
        RET

.AUTO8C43
        LD      B,A
        EXX
        LD      C,'@'
        CALL    AUTO8C52
        EXX
        JP      AUTO8D89

.AUTO8C4E
        LD      B,A
        EXX
        LD      C,$10

.AUTO8C52
        LD      E,$08
        EXX
        LD      L,B
        XOR     A

.AUTO8C57
        INC     L
        BIT     3,L
        JR      NZ,AUTO8C65
        OR      (HL)
        JR      Z,AUTO8C57
        LD      C,$01
        CALL    AUTO8C98
        XOR     A

.AUTO8C65
        LD      L,B

.AUTO8C66
        DEC     L
        BIT     3,L
        JR      NZ,AUTO8C73
        OR      (HL)
        JR      Z,AUTO8C66
        LD      C,$FF
        CALL    AUTO8C98

.AUTO8C73
        LD      C,$10
        LD      A,B
        ADD     A,C
        CALL    P,AUTO8C80
        LD      C,$F0
        LD      L,B

.AUTO8C7D
        LD      A,L
        ADD     A,C
        RET     M

.AUTO8C80
        LD      L,A
        INC     E
        XOR     A
        OR      (HL)
        JR      Z,AUTO8C7D
        EXX
        LD      L,A
        XOR     B
        JP      M,AUTO8DB9
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        SET     1,L
        LD      A,(HL)
        AND     E
        EXX
        RET     Z
        JP      AUTO8DF8

.AUTO8C98
        EXX
        LD      L,A
        XOR     B
        JP      M,AUTO8DB9
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        SET     1,L
        LD      A,(HL)
        AND     E
        EXX
        RET     Z

.AUTO8CA7
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,AUTO8CA7
        EXX
        LD      L,A
        XOR     B
        JP      M,AUTO8E07
        INC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

.AUTO8CBB
        LD      B,A
        OR      'p'
        CP      B
        JP      NZ,AUTO8CCD

.AUTO8CC2
        DEC     E
        DEC     E
        DEC     E
        JP      AUTO8CCD

.AUTO8CC8
        LD      B,A
        AND     'p'
        JR      Z,AUTO8CC2

.AUTO8CCD
        EXX
        LD      C,$04
        EXX
        DEC     E

.AUTO8CD2
        LD      A,$0E
        ADD     A,B
        JP      M,AUTO8CF5
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15
        LD      A,$12
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15
        LD      A,$1F
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15
        LD      A,'!'
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15

.AUTO8CF5
        LD      A,$F2
        ADD     A,B
        RET     M
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15
        LD      A,$EE
        ADD     A,B
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15
        LD      A,$E1
        ADD     A,B
        RET     M
        LD      L,A
        AND     D
        CALL    Z,AUTO8D15
        LD      A,$DF
        ADD     A,B
        LD      L,A
        AND     D
        RET     NZ

.AUTO8D15
        INC     E
        OR      (HL)
        RET     Z
        EXX
        LD      L,A
        XOR     B
        JP      P,AUTO8D25
        LD      A,'x'
        AND     L
        JR      Z,AUTO8D2A
        DEC     L
        DEC     L

.AUTO8D25
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

.AUTO8D2A
        BIT     4,B
        JP      Z,AUTO8C28
        SET     7,(IX+$03)
        BIT     6,B
        SET     6,B
        EXX
        JR      NZ,AUTO8D44
        LD      L,B
        LD      L,(HL)
        INC     L
        INC     H
        SET     1,(HL)
        DEC     H
        INC     E
        INC     E
        RET

.AUTO8D44
        LD      (AUTOBD4E),A
        EX      AF,AF'
        ADD     A,$14
        EX      AF,AF'
        RET

.AUTO8D4C
        BIT     4,B
        JP      Z,AUTO8C28
        SET     7,(IX+$03)
        BIT     6,B
        SET     6,B
        EXX
        JR      NZ,AUTO8D6A
        LD      A,L
        LD      L,B
        LD      L,(HL)
        INC     L
        INC     H
        SET     1,(HL)
        DEC     H
        LD      L,A
        INC     E
        INC     E
        JP      AUTO8DAA

.AUTO8D6A
        LD      (AUTOBD4E),A
        EX      AF,AF'
        ADD     A,$14
        EX      AF,AF'
        JP      AUTO8DAA

.AUTO8D74
        LD      B,A
        AND     'p'
        JP      NZ,AUTO8D86

.AUTO8D7A
        DEC     E
        DEC     E
        DEC     E
        JP      AUTO8D86

.AUTO8D80
        LD      B,A
        OR      'p'
        CP      B
        JR      Z,AUTO8D7A

.AUTO8D86
        EXX
        LD      C,$04

.AUTO8D89
        LD      E,$C4
        EXX
        LD      C,$0F
        LD      A,B
        ADD     A,C
        LD      L,A
        AND     D
        CALL    Z,AUTO8DAF
        LD      C,$11
        LD      A,B
        ADD     A,C
        LD      L,A
        AND     D
        CALL    Z,AUTO8DAF
        LD      C,$EF
        LD      A,B
        ADD     A,C
        LD      L,A
        AND     D
        CALL    Z,AUTO8DAF
        LD      C,$F1
        LD      L,B

.AUTO8DAA
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ

.AUTO8DAF
        INC     E
        OR      (HL)
        JR      Z,AUTO8DAA
        EXX
        LD      L,A
        XOR     B
        JP      P,AUTO8DEC

.AUTO8DB9
        LD      A,'x'
        AND     L
        JR      Z,AUTO8D4C
        DEC     L
        DEC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        SET     2,L
        LD      A,(HL)
        AND     E
        JR      Z,AUTO8E19
        JP      M,AUTO8E19
        EXX

.AUTO8DCD
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,AUTO8DCD
        EXX
        LD      D,L
        LD      L,A
        XOR     B
        JP      P,AUTO8E15
        DEC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        INC     L

.AUTO8DE1
        INC     L
        INC     L
        LD      A,(HL)
        RRA
        JR      C,AUTO8E2F
        RRA
        JR      C,AUTO8E2B
        EXX
        RET

.AUTO8DEC
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A

.AUTO8DEF
        SET     1,L
        LD      A,(HL)
        AND     E
        EXX
        RET     Z
        JP      M,AUTO8E5C

.AUTO8DF8
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        INC     E
        OR      (HL)
        JR      Z,AUTO8DF8
        EXX
        LD      L,A
        XOR     B
        JP      P,AUTO8E0D

.AUTO8E07
        DEC     L

.AUTO8E08
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

.AUTO8E0D
        INC     L
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        DEC     L
        JP      AUTO8DEF

.AUTO8E15
        INC     L
        JP      AUTO8E08

.AUTO8E19
        EXX

.AUTO8E1A
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,AUTO8E1A
        EXX
        LD      D,L
        LD      L,A
        XOR     B
        JP      M,AUTO8DE1
        EXX
        RET

.AUTO8E2B
        BIT     6,C
        JR      NZ,AUTO8E58

.AUTO8E2F
        RES     1,D
        RES     2,D
        LD      HL,AUTOBD79

.AUTO8E36
        LD      A,(HL)
        OR      A
        JR      Z,AUTO8E47
        INC     L
        INC     L
        CP      D
        JP      NZ,AUTO8E36
        DEC     L
        LD      (HL),$00
        INC     H
        EXX
        INC     E
        RET

.AUTO8E47
        LD      A,L
        CP      $84
        JR      NC,AUTO8E57
        LD      (HL),D
        INC     L
        EXX
        LD      A,C
        EXX
        LD      (HL),A
        INC     L
        LD      (HL),$00
        SET     5,B

.AUTO8E57
        INC     H

.AUTO8E58
        EXX
        INC     E
        INC     E
        RET

.AUTO8E5C
        RLA
        XOR     C
        RET     P
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        RET     Z
        EXX
        LD      L,A
        INC     L
        XOR     B
        JP      P,AUTO8E6F
        DEC     L
        DEC     L

.AUTO8E6F
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

.AUTO8E74
        LD      IY,AUTOBD79
        PUSH    DE
        LD      H,AUTO00BD
        EXX
        LD      L,(IY+$00)

.AUTO8E7F
        LD      BC,AUTO8F1B
        PUSH    BC
        LD      A,L
        AND     $80
        LD      B,A
        LD      E,(HL)
        INC     L
        LD      A,(HL)
        AND     $8E
        JR      Z,AUTO8E9F
        JP      M,AUTO8EE4
        CP      $04
        JR      C,AUTO8EC9
        JR      Z,AUTO8ED5
        LD      C,$C0
        CALL    AUTO8ED7
        JP      AUTO8EED

.AUTO8E9F
        BIT     7,B
        LD      A,E
        EXX
        LD      B,A
        LD      C,$0F
        JR      Z,AUTO8EAA
        LD      C,$EF

.AUTO8EAA
        CALL    AUTO8EAF
        INC     C
        INC     C

.AUTO8EAF
        LD      A,(IY+$01)
        CP      C
        RET     Z
        NEG
        CP      C
        RET     Z
        LD      A,C
        ADD     A,B
        LD      L,A
        XOR     A
        OR      (HL)
        RET     Z
        EXX
        LD      L,A
        XOR     B
        JP      P,AUTO8EC6
        DEC     L
        DEC     L

.AUTO8EC6
        DEC     (HL)
        EXX
        RET

.AUTO8EC9
        LD      C,$FC
        SET     4,B
        LD      A,E
        EXX
        LD      D,$88
        LD      B,A
        JP      AUTO8CD2

.AUTO8ED5
        LD      C,$F0

.AUTO8ED7
        LD      A,E
        EXX
        LD      C,$01
        LD      B,A
        CALL    AUTO8EF4
        LD      C,$10
        JP      AUTO8EF4

.AUTO8EE4
        AND     $02
        JR      Z,AUTO8E9F
        LD      C,$FC
        LD      A,E
        EXX
        LD      B,A

.AUTO8EED
        LD      C,$0F
        CALL    AUTO8EF4
        LD      C,$11

.AUTO8EF4
        LD      A,(IY+$01)
        CP      C
        RET     Z
        NEG
        CP      C
        RET     Z
        LD      D,$88
        CALL    AUTO8F05
        XOR     A
        SUB     C
        LD      C,A

.AUTO8F05
        LD      L,B

.AUTO8F06
        LD      A,L
        ADD     A,C
        LD      L,A
        AND     D
        RET     NZ
        OR      (HL)
        JR      Z,AUTO8F06
        EXX
        LD      L,A
        XOR     B
        JP      P,AUTO8F16
        DEC     L
        DEC     L

.AUTO8F16
        LD      A,C
        ADD     A,(HL)
        LD      (HL),A
        EXX
        RET

.AUTO8F1B
        LD      A,(IY+$02)
        OR      A
        JR      Z,AUTO8F2A
        INC     IY
        INC     IY
        EXX
        LD      L,A
        JP      AUTO8E7F

.AUTO8F2A
        POP     DE
        LD      (AUTOBD79),A
        RET
        DEFS    $00D1

.BASE
        DEFW    $9226,$C469,$B0CA,$2E4C,$0A0D,$4916,$90E0
        DEFW    $3400,$970E,$0313,$8001,$22A0,$B670,$054B
        DEFW    $8094,$0000,$6480,$2912,$8A4A,$0000,$8060
        DEFW    $1051,$054F,$4282,$0040,$6400,$2912,$C94A
        DEFW    $6281,$A020,$1040,$0D28,$4208,$3002,$7130
        DEFW    $1400,$8626,$6005,$20A0,$000C,$2792,$4386
        DEFW    $E228,$6458,$0A38,$2404,$C1CC,$C090,$1C08
        DEFW    $001E,$C225,$8021,$5490,$8B00,$1203,$0490
        DEFW    $C0C4,$1A14,$4005,$2A11,$F0C0,$24B9,$5328
        DEFW    $8726,$0501,$0070,$4828,$059B,$0000,$4002
        DEFW    $1960,$5106,$530E,$4081,$0240,$A404,$070A
        DEFW    $0000,$4081,$040A,$0A00,$8005,$4002,$2A81
        DEFW    $1E3C,$2D1D,$4008,$F000,$0042,$0E32,$2000
        DEFW    $A50A,$50D2,$167C,$0D18,$0308,$30E1,$0100
        DEFW    $5A42,$8607,$A1C2,$FAA0,$123E,$001C,$8BA3
        DEFW    $31E1,$3808,$9E30,$82A7,$0080,$D0B0,$1A48
        DEFW    $93A0,$C286,$E080,$2852,$5D2C,$942C,$4103
        DEFW    $D850,$1661,$0A18,$0208,$E4A1,$747C,$0938
        DEFW    $0105,$9594,$5090,$3C54,$4600,$81D6,$2041
        DEFW    $8D58,$5B0B,$5745,$6203,$6870,$1D79,$491F
        DEFW    $4B94,$A141,$5010,$172E,$020E,$A0C2,$0404
        DEFW    $2E50,$095F,$1285,$B0C4,$4DB8,$4C3A,$034E
        DEFW    $C1C2,$9090,$1A35,$3155,$0111,$F0A0,$48AA
        DEFW    $1416,$840A,$2082,$6084,$0850,$070B,$0095
        DEFW    $0080,$4800,$2946,$184A,$2282,$8180,$26AC
        DEFW    $082A,$D009,$30E1,$3878,$1302,$8708,$A610
        DEFW    $0080,$3019,$0C69,$8308,$7422,$70B1,$1A00
        DEFW    $044A,$0680,$B880,$3671,$0C45,$C209,$C0C1
        DEFW    $88B8,$1008,$830D,$6092,$C000,$2C8C,$0318
        DEFW    $9184,$22A1,$58D1,$0B36,$1105,$4182,$C080
        DEFW    $1E00,$041A,$E020,$38A9,$88C8,$9720,$04AE
        DEFW    $88C2,$C8AA,$126C,$0F0B,$ABA1,$4001,$2692
        DEFW    $94AC,$A28B,$C4C0,$6892,$3A20,$0B0A,$C281
        DEFW    $5288,$28E1,$901A,$8386,$2802,$1A08,$A254
        DEFW    $0840,$4185,$74C0,$30A9,$0020,$8406,$E050
        DEFW    $88D2,$1C00,$030A,$0925,$00C0,$4890,$0F1A
        DEFW    $0105,$A511,$0020,$141C,$0911,$5185,$1020
        DEFW    $2C68,$142C,$8603,$0180,$AD24,$3054,$434B
        DEFW    $1311,$1084,$1D80,$4D00,$8307,$8192,$2820
        DEFW    $2C64,$0416,$4003,$14E1,$01A5,$4131,$850A
        DEFW    $0100,$C820,$1749,$0947,$4202,$64E0,$2C7C
        DEFW    $4818,$8348,$0413,$B0E0,$2E4C,$0A50,$4352
        DEFW    $40A1,$5420,$124C,$830D,$2083,$2855,$9E58
        DEFW    $3393,$E524,$C100,$3838,$8F10,$0627,$4083
        DEFW    $D2E2,$1C34,$0989,$0015,$F228,$3A7A,$1044
        DEFW    $020A,$6982,$3818,$0A4C,$2598,$0B01,$6081
        DEFW    $1058,$972C,$840D,$6182,$40A0,$3C54,$4217
        DEFW    $C814,$80A1,$5C30,$9F22,$8606,$C100,$1071
        DEFW    $3065,$0759,$D203,$60B1,$7458,$1A16,$0802
        DEFW    $2452,$71DA,$8C2C,$A353,$C814,$5004,$5C18
        DEFW    $580C,$8009,$240B,$40C0,$2C60,$2A81,$1086
        DEFW    $80C4,$2CB8,$842E,$0688,$6901,$B050,$BC28
        DEFW    $1090,$4002,$70A1,$5C20,$0938,$020B,$8104
        DEFW    $D534,$2618,$0A1C,$0286,$6021,$5C58,$1F40
        DEFW    $4042,$C10A,$F880,$224C,$0901,$0204,$7288
        DEFW    $1490,$0DAE,$4347,$E151,$B830,$AE54,$0A11
        DEFW    $C382,$6081,$70D0,$1220,$A903,$E103,$E050
        DEFW    $1E10,$2987,$0B05,$B0A0,$68F8,$411A,$8209
        DEFW    $0280,$2835,$2668,$4012,$0018,$F0C1,$44B8
        DEFW    $183A,$8100,$C2C1,$7C24,$0221,$2318,$0205
        DEFW    $0041,$7428,$1936,$C44A,$E450,$9880,$224C
        DEFW    $450D,$8B95,$0040,$7428,$5524,$8208,$6102
        DEFW    $00C0,$244D,$0C06,$4204,$4481,$48CC,$2208
        DEFW    $8412,$00C4,$0031,$2F51,$0249,$4292,$0081
        DEFW    $31C8,$000C,$810B,$1053,$B830,$102C,$0218
        DEFW    $4306,$80E1,$7800,$253C,$1653,$640A,$B890
        DEFW    $005C,$080D,$23A0,$80A1,$0062,$1AAC,$0407
        DEFW    $4183,$E280,$9024,$0A13,$C105,$E040,$3082
        DEFW    $0CA8,$030D,$C900,$0048,$2C30,$0912,$C285
        DEFW    $72E9,$0049,$1132,$0507,$E042,$18D4,$3A4C
        DEFW    $0108,$C205,$0400,$6CDD,$1B2C,$8210,$0180
        DEFW    $6060,$3860,$4305,$C045,$D0C0,$64B8,$0E0E
        DEFW    $8352,$640A,$3802,$3C48,$0A12,$D083,$F005
        DEFW    $3490,$0C32,$0601,$A1C0,$8414,$2844,$0929
        DEFW    $C080,$54E1,$4D19,$5428,$810E,$21C1,$2820
        DEFW    $0020,$460C,$8301,$60E1,$1CD0,$160E,$4443
        DEFW    $8102,$3944,$3658,$2B08,$C306,$60C1,$3202
        DEFW    $9B46,$8426,$A082,$8800,$1E20,$4B41,$9201
        DEFW    $82C4,$58C0,$0000,$820C,$0104,$4820,$2660
        DEFW    $8984,$C184,$4A48,$0088,$1820,$8003,$0143
        DEFW    $7840,$1E80,$8C92,$8386,$0022,$0090,$152E
        DEFW    $2606,$A408,$D182,$3A08,$000D,$5105,$6005
        DEFW    $78E8,$074E,$8205,$8942,$E078,$3E74,$0727
        DEFW    $4200,$5440,$6CD9,$142C,$030F,$A104,$2810
        DEFW    $1254,$A084,$8305,$9020,$24D0,$9A40,$818C
        DEFW    $40C3,$E890,$0A50,$1407,$4006,$2009,$5089
        DEFW    $1EB0,$0309,$A243,$0901,$4C30,$130F,$D203
        DEFW    $40C4,$7458,$0842,$4252,$C044,$30F1,$113D
        DEFW    $0806,$8180,$A040,$7D00,$5C40,$900C,$000A
        DEFW    $F060,$1054,$0610,$8284,$A5C0,$1818,$1C44
        DEFW    $870F,$C0C0,$B8C0,$2628,$0A0A,$4CA0,$80C1
        DEFW    $5C98,$0A38,$870B,$01C0,$5514,$307C,$0124
        DEFW    $D200,$E0D1,$1D89,$1902,$0100,$C0C4,$18D0
        DEFW    $3494,$4407,$D043,$60C5,$5CC9,$1A84,$944A
        DEFW    $8082,$F870,$2854,$2499,$8002,$2562,$1000
        DEFW    $0A2E,$9347,$0181,$98F0,$3080,$001A,$C401
        DEFW    $E480,$78E4,$18B2,$050C,$2511,$1081,$4020
        DEFW    $0A1D,$4106,$90E1,$14AA,$0F34,$124C,$2502
        DEFW    $3884,$161D,$2C13,$C300,$9421,$4468,$192E
        DEFW    $0311,$E983,$69D2,$AE58,$0620,$4301,$9082
        DEFW    $78D1,$0E20,$1847,$00C2,$F8B0,$1E58,$4000
        DEFW    $C146,$30E0,$44E0,$1906,$4347,$8101,$D0C0
        DEFW    $003D,$2A4E,$0284,$6005,$38F0,$001C,$010B
        DEFW    $6011,$1010,$3058,$040D,$D102,$C030,$0408
        DEFW    $1414,$0615,$8183,$0871,$500C,$4B20,$4141
        DEFW    $F080,$6998,$552C,$130B,$A141,$0220,$A224
        DEFW    $0511,$8083,$0480,$7004,$191A,$960F,$E080
        DEFW    $6800,$1E01,$031B,$4488,$9081,$3058,$1B38
        DEFW    $D652,$C001,$E030,$2C7C,$471D,$8245,$F0E1
        DEFW    $0809,$0C1A,$8808,$2253,$B020,$3654,$481E
        DEFW    $8992,$A0A1,$6CB0,$5428,$0607,$0040,$18E1
        DEFW    $2391,$0A43,$5385,$9051,$51B0,$0B38,$0424
        DEFW    $8043,$6070,$2278,$8D8A,$0302,$E041,$20D0
        DEFW    $9826,$8323,$4143,$E970,$4858,$100F,$0001
        DEFW    $A022,$02BA,$0718,$8609,$62C0,$A090,$3004
        DEFW    $2B96,$C905,$B000,$4821,$98A6,$012D,$20C2
        DEFW    $0200,$4A12,$0912,$4587,$B808,$6848,$11A6
        DEFW    $0000,$8802,$A090,$0044,$8E80,$63A0,$7000
        DEFW    $4478,$9EA8,$070C,$E882,$6182,$2C58,$2C52
        DEFW    $4386,$9064,$58A0,$0024,$860F,$E041,$A8C4
        DEFW    $3A48,$0B04,$1280,$2045,$48C8,$160E,$830E
        DEFW    $6903,$81A2,$2C44,$0B17,$8002,$F4E0,$5888
        DEFW    $1B3A,$0504,$0045,$4090,$3001,$071A,$0305
        DEFW    $9401,$4C51,$5F98,$070E,$C109,$8282,$065C
        DEFW    $0817,$8923,$02E2,$10C0,$2B24,$864A,$6143
        DEFW    $E860,$1E3D,$0F1A,$D385,$F0A4,$7CE0,$0918
        DEFW    $820A,$C5D3,$58B0,$2C60,$0804,$1104,$9034
        DEFW    $0CE8,$0014,$D754,$C083,$1D34,$2A10,$030F
        DEFW    $0181,$5520,$6014,$123A,$8308,$4552,$6122
        DEFW    $4A1C,$2913,$C300,$00E0,$5CD0,$0B3C,$2707
        DEFW    $6925,$5022,$4090,$060E,$4106,$30E8,$68D0
        DEFW    $1112,$2607,$01A4,$0821,$3C58,$8D8D,$4205
        DEFW    $8081,$28CA,$19BE,$042E,$2003,$7232,$1E5A
        DEFW    $121B,$8AA3,$80E1,$4C28,$A112,$0422,$0104
        DEFW    $2051,$283C,$0FA1,$4304,$F020,$9A72,$1B3C
        DEFW    $0507,$280B,$DAF0,$2A32,$0616,$8384,$A021
        DEFW    $7CBA,$5690,$1406,$81C2,$5052,$2250,$0D64
        DEFW    $C286,$3002,$4068,$0614,$2600,$0182,$E070
        DEFW    $326D,$2E4D,$4202,$3441,$4068,$1B12,$8009
        DEFW    $89C1,$7930,$AA60,$0810,$0326,$3001,$0862
        DEFW    $0E28,$974C,$8582,$70D0,$1C4D,$460E,$C197
        DEFW    $E004,$2042,$192A,$C446,$8492,$60A0,$1248
        DEFW    $AA9C,$0203,$40E1,$469A,$8C1C,$1521,$65C0
        DEFW    $7960,$0C5C,$060B,$0380,$A0C1,$6DE0,$0F41
        DEFW    $0308,$8592,$4140,$1454,$0961,$4287,$E0A1
        DEFW    $54A0,$232E,$A70D,$C183,$6054,$064C,$070D
        DEFW    $8284,$7029,$1450,$8BA6,$2208,$40E1,$D2F0
        DEFW    $963C,$2617,$8115,$D2A1,$888A,$1720,$A304
        DEFW    $0081,$3850,$301C,$4508,$C100,$6045,$64C8
        DEFW    $0A20,$4450,$2603,$D804,$3660,$5114,$4210
        DEFW    $D0C4,$5198,$0F40,$030F,$C101,$CDA4,$2C40
        DEFW    $0900,$0C84,$7441,$68E9,$8C0C,$0408,$7010
        DEFW    $B0A0,$2628,$2D1A,$1102,$A084,$9040,$8600
        DEFW    $0231,$0403,$F004,$3C6D,$044C,$0222,$8000
        DEFW    $00F8,$0708,$9253,$75C9,$8922,$2850,$070F
        DEFW    $4103,$A0C0,$8CDA,$0D98,$0609,$A10B,$0891
        DEFW    $3A48,$0E1C,$0386,$A0C9,$2CC0,$2138,$0507
        DEFW    $0880,$7800,$1A34,$0E59,$C3A3,$8001,$7058
        DEFW    $0E42,$000B,$0410,$61A2,$1438,$0E10,$02A4
        DEFW    $7081,$0C50,$1922,$8108,$61C0,$6830,$1C2D
        DEFW    $034F,$C090,$A0E1,$5998,$581E,$0404,$A0C1
        DEFW    $B0D0,$2C61,$4D1A,$0045,$4165,$4838,$0C34
        DEFW    $860C,$A104,$1875,$4C3C,$0858,$4000,$64E0
        DEFW    $34D0,$5B32,$820A,$E1D5,$E0A0,$1A59,$A753
        DEFW    $4393,$B021,$7060,$1700,$2402,$E109,$48A0
        DEFW    $2E40,$4A00,$82C4,$50A1,$08D0,$172C,$030D
        DEFW    $24D4,$B0C0,$3260,$4914,$8255,$81E1,$460A
        DEFW    $172A,$0807,$2904,$A520,$A430,$230A,$8087
        DEFW    $D001,$2C50,$1128,$834D,$C103,$1051,$286C
        DEFW    $0C87,$C385,$2061,$78A0,$8D34,$060A,$81C1
        DEFW    $4964,$1630,$0B17,$0382,$E021,$5872,$0B12
        DEFW    $9203,$814A,$6030,$3248,$051C,$0824,$F180
        DEFW    $4040,$931C,$830D,$4183,$1821,$4A50,$2383
        DEFW    $1203,$50E5,$4C62,$8CB4,$030B,$0413,$9122
        DEFW    $9842,$2C46,$8915,$60C8,$1DA9,$061E,$9048
        DEFW    $C101,$E880,$0068,$065A,$C1A3,$C0E1,$6060
        DEFW    $0F36,$9640,$A14A,$8890,$1838,$0F19,$4206
        DEFW    $10A1,$0092,$0D1C,$820E,$61C3,$02A1,$9C7C
        DEFW    $0E15,$5284,$B401,$4CC1,$100E,$0520,$4552
        DEFW    $A820,$2258,$079D,$4185,$4421,$2841,$0E3C
        DEFW    $A60D,$2108,$EA90,$A400,$2199,$4206,$E0E8
        DEFW    $6CD0,$8F26,$068A,$8502,$5210,$321C,$0C14
        DEFW    $0B07,$E3A8,$3822,$8E26,$D407,$2049,$FA30
        DEFW    $3828,$0603,$8002,$0008,$1C30,$1906,$0300
        DEFW    $2511,$6122,$B404,$2D13,$0100,$CAE1,$3801
        DEFW    $103C,$A629,$818B,$F200,$2840,$0B13,$4105
        DEFW    $D008,$7860,$0110,$984D,$C042,$E0F0,$1F4D
        DEFW    $0D20,$0083,$A040,$5C10,$1F22,$144B,$0040
        DEFW    $C8B2,$3A88,$0E8C,$0383,$14A2,$8861,$0B00
        DEFW    $162E,$2293,$C924,$3434,$0317,$C3A0,$4109
        DEFW    $0018,$1930,$8626,$09C3,$F000,$1C6C,$5018
        DEFW    $8244,$F061,$3400,$0016,$880B,$6282,$1034
        DEFW    $AE44,$0C0F,$0304,$B060,$00A8,$593A,$1440
        DEFW    $00D0,$0124,$1248,$0E42,$4001,$6402,$7551
        DEFW    $0A00,$1240,$2241,$0004,$1E28,$070E,$9104
        DEFW    $F434,$690C,$4D02,$940A,$A101,$C840,$B050
        DEFW    $2E1C,$9283,$4006,$4C72,$0E26,$064E,$80C3
        DEFW    $72D0,$1C5C,$0712,$8183,$80E1,$8A3A,$1522
        DEFW    $A426,$81C1,$6230,$9242,$030F,$4A82,$3081
        DEFW    $4860,$1C32,$2405,$600B,$D0E0,$4648,$1214
        DEFW    $C0A0,$F180,$4040,$931C,$14A6,$C410,$30F4
        DEFW    $3335,$0206,$0382,$3001,$44C0,$1214,$D240
        DEFW    $8442,$50C0,$2A61,$0B0E,$1385,$F2D0,$0C90
        DEFW    $1134,$8105,$E002,$0000,$2059,$0109,$C9A1
        DEFW    $D024,$3419,$4994,$012C,$41C1,$80D0,$0A2C
        DEFW    $0B9E,$C001,$0020,$12A2,$0D2E,$8508,$0983
        DEFW    $B1A0,$2258,$0214,$C826,$40C0,$4800,$0006
        DEFW    $844D,$81D1,$5252,$2C70,$4211,$4812,$50C0
        DEFW    $1420,$1328,$250B,$6040,$A840,$0854,$0904
        DEFW    $8004,$4480,$08A0,$1006,$9509,$2003,$C8E0
        DEFW    $164C,$030E,$D486,$6041,$6CE0,$1734,$0008
        DEFW    $81C2,$B8E0,$2065,$2543,$4A83,$1081,$5458
        DEFW    $0C1C,$0202,$61C4,$E974,$1A44,$010D,$6222
        DEFW    $5268,$4810,$0124,$8000,$0100,$9010,$3921
        DEFW    $2909,$4123,$DA48,$3431,$8B00,$8005,$7011
        DEFW    $59F0,$1610,$4311,$4183,$F460,$4038,$4800
        DEFW    $0006,$844D,$81D1,$5252,$2C70,$4211,$4812
        DEFW    $50C0,$1420,$1328,$250B,$6040,$A840,$0854
        DEFW    $0904,$8004,$4480,$08A0,$1006,$9509,$2003
        DEFW    $C8E0,$164C,$030E,$FFEF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFEB,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFB3,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFD3
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FF97,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FF77,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FF6F,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0105,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFDB,$0101,$FFFF,$0101,$FFFF,$0101,$FFFB
        DEFW    $0101,$FF87,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FF9F,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FF8B,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FF0F,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFB3
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FF57,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0105,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0105,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFB,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101,$FFFF,$0101,$FFFF
        DEFW    $0101,$FFFF,$0101,$FFFF,$0101,$FFFF,$0101
        DEFW    $FFFF,$0101,$FFFF,$0101