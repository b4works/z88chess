# Chezz, based on Cyrus IS Chess for the Sinclair ZXSpectrum 48K, for the Cambridge Z88

```
   ____   ___
  6MMMMb/ `MM
 8P    YM  MM
6M      Y  MM  __     ____   _________ _________
MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
MM         MM'   MM MM    MM     dMP       dMP
MM         MM    MM MMMMMMMM    dMP       dMP
YM      6  MM    MM MM         dMP       dMP
 8b    d9  MM    MM YM    d9  dMP    /  dMP    /
  YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM

```

Conversion and enhancements (c) 1999 Keith Rickard

## Introduction

Chezz is a conversion of one of the strongest chess programs available for the ZX Spectrum, Cyrus IS Chess. This is a beta version of the software, so may still contain some bugs, and has several unimplemented features. However, none of this should detract from what I hope is an excellent and extremely playable game for all you travelling chessmasters. This game is the continuation of the original Z88Chess V0.03 Beta release.

Current available features include:

- 8 levels of play (including 3 problem-solving levels)
- take back move facility
- game replay
- force Chezz to play best move
- board setup
- sound effects
- 0-2 player modes
- invert board (black at bottom, white at top)
- keeps a record of pieces taken
- swap chessmen to text notation
- call up board square references

Press ESC to stop Chezz thinking; this may turn the Z88 off if its timeout expires.

Keith Rickard
1 October 2019


## Downloading and installing Z88 Assembler Workbench tools for your desktop platform
T.B.D

## Re-compiling original ZX Spectrum game
T.B.D

## Compiling source code into Z88 Application Card Binary
T.B.D.

## Blowing Application Card Binary to EPROM or FLASH CARD
T.B.D

## Chess logo
Ascii art has been generated from http://patorjk.com/software/taag/ using Giorgi16 font