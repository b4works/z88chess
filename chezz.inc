; ------------------------------------------------------------------------------------
;   ____   ___
;   6MMMMb/ `MM
;  8P    YM  MM
; 6M      Y  MM  __     ____   _________ _________
; MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
; MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
; MM         MM'   MM MM    MM     dMP       dMP
; MM         MM    MM MMMMMMMM    dMP       dMP
; YM      6  MM    MM MM         dMP       dMP
;  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
;   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
;
; Based on Cyrus IS Chess for the Sinclair ZX Spectrum
; (C) 1982 Richard Lang, Intelligent Software Ltd
;
; Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
;
; Chezz is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published by the Free Software Foundation; either
; version 2, or (at your option) any later version. Chezz is distributed in the hope
; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details. You should have received a copy
; of the GNU General Public License along with Chezz; see the file LICENSE.
; If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ------------------------------------------------------------------------------------


;Define the variables area
DEFVARS $2C00-158
        clockinput      ds.b 9  ;2B62
        wclockstart     ds.b 2  ;2B6B
        wclockticks     ds.b 2  ;2B7D
        bclockstart     ds.b 2  ;2B7F
        bclockticks     ds.b 2  ;2B71
        ticks           ds.b 1  ;2B73   Not used

        reprint_msg     ds.b 2  ;2B74
        zcflags         ds.b 1  ;2B76
        zcflags2        ds.b 1  ;2B77
        err_code        ds.b 1  ;2B78   Not used

        inputfname      ds.b 2  ;2B79   Not used
        explicit        ds.b 2  ;2B7B   Not used

        zcstack         ds.b 2  ;2B7D
        stack           ds.b 2  ;2B7F

        movetxtptr      ds.b 1  ;2B81
        movetext        ds.b 26 ;2B82

        blackpcs        ds.b 6  ;2B9C
        whitepcs        ds.b 6  ;2BA2
        blackold        ds.b 6  ;2BA8
        whiteold        ds.b 6  ;2BAE
        blackcount      ds.b 6  ;2BB4
        whitecount      ds.b 6  ;2BBA
        oldboard        ds.b 64 ;2BC0
                                ;2C00
ENDDEF

;zcflags flags:
        DEFC    ip = 7  ;1 = Game in progress
        DEFC    pp = 6  ;1 = Pawn promotion
        DEFC    pm = 5  ;1 = Play mode
        DEFC    yn = 4  ;Yes/No
        DEFC    sb = 3  ;Board setup mode
;__NOTE pr flag does not seem to be used
        DEFC    sr = 2  ;Square references mode
;__
        DEFC    pr = 1  ;Print square references
        DEFC    cm = 0  ;Chessmen

;zcflags2 flags
        DEFC    bp = 3  ;A black piece has been captured
        DEFC    wp = 2  ;A white piece has been captured
        DEFC    bb = 1
        DEFC    ww = 0

        DEFC    GAME_DATA = $2C00               ;Default $C000
        DEFC    WORKSPACE = $1C00               ;Default $B000 - has be on a 256 byte address boundary
        DEFC    THINKAREA = $2000               ;Default $B400
        DEFC    LOADSPACE = $C000               ;Default $E000        
        DEFC    IOPORTFEH = $B0                 ;1bit Beeper port on the Z88
        DEFC    CHARS = $C000                   ;Default $5C36
        DEFC    FLAGS = $C000                   ;Default $5C3B
        DEFC    LAST_K = $C000                  ;Default $5C08
        DEFC    RAMTOP = $C000                  ;Default $5CB2
        DEFC    P_RAMT = $C000                  ;Default $5CB4

        DEFC    auto04C6 = $04C6                ;ZX Spectrum SA-BYTES routine for saving to tape
        DEFC    auto0562 = $0562                ;ZX Spectrum LD-BYTES routine for loading from tape

        DEFC    auto00BC = WORKSPACE/256+$0C
        DEFC    auto00BD = WORKSPACE/256+$0D    ; MSB of cyrusBoard
        DEFC    auto00BE = WORKSPACE/256+$0E    ;Chess piece on board info address MSB
        DEFC    auto00BF = WORKSPACE/256+$0F

        DEFC    autoB400 = THINKAREA+0          ;Thinking workspace
        DEFC    autoB401 = THINKAREA+$0001
        DEFC    autoB403 = THINKAREA+$0003
        DEFC    autoB404 = THINKAREA+$0004

        DEFC    autoBC83 = WORKSPACE+$0C83
        DEFC    autoBC91 = WORKSPACE+$0C91
        DEFC    autoBC9E = WORKSPACE+$0C9E
        DEFC    autoBC9F = WORKSPACE+$0C9F
        DEFC    autoBCA0 = WORKSPACE+$0CA0
        DEFC    pieceColour = WORKSPACE+$0CA2   ; $03 = Black, $83 = White
        DEFC    autoBCA3 = WORKSPACE+$0CA3
        DEFC    autoBCA9 = WORKSPACE+$0CA9
        DEFC    autoBCAA = WORKSPACE+$0CAA
        DEFC    autoBCAB = WORKSPACE+$0CAB      ;Thinking workspace pointer
        DEFC    autoBCB2 = WORKSPACE+$0CB2
        DEFC    autoBCB3 = WORKSPACE+$0CB3
        DEFC    autoBCB6 = WORKSPACE+$0CB6
        DEFC    autoBCBD = WORKSPACE+$0CBD
        DEFC    playerTurn = WORKSPACE+$0CBE      ;0=White's turn 1 =Black's turn
        DEFC    gameOver = WORKSPACE+$0CBF      ;<>0 Game finished
        DEFC    lastKey = WORKSPACE+$0CC0       ;Pressed key code
        DEFC    autoBCC1 = WORKSPACE+$0CC1      ;Character to be printed
        DEFC    contenders = WORKSPACE+$0CC2      ;Bit 0=1, Comp plays B, Bit 1=1 Comp plays W
        DEFC    replayGame = WORKSPACE+$0CC3      ;Replay/Play this move? 1=Replay move
        DEFC    ZX_scn_Sqpc_attr = WORKSPACE+$0CC4   ;Scn [Sq/Pc] attribute (ZX Spectrum !!DELETE)
        DEFC    orientation = WORKSPACE+$0CC7   ;Board orientation
        DEFC    autoBCC8 = WORKSPACE+$0CC8      ;ZXSpectrum IY value
        DEFC    cur_pos = WORKSPACE+$0CCA       ;Cursor position
        DEFC    autoBCCB = WORKSPACE+$0CCB
        DEFC    autoBCCC = WORKSPACE+$0CCC
        DEFC    level = WORKSPACE+$0CCD         ;Computer's playing level+1. 0-7 are playing levels, 8-10 are problem solving
        DEFC    randomNo = WORKSPACE+$0CCE      ;Random number
        DEFC    autoBCCF = WORKSPACE+$0CCF
        DEFC    autoBCD1 = WORKSPACE+$0CD1      ;Beep parameter 1
        DEFC    autoBCD2 = WORKSPACE+$0CD2      ;Beep parameter 2
        DEFC    beeper = WORKSPACE+$0CD3        ;Sound, 0=on, 1=off
        DEFC    ZXPrtListing = WORKSPACE+$0CD4  ;Print listing on (0) or off(1)? (no longer used)
        DEFC    ZX_CHARS = WORKSPACE+$0CD5      ;ZXSpectrum CHARS pointer
        DEFC    blackPcCol = WORKSPACE+$0CD7    ;BPc colour
        DEFC    whitePcCol = WORKSPACE+$0CD8    ;WPc colour
        DEFC    blackSqCol = WORKSPACE+$0CD9    ;BSq colour
        DEFC    whiteSqCol = WORKSPACE+$0CDA    ;WSq colour
        DEFC    autoBCDB = WORKSPACE+$0CDB      ;(This may have something to do with how the Knight moves)
        DEFC    autoBCDC = WORKSPACE+$0CDC      ;ZXPrinter buffer x-coord (no longer used)
        DEFC    autoBCDD = WORKSPACE+$0CDD
        DEFC    promoted = WORKSPACE+$0CDE      ;Last promoted piece
        DEFC    autoBCDF = WORKSPACE+$0CDF

        DEFC    cyrusBoard = WORKSPACE+$0D00    ;Current board
        DEFC    autoBD09 = WORKSPACE+$0D09      ;(Used by take-Back)
        DEFC    autoBD0A = WORKSPACE+$0D0A      ;(Used by take-Back)
        DEFC    autoBD0B = WORKSPACE+$0D0B
        DEFC    autoBD0C = WORKSPACE+$0D0C
        DEFC    autoBD0D = WORKSPACE+$0D0D
        DEFC    autoBD0E = WORKSPACE+$0D0E
        DEFC    autoBD19 = WORKSPACE+$0D19
        DEFC    autoBD1A = WORKSPACE+$0D1A
        DEFC    autoBD1E = WORKSPACE+$0D1E
        DEFC    autoBD29 = WORKSPACE+$0D29
        DEFC    autoBD2B = WORKSPACE+$0D2B
        DEFC    nextToPlay = WORKSPACE+$0D2D      ;0=White's turn, 1=Black's turn
        DEFC    autoBD2E = WORKSPACE+$0D2E
        DEFC    autoBD39 = WORKSPACE+$0D39
        DEFC    autoBD3B = WORKSPACE+$0D3B
        DEFC    autoBD3D = WORKSPACE+$0D3D
        DEFC    moveNo = WORKSPACE+$0D49      ;Move number
        DEFC    autoBD4A = WORKSPACE+$0D4A
        DEFC    autoBD4B = WORKSPACE+$0D4B
        DEFC    autoBD4C = WORKSPACE+$0D4C
        DEFC    autoBD4D = WORKSPACE+$0D4D
        DEFC    autoBD4E = WORKSPACE+$0D4E
        DEFC    autoBD58 = WORKSPACE+$0D58

        DEFC    autoBD59 = WORKSPACE+$0D59      ;Workspace of 6 byte (Used by take-Back
        DEFC    autoBD5A = WORKSPACE+$0D5A
        DEFC    autoBD5B = WORKSPACE+$0D5B
        DEFC    autoBD5C = WORKSPACE+$0D5C
        DEFC    repeatedMoveCount = WORKSPACE+$0D5E      ;Counts the number of moves repeated. When 6 game is drawn

        DEFC    autoBD6D = WORKSPACE+$0D6D
        DEFC    autoBD6E = WORKSPACE+$0D6E
        DEFC    autoBD79 = WORKSPACE+$0D79
        DEFC    autoBDF4 = WORKSPACE+$0DF4      ;Initial stack pointer
        DEFC    autoBDF6 = WORKSPACE+$0DF6

        DEFC    autoBE00 = WORKSPACE+$0E00      ;Active chess piece info
        DEFC    autoBE08 = WORKSPACE+$0E08
        DEFC    autoBE09 = WORKSPACE+$0E09
        DEFC    autoBE0A = WORKSPACE+$0E0A
        DEFC    autoBE0C = WORKSPACE+$0E0C

        DEFC    autoBE10 = WORKSPACE+$0E10
        DEFC    autoBE12 = WORKSPACE+$0E12
        DEFC    autoBE14 = WORKSPACE+$0E14
        DEFC    autoBE18 = WORKSPACE+$0E18
        DEFC    autoBE1A = WORKSPACE+$0E1A
        DEFC    autoBE1C = WORKSPACE+$0E1C

        DEFC    autoBE20 = WORKSPACE+$0E20
        DEFC    autoBE22 = WORKSPACE+$0E22
        DEFC    autoBE24 = WORKSPACE+$0E24
        DEFC    autoBE28 = WORKSPACE+$0E28
        DEFC    autoBE2A = WORKSPACE+$0E2A
        DEFC    autoBE2C = WORKSPACE+$0E2C

        DEFC    autoBE30 = WORKSPACE+$0E30
        DEFC    autoBE32 = WORKSPACE+$0E32
        DEFC    autoBE34 = WORKSPACE+$0E34
        DEFC    autoBE38 = WORKSPACE+$0E38
        DEFC    autoBE3A = WORKSPACE+$0E3A
        DEFC    autoBE3C = WORKSPACE+$0E3C

        DEFC    autoBE40 = WORKSPACE+$0E40
        DEFC    autoBE41 = WORKSPACE+$0E41
        DEFC    autoBE42 = WORKSPACE+$0E42
        DEFC    autoBE44 = WORKSPACE+$0E44
        DEFC    autoBE48 = WORKSPACE+$0E48
        DEFC    autoBE49 = WORKSPACE+$0E49
        DEFC    autoBE4A = WORKSPACE+$0E4A
        DEFC    autoBE4C = WORKSPACE+$0E4C

        DEFC    autoBE50 = WORKSPACE+$0E50
        DEFC    autoBE51 = WORKSPACE+$0E51
        DEFC    autoBE52 = WORKSPACE+$0E52
        DEFC    autoBE54 = WORKSPACE+$0E54
        DEFC    autoBE58 = WORKSPACE+$0E58
        DEFC    autoBE59 = WORKSPACE+$0E59
        DEFC    autoBE5A = WORKSPACE+$0E5A
        DEFC    autoBE5C = WORKSPACE+$0E5C

        DEFC    autoBE60 = WORKSPACE+$0E60
        DEFC    autoBE61 = WORKSPACE+$0E61
        DEFC    autoBE62 = WORKSPACE+$0E62
        DEFC    autoBE64 = WORKSPACE+$0E64
        DEFC    autoBE68 = WORKSPACE+$0E68
        DEFC    autoBE69 = WORKSPACE+$0E69
        DEFC    autoBE6A = WORKSPACE+$0E6A
        DEFC    autoBE6C = WORKSPACE+$0E6C

        DEFC    autoBE70 = WORKSPACE+$0E70
        DEFC    autoBE71 = WORKSPACE+$0E71
        DEFC    autoBE72 = WORKSPACE+$0E72
        DEFC    autoBE74 = WORKSPACE+$0E74
        DEFC    autoBE78 = WORKSPACE+$0E78
        DEFC    autoBE79 = WORKSPACE+$0E79
        DEFC    autoBE7A = WORKSPACE+$0E7A
        DEFC    autoBE7C = WORKSPACE+$0E7C

        DEFC    autoBE80 = WORKSPACE+$0E80
        DEFC    autoBE88 = WORKSPACE+$0E88
        DEFC    autoBE89 = WORKSPACE+$0E89
        DEFC    autoBE8A = WORKSPACE+$0E8A
        DEFC    autoBE8C = WORKSPACE+$0E8C
        DEFC    autoBE90 = WORKSPACE+$0E90
        DEFC    autoBE92 = WORKSPACE+$0E92
        DEFC    autoBE94 = WORKSPACE+$0E94
        DEFC    autoBE98 = WORKSPACE+$0E98
        DEFC    autoBE9A = WORKSPACE+$0E9A
        DEFC    autoBE9C = WORKSPACE+$0E9C
        DEFC    autoBEA0 = WORKSPACE+$0EA0
        DEFC    autoBEA2 = WORKSPACE+$0EA2
        DEFC    autoBEA4 = WORKSPACE+$0EA4
        DEFC    autoBEA8 = WORKSPACE+$0EA8
        DEFC    autoBEAA = WORKSPACE+$0EAA
        DEFC    autoBEAC = WORKSPACE+$0EAC
        DEFC    autoBEB0 = WORKSPACE+$0EB0
        DEFC    autoBEB2 = WORKSPACE+$0EB2
        DEFC    autoBEB4 = WORKSPACE+$0EB4
        DEFC    autoBEB8 = WORKSPACE+$0EB8
        DEFC    autoBEBA = WORKSPACE+$0EBA
        DEFC    autoBEBC = WORKSPACE+$0EBC
        DEFC    autoBEC0 = WORKSPACE+$0EC0
        DEFC    autoBEC1 = WORKSPACE+$0EC1
        DEFC    autoBEC2 = WORKSPACE+$0EC2
        DEFC    autoBEC4 = WORKSPACE+$0EC4
        DEFC    autoBEC8 = WORKSPACE+$0EC8
        DEFC    autoBEC9 = WORKSPACE+$0EC9
        DEFC    autoBECA = WORKSPACE+$0ECA
        DEFC    autoBECC = WORKSPACE+$0ECC
        DEFC    autoBED0 = WORKSPACE+$0ED0
        DEFC    autoBED1 = WORKSPACE+$0ED1
        DEFC    autoBED2 = WORKSPACE+$0ED2
        DEFC    autoBED4 = WORKSPACE+$0ED4
        DEFC    autoBED8 = WORKSPACE+$0ED8
        DEFC    autoBED9 = WORKSPACE+$0ED9
        DEFC    autoBEDA = WORKSPACE+$0EDA
        DEFC    autoBEDC = WORKSPACE+$0EDC
        DEFC    autoBEE0 = WORKSPACE+$0EE0
        DEFC    autoBEE1 = WORKSPACE+$0EE1
        DEFC    autoBEE2 = WORKSPACE+$0EE2
        DEFC    autoBEE4 = WORKSPACE+$0EE4
        DEFC    autoBEE8 = WORKSPACE+$0EE8
        DEFC    autoBEE9 = WORKSPACE+$0EE9
        DEFC    autoBEEA = WORKSPACE+$0EEA
        DEFC    autoBEEC = WORKSPACE+$0EEC
        DEFC    autoBEF0 = WORKSPACE+$0EF0
        DEFC    autoBEF1 = WORKSPACE+$0EF1
        DEFC    autoBEF2 = WORKSPACE+$0EF2
        DEFC    autoBEF4 = WORKSPACE+$0EF4
        DEFC    autoBEF8 = WORKSPACE+$0EF8
        DEFC    autoBEF9 = WORKSPACE+$0EF9
        DEFC    autoBEFA = WORKSPACE+$0EFA
        DEFC    autoBEFC = WORKSPACE+$0EFC
        DEFC    autoBF00 = WORKSPACE+$0F00

        DEFC    autoC000 = GAME_DATA+0
        DEFC    GDmoveOffset = GAME_DATA+$0001  ; Offset to point to move in Move history
        DEFC    autoC003 = GAME_DATA+$0003      ; To move (for automatic playback??): <>0 means play a move
        DEFC    autoC005 = GAME_DATA+$0005      ; Whose turn to play: 0=White, 1=Black
        DEFC    autoC006 = GAME_DATA+$0006      ; Current move number
        DEFC    autoC007 = GAME_DATA+$0007      
        DEFC    autoC047 = GAME_DATA+$0047      ; Promoted piece??
        DEFC    autoC057 = GAME_DATA+$0057
        DEFC    GDmoveHistory = GAME_DATA+$0058      ; Start of Move history space - terminated by $FF

        DEFC    autoE000 = LOADSPACE+0
        DEFC    autoE058 = LOADSPACE+$0058

        DEFC    GameMap  = $C000                ; for zcdata.asm, referenced by z88cyrus.asm

; MTH key codes
        DEFC    CMD_WS = 128    ;Set White Player
        DEFC    CMD_BS = 129    ;Set Black Player
        DEFC    CMD_WC = 130    ;Set White's Clock
        DEFC    CMD_BC = 131    ;Set Black's Clock
        DEFC    CMD_L = 132     ;Set Chezz' level
        DEFC    CMD_G = 133     ;New Game
        DEFC    CMD_SS = 134    ;Swap Sides
        DEFC    CMD_T = 135     ;Toggle Sound On/Off
        DEFC    CMD_I = 136     ;Invert Board
        DEFC    CMD_M = 137     ;Toggle Chessmen
        DEFC    CMD_H = 138     ;Show Square References

        DEFC    CMD_P = 139     ;Chezz Plays Move
        DEFC    CMD_U = 140     ;Undo Last Move
        DEFC    CMD_A = 141     ;Advance a Move
        DEFC    CMD_R = 142     ;Replay All Moves
        DEFC    CMD_N = 143     ;Play Next Best Move
        DEFC    CMD_LEFT = $FC  ;Cursor Left
        DEFC    CMD_RIGHT = $FD ;Cursor Right
        DEFC    CMD_UP = $FF    ;Cursor Up
        DEFC    CMD_DOWN = $FE  ;Cursor Down
        DEFC    CMD_ENTER = 13  ;Select/Start Game

        DEFC    CMD_SB = 144    ;Setup Board Mode
        DEFC    CMD_SC = 145    ;Clear Board
        DEFC    CMD_SR = 146    ;Reset Board
        DEFC    CMD_ESC = 27    ;Exit Board Setup
        DEFC    CMD_DEL = 127   ;Empty Square
        DEFC    CMD_WK = 147    ;White King
        DEFC    CMD_WQ = 148    ;White Queen
        DEFC    CMD_WR = 149    ;White Rook
        DEFC    CMD_WN = 150    ;White Knight
        DEFC    CMD_WB = 151    ;White Bishop
        DEFC    CMD_WP = 152    ;White Pawn
        DEFC    CMD_BK = 153    ;Black King
        DEFC    CMD_BQ = 154    ;Black Queen
        DEFC    CMD_BR = 155    ;Black Rook
        DEFC    CMD_BN = 156    ;Black Knight
        DEFC    CMD_BB = 157    ;Black Bishop
        DEFC    CMD_BP = 158    ;Black Pawn

        DEFC    CMD_FL = 159    ;Load Game
        DEFC    CMD_FS = 160    ;Load Save
        DEFC    CMD_FM = 161    ;Record Move History
        DEFC    CMD_FB = 162    ;Record Board History
        DEFC    CMD_QUIT = 163  ;LEAVE Chezz
