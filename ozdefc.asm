LSTOFF

IF !__OZDEFC__
define __OZDEFC__

include "macros.def"

;Z88 system manifest equates per Z88 Developers notes V3

;Alarms
        DEFC    OS_ALM = $81    ;alarm manipulation (OZ usage)
        DEFC    GN_AAB = $6809  ;allocate alarm block
        DEFC    GN_ALP = $7009  ;process an expired alarm
        DEFC    GN_FAB = $6A09  ;free alarm block
        DEFC    GN_LAB = $6C09  ;link an alarm block into the alarm chain
        DEFC    GN_UAB = $6E09  ;unlink an alarm block from the alarm chain
;       DEFC    OS_ALM = $81    ;alarm manipulation (OZ usage)

;Application
        DEFC    OS_BYE = $21    ;exit application
        DEFC    DC_NAM = $0C0C  ;name application

;Arithmetic
        DEFC    GN_D16 = $7409  ;16bit unsigned division
        DEFC    GN_D24 = $7809  ;24bit unsigned division
        DEFC    GN_GDN = $1009  ;convert an ASCII string to a binary number
        DEFC    GN_M16 = $7209  ;16bit unsigned multiplication
        DEFC    GN_M24 = $7609  ;24bit unsigned multiplication
        DEFC    GN_PDN = $1209  ;write number as decimal ASCII string

;Binding
        DEFC    OS_MGB = $5A    ;get old bank binding
        DEFC    OS_MPB = $5D    ;set (put) new bank binding
        DEFC    OS_FC = $8A     ;fast code (bank binding)

;Characters
        DEFC    GN_CLS = $3009  ;classify a character
        DEFC    GN_SKC = $3209  ;skip character
        DEFC    GN_SKD = $3409  ;skip to delimiter
        DEFC    GN_SKT = $3609  ;skip to value

;CLI
        DEFC    DC_ICL = $140C  ;invoke new CLI
        DEFC    DC_RBD = $1C0C  ;rebind streams in CLI layer
        DEFC    OS_CLI = $84    ;CLI interface (OZ usage)

;Close
        DEFC    OS_CL = $0E806  ;internal close (OZ usage)
        DEFC    GN_FLC = $2409  ;close filter
        DEFC    OS_MCL = $51    ;close (free) memory pool
        DEFC    GN_WCL = $5409  ;close wildcard handler

;Dates
        DEFC    GN_GDT = $0609  ;convert ASCII to internal date
        DEFC    GN_DEI = $1609  ;convert zoned format date to internal format
        DEFC    GN_DIE = $1409  ;convert internal format date to zoned format
        DEFC    GN_GMD = $1809  ;get (read) machine date in internal format
        DEFC    GN_PDT = $0809  ;write internal date as ASCII string
        DEFC    GN_PMD = $1C09  ;put (set) machine date

;Delay
        DEFC    OS_DLY = $0D606 ;fixed time delay

;Errors
        DEFC    GN_ERR = $4A09  ;display system error box
        DEFC    GN_ESP = $4C09  ;return an extended pointer to a system error message
        DEFC    OS_ERC = $72    ;get error context
        DEFC    OS_ERH = $75    ;set error handler
        DEFC    OS_ESC = $6F    ;examine special condition

;Extended addresses
        DEFC    OS_BHL = $0DC06 ;copy bytes from extended address
        DEFC    GN_CME = $4209  ;compare null-terminated strings, one local, one extended
        DEFC    GN_RBE = $3E09  ;read byte at extended address
        DEFC    GN_WBE = $4009  ;write byte at extended address
        DEFC    OS_BDE = $0DA06 ;copy bytes to extended address

;File and stream operations
        DEFC    GN_OPF = $6009  ;file or stream open
        DEFC    GN_CL = $6209   ;close file
        DEFC    GN_DEL = $6409  ;delete a file from memory
        DEFC    GN_REN = $6609  ;rename file
        DEFC    OS_DEL = $0E606 ;file delete (OZ usage)
        DEFC    OS_FRM = $48    ;file read miscellaneous
        DEFC    OS_FWM = $4B    ;file write miscellaneous
        DEFC    OS_GB = $39     ;get byte from file (or device)
        DEFC    OS_GBT = $3F    ;get byte from file (or device) with timeout
        DEFC    OS_MV = $45     ;move bytes between stream and memory
        DEFC    OS_OP = $0EA06  ;internal open (OZ usage)
        DEFC    OS_PB = $3C     ;write byte to file, device
        DEFC    OS_PBT = $42    ;write byte to file, device with timeout
        DEFC    OS_REN = $0E406 ;file rename (OZ usage)

;Filenames
        DEFC    GN_ESA = $5E09  ;read & write to filename segments
        DEFC    GN_FCM = $4E09  ;produce compressed filename
        DEFC    GN_FEX = $5009  ;expand a filename
        DEFC    GN_OPW = $5209  ;open wildcard handler
        DEFC    GN_PFS = $5A09  ;parse filename segment
        DEFC    GN_PRS = $5809  ;parse filename
;       DEFC    GN_WCL = $5409  ;close wildcard handle
        DEFC    GN_WFN = $5609  ;fetch next match for wildcard string (handle)
        DEFC    GN_WSM = $5C09  ;match filename segment to wildcard string

;Filters
        DEFC    GN_FLO = $2209  ;open a filter
;       DEFC    GN_FLC = $2409  ;close filter
        DEFC    GN_FLF = $2A09  ;flush filter
        DEFC    GN_FLR = $2809  ;read from filter
        DEFC    GN_FLW = $2609  ;write character to filter
        DEFC    GN_FPB = $2C09  ;push back into filter

;Input
        DEFC    GN_SIP = $3809  ;system input line routine
        DEFC    OS_IN = $2A     ;wait for character from standard input
        DEFC    OS_PUR = $33    ;purge keyboard buffer
        DEFC    OS_TIN = $2D    ;read character from standard input, with timeout
        DEFC    OS_XIN = $30    ;examine input

;Linked lists
        DEFC    GN_XNX = $4409  ;index next entry in a linked list
        DEFC    GN_XDL = $4809  ;delete an entry from a linked list
        DEFC    GN_XIN = $4609  ;insert an entry into a linked list

;Mailboxes
        DEFC    OS_SR = $6C     ;save & restore

;Miscellaneous
        DEFC    OS_OFF = $0EC06 ;Switch machine off
        DEFC    OS_BLP = $0D806 ;Bleep generation
;       DEFC    OS_SR = $6C     ;Page Wait
        DEFC    OS_EPR = $0F006 ;File Eprom manipulation

;Memory
        DEFC    OS_BIX = $60    ;bind in extended address
        DEFC    OS_BOX = $63    ;restore bindings after OS_BIX
;       DEFC    OS_FC = $8A     ;select fast code (fast bank switching)
        DEFC    OS_MAL = $54    ;allocate memory
;       DEFC    OS_MCL = $51    ;close memory (free memory pool)
        DEFC    OS_MFR = $57    ;free (previously allocated block) memory
;       DEFC    OS_MGB = $5A    ;get current bank binding in segment
        DEFC    OS_MOP = $4E    ;open memory (allocate memory pool)
;       DEFC    OS_MPB = $5D    ;set new bank binding in segment

;Integer conversion
;       DEFC    GN_GDN = $1009  ;convert an ASCII string to a binary number
;       DEFC    GN_PDN = $1209  ;write number as decimal ASCII string

;Open
;       DEFC    GN_OPF = $6009  ;open file, device
;       DEFC    GN_FLO = $2209  ;open filter
;       DEFC    GN_OPW = $5209  ;open wildcard handler
;       DEFC    OS_MOP = $4E    ;open memory (pool)

;Output
        DEFC    GN_NLN = $2E09  ;send newline (CR/LF) to standard output
        DEFC    GN_SOE = $3C09  ;write string at extended address to standard output
        DEFC    GN_SOP = $3A09  ;write string to standard output, local memory
        DEFC    OS_OUT = $27    ;write character to standard output
        DEFC    OS_PRT = $24    ;output character to printer

;Screen
        DEFC    OS_MAP = $0F406 ;high resolution graphics manipulation
;       DEFC    OS_SR = $6C     ;save & restore screen file
        DEFC    OS_SCI = $0D406 ;low level screen hardware interface
        DEFC    OS_NQ = $66     ;fetch low level window information

;Settings, System parameters
        DEFC    OS_SP = $69     ;set Panel and PrinterEd values
;       DEFC    OS_NQ = $66     ;read Panel and PrinterEd values

;Time
        DEFC    GN_GMT = $1A09  ;fetch machine time
        DEFC    GN_GTM = $0A09  ;convert an ASCII string to a time in internal format
        DEFC    GN_MSC = $2009  ;miscellaneous time operations
        DEFC    GN_PMT = $1E09  ;put (set) machine time
        DEFC    GN_PTM = $0C09  ;write internal time as ASCII string
        DEFC    GN_SDO = $0E09  ;send date and time to standard output
;       DEFC    OS_DLY = $0D606 ;delay a given period

;Wildcards
;       DEFC    GN_OPW = $5209  ;open wildcard handler
;       DEFC    GN_WCL = $5409  ;close wildcard handle
;       DEFC    GN_WFN = $5609  ;fetch next match for wildcard string (handle)
;       DEFC    GN_WSM = $5C09  ;match filename segment to wildcard string

;Floating point package codes for use with "RST 18H: .DB code"
        DEFC    FP_ABS = 81
        DEFC    FP_ACS = 84
        DEFC    FP_ADD = 66
        DEFC    FP_AND = 33
        DEFC    FP_ASN = 87
        DEFC    FP_ATN = 90
        DEFC    FP_CALL = 162
        DEFC    FP_COMPARE = 156
        DEFC    FP_COS = 93
        DEFC    FP_DEG = 96
        DEFC    FP_DIV = 36
        DEFC    FP_DIVN = 78
        DEFC    FP_EOR = 39
        DEFC    FP_EQ = 60
        DEFC    FP_EXP = 99
        DEFC    FP_FIX = 147
        DEFC    FP_FLOAT = 150
        DEFC    FP_FONE = 132
        DEFC    FP_GE = 54
        DEFC    FP_GT = 69
        DEFC    FP_INT = 102
        DEFC    FP_LE = 48
        DEFC    FP_LN = 105
        DEFC    FP_LOG = 108
        DEFC    FP_LT = 57
        DEFC    FP_MOD = 42
        DEFC    FP_MULT = 63
        DEFC    FP_NE = 51
        DEFC    FP_NEG = 159
        DEFC    FP_NOT = 111
        DEFC    FP_OR = 45
        DEFC    FP_PI = 138
        DEFC    FP_PWR = 75
        DEFC    FP_RAD = 114
        DEFC    FP_SGN = 117
        DEFC    FP_SIN = 120
        DEFC    FP_SQR = 123
        DEFC    FP_STR = 144
        DEFC    FP_SUB = 72
        DEFC    FP_TAN = 126
        DEFC    FP_TEST = 153
        DEFC    FP_TRUE = 135
        DEFC    FP_VAL = 141
        DEFC    FP_ZERO = 129

;Floating point package codes for use with "RST 18H: .DB FP_CALL"
;Calculated as: (FPP code - 33)/3
        DEFC    FPC_ABS = 16
        DEFC    FPC_ACS = 17
        DEFC    FPC_ADD = 11
        DEFC    FPC_AND = 0
        DEFC    FPC_ASN = 18
        DEFC    FPC_ATN = 19
        DEFC    FPC_COMPARE = 41
        DEFC    FPC_COS  = 20
        DEFC    FPC_DEG  = 21
        DEFC    FPC_DIV  = 1
        DEFC    FPC_DIVN  = 15
        DEFC    FPC_EOR  = 2
        DEFC    FPC_EQ  = 9
        DEFC    FPC_EXP  = 22
        DEFC    FPC_FIX = 38
        DEFC    FPC_FLOAT = 39
        DEFC    FPC_FONE = 33
        DEFC    FPC_GE  = 7
        DEFC    FPC_GT  = 12
        DEFC    FPC_INT  = 23
        DEFC    FPC_LE  = 5
        DEFC    FPC_LN  = 24
        DEFC    FPC_LOG  = 25
        DEFC    FPC_LT  = 8
        DEFC    FPC_MOD  = 3
        DEFC    FPC_MULT = 10
        DEFC    FPC_NE  = 6
        DEFC    FPC_NEG = 42
        DEFC    FPC_NOT  = 26
        DEFC    FPC_OR  = 4
        DEFC    FPC_PI  = 35
        DEFC    FPC_PWR  = 14
        DEFC    FPC_RAD  = 27
        DEFC    FPC_SGN  = 28
        DEFC    FPC_SIN  = 29
        DEFC    FPC_SQR  = 30
        DEFC    FPC_STR  = 37
        DEFC    FPC_SUB  = 13
        DEFC    FPC_TAN  = 31
        DEFC    FPC_TEST = 40
        DEFC    FPC_TRUE = 34
        DEFC    FPC_VAL  = 36
        DEFC    FPC_ZERO = 32

;System error codes
        DEFC    RC_OK = $00     ;Success!
        DEFC    RC_ESC = $01    ;Escape
        DEFC    RC_TIM = $02    ;Timeout
        DEFC    RC_UNK = $03    ;Unknown request
        DEFC    RC_BAD = $04    ;Bad argument(s)
        DEFC    RC_MS = $05     ;Bad memory segment error
        DEFC    RC_NA = $06     ;Call not implemented
        DEFC    RC_ROOM = $07   ;No room
        DEFC    RC_HAND = $08   ;Bad handle
        DEFC    RC_EOF = $09    ;End of file
        DEFC    RC_FLF = $0A    ;Filter full
        DEFC    RC_OVF = $0B    ;Overflow
        DEFC    RC_SNTX = $0C   ;Bad syntax
        DEFC    RC_WRAP = $0D   ;Wrap
        DEFC    RC_PUSH = $0E   ;Pushback error (Cannot satisfy request)
        DEFC    RC_ERR = $0F    ;Internal error
        DEFC    RC_TYPE = $10   ;Unexpected type
        DEFC    RC_PRE = $11    ;No room
        DEFC    RC_ONF = $12    ;File not found
        DEFC    RC_RP = $13     ;Read protected
        DEFC    RC_WP = $14     ;Write protected
        DEFC    RC_USE = $15    ;In use
        DEFC    RC_FAIL = $16   ;Cannot satisfy request
        DEFC    RC_IVF = $17    ;Bad filename
        DEFC    RC_FTM = $18    ;File type mismatch
        DEFC    RC_EXIS = $19   ;File already exists
        DEFC    RC_ADDR = $32   ;Bad address
        DEFC    RC_SIZE = $33   ;Bad size
        DEFC    RC_BANK = $34   ;Bad bank
        DEFC    RC_FRM = $35    ;Frame error
        DEFC    RC_PAR = $36    ;Parity error
        DEFC    RC_DVZ = $46    ;Divide by 0
        DEFC    RC_TBG = $47    ;Number too big
        DEFC    RC_NVR = $48    ;-ve root
        DEFC    RC_LRG = $49    ;Log range
        DEFC    RC_ACL = $4A    ;Accuracy lost
        DEFC    RC_EXR = $4B    ;Exponent range
        DEFC    RC_BDN = $4C    ;Bad number
        DEFC    RC_DRAW = $66   ;Redraw
        DEFC    RC_QUIT = $67   ;Unknown error
        DEFC    RC_SUSP = $69   ;Suspended

ENDIF

LSTON
