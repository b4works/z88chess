; ------------------------------------------------------------------------------------
;   ____   ___
;   6MMMMb/ `MM
;  8P    YM  MM
; 6M      Y  MM  __     ____   _________ _________
; MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
; MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
; MM         MM'   MM MM    MM     dMP       dMP
; MM         MM    MM MMMMMMMM    dMP       dMP
; YM      6  MM    MM MM         dMP       dMP
;  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
;   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
;
; Based on Cyrus IS Chess for the Sinclair ZX Spectrum
; (C) 1982 Richard Lang, Intelligent Software Ltd
;
; Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
;
; Chezz is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published by the Free Software Foundation; either
; version 2, or (at your option) any later version. Chezz is distributed in the hope
; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details. You should have received a copy
; of the GNU General Public License along with Chezz; see the file LICENSE.
; If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ------------------------------------------------------------------------------------

        MODULE  ZcPrint

        INCLUDE "ozdefc.asm"
        INCLUDE "chezz.inc"

        XDEF THINK_CLOCK
        XDEF PRINT_MOVE
        XDEF PRINT_BOARD
        XDEF PRINT_LEVEL
        XDEF PRINT_LDSPKR
        XDEF REDRAW_MOVE
        XDEF DRAW_SCN
        XDEF PRINT_NAMES
        XDEF P_CURSOR_REF
        XDEF MP01, MP02, MP03, MP04, MP05, MP06, MP07
        XDEF MP08, MP09, MP10, MP11, MP12, MP13
        XDEF MSG_REPRINT
        XDEF NYI

        XDEF P_WCLOCK, P_BCLOCK, P_CLOCK
        XDEF WIN_4, WIN_5
        XDEF BPCS

        XREF REDRAW_BD                          ; chezz.asm


;***************************************************************************
.THINK_CLOCK
        RET

;***************************************************************************
;Print's last move made
.PRINT_MOVE
        LD      A,(nextToPlay)	                ; Is it Black's turn?
        RRA
        JR      NC,PM05		                ; Skip if it is

        LD      HL,movetxtptr	                ; Points to text buffer for printing
        LD      BC,27*256+0	                ; Reset the 27 bytes of the buffer
.PM00   LD      (HL),C
        INC     HL
        DJNZ    PM00

        LD      A,(moveNo)	                ; Get move number
        LD      E,A
        LD      B,'0'-1                         ; Counter starting from '0'-1
.PM01   INC     B		                ; Count how many 100s
        SUB     100
        JR      NC,PM01
        ADD     A,100                           ; Add 100 to get the remainder
        LD      C,A		                ; C holds the reminder (0-99)
        LD      A,B                             ; Get ASCII digit to print
        CP      '0'                             ; Is the first digit '0'?
        JR      NZ,PM02		                ; Skip if not

        LD      A,E                             ; Is the move number < 10?
        CP      10
        LD      A,' '		                ; A space is to be printed
        CALL    C,PM_TEXT                       ; If move number is less 10, another leading space is needed

.PM02   CALL    PM_TEXT		                ; Place the character in the buffer for 100s (or a space)
        LD      A,C
        LD      B,'0'-1                         ; Counter starting from '0'-1
 PM03   INC     B		                ; Count how many 10s
        SUB     10
        JR      NC,PM03
        ADD     A,10                            ; Add 10 to get the remainder
        LD      C,A		                ; C holds the reminder (0-9)
        LD      A,B		                ; Convert the 10s count into ASCII
        CP      '0'		                ; Is it not a '0'? (i.e. no 10s)
        CALL    NZ,PM_TEXT                      ; Place character for 10s
 PM04   LD      A,'0'		                ; Now convert the units in C in ASCII
        ADD     A,C
        CALL    PM_TEXT		                ; And place this in the buffer for units
        LD      A,'.'		                ; Next a dot
        CALL    PM_TEXT
        LD      A,' '		                ; Then a space
        CALL    PM_TEXT

.PM05   LD      A,(IX+1)                        ; The move's From square
        CALL    P_SQ_REF	                ; Write the From square reference to the buffer
        LD      A,'-'                           ; To an empty square?
        BIT     3,(IX+3)                        ; Or was a piece taken?
        JR      Z,PM06                          ; Skip if an empty square
        LD      A,'x'

.PM06   CALL    PM_TEXT		                ; Put a '-' or 'x' in the buffer
        LD      A,(IX+2)                        ; Get the To square
        CALL    P_SQ_REF	                ; Write the To square reference to the buffer
        LD      HL,promoted	                ; Has a pawn been promoted?
        LD      A,(HL)
        OR      A
        JR      Z,PM08                          ; Skip if a pawn hasn't been promoted
        LD      A,'/'		                ; Otherwise a '/' is placed in the buffer
        CALL    PM_TEXT
        LD      A,(HL)                          ; Now to work out what new promoted piece is
        AND     $0F
        CP      $05		                ; Is it a Rook?
        LD      A,'R'
        JR      Z,PM07		                ; Skip if it is
        LD      A,'Q'		                ; Is it a Queen?
        JR      NC,PM07		                ; Skip if it is
        BIT     7,(HL)		                ; Is it a Knight?
        LD      A,'N'
        JR      Z,PM07		                ; Skip if it is
        LD      A,'B'		                ; Must be a Bishop
.PM07   CALL    PM_TEXT		                ; Place the piece name into the buffer
.PM08   BIT     7,(IX+3)                        ; Is the enemy king in check?
        LD      A,'+'		                ; A '+' signifies check
        JR      NZ,PM09                         ; Skip if not in check
        LD      A,' '		                ; Otherwise a space will be written
.PM09   CALL    PM_TEXT
        LD      A,(HL)                          ; Pad out if no promotion
        OR      A
        JR      NZ,PM10		                ; Skip if there was a promotion
        LD      A,' '		                ; Two spaces are written
        CALL    PM_TEXT
        CALL    PM_TEXT
.PM10   LD      A,(nextToPlay)                  ; Write 2 spaces if this is a White move
        RRA
        JR      NC,REDRAW_MOVE
        LD      A,' '		                ; Writing these spaces leaves the buffer ready
        CALL    PM_TEXT		                ; for Black's move later.
        CALL    PM_TEXT

;***************************************************************************
; This redraws the move text in case Chezz received
; a RC.DRAW system error code after being suspended
;
.REDRAW_MOVE
        LD      HL,WIN_2                        ; Select Move window
        OZ      GN_Sop
        OZ      GN_Nln
        LD      HL,movetext                     ; Print the move text buffer
        OZ      GN_Sop
        LD      HL,WIN_3                        ; Select the Board window
        OZ      GN_Sop
        RET

;***************************************************************************
; This routine adds a character in the Move Text buffer
;
.PM_TEXT
        EXX                                     ; Preserve BC and HL
        LD      HL,movetxtptr                   ; Get text buffer pointer
        INC     (HL)                            ; Move the point along one
        LD      B,A                             ; Save the character for adding
        LD      C,(HL)                          ; Point to next place to put the character
        LD      A,C                             ; Has the end be reached?
        CP      26
        LD      A,B                             ; (Restore the character in A)
        RET     NC                              ; Return if so
        LD      B,0                             ; Form offset for position in buffer
        ADD     HL,BC                           ; Point to the position
        LD      (HL),A                          ; Place the character there
        EXX                                     ; Restore BC and HL
        RET                                     ; Return

.P_SQ_REF
        LD      B,A                             ; Save square reference in B
        AND     7                               ; Bits 0-2 is the file (X coord)
        ADD     A,'A'                           ; Form the letter A to H
        CALL    PM_TEXT                         ; Send letter to the text buffer
        LD      A,$70                           ; Bits 4-6 is the rank (Y coord)
        AND     B
        RRA                                     ; Move bits 4-6 to bits 0-2
        RRA
        RRA
        RRA
        NEG                                     ; Negate the value (A = -A)
        ADD     A,'8'                           ; Subtract from the value of ASCII character '8'
        CALL    PM_TEXT                         ; Send this to the buffer
        RET                                     ; Return

;***************************************************************************
; This is called from the Cyrus code to print the Cursor location while the Human player
; formed their move
;
.P_CURSOR_REF
        LD      HL,WIN_5                        ; Select the Comms window
        OZ      GN_Sop
        LD      B,A                             ; Save the square reference in B
        AND     7                               ; Bits 0-2 is the file (X Coord)
        ADD     A,'A'                           ; Form the letter A to H
        OZ      OS_Out                          ; Print it
        LD      A,$70                           ; Bits 4-6 is the rank (Y coord)
        AND     B
        RRA                                     ; Move bits 4-6 to bits 0-2
        RRA
        RRA
        RRA
        NEG                                     ; Negate the value (A = -A)
        ADD     A,'8'                           ; Subtract from the value of ASCII character '8'
        OZ      OS_Out                          ; Print it
        LD      HL,WIN_3                        ; Select Board window
        OZ      GN_Sop
        JP      PRINT_CURSOR                    ; Move the cursor to its Board position

;***************************************************************************
.PRINT_REFS
        LD      HL,WIN_3                        ; Select the Board window
        OZ      GN_Sop
/*
;__NOTE The next 4 lines don't make sense!  What is the point of pr?
        LD      HL,zcflags      ; Are we printing square references?
        BIT     pr,(HL)
        JP      NZ,PRINT_CURSOR ; Jump if not - just position the cursor
        SET     pr,(HL)
;__
*/
        LD      D,0                             ; D hold the board cursor postion, start at square A8 (Top left)
        LD      BC,'8'*256+'A'                  ; BC hold characters for A8
.PR01   LD      E,D                             ; E holds board cursor positon for the PB_AT subroutine
        CALL    PB_AT                           ; Position the cursor
        INC     D                               ; Move the cursor position along the X axis
        LD      A,C                             ; Print the square reference letter (If A8, A is printed)
        OZ      OS_Out
        LD      A,B                             ; Print the square reference digit (If A8, 8 is printed)
        OZ      OS_Out
        LD      HL,P_REVERSE                    ; The next square will be of opposite colour, so invert video mode
        OZ      GN_Sop
        INC     C                               ; Next letter to be printed (A8 becomes B8)
        BIT     3,D                             ; Has the edge of the board been reached?
        JR      Z,PR01                          ; Loop back if not

        LD      HL,P_REVERSE                    ; The colour of the first square of the next line is the same as the last
        OZ      GN_Sop                          ; of the previous line, so invert video mode again to make them the same.
        DEC     B                               ; Decrease the row number
        LD      A,D                             ; Move the board cursor position on 8 squares
        ADD     A,8
        LD      D,A
        LD      C,'A'                           ; Start on column A again (if last square was A8, the next will be A7)
        BIT     7,D                             ; Has the bottom right square been reached?
        JR      Z,PR01                          ; Loop back if not
        JP      PRINT_CURSOR                    ; Finally, repostion the move cursor

;***************************************************************************
; This draws the screen when Chezz is invoked or when it gets a system request to do so
;
.DRAW_SCN
        PUSH    AF                              ; Protect A in case of RC.DRAW system error
        LD      HL,SCN_SETUP                    ; Set up the Screen and UDGs
        OZ      GN_Sop
        CALL    PRINT_LDSPKR                    ; Display loud speaker if necessary
        CALL    PRINT_LEVEL                     ; Display the Chezz play level
        CALL    REDRAW_BD                       ; Draw whole board
        POP     AF

        CP      RC_DRAW                         ; Was there a system redraw request?
        RET     NZ                              ; Return if not
        CALL    REDRAW_MOVE                     ; Print the last move detail
        CALL    MSG_REPRINT                     ; Reprint last message
        LD      HL,zcflags2
        SET     bp,(HL)
        SET     wp,(HL)
        DEC     HL
        RES     pr,(HL)

;***************************************************************************
; This routine prints, as required, the following:
;  - Arrow indicator of whose turn it is
;  - Whether Chezz or human is playing each colour
;  - Inverted board (White at top, Black bottom)
;  - Prints square references
;  - Coloured squares
;  - UDGs or text notation for chess pieces
;
.PRINT_BOARD
        CALL    ARROW                           ; Put arrow against whose turn it is
        CALL    PRINT_NAMES                     ; Display who is playing Black or White
        LD      HL,WIN_3                        ; Select Board window
        OZ      GN_Sop
        LD      HL,P_CURSOR_ON                  ; Ensure cursor is on and flashing
        OZ      GN_Sop
        LD      HL,zcflags                      ; Do square references need to be printed?
        BIT     sr,(HL)
        JP      NZ,PRINT_REFS                   ; Goto print references

        PUSH    IY
        RES     pr,(HL)
        BIT     cm,(HL)                         ; Should piece UDGs be printed?
        EX      AF,AF'                          ; Save this result for later
        LD      BC,12                           ; Remember current piece count
        LD      HL,blackpcs
        LD      DE,blackold
        PUSH    HL
        LDIR
        POP     HL
        LD      B,12                            ; Reset current piece count
.PB00   LD      (HL),C                          ; C=0
        INC     HL
        DJNZ    PB00

        EX      AF,AF'                          ; OK -should UDGs be printed?
        LD      HL,ON_WSQ                       ; Table for printing UDGs on white squares
        LD      DE,ON_BSQ                       ; Table for printing UDGs on black squares
                                                ; DE and HL get switched when working on the next square
        JR      Z,PB01                          ; Skip if UDGs to be printed

        LD      HL,ON_WSQN                      ; Table for printing piece notations on white squares
        LD      DE,ON_BSQN                      ; Table for printing piece notations on black squares

.PB01   EXX                                     ; Save piece table pointers for later
        LD      HL,oldboard                     ; Previous board
        LD      DE,cyrusBoard                   ; Current board

;Now look at each square in turn
.PB02   LD      A,(DE)                          ; Starting at A1, look at square contents
        CP      (HL)                            ; Is this square different before last move?
        LD      (HL),A                          ; Update the previous record of this square
        EX      AF,AF'                          ; Remember this result for later
        LD      A,(HL)                          ; Get the square contents
        PUSH    DE                              ; Preserve registers
        PUSH    HL
        EXX
        PUSH    HL
        PUSH    DE
        OR      A                               ; Is the square empty?
        LD      C,WS-ON_WSQ                     ; Offset for empty square print sequence
        JR      Z,PB03                          ; Print the square if empty

        BIT     7,A                             ; There is a piece - is it White (Bit 7=1)?
        LD      IY,blackpcs                     ; Point to Black piece count data
        CALL    NZ,PB_WHITE                     ; If White piece, point to White count and print data
        LD      B,A                             ; Save square info

;__NOTE - not sure if this code to print a Pawn is necessary. Is it for a promoting Pawn?
        AND     $7F                             ; Reset bit 7 (don't care about its colour)
        INC     (IY+5)                          ; Assume a Pawn and increase count
        CP      $43                             ; Is it a Pawn?
        LD      C,BPW-ON_WSQ                    ; Sequence table for printing a Pawn
        JR      Z,PB03                          ; Skip to print if it a Pawn
        DEC     (IY+5)                          ; Not a Pawn - decrease Pawn count
;__
        LD      A,B                             ; Decode the piece to be printed
        AND     $F8                             ; Calculate the lookup address for the piece's info
        INC     A
        LD      C,A
        LD      B,auto00BE                      ; BC = 2A00 + C (piece info occupies 8 bytes)
        LD      A,(BC)                          ; Get the piece type
        LD      B,A                             ; Save piece type

        INC     (IY+0)                          ; Assume a King and increase count
        AND     $0F                             ; Is it a King?
        LD      C,0                             ; Sequence table offset for a King
        JR      Z,PB03                          ; Skip to print if it is a King
        DEC     (IY+0)                          ; Not a King - decrease King count

        INC     (IY+5)                          ; Assume a Pawn and increase count
        CP      $01                             ; Is it a Pawn?
        LD      C,BPW-ON_WSQ                    ; Sequence table offset for a Pawn
        JR      Z,PB03                          ; Skip to print if it is a Pawn
        DEC     (IY+5)                          ; Not a Pawn - decrease Pawn count

        INC     (IY+2)                          ; Assume a Rook and increase count
        CP      $05                             ; Is it a Rook?
        LD      C,BRW-ON_WSQ                    ; Sequence table offset for a Rook
        JR      Z,PB03                          ; Skip to print if it is a Rook
        DEC     (IY+2)                          ; Not a Pawn - decrease Pawn count

        INC     (IY+1)                          ; Assume a Queen and increase count
        LD      C,BQW-ON_WSQ                    ; Sequence table offset for a Queen
        JR      NC,PB03                         ; Skip to print if it is a Queen (note CF test!)
        DEC     (IY+1)                          ; Not a Queen - decrease Queen count

        INC     (IY+3)                          ; Assume a Bishop and increase count
        BIT     7,B                             ; Is it a Bishop?
        LD      C,BBW-ON_WSQ                    ; Sequence table offset for a Bishop
        JR      NZ,PB03                         ; Skip to print if it is a Bishop
        DEC     (IY+3)                          ; Not a Bishop - decrease Bishop count

        INC     (IY+4)                          ; Piece has to be a knight - increase knight count
        LD      C,BNW-ON_WSQ                    ; Sequence table offset for a Bishop

.PB03   EX      AF,AF'                          ; OK - is this square different before last move?
        JR      Z,PB04                          ; No?  Then nothing to print here

        LD      B,0                             ; At last - let's print the piece
        ADD     HL,BC
        PUSH    HL                              ; Preserve HL and HL'
        EXX
        PUSH    HL
        CALL    PB_AT                           ; Position print cursor for the printing the piece
        POP     HL
        EXX
        POP     HL                              ; HL points to the piece print sequence (null terminated)
        OZ      GN_Sop                          ; A UDG or piece notation is printed (at last phew!)
.PB04   POP     DE                              ; Recover registers
        POP     HL                              ; (Why not POP HL then POP DE and leave out EX DE,HL!)
        EX      DE,HL                           ; The next square is the opposite colour, so switch pointers
        EXX
        POP     HL
        POP     DE
        INC     HL                              ; Move to next square on the Cyrus board
        INC     E                               ; Move along the
        BIT     3,E                             ; Board pointer - has first rank been printed?
        JP      Z,PB02                          ; Loop back if not
        LD      A,8                             ; Move the pointer in E along another 8 bytes for next rank
        ADD     A,E
        LD      E,A
        EXX
        EX      DE,HL                           ; SwitWIN_4ch pointers to suit the next square colour
        EXX
        BIT     7,E                             ; Has the end of the board been reached?
        JP      Z,PB02                          ; Loop back if not
        POP     IY                              ; Recover IY
        CALL    COUNT_PCS                       ; Find out the number of captured pieces
        CALL    DRAW_CAPTURED

;***************************************************************************
; This routines positions the cursor to the correct position on the Board.
; It takes account of whether the board is inverted or not.
;
.PRINT_CURSOR
        LD      A,(cur_pos)                     ;  Cursor position
        CP      $78
        RET     NC
        LD      E,A

.PB_AT  LD      HL,P_AT                         ; E holds cursor position
        OZ      GN_Sop
        LD      A,(orientation)                 ; Board orientation
        OR      A                               ; 0 = upright, 1 = inverted
        JR      Z,PB_AT1                        ; Skip if upright
        LD      A,$77
        SUB     E
        LD      E,A
.PB_AT1 LD      A,E
;The cursor position held in E has to be converted into X and Y coordinates.
;E =0 is square A8, =8 is square H8, =9 i A7, =64 is H1
        AND     7                               ; X coord is held in bits 0-2
        ADD     A,A                             ; Doubled as pieces are formed of two characters
        ADD     A,32                            ; A holds X coord + 32
        OZ      OS_Out                          ; Send X coordinate
        LD      A,E                             ; Y coord is held in bits 3-5
        RRCA                                    ; Move bits 3-5 into bit 0-2
        RRCA
        RRCA
        RRCA
        AND     7                               ; Keep only bits 0-2
        ADD     A,32                            ; A holds Y coord + 32
        OZ      OS_Out                          ; Send Y coord
        RET

;***************************************************************************
.PB_WHITE
        LD      IY,whitepcs                     ; Point to White piece count data
        LD      BC,WKW-BKW                      ; Update pointer for printing White pieces
        ADD     HL,BC
        RET

;***************************************************************************
.COUNT_PCS
        PUSH    IY
;__NOTE - need to see if there is a better way of doing this bit
        LD      HL,blackpcs                     ; Has the number of black pieces on the board
        LD      DE,blackold                     ; changed?
        XOR     A
        LD      B,6
.CP01   ADD     A,(HL)                          ; See if there is a difference between counts
        EX      DE,HL                           ; stored at blackpcs and blackold
        SUB     (HL)
        EX      DE,HL
        INC     HL
        INC     DE
        DJNZ    CP01
        OR      A                               ; A will 0 if no change.
        JR      Z,CP03                          ; Skip if there is a change
;__
        LD      HL,zcflags2                     ; A black piece has been captured
        SET     bp,(HL)                         ; Flag this
        LD      HL,blackpcs                     ; Adjust count of any pieces removed
        LD      DE,blackold                     ; Compare blackpcs with blackold
        LD      IY,blackcount                   ; Pointer to captured piece counts
        LD      B,6                             ; The are 6 piece types
.CP02   LD      A,(DE)                          ; Work out the number of captured Black Kings(!) first
        SUB     (HL)                            ; then Queens, Rooks, Bishops, Knights then Pawns
        ADD     A,(IY+0)
        LD      (IY+0),A
        INC     DE
        INC     HL
        INC     IY
        DJNZ    CP02

;__NOTE - need to see if there is a better way of doing this bit
.CP03   LD      HL,whitepcs                     ; Has the number of white pieces on the board
        LD      DE,whiteold                     ; changed?
        XOR     A
        LD      B,6
.CP04   ADD     A,(HL)                          ; See if there is a difference between counts
        EX      DE,HL                           ; stored at whitepcs and whiteold
        SUB     (HL)
        EX      DE,HL
        INC     HL
        INC     DE
        DJNZ    CP04
        OR      A                               ; A will 0 if no change.
        JR      Z,CP06                          ; Skip if not
;__
        LD      HL,zcflags2                     ; A white piece has been captured
        SET     wp,(HL)                         ; Flag this
        LD      HL,whitepcs                     ; Adjust count of any pieces removed
        LD      DE,whiteold                     ; Compare blackpcs with blackold
        LD      IY,whitecount                   ; Pointer to captured piece counts
        LD      B,6                             ; The are 6 piece types
.CP05   LD      A,(DE)                          ; Work out the number of captured White Kings(!) first
        SUB     (HL)                            ; then Queens, Rooks, Bishops, Knights, then Pawns
        ADD     A,(IY+0)
        LD      (IY+0),A
        INC     DE
        INC     HL
        INC     IY
        DJNZ    CP05

.CP06   POP     IY
        RET                                     ; Counting complete - time to return

;***************************************************************************
;Draw pieces captured by both white and black (this code could do with re-writing)
.DRAW_CAPTURED
;__NOTE - Do we really have to know if a piece has been captured; why not just print them?
        LD      A,(zcflags2)                    ; Have black pieces been captured?
        BIT     bp,A
        JR      NZ,DC01
        BIT     wp,A
        RET     Z
;__
.DC01   LD      HL,WIN_4                        ; Select Info window
        OZ      GN_Sop
        LD      HL,zcflags                      ; Are we print UDGs or notation?
        BIT     cm,(HL)
        LD      HL,BKW                          ; UDGs print sequence table
        LD      DE,WKW
        JR      Z,DC02                          ; Skip to print UDGs
        LD      HL,BKWN                         ; Notiation print sequence table
        LD      DE,WKWN
;__NOTE - Do we really have to know if a piece has been captured; why not just print them?
.DC02   BIT     bp,A                            ; Do black captured pieces have to be printed?
        JR      Z,DC03                          ; Skip if not
; __
        PUSH    DE                              ; Get ready to print Black captured pieces
        PUSH    HL
        LD      HL,BPCS                         ; Move cursor ready for printing the pieces
        OZ      GN_Sop
        POP     HL
        LD      DE,blackcount                   ; Now print the Black captured pieces
        CALL    PRINT_CAPTURED
        POP     DE
;__NOTE - Do we really have to know if a piece has been captured; why not just print them?
        LD      A,(zcflags2)
.DC03   BIT     wp,A                            ; Do we need to print White captured pieces?
        RES     wp,A                            ; Reset captured piece flags (work will have been done)
        RES     bp,A
        LD      (zcflags2),A
        RET     Z                               ; No White captured pieces to print so return
;__
        LD      HL,WPCS                         ; Move cursor ready for printing the pieces
        OZ      GN_Sop
        EX      DE,HL                           ; White piece print sequences table
        LD      DE,whitecount                   ; Now print the White captured pieces
        CALL    PRINT_CAPTURED
        LD      HL,WIN_3
.AR01   OZ      GN_Sop
        RET                                     ; All done, so return.

;***************************************************************************
; A right arrow is printed next to the player whose turn it is
;
.ARROW
        LD      HL,WIN_4                        ; Select Info window
        OZ      GN_Sop
        LD      A,(nextToPlay)                  ; See whose turn it is
        RRA
        LD      HL,P_BTURN                      ; String for printing arrow for Black
        JR      NC,AR01                         ; Skip if Black's turn
        LD      HL,P_WTURN                      ; String for printing arrow for White
        JR      AR01

; ***************************************************************************
; Print captured pieces - starting with Kings(!), then Queens, Rook, Bishop, Knights then Pawns
; On entry, DE points to the the piece counts and HL points to the piece print sequence table
.PRINT_CAPTURED
        LD      BC,$0610                        ; There are 6 piece types, a maximum of 16 pieces
.PC01   LD      A,(DE)                          ; Get piece count
        INC     DE                              ; Move pointer to next piece count
        OR      A                               ; Is it 0?
        JR      Z,PC03                          ; Skip if so
;__NOTE - I have no idea what is being tested here
        BIT     7,A
        JR      NZ,PC03
;__
.PC02   PUSH    HL                              ; Preserve the pointer to the current piece sequence
        OZ      GN_Sop                          ; Print the piece
        POP     HL                              ; Recover piece sequence pointer
        DEC     C                               ; Decrement total piece count
        RET     Z                               ; Skip if 16 pieces have been printed
        DEC     A                               ; Are there any more pieces of this type to be printed?
        JR      NZ,PC02                         ; Loop back if so
.PC03   PUSH    DE                              ; Preserve DE
        LD      DE,BQW-BKW                      ; Move piece sequence pointer onto next sequence
        ADD     HL,DE                           ; (BQW-BKW is the length of each sequence)
        POP     DE                              ; Restore DE
        DJNZ    PC01                            ; Loop back if there are more piece types to look at

.PC04   LD      B,C                             ; Pad out where missing pieces would be with spaces
        LD 	A,' '
.PC05   OZ      OS_Out                          ; Print two spaces as UDGs are made of two characters
        OZ      OS_Out
        DJNZ    PC05
        RET

;***************************************************************************
.PRINT_LDSPKR
        LD      A,(beeper)                      ; Is loud speaker active?
        LD      HL,P_LDSPKR                     ; (Print sequence for print loud speaker icon)
        OR      A                               ; (1 = yes, 0 = no)
        JR      Z,PL01                          ; Skip if not
        LD      HL,P_NOSPKR                     ; Print sequence for no loud speaker
.PL01   OZ      GN_Sop                          ; Print the loud speaker (or not!)
        RET                                     ; Done

;***************************************************************************
.PRINT_LEVEL
        LD      HL,P_LEVEL                      ; Print "LEVEL "
        OZ      GN_Sop
        LD      A,(level)                       ; Get Cyrus playing level
        PUSH    AF                              ; Save it
        AND     $08                             ; Is bit 3 set, i.e. Problem solving mode?
        LD      A,' '                           ; If not, print a space instead of a 'P'
        JR      Z,PL_01                         ; Skip if not
        LD      A,'P'                           ; In Problem solving mode, so print a 'P'

.PL_01  OZ      OS_Out                          ; Print a space or 'P'
        POP     AF                              ; Get Cyrus playing level
        AND     $07                             ; Keep bits 0 to 2
        ADD     A,'1'                           ; Form an ASCII digit (1 to 8)
        OZ      OS_Out                          ; Print this
        RET

;***************************************************************************
.PRINT_NAMES
        LD      HL,WIN_4                        ; Select Info window
        OZ      GN_Sop
        LD      A,(zcflags2)                    ; Look at zcflags2 to see who's playing
        LD      HL,P_BNAME                      ; Print Black player's name first
        CALL    PN01
        LD      HL,P_WNAME                      ; Now print White player's name

.PN01   OZ      GN_Sop
        RRCA                                    ;If bit 0 =1, print "COMPUTER"
        LD      HL,P_COMPUTER
        JR      C,PN02
        LD      HL,P_PLAYER                     ;Else print "PLAYER"
.PN02   OZ      GN_Sop
        RET

;***************************************************************************
.MP01   LD      A,$FF                           ; Switch off board cursor
        LD      (cur_pos),A
        LD      DE,MSG01                        ; "Thinking"
.MSG_PRINT
        LD      (reprint_msg),DE                ; Remember message in case OZ asks for screen to be redrawn
.MSG_REPRINT
        LD      DE,(reprint_msg)                ; Print message at stored pointer
        LD      HL,WIN_5                        ; Message to be printed in Comms window
        OZ      GN_Sop
        OZ      GN_Nln
        OZ      GN_Nln
        EX      DE,HL
        OZ      GN_Sop                          ; Now print message
        LD      HL,WIN_3
        OZ      GN_Sop
        RET

;***************************************************************************
.MP02   LD      DE,MSG02                        ; No mate
        JR      MSG_PRINT

;***************************************************************************
.MP03   LD      DE,MSG03                        ; "Your move please:"
        LD      A,(nextToPlay)                  ; White's turn?
        LD      B,A
        RRA
        LD      A,(zcflags2)
        JR      NC,MP03_1                       ; Skip if B's
        RRA                                     ; Is Cyrus playing White?
        RRA
        JR      NC,MSG_PRINT                    ; Jump if not
        JR      MP03B

.MP03_1 RRA                                     ; Is Cyrus playing Black?
        JR      NC,MSG_PRINT                    ; Jump if not
.MP03B  LD      DE,MSG03B                       ; PAUSED - Enter to continue
        RR      B                               ; Jump if Black's turn
        JR      NC,MSG_PRINT
        LD      A,(moveNo)                      ; Jump if not first move?
        DEC     A
        JR      NZ,MSG_PRINT
.MP03A  LD      DE,MSG03A                       ; Press ENTER to start game
        JR      MSG_PRINT

;***************************************************************************
.MP04   LD      DE,MSG04                        ; Promote to? (Q,R,B or N)
        LD      HL,zcflags
        SET     pp,(HL)                         ; Promote key mode
        JR      MSG_PRINT

;***************************************************************************
.MP05   LD      DE,MSG05                        ; STALEMATE
        JR      MSG_PRINT

;***************************************************************************
.MP06   LD      DE,MSG06                        ; CHECKMATE
        JR      MSG_PRINT

;***************************************************************************
.MP07   LD      DE,MSG07                        ; DRAWN
        JR      MSG_PRINT

;***************************************************************************
.MP08   LD      DE,MSG08                        ; Interrupted
        JR      MSG_PRINT

;***************************************************************************
.MP09   LD      DE,MSG09                        ; Replaying game
        JR      MSG_PRINT

;***************************************************************************
.MP10   LD      DE,MSG10                        ; New game (Y/N)?
        JR      MSG_PRINT

;***************************************************************************
.MP11   LD      DE,MSG11                        ; BOARD SETUP MODE
        JR      MSG_PRINT

;***************************************************************************
.MP12   LD      DE,MSG12                        ; ILLEGAL
        JP      MSG_PRINT

;***************************************************************************
.MP13   LD      DE,MSG13                        ; No more moves!
        JP      MSG_PRINT

;***************************************************************************
.NYI    LD      DE,MSG14                        ; Sorry - not yet implemented!
        CALL    MSG_PRINT
        LD      BC,200                          ; Pause for 2 secs
        OZ      OS_Dly
        JR      MP03

;***************************************************************************
;Here is the list of Chezz's comms messages
.MSG01  DEFM    1,"2H3",1,"2-C",1,"2H5","Thinking...",0
.MSG02  DEFM    "No checkmate found",0
.MSG03  DEFM    "Your move please: ",0
.MSG03A DEFM    "Press ENTER to start ",0
.MSG03B DEFM    "Press ENTER to continue",0
.MSG04  DEFM    "Promote to? (Q,R,B or N)",0
.MSG05  DEFM    "STALEMATE",0
.MSG06  DEFM    "CHECKMATE",0
.MSG07  DEFM    "DRAWN",0
.MSG08  DEFM    "Interrupted",0
.MSG09  DEFM    "Replaying game",0
.MSG10  DEFM    "New game (Y/N)?",0
.MSG11  DEFM    "BOARD SETUP MODE",0
.MSG12  DEFM    "ILLEGAL!",0
.MSG13  DEFM    "No more moves!",0
.MSG14  DEFM    "Sorry - not yet",10,13,"implemented!",0

;***************************************************************************
;Table of chess piece print sequences
.ON_WSQ                                                         ; TABLE FOR PRINTING UDGS
.BKW    DEFB    1,'2','-','R',1,'2','?',64,1,'2','?',65,0       ; Black king on white square
.BQW    DEFB    1,'2','-','R',1,'2','?',66,1,'2','?',67,0       ; Black queen on white square
.BRW    DEFB    1,'2','-','R',1,'2','?',68,1,'2','?',69,0       ; Black rook on white square
.BBW    DEFB    1,'2','-','R',1,'2','?',70,1,'2','?',71,0       ; Black bishop on white square
.BNW    DEFB    1,'2','-','R',1,'2','?',72,1,'2','?',73,0       ; Black knight on white square
.BPW    DEFB    1,'2','-','R',1,'2','?',74,1,'2','?',75,0       ; Black pawn on white square

.WKW    DEFB    1,'2','-','R',1,'2','?',76,1,'2','?',77,0       ; White king on white square
.WQW    DEFB    1,'2','-','R',1,'2','?',78,1,'2','?',79,0       ; White queen on white square
.WRW    DEFB    1,'2','-','R',1,'2','?',80,1,'2','?',81,0       ; White rook on white square
.WBW    DEFB    1,'2','-','R',1,'2','?',82,1,'2','?',83,0       ; White bishop on white square
.WNW    DEFB    1,'2','-','R',1,'2','?',84,1,'2','?',85,0       ; White knight on white square
.WPW    DEFB    1,'2','-','R',1,'2','?',86,1,'2','?',87,0       ; White pawn on white square

.WS     DEFB    1,'2','-','R',' ',' ',0                         ; Empty white square

.ON_BSQ
.BKB    DEFB    1,'2','+','R',1,'2','?',76,1,'2','?',77,0       ; Black king on black square
.BQB    DEFB    1,'2','+','R',1,'2','?',78,1,'2','?',79,0       ; Black queen on black square
.BRB    DEFB    1,'2','+','R',1,'2','?',80,1,'2','?',81,0       ; Black rook on black square
.BBB    DEFB    1,'2','+','R',1,'2','?',82,1,'2','?',83,0       ; Black bishop on black square
.BNB    DEFB    1,'2','+','R',1,'2','?',84,1,'2','?',85,0       ; Black knight on black square
.BPB    DEFB    1,'2','+','R',1,'2','?',86,1,'2','?',87,0       ; Black pawn on black square

.WKB    DEFB    1,'2','+','R',1,'2','?',64,1,'2','?',65,0       ; White king on black square
.WQB    DEFB    1,'2','+','R',1,'2','?',66,1,'2','?',67,0       ; White queen on black square
.WRB    DEFB    1,'2','+','R',1,'2','?',68,1,'2','?',69,0       ; White rook on black square
.WBB    DEFB    1,'2','+','R',1,'2','?',70,1,'2','?',71,0       ; White bishop on black square
.WNB    DEFB    1,'2','+','R',1,'2','?',72,1,'2','?',73,0       ; White knight on black square
.WPB    DEFB    1,'2','+','R',1,'2','?',74,1,'2','?',75,0       ; White pawn on black square

.BS     DEFB    1,'2','+','R',' ',' ',0                         ; Empty black square

.ON_WSQN                                                        ; TABLE FOR PRINTING PIECE NOTATIONS
.BKWN   DEFB    1,'2','-','R','b','k',0,0,0,0,0,0,0             ; Black king on white square
.BQWN   DEFB    1,'2','-','R','b','q',0,0,0,0,0,0,0             ; Black queen on white square
.BRWN   DEFB    1,'2','-','R','b','r',0,0,0,0,0,0,0             ; Black rook on white square
.BBWN   DEFB    1,'2','-','R','b','b',0,0,0,0,0,0,0             ; Black bishop on white square
.BNWN   DEFB    1,'2','-','R','b','n',0,0,0,0,0,0,0             ; Black knight on white square
.BPWN   DEFB    1,'2','-','R','b','p',0,0,0,0,0,0,0             ; Black pawn on white square

.WKWN   DEFB    1,'2','-','R','W','K',0,0,0,0,0,0,0             ; White king on white square
.WQWN   DEFB    1,'2','-','R','W','Q',0,0,0,0,0,0,0             ; White queen on white square
.WRWN   DEFB    1,'2','-','R','W','R',0,0,0,0,0,0,0             ; White rook on white square
.WBWN   DEFB    1,'2','-','R','W','B',0,0,0,0,0,0,0             ; White bishop on white square
.WNWN   DEFB    1,'2','-','R','W','N',0,0,0,0,0,0,0             ; White knight on white square
.WPWN   DEFB    1,'2','-','R','W','P',0,0,0,0,0,0,0             ; White pawn on white square

.WSN    DEFB    1,'2','-','R',' ',' ',0                         ; Empty white square

.ON_BSQN
.BKBN   DEFB    1,'2','+','R','b','k',0,0,0,0,0,0,0             ; Black king on black square
.BQBN   DEFB    1,'2','+','R','b','q',0,0,0,0,0,0,0             ; Black queen on black square
.BRBN   DEFB    1,'2','+','R','b','r',0,0,0,0,0,0,0             ; Black rook on black square
.BBBN   DEFB    1,'2','+','R','b','b',0,0,0,0,0,0,0             ; Black bishop on black square
.BNBN   DEFB    1,'2','+','R','b','n',0,0,0,0,0,0,0             ; Black knight on black square
.BPBN   DEFB    1,'2','+','R','b','p',0,0,0,0,0,0,0             ; Black pawn on black square

.WKBN   DEFB    1,'2','+','R','W','K',0,0,0,0,0,0,0             ; White king on black square
.WQBN   DEFB    1,'2','+','R','W','Q',0,0,0,0,0,0,0             ; White queen on black square
.WRBN   DEFB    1,'2','+','R','W','R',0,0,0,0,0,0,0             ; White rook on black square
.WBBN   DEFB    1,'2','+','R','W','B',0,0,0,0,0,0,0             ; White bishop on black square
.WNBN   DEFB    1,'2','+','R','W','N',0,0,0,0,0,0,0             ; White knight on black square
.WPBN   DEFB    1,'2','+','R','W','P',0,0,0,0,0,0,0             ; White pawn on black square

.BSN    DEFB    1,'2','+','R',' ',' ',0                         ; Empty black square

;Select a window
.WIN_1  DEFM    1,"2H1",0                       ;
.WIN_2  DEFM    1,"2H2",1,"2+S",0               ; Move window
.WIN_3  DEFM    1,"2H3",1,"2+T",1,"2-R",0       ; Board window
.WIN_4  DEFM    1,"2H4",1,"2-T",0               ; Info window
.WIN_5  DEFM    1,"2H5",1,"2+S",1,"2+C",0       ; Comms window

;Postion for captured pieces
.BPCS   DEFM    1,"3@",32,32+2,0
.WPCS   DEFM    1,"3@",32,32+6,0

;Position for player names and computer names
.P_WNAME
        DEFM    1,"3@",32+8,32,0
.P_BNAME
        DEFM    1,"3@",32+8,32+4,0
.P_COMPUTER
        DEFM    1,"2+TCOMPUTER",0
.P_PLAYER
        DEFM    1,"2+TPLAYER  ",0

;Clock input
.P_WCLOCK
        DEFM    "Enter White's clock:",0
.P_BCLOCK
        DEFM    "Enter Black's clock:",0
.P_CLOCK
        DEFM    13,10,"(hh:mm:ss) ",0
;Arrow turn (print arrow against current player and clear arrow against other player)
.P_WTURN
        DEFM    1,"3@",32,32,1,245,1,"3@",32,36," ",0
.P_BTURN
        DEFM    1,"3@",32,36,1,245,1,"3@",32,32," ",0

;Position cursor
.P_AT   DEFM    1,"3@",0

;Turn cursor on
.P_CURSOR_ON
        DEFM    1,"2+C",0
.P_CURSOR_OFF
        DEFM    1,"2-C",0

;Reverse text (black become white and vice versa)
.P_REVERSE
        DEFM    1,"R",0

;Loud Speaker
.P_LDSPKR
        DEFM    1,"2H1",1,"3@",32+23,32,1,"2?",88,1,"2?",89,0
.P_NOSPKR
        DEFM    1,"2H1",1,"3@",32+23,32,"  ",0

;Level
.P_LEVEL
        DEFM    1,"2H1",1,"3@",32+14,32,1,"2+T","LEVEL:",0
;---------------------------------------------------------------------------
.SCN_SETUP
;This long string of characters sets up the Z88 screen and defines UDGS
        DEFM    1,"7#1",32,32,32+94,32+8,128
        DEFM    1,"2H1",1,"2G+"
;Move window (2)
        DEFM    1,"7#2",32+1,32,32+25,32+1,131,1,"2I2"
        DEFM    1,"4+RTU",1,"2JC",12,"WHITE     BLACK"
        DEFM    1,"7#2",32+1,32+1,32+25,32+3,129,1,"2I2"
        DEFM    1,"7#2",32+1,32+2,32+25,32+1,129,1,"2I2"
;Comms window (5)
        DEFM    1,"7#5",32+1,32+4,32+25,32+1,131,1,"2I5"
        DEFM    1,"4+RTU",12,"COMPUTER SAYS..."
        DEFM    1,"7#5",32+1,32+5,32+25,32+3,129,1,"2I5"
        DEFM    1,"7#5",32+1,32+5,32+25,32+2,129,1,"2I5"
;Board window (3)
        DEFM    1,"7#3",32+27,32,32+18,32+8,128,1,"2I3"
        DEFM    1,"7#3",32+28,32,32+16,32+8,128,1,"2I3"
;Info window (4)
        DEFM    1,"7#4",32+46,32,32+16,32+8,129,1,"2I4"
        DEFM    1,"3@",32+1,32+0,1,"4+RTUWHITE:"
        DEFM    1,"4-RTU"
;       DEFM    10,13,9,1,"4-RTU00:00:00"
        DEFM    1,"3@",32+1,32+4,1,"4+RTUBLACK:"
;       DEFM    10,13,9,1,"4-RTU00:00:00"
        DEFM    1,"4-RTU"
;Window (1)
        DEFM    1,"7#1",32+1,32+7,32+25,32+1,129,1,"2I1"

.UDGS
;Here are the print sequences for user defined characters for chess pieces.
;Two characters are defined for each piece and are joined together.
;The definitons are for the piece to be printed on white squares.
;When wanting to put a White piece on a black square, an inverted Black piece is used.
;Similarily for printing a Black piece on a black square

;Black King
        DEFB    1,138,'=',64
        DEFB    @10000001
        DEFB    @10000011
        DEFB    @10001101
        DEFB    @10001111
        DEFB    @10001111
        DEFB    @10001010
        DEFB    @10001111
        DEFB    @10000000
        DEFB    1,138,'=',65
        DEFB    @10000000
        DEFB    @10100000
        DEFB    @10011000
        DEFB    @10111000
        DEFB    @10111000
        DEFB    @10101000
        DEFB    @10111000
        DEFB    @10000000
;Black Queen
        DEFB    1,138,'=',66
        DEFB    @10000010
        DEFB    @10001000
        DEFB    @10000010
        DEFB    @10001010
        DEFB    @10001011
        DEFB    @10001111
        DEFB    @10000111
        DEFB    @10000000
        DEFB    1,138,'=',67
        DEFB    @10100000
        DEFB    @10001000
        DEFB    @10100000
        DEFB    @10101000
        DEFB    @10101000
        DEFB    @10111000
        DEFB    @10110000
        DEFB    @10000000
;Black Rook
        DEFB    1,138,'=',68
        DEFB    @10000101
        DEFB    @10000111
        DEFB    @10000011
        DEFB    @10000011
        DEFB    @10000011
        DEFB    @10000111
        DEFB    @10000111
        DEFB    @10000000
        DEFB    1,138,'=',69
        DEFB    @10010000
        DEFB    @10110000
        DEFB    @10100000
        DEFB    @10100000
        DEFB    @10100000
        DEFB    @10110000
        DEFB    @10110000
        DEFB    @10000000
;Black Bishop
        DEFB    1,138,'=',70
        DEFB    @10000001
        DEFB    @10000011
        DEFB    @10000101
        DEFB    @10000110
        DEFB    @10000111
        DEFB    @10000011
        DEFB    @10000110
        DEFB    @10000000
        DEFB    1,138,'=',71
        DEFB    @10000000
        DEFB    @10100000
        DEFB    @10110000
        DEFB    @10110000
        DEFB    @10110000
        DEFB    @10100000
        DEFB    @10110000
        DEFB    @10000000
;Black Knight
        DEFB    1,138,'=',72
        DEFB    @10000001
        DEFB    @10000001
        DEFB    @10000010
        DEFB    @10000111
        DEFB    @10000110
        DEFB    @10000001
        DEFB    @10000011
        DEFB    @10000000
        DEFB    1,138,'=',73
        DEFB    @10010000
        DEFB    @10110000
        DEFB    @10111000
        DEFB    @10111000
        DEFB    @10111000
        DEFB    @10111000
        DEFB    @10111000
        DEFB    @10000000
;Black Pawn
        DEFB    1,138,'=',74
        DEFB    @10000001
        DEFB    @10000011
        DEFB    @10000011
        DEFB    @10000001
        DEFB    @10000001
        DEFB    @10000001
        DEFB    @10000011
        DEFB    @10000000
        DEFB    1,138,'=',75
        DEFB    @10100000
        DEFB    @10110000
        DEFB    @10110000
        DEFB    @10100000
        DEFB    @10100000
        DEFB    @10100000
        DEFB    @10110000
        DEFB    @10000000
;White King
        DEFB    1,138,'=',76
        DEFB    @10000010
        DEFB    @10001100
        DEFB    @10010010
        DEFB    @10010000
        DEFB    @10010000
        DEFB    @10010101
        DEFB    @10010000
        DEFB    @10001111
        DEFB    1,138,'=',77
        DEFB    @10100000
        DEFB    @10011000
        DEFB    @10100100
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10010100
        DEFB    @10000100
        DEFB    @10111000
;White Queen
        DEFB    1,138,'=',78
        DEFB    @10001101
        DEFB    @10010111
        DEFB    @10011101
        DEFB    @10010101
        DEFB    @10010100
        DEFB    @10010000
        DEFB    @10001000
        DEFB    @10000111
        DEFB    1,138,'=',79
        DEFB    @10011000
        DEFB    @10110100
        DEFB    @10011100
        DEFB    @10010100
        DEFB    @10010100
        DEFB    @10000100
        DEFB    @10001000
        DEFB    @10110000
;White Rook
        DEFB    1,138,'=',80
        DEFB    @10001010
        DEFB    @10001000
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10001000
        DEFB    @10001000
        DEFB    @10001111
        DEFB    1,138,'=',81
        DEFB    @10101000
        DEFB    @10001000
        DEFB    @10010000
        DEFB    @10010000
        DEFB    @10010000
        DEFB    @10001000
        DEFB    @10001000
        DEFB    @10111000
;White Bishop
        DEFB    1,138,'=',82
        DEFB    @10000010
        DEFB    @10000100
        DEFB    @10001010
        DEFB    @10001001
        DEFB    @10001000
        DEFB    @10000100
        DEFB    @10001001
        DEFB    @10000110
        DEFB    1,138,'=',83
        DEFB    @10100000
        DEFB    @10010000
        DEFB    @10001000
        DEFB    @10001000
        DEFB    @10001000
        DEFB    @10010000
        DEFB    @10001000
        DEFB    @10110000
;White Knight
        DEFB    1,138,'=',84
        DEFB    @10000010
        DEFB    @10000010
        DEFB    @10000101
        DEFB    @10001000
        DEFB    @10001001
        DEFB    @10000110
        DEFB    @10000100
        DEFB    @10000111
        DEFB    1,138,'=',85
        DEFB    @10101000
        DEFB    @10001000
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10111100
;White Pawn
        DEFB    1,138,'=',86
        DEFB    @10000010
        DEFB    @10000100
        DEFB    @10000100
        DEFB    @10000010
        DEFB    @10000010
        DEFB    @10000010
        DEFB    @10000100
        DEFB    @10000111
        DEFB    1,138,'=',87
        DEFB    @10010000
        DEFB    @10001000
        DEFB    @10001000
        DEFB    @10010000
        DEFB    @10010000
        DEFB    @10010000
        DEFB    @10001000
        DEFB    @10111000
;Loud Speaker
        DEFB    1,138,'=',88
        DEFB    @10000000
        DEFB    @10000000
        DEFB    @10000011
        DEFB    @10000011
        DEFB    @10000011
        DEFB    @10000000
        DEFB    @10000000
        DEFB    @10000000
        DEFB    1,138,'=',89
        DEFB    @10010001
        DEFB    @10110010
        DEFB    @10101000
        DEFB    @10101011
        DEFB    @10101000
        DEFB    @10110010
        DEFB    @10010001
        DEFB    @10000000
        DEFB    0
