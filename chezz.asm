; ------------------------------------------------------------------------------------
;   ____   ___
;   6MMMMb/ `MM
;  8P    YM  MM
; 6M      Y  MM  __     ____   _________ _________
; MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
; MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
; MM         MM'   MM MM    MM     dMP       dMP
; MM         MM    MM MMMMMMMM    dMP       dMP
; YM      6  MM    MM MM         dMP       dMP
;  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
;   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
;
; Based on Cyrus IS Chess for the Sinclair ZX Spectrum
; (C) 1982 Richard Lang, Intelligent Software Ltd
;
; Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
;
; Chezz is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published by the Free Software Foundation; either
; version 2, or (at your option) any later version. Chezz is distributed in the hope
; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details. You should have received a copy
; of the GNU General Public License along with Chezz; see the file LICENSE.
; If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ------------------------------------------------------------------------------------

        MODULE  Chezz

        INCLUDE "ozdefc.asm"
        INCLUDE "chezz.inc"

        XDEF Chezz
        XDEF SETUP_BOARD
        XDEF Z88CTL1, Z88CTL2
        XDEF REDRAW_BD

        XREF Z88CYRUS                           ; z88cyrus.asm
        XREF DRAW_SCN                           ; zcprint.asm
        XREF PRINT_NAMES                        ; zcprint.asm
        XREF PRINT_BOARD                        ; zcprint.asm
        XREF REDRAW_MOVE                        ; zcprint.asm
        XREF PRINT_LDSPKR                       ; zcprint.asm
        XREF MP03                               ; zcprint.asm
        XREF NYI                                ; zcprint.asm
        XREF MSG_REPRINT                        ; zcprint.asm
        XREF WIN_4, WIN_5                       ; zcprint.asm
        XREF BPCS                               ; zcprint.asm
        XREF P_WCLOCK, P_BCLOCK, P_CLOCK        ; zcprint.asm


;***************************************************************************
;It's from here it all happens!
.Chezz
        LD      HL,0                            ; Store value of stack at entry
        ADD     HL,SP
        LD      (zcstack),HL                    ; SP is reset to this value when a new game is started
        XOR     A
        LD      (beeper),A
        LD      (zcflags),A
        INC     A
        LD      (zcflags2),A
        LD      HL,18*3600
        LD      (wclockstart),HL
        LD      (bclockstart),HL
        XOR     A                               ; Set current error handler
        LD      B,A
        LD      HL,ERRHAN
        OZ      OS_Erh
        LD      A,5                             ; Enable Escape detection
        OZ      OS_Esc
        CALL    DRAW_SCN                        ;Setup the Z88 screen for Chezz
        CALL    REDRAW_BD
        CALL    SETUP_BOARD
        JP      Z88CYRUS

;***************************************************************************
.SETUP_BOARD
        LD      HL,0
        LD      (movetxtptr),HL
        LD      (bclockticks),HL
        LD      (wclockticks),HL
        LD      BC,36                   ;Setup pieces count
        LD      DE,blackpcs             ;All pieces are present at start of the game
        LD      HL,SET_data
        LDIR
        LD      HL,WIN_4                ;Select Info window
        OZ      GN_Sop
        LD      HL,BPCS                 ;and clear printed captured Black pieces
        CALL    SET01
        INC     HL                      ;Now do the White pieces
.SET01  OZ      GN_Sop                  ;Select position of captured pieces are printed
        LD      A,32                    ;Print over using spaces
        LD      B,A                     ;Print 32 spacess
.SUB02  OZ      OS_Out
        DJNZ    SUB02
        RET

.SET_data
;The first byte is no. of kings, then queens, rooks, bishops, knights and then pawns
;Chezz compares the current board with previous move and updates the capture piece count
;This facilitates the drawing of the board, etc, after each move

        DEFB    1,1,2,2,2,8     ;blackpcs: current count of Black pieces in play
        DEFB    1,1,2,2,2,8     ;whitepcs: current count of White pieces in play
        DEFB    1,1,2,2,2,8     ;blackold: count of Black pieces before last move
        DEFB    1,1,2,2,2,8     ;whiteold: count of White pieces before last move
        DEFB    0,0,0,0,0,0     ;blackcount: count of Black pieces captured
        DEFB    0,0,0,0,0,0     ;whitecount: count of White pieces captured

;***************************************************************************
;Error handler routine
.ERRHAN RET     Z               ; Return immediately if there is a fatal system error
        CP      RC_ESC          ; Has ESC key been pressed?
        JR      NZ,EH02

        OZ      OS_Esc
        LD      A,CMD_ESC       ; Chezz is to respond to the ESC key being pressed
.EH01   CP      A               ; Acknowledge error
        RET

.EH02   CP      RC_QUIT         ; Has a system KILL request been received?
        JR      NZ,EH03         ; Skip if not

.DO_CMD_QUIT
        XOR     A               ; Chezz has been killed by the system
        OZ      OS_Bye          ; Bye! (Kill Chezz and return to OZ)

.EH03   CP      RC_DRAW         ; System request to redraw screen?
        JR      NZ,EH01
        CALL    DRAW_SCN
        LD      A,RC_DRAW
        CP      A
        RET
;***************************************************************************
;We come here from the Cyrus code when waiting for a menu key press
.Z88CTL1
        PUSH    HL
        PUSH    DE
        PUSH    BC
        EXX
        PUSH    HL
        PUSH    DE
        PUSH    BC
        CALL    ZC1_00
        POP     BC
        POP     DE
        POP     HL
        EXX
        POP     BC
        POP     DE
        POP     HL
        RET

.ZC1_00
        OZ      OS_Pur                  ; Purge the keyboard buffer

.ZC1_01 LD      HL,randomNo		; Randomize random number
        INC     (HL)
        LD      BC,1                    ; Look read a character from the keyboard
        OZ      OS_Tin                  ; but only wait for 0.01 secs

.ZC1_02 LD      HL,zcflags              ; Are we wanting a yes/no response?
        BIT     yn,(HL)
        JR      NZ,YESNO                ; Jump if so
        BIT     pp,(HL)                 ; Are we wanting a pawn promotion response?
        JR      NZ,PROMOTE              ; Jump if so
        LD      HL,KEYJUMPS             ; See if a valid key has been pressed
        LD      BC,KEYJUMPS-KEYCODES
        PUSH    HL
        CPDR                            ; Look at all valid key codes point to by HL
        POP     HL
        JR      NZ,ZC1_01               ; Loop back if code has not been found

        INC     BC                      ; Get the service routine for key
;__NOTE What's the point of this next instruction?
        XOR     A
        ADD     HL,BC
        ADD     HL,BC                   ; HL points to the jump address
        LD      C,(HL)                  ; Get this into BC
        INC     HL
        LD      B,(HL)
        LD      HL,zcflags              ; Test sb flag (Board Setup) for service routine
        BIT     sb,(HL)                 ; NZ = Board Setup mode, Z = Play Mode
        PUSH    BC                      ; Put jump address on the stack
        RET                             ; Jump to it with HL pointing to zcflags
;***************************************************************************
.YESNO  RES     yn,(HL)                 ; Assume Y or N request will be satisfied
        LD      B,(HL)                  ; Remember zcflags status in B
        RES     ip,(HL)                 ; Assume current game will be cancelled
        RES     sb,(HL)                 ; Assume will not be in Board Setup mode
        CP      'Y'                     ; If 'Y' or 'y' then assumption is correct
        RET     Z
        CP      'y'
        RET     Z
        LD      (HL),B                  ; Restore zcflags
        CP      'N'                     ; If "N" or "n" then return
        RET     Z
        CP      'n'
        RET     Z
        SET     yn,(HL)                 ; Neither 'Y' or 'N' was pressed so restore yn flag
        JR      ZC1_01                  ; Loop back
;***************************************************************************
.PROMOTE
        RES     pp,(HL)                 ; Assume promotion request will be satisfied
        CP      'Q'                     ; Return if Queen selected
        RET     Z
        CP      'q'
        RET     Z
        CP      'R'                     ; Return if Rook selected
        RET     Z
        CP      'r'
        RET     Z
        CP      'B'                     ; Return if Bishop selected
        RET     Z
        CP      'b'
        RET     Z
        CP      'N'                     ; Return if Knight selected
        RET     Z
        CP      'n'
        RET     Z
        SET     pp,(HL)                 ; No valid key pressed, still waiting
        JR      ZC1_01                  ; for promotion. Loop back

;***************************************************************************
;We come here from the Cyrus code when a check of a key being pressed during
;the "THINKING" process is made
;If ESC key has been pressed, Z Flag = NZ is returned
;If not ESC or no key,        Z Flag = Z is returned
.Z88CTL2
;       LD      BC,$D1          ;See if clocks need up dating
;       IN      B,(C)
;       LD      A,(ticks)
;       CP      B
;       CALL    THINK_CLOCKS

; Quickly check that no key is being pressed and return immediately.
        XOR     A               ; A=0 means whole keyboard is to be read.
                                ; Address lines A8 to A15 are each set to 0 meaning all rows are looked at
        IN      A,($B2)         ; Bits 0 to 7 indicate which row is being pressed

;__NOTE an INC A instruction could be used instead of the next two instruction and would be quicker
        CPL                     ; If A=$FF then no key is being pressed. CPL complements the bits
        OR      A               ; See if A=0 (i.e. no key pressed)
        RET     Z               ; If so, no key is being pressed - return immediately.
                                ; A=0 & Z Flag=Z

        LD      BC,1            ; Find out what key is being pressed
        OZ      OS_Tin          ; Spend a maximum of 0.01 secs to get key
        SUB     CMD_ESC         ; If ESC pressed, return A=0 & Z Flag=NZ
        CP      1               ; If ESC not pressed, return A=0 & Z Flag=Z
        SBC     A,A
        CPL
        RET
;***************************************************************************
.KEYCODES
        DEFB    CMD_WS
        DEFB    CMD_WS
        DEFB    CMD_BS
        DEFB    CMD_WC
        DEFB    CMD_BC
        DEFB    CMD_L
        DEFB    CMD_G
        DEFB    CMD_SS
        DEFB    CMD_T
        DEFB    CMD_I
        DEFB    CMD_H
        DEFB    CMD_M
        DEFB    CMD_P
        DEFB    CMD_U
        DEFB    CMD_A
        DEFB    CMD_R
        DEFB    CMD_N
        DEFB    CMD_LEFT
        DEFB    CMD_RIGHT
        DEFB    CMD_UP
        DEFB    CMD_DOWN
        DEFB    CMD_ENTER
        DEFB    CMD_SB
        DEFB    CMD_SC
        DEFB    CMD_SR
        DEFB    CMD_ESC
        DEFB    CMD_DEL
        DEFB    CMD_WK
        DEFB    CMD_WQ
        DEFB    CMD_WR
        DEFB    CMD_WN
        DEFB    CMD_WB
        DEFB    CMD_WP
        DEFB    CMD_BK
        DEFB    CMD_BQ
        DEFB    CMD_BR
        DEFB    CMD_BN
        DEFB    CMD_BB
        DEFB    CMD_BP
        DEFB    CMD_FL
        DEFB    CMD_FS
        DEFB    CMD_FM
        DEFB    CMD_FB
        DEFB    CMD_QUIT
.KEYJUMPS
        DEFW    DO_CMD_WS
        DEFW    DO_CMD_WS
        DEFW    DO_CMD_BS
        DEFW    DO_CMD_WC
        DEFW    DO_CMD_BC
        DEFW    DO_CMD_L
        DEFW    DO_CMD_G
        DEFW    DO_CMD_SS
        DEFW    DO_CMD_T
        DEFW    DO_CMD_I
        DEFW    DO_CMD_H
        DEFW    DO_CMD_M
        DEFW    DO_CMD_P
        DEFW    DO_CMD_U
        DEFW    DO_CMD_A
        DEFW    DO_CMD_R
        DEFW    DO_CMD_N
        DEFW    DO_CMD_LEFT
        DEFW    DO_CMD_RIGHT
        DEFW    DO_CMD_UP
        DEFW    DO_CMD_DOWN
        DEFW    DO_CMD_ENTER
        DEFW    DO_CMD_SB
        DEFW    DO_CMD_SC
        DEFW    DO_CMD_SR
        DEFW    DO_CMD_ESC
        DEFW    DO_CMD_DEL
        DEFW    DO_CMD_WK
        DEFW    DO_CMD_WQ
        DEFW    DO_CMD_WR
        DEFW    DO_CMD_WN
        DEFW    DO_CMD_WB
        DEFW    DO_CMD_WP
        DEFW    DO_CMD_BK
        DEFW    DO_CMD_BQ
        DEFW    DO_CMD_BR
        DEFW    DO_CMD_BN
        DEFW    DO_CMD_BB
        DEFW    DO_CMD_BP
        DEFW    DO_CMD_FL
        DEFW    DO_CMD_FS
        DEFW    DO_CMD_FM
        DEFW    DO_CMD_FB
        DEFW    DO_CMD_QUIT

;***************************************************************************
; What follows are the actions for key presses from the Z88 keyboard, mainly
; Menu keyboard sequences.
; Upon entry, the Z80's Z flag is set if Chezz is in board set up mode
; and HL points to zcflags.
;***************************************************************************
.DO_CMD_WS                      ; Set White Player
        LD      B,@0010         ; Prepare to toggle bit 1 of zcflags
                                ; Bit 1 set = Human , reset = Chezz
.DO_CMD_WS01
        LD      A,(zcflags2)	; Flip the player bit
        XOR     B
        LD      (zcflags2),A
.DO_CMD_WS02
        CALL    PRINT_NAMES	; Update who's playing who on the screen
        CALL    MP03
        XOR     A		; Cyrus key code - do nothing
        RET
;;***************************************************************************
.DO_CMD_BS                      ; Set Black Player
        LD      B,@01           ; Prepare to toggle bit 0 (bb) of zcflags
                                ; Bit 0 set = Human, reset = Chezz
        JR      DO_CMD_WS01
;***************************************************************************
.DO_CMD_WC			;Set White's Clock
        JP      NYI		;NOT YET IMPLEMENTED
;__NOTE The following code was to handle chess clocks but was never finished
        LD      DE,P_WCLOCK
        CALL    CLOCK_INPUT
        RET     Z
        LD      (wclockstart),HL
        LD      HL,0
        LD      (wclockticks),HL
        RET

.CLOCK_INPUT
        LD      HL,WIN_5
        OZ      GN_Sop
        OZ      GN_Nln
        EX      DE,HL
        OZ      GN_Sop
        LD      HL,P_CLOCK
        OZ      GN_Sop
        LD      DE,clockinput
        LD      H,D
        LD      L,E
        LD      BC,8*256+'0'

.CI01   LD      (HL),C
        INC     HL
        DJNZ    CI01
        LD      C,0
        LD      (HL),C
        LD      A,':'
        LD      (clockinput+2),A
        LD      (clockinput+5),A
.CI02   LD      B,9
        LD      A,@00000111
        LD      L,7
        PUSH    DE
        OZ      GN_Sip
        POP     DE
        SUB     CMD_ESC
        RET     Z
        PUSH    DE
        EX      DE,HL
        LD      DE,2
        OZ      GN_Gtm
        POP     DE
        JR      C,CI02
        LD      L,C
        LD      H,B
        LD      B,A
        LD      DE,100
        LD      C,D
        OZ      GN_D24
        INC     B
        DEC     B
        JR      NZ,CI02
        PUSH    HL
        CALL    MSG_REPRINT
        POP     HL
        XOR     A
        INC     B
        RET     NZ
        DEC     B
        RET
;***************************************************************************
.DO_CMD_BC			;Set Black's Clock
        JP      NYI		;NOT YET IMPLEMENTED
;__NOTE The following code was to handle chess clocks but was never finished
        LD      DE,P_BCLOCK
        CALL    CLOCK_INPUT
        RET     Z
        LD      (bclockstart),BC
        LD      HL,0
        LD      (bclockticks),HL
        RET
;***************************************************************************
.DO_CMD_L                       ; Set Chezz's Level
        LD      A,'L'           ; Cyrus key code for Level
        RET
;***************************************************************************
.DO_CMD_G                       ; New Game
        SET     yn,(HL)         ; Flag that Yes/No confirmation is required
        LD      A,'G'           ; Cyrus key code for new-Game
        RET
;***************************************************************************
.DO_CMD_SS                      ; Swap Sides
        INC     HL              ; Point to zcflags2
        LD      A,(HL)          ; Switch the flags bb and ww
        AND     @00000001
        RRCA
        LD      B,A
        LD      A,(HL)
        AND     @00000010
        RLCA
        LD      A,(HL)
        AND     @11111100
        OR      B
        LD      (HL),A

;        INC     HL              ; Point to zcflags2
;        LD      A,(HL)		 ; Make White be Black's player and vice versa
;        LD      B,0
;        AND     3
;        RRCA
;        RL      B
;        RLC     B
;        OR      B
;        LD      (HL),A

        JP      DO_CMD_WS02
;***************************************************************************
.DO_CMD_T                       ; Toggle Sound On/Off
        LD      A,(beeper)
        XOR     1
        LD      (beeper),A
        CALL    PRINT_LDSPKR
        CALL    PRINT_BOARD
        XOR     A		; Cyrus key code - do nothing
        RET
;***************************************************************************
.DO_CMD_I                       ; Invert Board
        RES     pr,(HL)
        LD      A,'O'           ; Cyrus key code for Orient'n
.REDRAW_BD
        LD      BC,$40FF        ; Reset oldboard (completely different to
        LD      HL,oldboard     ; current board) to force board redraw
.RD01   LD      (HL),C
        INC     HL
        DJNZ    RD01
        RET
;***************************************************************************
.DO_CMD_H                       ; Show Square References
        LD      B,2**sr
        JR      DO_CMD_M01
;***************************************************************************
.DO_CMD_M                       ; Toggle Chessmen
        RES     sr,(HL)         ; Flips between UDGs and notation for the chess pieces
        RES     pr,(HL)
        INC     HL
        SET     bp,(HL)
        SET     wp,(HL)
        DEC     HL
        LD      B,2**cm         ;Toggle the flag between UDGs and notation
.DO_CMD_M01
        LD      A,(HL)
        XOR     B
        LD      (HL),A
        CALL    REDRAW_BD
        CALL    PRINT_BOARD
        XOR     A
        RET
;***************************************************************************
.DO_CMD_P                       ; Chezz Plays Move
        RET     NZ		; Return if in Setup mode
        LD      A,'M'           ; Cyrus key code for Move
        RET
;***************************************************************************
.DO_CMD_U                       ; Undo Last Move
        RET     NZ		; Return if in Setup mode
        LD      A,(nextToPlay)  ; Skip if W's move
        RRA
        JR      C,DO_CMD_U02
        LD      A,(MoveNo)      ; Skip if not move number 1
        CP      1
        JR      NZ,DO_CMD_U02
.DO_CMD_U01
        LD      HL,0
        LD      (movetxtptr),HL
        CALL    REDRAW_MOVE
.DO_CMD_U02
        LD      A,'B'		; Cyrus key code for take-Back
        RET
;***************************************************************************
.DO_CMD_A                       ; Advance a Move
        RET     NZ		; Return if in Setup mode
        LD      A,'F'           ; Cyrus key code for take-Forward
        RET
;***************************************************************************
.DO_CMD_R                       ; Replay All Moves
        RET     NZ		; Return if in Setup mode
        CALL    SETUP_BOARD
        LD      A,'R'           ; Cyrus key code for Replay
        RET
;***************************************************************************
.DO_CMD_N                       ; Play Next Best Move
        RET     NZ		; Return if in Setup mode
        LD      A,'N'           ; Cyrus key code for Next-best
        RET
;***************************************************************************
.DO_CMD_LEFT                    ; Cursor left
        LD      A,'5'           ; Cyrus key code for left
        RET
;***************************************************************************
.DO_CMD_RIGHT                   ; Cursor right
        LD      A,'8'           ; Cyrus key code for right
        RET
;***************************************************************************
.DO_CMD_UP                      ; Cursor up
        LD      A,'7'           ; Cyrus key code for up
        RET
;***************************************************************************
.DO_CMD_DOWN                    ; Cursor down
        LD      A,'6'           ; Cryus key code for down
        RET
;***************************************************************************
.DO_CMD_ENTER                   ; Select/Start Game

.START_GAME
        SET     ip,(HL)
        LD      A,(nextToPlay)  ; Whose turn is it?
        INC     HL              ; Move to zcflags2
        RRA
        LD      A,(HL)
        JR      NC,SG01         ; Skip if Black's turn
        RRA
.SG01   RRA                     ; Does Human play this turn?
        LD      A,13
        RET     NC              ; Return if so
        LD      A,@00000011
        AND     (HL)
        SUB     3
        LD      A,'M'           ; Computer plays this turn
        RET     NZ
        LD      A,'D'           ; Computer plays both colours
        RET
;***************************************************************************
.DO_CMD_SB                      ; Setup Board Mode
        RET     NZ		; Return if in Setup mode
        SET     sb,(HL)
        LD      A,'C'           ; Cyrus code for Change-pos'n
        RET
;***************************************************************************
.DO_CMD_SC                      ; Clear Board
        RET     Z               ; Return if not in Setup Mode
        LD      A,'A'           ; Cyrus code for All-clear
        RET
;***************************************************************************
.DO_CMD_SR                      ; Reset Board
        RET     Z		; Return if in Setup mode
        RES     sb,(HL)
        CALL    DO_CMD_U01
        LD      A,'G'           ; Cyrus code for new-Game
        RET
;***************************************************************************
.DO_CMD_ESC                     ; Exit Board Setup
        RES     sb,(HL)		; Return if not in Setup Mode
        RET     Z		; Return if not in Setup mode
        LD      A,'E'           ; Cyrus code for Exit
        RET
;***************************************************************************
.DO_CMD_DEL                     ; Empty Square
        RET     Z		; Return if not in Setup Mode
        LD      A,'U'           ; Cyrus code for Unoccupied
        RET
;***************************************************************************
.DO_CMD_WK			; White King
        RET     Z		; Return if not in Setup Mode
        CALL    WPC		; Note that a White piece is being placed
        LD      A,'K'		; Cyrus key code for King
        RET

.WPC    LD      A,$83		;A White piece is being placed on the board
        LD      (pieceColour),A
        RET
;***************************************************************************
.DO_CMD_WQ			; White Queen
        RET     Z		; Return if not in Setup Mode
        CALL    WPC		; Note that a White piece is being placed
        LD      A,'Q'		; Cyrus key code for Queen
        RET
;***************************************************************************
.DO_CMD_WR			; White Rook
        RET     Z		; Return if not in Setup Mode
        CALL    WPC		; Note that a White piece is being placed
        LD      A,'R'		; Cyrus key code for Rook
        RET
;***************************************************************************
.DO_CMD_WN			; White Knight
        RET     Z		; Return if not in Setup Mode
        CALL    WPC		; Note that a White piece is being placed
        LD      A,'N'		; Cyrus key code for Rook
        RET
;***************************************************************************
.DO_CMD_WB			; White Bishop
        RET     Z		; Return if not in Setup Mode
        CALL    WPC		; Note that a White piece is being placed
        LD      A,'B'		; Cyrus key code for Bishop
        RET
;***************************************************************************
.DO_CMD_WP			; White King
        RET     Z		; Return if not in Setup Mode
        CALL    WPC		; Note that a White piece is being placed
        LD      A,'P'		; Cyrus key code for Pawn
        RET
;***************************************************************************
.DO_CMD_BK			; Black King
        RET     Z		; Return if in Setup Mode
        CALL    BPC		; Note that a Black piece is being placed
        LD      A,'K'		; Cyrus key code for King
        RET

.BPC    LD      A,$03		; A Black piece is being placed on the board
        LD      (pieceColour),A
        RET
;***************************************************************************
.DO_CMD_BQ			; Black Queen
        RET     Z		; Return if not in Setup Mode
        CALL    BPC		; Note that a Black piece is being placed
        LD      A,'Q'		; Cyrus key code for Queen
        RET
;***************************************************************************
.DO_CMD_BR			; Black Rook
        RET     Z		; Return if not in Setup Mode
        CALL    BPC		; Note that a Black piece is being placed
        LD      A,'R'		; Cyrus key code for Rook
        RET
;***************************************************************************
.DO_CMD_BN			; Black Knight
        RET     Z		; Return if not in Setup Mode
        CALL    BPC		; Note that a Black piece is being placed
        LD      A,'N'		; Cyrus key code for Knight
        RET
;***************************************************************************
.DO_CMD_BB			; Black Bishop
        RET     Z		; Return if not in Black Bishop
        CALL    BPC		; Note that a Black piece is being placed
        LD      A,'B'		; Cyrus key code for Bishop
        RET
;***************************************************************************
.DO_CMD_BP			; Black Pawn
        RET     Z		; Return if not in Black Pawn
        CALL    BPC		; Note that a Black piece is being placed
        LD      A,'P'		; Cyrus key code for Pawn
        RET
;***************************************************************************
.DO_CMD_FL			; Load Game
        JP      NYI		; NOT YET IMPLEMENTED
;***************************************************************************
.DO_CMD_FS			; Save Game
        JP      NYI		; NOT YET IMPLEMENTED
;***************************************************************************
.DO_CMD_FM			; Record Move History
        JP      NYI		; NOT YET IMPLEMENTED
;***************************************************************************
.DO_CMD_FB			; Record Board History
        JP      NYI		; NOT YET IMPLEMEMTED
;***************************************************************************
