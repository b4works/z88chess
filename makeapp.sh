#!/bin/bash

# **************************************************************************************************
# Chezz compilation script for Mac/Linux
# (C) Gunther Strube (hello@bits4fun.net) 2019
#
#   ____   ___
#   6MMMMb/ `MM
#  8P    YM  MM
# 6M      Y  MM  __     ____   _________ _________
# MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
# MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
# MM         MM'   MM MM    MM     dMP       dMP
# MM         MM    MM MMMMMMMM    dMP       dMP
# YM      6  MM    MM MM         dMP       dMP
#  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
#   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
#
# Based on Cyrus IS Chess for the Sinclair ZX Spectrum
# (C) 1982 Richard Lang, Intelligent Software Ltd
#
# Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
#
# Chezz is free software; you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation; either
# version 2, or (at your option) any later version. Chezz is distributed in the hope
# that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details. You should have received a copy
# of the GNU General Public License along with Chezz; see the file LICENSE.
# If not, write to:
#                                  Free Software Foundation, Inc.
#                                  59 Temple Place-Suite 330,
#                                  Boston, MA 02111-1307, USA.
#
# **************************************************************************************************

command -v mpm >/dev/null 2>&1 || { echo >&2 "Mpm assembler tool not found in PATH"; exit 1; }
command -v z88card >/dev/null 2>&1 || { echo >&2 "Z88Card build tool not found in PATH"; exit 1; }
command -v mthtoken >/dev/null 2>&1 || { echo >&2 "MthToken build tool not found in PATH"; exit 1; }

# -------------------------------------------------------------------------------------------------
# delete previous error and warning files...
find . -name "*.err" | xargs rm -f
find . -name "*.wrn" | xargs rm -f
# delete all compile output files, if available..
rm -f *.epr gitrevision.inc romupdate.cfg

GIT_FOUND=1
GITDIR_FOUND=0
if [ $GIT_FOUND -eq 1 ]; then
    if [[ -d ".git" ]]; then
        GITDIR_FOUND=1
          # output current Git revision as a 32bit hex string, to be included as part of Zetrix release information
          git log --pretty=format:'defm "'%h'"' -n 1 > gitrevision.inc
          [ -n "$(tail -c1 gitrevision.inc)" ] && echo >> gitrevision.inc
    fi
fi
if [ $GITDIR_FOUND -eq 0 ]; then
    # git command or .git folder was not found, set build ID to '0'
    echo -e 'defm "0"\n' > gitrevision.inc
fi

# return version of Mpm to command line environment.
# validate that MPM is V1.5 or later - only this version or later supports source file dependency
MPM_VERSIONTEXT=`mpm -version`

if test $? -lt 15; then
  echo Mpm version is less than V1.5, build aborted.
  echo Mpm displays the following:
  mpm
  exit 1
fi

# return version of Z88Card to command line environment.
# validate that Z88Card is V2.0 or later - only this version or later supports advanced loadmap syntax
Z88CARD_VERSIONTEXT=`z88card -v`

if test $? -lt 20; then
  echo Z88Card version is less than V2.0, build aborted.
  echo Z88Card displays the following:
  z88card
  exit 1
fi

# build Chezz game executable, to be loaded at beginning of bank $3E
mpm -gb -ochezz.bin z88cyrus.asm chezz.asm zcprint.asm
if test `find . -name '*.err' | wc -l` != 0; then
  echo build aborted.
  exit 1
fi

# build Chezz game data and MTH structures to be loaded into bank $3F
mpm -gb -nMap zcdata.asm mth.asm
if test `find . -name '*.err' | wc -l` != 0; then
  echo build aborted.
  exit 1
fi

# build Application Card header to be loaded at top of bank $3F
mpm -b -nMap romhdr.asm
if test `find . -name '*.err' | wc -l` != 0; then
  echo build aborted.
  exit 1
fi

z88card -f chezz.ldm

# the chezz.app RAM Application template file already exists in project, copy the 16K images for .app
cp -f chezz.63 chezz.ap0
cp -f chezz.62 chezz.ap1
