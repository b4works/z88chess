:: **************************************************************************************************
:: Chezz cleanup compilation script for Windows/DOS
:: (C) Gunther Strube (hello@bits4fun.net) 2019
::
::   ____   ___
::   6MMMMb/ `MM
::  8P    YM  MM
:: 6M      Y  MM  __     ____   _________ _________
:: MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
:: MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
:: MM         MM'   MM MM    MM     dMP       dMP
:: MM         MM    MM MMMMMMMM    dMP       dMP
:: YM      6  MM    MM MM         dMP       dMP
::  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
::   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
::
:: Based on Cyrus IS Chess for the Sinclair ZX Spectrum
:: (C) 1982 Richard Lang, Intelligent Software Ltd
::
:: Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
::
:: Chezz is free software; you can redistribute it and/or modify it under the terms of
:: the GNU General Public License as published by the Free Software Foundation; either
:: version 2, or (at your option) any later version. Chezz is distributed in the hope
:: that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
:: of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:: See the GNU General Public License for more details. You should have received a copy
:: of the GNU General Public License along with Chezz; see the file LICENSE.
:: If not, write to:
::                                  Free Software Foundation, Inc.
::                                  59 Temple Place-Suite 330,
::                                  Boston, MA 02111-1307, USA.
::
:: **************************************************************************************************
@ECHO OFF

del /Q *.sym *.lst *.map *.obj *.wrn *.err *.bin *.62 *.63 *.epr 2>nul >nul

:: remove generated INC + DEF files (they are part of the compile dependencies...)
del /Q gitrevision.inc z88cyrus.def zcdata.def 2>nul >nul
