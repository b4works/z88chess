; ------------------------------------------------------------------------------------
;   ____   ___
;   6MMMMb/ `MM
;  8P    YM  MM
; 6M      Y  MM  __     ____   _________ _________
; MM         MM 6MMb   6MMMMb  MMMMMMMMP MMMMMMMMP
; MM         MMM9 `Mb 6M'  `Mb /    dMP  /    dMP
; MM         MM'   MM MM    MM     dMP       dMP
; MM         MM    MM MMMMMMMM    dMP       dMP
; YM      6  MM    MM MM         dMP       dMP
;  8b    d9  MM    MM YM    d9  dMP    /  dMP    /
;   YMMMM9  _MM_  _MM_ YMMMM9  dMMMMMMMM dMMMMMMMM
;
; Based on Cyrus IS Chess for the Sinclair ZX Spectrum
; (C) 1982 Richard Lang, Intelligent Software Ltd
;
; Ported to Cambridge Z88 by Keith Rickard, Gunther Strube (C) 1999-2019
;
; Chezz is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published by the Free Software Foundation; either
; version 2, or (at your option) any later version. Chezz is distributed in the hope
; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details. You should have received a copy
; of the GNU General Public License along with Chezz; see the file LICENSE.
; If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ------------------------------------------------------------------------------------

        MODULE ChezzMth

        INCLUDE "ozdefc.asm"            ; OZ API
        INCLUDE "chezz.inc"             ; Chezz variables and constants
        INCLUDE "z88cyrus.def"          ; global addresses from executable code

        XDEF CHESS_DOR

; Chezz application DOR and Menu/Topic/Help data structures

.CHESS_DOR
        DEFB    0,0,0           ;Link to parent
        DEFB    0,0,0           ;Link to brother
        DEFB    0,0,0           ;Link to son
        DEFB    $83             ;Application ROM DOR type
        DEFB    DOR7-DOR1       ;Total DOR length
.DOR1   DEFB    '@'             ;Key to info section
        DEFB    DOR3-DOR2       ;Length of info section
.DOR2   DEFW    0               ;Reserved
        DEFB    'C'             ;Application letter ("[]ZC")
        DEFB    32              ;8K of contiguous RAM required
        DEFW    0               ;Estimate of overhead
        DEFW    0               ;Unsafe workspace required
        DEFW    0               ;Safe workspace required
        DEFW    ENTRY           ;Entry point for application
        DEFB    0               ;Binding for Segment 0 on entry
        DEFB    0               ;Binding for Segment 1 on entry
        DEFB    $3E             ;Binding for Segment 2 on entry
        DEFB    $3F             ;Binding for Segment 3 on entry
        DEFB    2               ;Bad application
        DEFB    3               ;CAPS lock on in inverted mode
.DOR3   DEFB    'H'             ;Key to help section
        DEFB    DOR5-DOR4       ;Length of help section
.DOR4   DEFW    TOPICS          ;Pointer to topics
        DEFB    $3F
        DEFW    COMMANDS        ;Pointer to commands
        DEFB    $3F
        DEFW    HELP            ;Pointer to application help
        DEFB    $3F
        DEFW    TOKENS          ;Pointer to token base
        DEFB    $3F

.DOR5   DEFB    'N'             ;Key to name section
        DEFB    DOR6-DOR7       ;Length of name section
.DOR6   DEFM    "Chezz",0       ;Null terminated application name
.DOR7   DEFB    $FF             ;DOR terminator byte
.DOR8


;***************************************************************************
;Chezz Application Entry point when called from OZ via DOR (bank $3F)
.ENTRY  JP      Chezz                           ;Jump to it!

.ENQUIRY
;       SCF
;       RET
        PUSH    AF
        LD      BC,GDmoveHistory               ;Find hole start
.ENQ_LOOP
        LD      A,(BC)
        INC     BC
        INC     A
        JR      NZ,ENQ_LOOP
        LD      DE,$4000                        ;Hole end
        POP     AF
        OR      A                               ;Clear carry
        RET


;***************************************************************************
;Topic information data stucture
.TOPICS
        DEFB    0               ;Start marker

.T1     DEFB    T2-T1
        DEFM    "SETUP",0       ;Topic name
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    T2-T1

.T2     DEFB    T3-T2
        DEFM    "MOVE",0        ;Topic name
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    T3-T2

.T3     DEFB    T4-T3
        DEFM    "BOARD",0       ;Topic name
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    T4-T3

.T4     DEFB    T5-T4
        DEFM    "FILES",0       ;Topic name
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    T5-T4

.T5     DEFB    T6-T5
        DEFM    "HELP",0        ;Topic name
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $01             ;Attribute - information Topic
        DEFB    T6-T5

.T6     DEFB    0               ;End marker

;***************************************************************************
;COMMAND information data structure
.COMMANDS
        DEFB    0               ;Start marker

;GAME topic commands
.C111   DEFB    C112-C111
        DEFB    CMD_WS          ;Command code
        DEFM    "WS",0          ;Null terminated keyboard sequence
        DEFM    "Set White player",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C112-C111

.C112   DEFB    C113-C112
        DEFB    CMD_BS          ;Command code
        DEFM    "BS",0          ;Null terminated keyboard sequence
        DEFM    "Set Black Player",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C113-C112

.C113   DEFB    C114-C113
        DEFB    CMD_WC          ;Command code
        DEFM    "WC",0          ;Null terminated keyboard sequence
        DEFM    "Set White's Clock",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C114-C113

.C114   DEFB    C115-C114
        DEFB    CMD_BC          ;Command code
        DEFM    "BC",0          ;Null terminated keyboard sequence
        DEFM    "Set Black's Clock",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C115-C114

.C115   DEFB    C116-C115
        DEFB    CMD_L           ;Command code
        DEFM    "L",0           ;Null terminated keyboard sequence
        DEFM    "Set Chezz' Level",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C116-C115
.C116
.C121   DEFB    C122-C121
        DEFB    CMD_G           ;Command code
        DEFM    "G",0           ;Null terminated keyboard sequence
        DEFM    "New Game",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    1               ;Attribute byte (new column)
        DEFB    C122-C121

.C122   DEFB    C123-C122
        DEFB    CMD_SS          ;Command code
        DEFM    "SS",0          ;Null terminated keyboard sequence
        DEFM    "Swap Sides",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C123-C122

.C123   DEFB    C124-C123
        DEFB    CMD_T           ;Command code
        DEFM    "T",0           ;Null terminated keyboard sequence
        DEFM    "Toggle Sound On/Off",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C124-C123

.C124   DEFB    C125-C124
        DEFB    CMD_I           ;Command code
        DEFM    "I",0           ;Null terminated keyboard sequence
        DEFM    "Invert Board",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C125-C124

.C125   DEFB    C126-C125
        DEFB    CMD_M           ;Command code
        DEFM    "M",0           ;Null terminated keyboard sequence
        DEFM    "Toggle Chessmen",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C126-C125

.C126   DEFB    C127-C126
        DEFB    CMD_H           ;Command code
        DEFM    "H",0           ;Null terminated keyboard sequence
        DEFM    "Show Square References",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C127-C126
.C127
        DEFB    1               ;End of SETUP topic

;PLAY topic commands
.C211   DEFB    C212-C211
        DEFB    CMD_P           ;Command code
        DEFM    "P",0           ;Null terminated keyboard sequence
        DEFM    "Chezz Plays Move",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C212-C211

.C212   DEFB    C213-C212
        DEFB    CMD_U           ;Command code
        DEFM    "U",0           ;Null terminated keyboard sequence
        DEFM    "Undo Last Move",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C213-C212

.C213   DEFB    C214-C213
        DEFB    CMD_A           ;Command code
        DEFM    "A",0           ;Null terminated keyboard sequence
        DEFM    "Advance a Move",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C214-C213

.C214   DEFB    C215-C214
        DEFB    CMD_R           ;Command code
        DEFM    "R",0           ;Null terminated keyboard sequence
        DEFM    "Replay All Moves",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C215-C214

.C215   DEFB    C216-C215
        DEFB    CMD_N           ;Command code
        DEFM    "N",0           ;Null terminated keyboard sequence
        DEFM    "Play Next Best Move",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C216-C215
.C216
.C221   DEFB    C222-C221
        DEFB    CMD_LEFT        ;Command code
        DEFB    CMD_LEFT,0      ;Null terminated keyboard sequence
        DEFM    "Cursor Left",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    1               ;Attribute byte (new column)
        DEFB    C222-C221

.C222   DEFB    C223-C222
        DEFB    CMD_RIGHT       ;Command code
        DEFB    CMD_RIGHT,0     ;Null terminated keyboard sequence
        DEFM    "Cursor Right",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C223-C222

.C223   DEFB    C224-C223
        DEFB    CMD_UP          ;Command code
        DEFB    CMD_UP,0        ;Null terminated keyboard sequence
        DEFM    "Cursor Up",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C224-C223

.C224   DEFB    C225-C224
        DEFB    CMD_DOWN        ;Command code
        DEFB    CMD_DOWN,0      ;Null terminated keyboard sequence
        DEFM    "Cursor Down",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C225-C224

.C225   DEFB    C226-C225
        DEFB    CMD_ENTER       ;Command code
        DEFM    225,0   ;       Null terminated keyboard sequence
        DEFM    "Select/Start Game",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C226-C225

.C226   DEFB    1               ;End of PLAY topic

;SETUP topic commands
.C311   DEFB    C312-C311
        DEFB    CMD_SB          ;Command code
        DEFM    "SB",0          ;Null terminated keyboard sequence
        DEFM    "Setup Board Mode",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C312-C311

.C312   DEFB    C313-C312
        DEFB    CMD_SC          ;Command code
        DEFM    "SC",0          ;Null terminated keyboard sequence
        DEFM    "Clear Board",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C313-C312

.C313   DEFB    C314-C313
        DEFB    CMD_SR          ;Command code
        DEFM    "SR",0          ;Null terminated keyboard sequence
        DEFM    "Reset Board",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C314-C313

.C314   DEFB    C315-C314
        DEFB    CMD_ESC         ;Command code
        DEFM    CMD_ESC,0       ;Null terminated keyboard sequence
        DEFM    "Exit Board Setup",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C315-C314

.C315   DEFB    C316-C315
        DEFB    CMD_DEL         ;Command code
        DEFM    227,0           ;Null terminated keyboard sequence
        DEFM    "Empty Square",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C316-C315

.C316
.C321   DEFB    C322-C321
        DEFB    CMD_WK          ;Command code
        DEFM    "WK",0          ;Null terminated keyboard sequence
        DEFM    "White King",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    1               ;Attribute byte (new column)
        DEFB    C322-C321

.C322   DEFB    C323-C322
        DEFB    CMD_WQ          ;Command code
        DEFM    "WQ",0          ;Null terminated keyboard sequence
        DEFM    "White Queen",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C323-C322

.C323   DEFB    C324-C323
        DEFB    CMD_WR          ;Command code
        DEFM    "WR",0          ;Null terminated keyboard sequence
        DEFM    "White Rook",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C324-C323

.C324   DEFB    C325-C324
        DEFB    CMD_WN          ;Command code
        DEFM    "WN",0          ;Null terminated keyboard sequence
        DEFM    "White Knight",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C325-C324

.C325   DEFB    C326-C325
        DEFB    CMD_WB          ;Command code
        DEFM    "WB",0          ;Null terminated keyboard sequence
        DEFM    "White Bishop",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C326-C325

.C326   DEFB    C327-C326
        DEFB    CMD_WP          ;Command code
        DEFM    "WP",0          ;Null terminated keyboard sequence
        DEFM    "White Pawn",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C327-C326

.C327
.C331   DEFB    C332-C331
        DEFB    CMD_BK          ;Command code
        DEFM    "BK",0          ;Null terminated keyboard sequence
        DEFM    "Black King",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    1               ;Attribute byte (new column)
        DEFB    C332-C331

.C332   DEFB    C333-C332
        DEFB    CMD_BQ          ;Command code
        DEFM    "BQ",0          ;Null terminated keyboard sequence
        DEFM    "Black Queen",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C333-C332

.C333   DEFB    C334-C333
        DEFB    CMD_BR          ;Command code
        DEFM    "BR",0          ;Null terminated keyboard sequence
        DEFM    "Black Rook",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C334-C333

.C334   DEFB    C335-C334
        DEFB    CMD_BN          ;Command code
        DEFM    "BN",0          ;Null terminated keyboard sequence
        DEFM    "Black Knight",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C335-C334

.C335   DEFB    C336-C335
        DEFB    CMD_BB          ;Command code
        DEFM    "BB",0          ;Null terminated keyboard sequence
        DEFM    "Black Bishop",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C336-C335

.C336   DEFB    C337-C336
        DEFB    CMD_BP          ;Command code
        DEFM    "BP",0          ;Null terminated keyboard sequence
        DEFM    "Black Pawn",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C337-C336

.C337   DEFB    1               ;End of BOARD topic

;FILES topic commands
.C411   DEFB    C412-C411
        DEFB    CMD_FL          ;Command code
        DEFM    "FL",0          ;Null terminated keyboard sequence
        DEFM    "Load Game",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C412-C411

.C412   DEFB    C413-C412
        DEFB    CMD_FS          ;Command code
        DEFM    "FS",0          ;Null terminated keyboard sequence
        DEFM    "Save Game",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C413-C412

.C413
.C421   DEFB    C422-C421
        DEFB    CMD_FM          ;Command code
        DEFM    "FM",0          ;Null terminated keyboard sequence
        DEFM    "Record Move History",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    1               ;Attribute byte (new column)
        DEFB    C422-C421

.C422   DEFB    C423-C422
        DEFB    CMD_FB          ;Command code
        DEFM    "FB",0          ;Null terminated keyboard sequence
        DEFM    "Record Board History",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    0               ;Attribute byte
        DEFB    C423-C422

.C423   DEFB    C424-C423
        DEFB    CMD_QUIT        ;Command code
        DEFM    "KILL",0        ;Null terminated keyboard sequence
        DEFM    $80,"LEAVE CHEZZ",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    1+8             ;Attribute byte (new column)
        DEFB    C424-C423

.C424   DEFB    1               ;End of PLAY topic

;Help topic commands
.C511   DEFB    C512-C511
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "History of Chess",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C512-C511

.C512   DEFB    C513-C512
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Objective of the Game",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C513-C512


.C513   DEFB    C514-C513
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Moving the King",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C514-C513

.C514   DEFB    C515-C514
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Moving the Queen",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C515-C514

.C515   DEFB    C516-C515
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Moving the Rook",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C516-C515

.C516   DEFB    C517-C516
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Moving the Bishop",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C517-C516

.C517   DEFB    C518-C517
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Moving the Knight",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C518-C517

.C518   DEFB    C519-C518
        DEFB    0               ;Command code
        DEFM    0               ;Null terminated keyboard sequence
        DEFM    "Moving the Pawn",0
        DEFB    0,0             ;Help page (MSB first)
        DEFB    $10             ;Attribute byte (help page)
        DEFB    C519-C518

.C519   DEFB    0               ;End of Help topic and all topics

;***************************************************************************
;Application help information
.HELP   DEFB    $7F
        DEFM    "Version 0.1B rev. "
INCLUDE "gitrevision.inc"
        DEFB $7F
        DEFM    "Conversion and enhancements:",$7F
        DEFM    "(C) 1999-2019 Keith Rickard, Gunther Strube",$7F,$7F
        DEFM    "Based on Cyrus IS Chess for the Sinclair ZX Spectrum",$7F
        DEFM    "(C) 1982 Richard Lang, Intelligent Software Ltd",0

;***************************************************************************
;Token information
.TOKENS DEFB    0               ;Recursive token boundary
        DEFB    1               ;Number of tokens
        DEFW    TOK1-TOKENS
        DEFW    TOK2-TOKENS
.TOK1   DEFM    1,"2+T"         ;Tiny text
.TOK2
